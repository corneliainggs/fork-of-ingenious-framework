import os
import json
import threading
import traceback
import numpy as np
import asyncio
import plotly.io as pio
import imageio
import sobol
import pickle
import warnings
warnings.filterwarnings("ignore")

from ax.modelbridge.generation_strategy import GenerationStrategy, GenerationStep
from ax.modelbridge.registry import Models
from ax.service.ax_client import AxClient
from ax.service.ax_client import AxClient, ObjectiveProperties
from ax.utils.notebook.plotting import render, init_notebook_plotting
from ax.plot.trace import optimization_trace_single_method
from ax.plot.pareto_utils import compute_posterior_pareto_frontier
from ax.plot.pareto_frontier import plot_pareto_frontier
from ax.plot.slice import plot_slice
from statsmodels.stats.proportion import proportion_confint

# TODO: Handle core count etc when reloading models

WINRATES_PATH_PREFIX = f"results/winrates/"
IMAGES_PATH_PREFIX = f"results/images/"
TRACES_PATH_PREFIX = f"results/traces/"
GIFS_PATH_PREFIX = f"results/gifs/"
MODELS_PATH_PREFIX = f"results/models/"
PATH_TO_SCRIPT_PREFIX = "../../scripts/"
PATH_FROM_SCRIPT = "../../ParameterTuning/BO_v2/"
ENHANCEMENT_CONFIG_PATH_PREFIX = "enhancementConfigurations/"

GAMES = ["go", "othello", "nim"]

class Optimiser:

    def __init__(self, workers : int, threadsPerInstance : int, bayes_trials : int, game : str, boardSize : int,
                 turnDuration : int, enhancement : str, init_points : int, bayes_workers : int = 1,
                 sobol_workers : int = 1, c : float = 0.5, verbose : bool = False, required_num_games : int = 120,
                 override_core_limit : bool = False, overwrite_previous_saved_models : bool = False,
                 save_folder_postfix : str = "") -> None:
        """
        Initialises the optimiser.

        Parameters
        ----------
        workers : int
            Number of game instances to use for the optimisation.
            Note: bayes_workers + sobol_workers must be smaller than workers. Any remaining workers will be used for the
            re-evaluation of already evaluated points to reduce the effects of noise and increase model accuracy.
        threadsPerInstance : int
            Number of threads per game instance to use for the optimisation.
        bayes_trials : int
            Number of bayesian trials to run.
        game : str
            Game to use for the optimisation.
        boardSize : int
            Board size to use for the optimisation.
        turnDuration : int
            Turn duration to use for the optimisation.
        enhancement : str
            Enhancement to use for the optimisation.
        init_points : int, optional
            Number of initial points to run. The default is 10.
        bayes_workers : int, optional
            Number of bayesian optimisation workers to use. The default is 1.
        sobol_workers : int, optional
            Number of Sobol workers to use. The default is 1.
        c : float, optional
            c to use for the UCB. The default is 0.5.
        verbose : bool, optional
            Whether to log information about the optimisation. The default is False.
        required_num_games : int, optional
            Number of games to play through promising points. The default is 120.
        override_core_limit : bool, optional
            Whether to override the core limit. The default is False.
        overwrite_previous_saved_models : bool, optional
            Whether to overwrite previous saved models. The default is False.
        save_folder_postfix : str, optional
            Postfix to add to the save folder. The default is "". This is useful for running different versions of the
            same enhancement, while still being able to compare the results.
        """
        if not (override_core_limit):
            assert workers*threadsPerInstance > 0 and workers*threadsPerInstance <= os.cpu_count(), "Total number " + \
                "of threads must be larger than 0 and smaller than or equal to the number of physical threads on " + \
                "the system"
        assert bayes_trials > 0, "Number of trials must be larger than 0"
        assert game in GAMES, f"Game must be one of {GAMES}"
        assert boardSize > 0, "Board size must be larger than 0"
        assert turnDuration > 0, "Turn duration must be larger than 0"
        assert f"EnhancementChoice{enhancement}.json" in os.listdir("enhancementConfigurations/"), "Enhancement " + \
            "must be one of the enhancements in the enhancementConfigurations folder"
        assert init_points > 0, "Number of initial points must be larger than 0"
        assert bayes_workers + sobol_workers > 0 and bayes_workers + sobol_workers <= workers, "Number of bayesian " + \
            "+ sobol workers must be larger than 0 and smaller than or equal to the number of workers"
        assert c > 0, "c must be larger than 0"
        self.workers = workers
        self.threadsPerInstance = threadsPerInstance
        self.bayes_trials = bayes_trials
        self.game = game
        self.boardSize = boardSize
        self.turnDuration = turnDuration
        self.enhancement = enhancement
        self.init_points = init_points
        self.bayes_workers = bayes_workers
        self.sobol_workers = sobol_workers
        self.c = c
        self.verbose = verbose
        self.required_num_games = required_num_games
        self.overwrite_previous_saved_models = overwrite_previous_saved_models
        self.save_folder_postfix = f"_{save_folder_postfix}" if save_folder_postfix != "" else ""
        self._setParams()
        self.script_path = f"{PATH_TO_SCRIPT_PREFIX}{self.game}"
        self.scriptName = "test.sh"
        self.eval_data = {}
        self.total_evaluations = 0
        self.sobol_evaluations = 0
        self.total_post_tuning_evaluations = 0
        self.failed_evaluations = 0
        self._create_experiment()

    def get_current_save_paths(self):
        """
        Gets the current save paths. The first path is the path to the json file containing the model and the second
        path is the path to the pickle file containing auxiliary data.
        """
        path_prefix = f"{MODELS_PATH_PREFIX}{self.game}/{self.enhancement}{self.save_folder_postfix}/" + \
            f"{self.boardSize}_{self.turnDuration}_{self.threadsPerInstance}/evals_completed={self.total_evaluations}"
        return f"{path_prefix}.json", f"{path_prefix}.pkl"

    def save_model(self):
        """
        Saves the model and aux data.
        """
        json, pkl = self.get_current_save_paths()
        # check if the path exists
        if not os.path.exists(os.path.dirname(json)):
            # create the directory
            os.makedirs(os.path.dirname(json))
        # check if the file already exists
        if os.path.exists(json):
            # ask the user if they want to overwrite the file
            if (not self.overwrite_previous_saved_models or \
                input(f"File {os.path.basename(json)} exists. Do you want to overwrite it? (y/n)") not in ["y", "Y"]):
                    print("Not saving the model.")
                    return
        # save the model
        self.ax_client.save_to_json_file(json)
        # save the aux data
        with open(pkl, "wb") as f:
            pickle.dump(self, f)

        # remove prior saves
        directory = os.path.dirname(json)
        for file in os.listdir(directory):
            if "evals_completed=" in file:
                evals = int(file.split("=")[1].split(".")[0])
                if evals < self.total_evaluations:
                    # delete the file
                    os.remove(os.path.join(directory, file))

    def __getstate__(self):
        """
        Gets the state of the object. Excludes the ax_client as it is more efficient to use Ax's save/load functions.
        """
        state = self.__dict__.copy()
        del state["ax_client"]
        del state["sobol_generator"]
        del state["eval_data"]
        state["eval_data"] = {}
        for trial_key in self.eval_data.keys():
            state["eval_data"][trial_key] = self.eval_data[trial_key].copy()
            del state["eval_data"][trial_key]["lock"]
        return state
    
    def load_latest_model(self):
        """
        Loads the latest model and aux data.
        """
        # get the latest model
        json, _ = self.get_current_save_paths()
        # check if the path exists
        if not os.path.exists(os.path.dirname(json)):
            # create the directory
            os.makedirs(os.path.dirname(json))
        # get the directory and files within it
        directory = os.path.dirname(json)
        files = os.listdir(directory)
        most_evals = 0
        path = ""
        for file in files:
            if "evals_completed=" in file and file.endswith(".json"):
                evals = int(file.split("=")[1].split(".")[0])
                if evals > most_evals:
                    most_evals = evals
                    path = f"{directory}/{file}"
        if path == "":
            print(f"Could not load model. No model found in {directory}.")
            return
        self.load_model(path.split(".")[0])

    def load_model(self, path : str):
        """
        Loads the model and aux data from the given path.

        Parameters
        ----------
        path : str
            The path to the model and aux data, excluding the file extension.
        """
        # load the model
        self.ax_client = AxClient.load_from_json_file(f"{path}.json")
        # load the aux data
        with open(f"{path}.pkl", "rb") as f:
            self.__dict__.update(pickle.load(f).__dict__)
        # recreate the sobol generator
        self.sobol_generator = sobol.generator(
            dimension=len(self.variables),
            skip=self.sobol_evaluations
        )
        # recreate the locks
        for trial_key in self.eval_data.keys():
            self.eval_data[trial_key]["lock"] = threading.Lock()

    def _setParams(self):
        """
        Sets the parameters to optimise for.
        """
        with open(f"enhancementConfigurations/EnhancementChoice{self.enhancement}.json") as file:
            parameters = json.load(file)

        exempt = [
            "Selection",
            "Expansion",
            "Simulation",
            "Backpropagation"
        ]

        self.variables = []
        for key in parameters.keys():
            if key not in exempt and not ("Upper" in key or "Lower" in key):
                for prefix in parameters.keys():
                    if ((key + "Upper") in prefix):
                        self.variables.append(key)
                        break

        assert len(self.variables) > 0, "No parameters to optimise for"

        self.params = [{
            "name" : f"{self.variables[i]}",
            "value_type" : "float",
            "type" : "range",
            "bounds" : [parameters[f"{self.variables[i]}Lower"], parameters[f"{self.variables[i]}Upper"]]
        } for i in range(len(self.variables))]

        # Create sobol generator for parameters
        self.sobol_generator = sobol.generator(dimension=len(self.variables))

    def _explore_sobol_points(self, num_points : int):
        points = []
        for _ in range(num_points):
            # Get next sobol point
            point = {}
            values = next(self.sobol_generator)
            for i in range(len(self.params)):
                # Map sobol point to parameter bounds
                bounds = self.params[i]["bounds"]
                value = bounds[0] + values[i] * (bounds[1] - bounds[0])
                point[self.params[i]["name"]] = value
            points.append(point)
        
        self.sobol_evaluations += num_points

        coroutines = []
        for point in points:
            parameters, trial_index = self.ax_client.attach_trial(parameters=point)
            coroutines.append(self._eval(trial_index, parameters))
        return coroutines

    def _explore_bayes_points(self, N):
        trials, done = self.ax_client.get_next_trials(N)
        return [self._eval(trial, trials[trial]) for trial in trials], done

    def _exploit_previous_points(self, N):
        trial_results = list(self.eval_data.values())
        trial_results.sort(key=self._ucb1, reverse=True)

        coroutines = []
        for i in range(N):
            parameters, trial_index = self.ax_client.attach_trial(parameters=trial_results[i]["parameters"])
            coroutines.append(self._eval(trial_index, parameters))
        return coroutines

    def _ucb1(self, x):
        return x["winrate"] + np.sqrt(self.c*np.log(self.total_evaluations) / x["visits"])

    async def _complete(self, coroutines):
        results = await asyncio.gather(*coroutines, return_exceptions=True)
        for r in results:
            if r["winrate"] is not None:
                self.ax_client.complete_trial(trial_index=r["trial_index"], 
                                            raw_data={"winrate": r["winrate"]})
            else:
                # trial failed, abandon this trial
                self.ax_client.abandon_trial(trial_index=r["trial_index"])
                self.failed_evaluations += 1
        self.total_evaluations += len(results)

    async def _eval(self, trial_index, trial_params, game_id=""):
        try:
            with open(f"{ENHANCEMENT_CONFIG_PATH_PREFIX}EnhancementChoice{self.enhancement}.json") as file:
                parameters = json.load(file)        
            for i in range(len(self.variables)):
                parameters[self.variables[i]] = trial_params[self.variables[i]]
            
            aux_json_path = f"{ENHANCEMENT_CONFIG_PATH_PREFIX}/auxJSONs/{trial_index}/"
            
            # create directory and enhancement.json file 
            if not os.path.exists(aux_json_path):
                os.makedirs(aux_json_path)
                with open(f"{aux_json_path}EnhancementChoice{self.enhancement}.json", "w") as outfile:
                    json.dump(parameters, outfile, indent=4)        

            # create the command to run the script
            command = f"cd {self.script_path} ; " + \
                f"./{self.scriptName} 1 {self.boardSize} 1 {self.threadsPerInstance} {self.turnDuration} " + \
                f"{PATH_FROM_SCRIPT}{aux_json_path}EnhancementChoice{self.enhancement}.json " + \
                f"{self.enhancement} {trial_index}_{game_id}"
            
            # run the command in a subprocess
            proc = await asyncio.create_subprocess_shell(
                command,
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.PIPE,
            )
            stdout, _ = await proc.communicate()
            processed = stdout.decode('utf-8').strip().split("\n")[-3:]

            # get the score from the output
            score = None
            for line in processed:
                if (line.split(" ", 1)[0] == self.enhancement):
                    score = float(line.strip().split(' ')[-1].replace("%", ''))/100.0
                    break
            
            if score is not None:
                trial_key = "_".join([f"{trial_params[var]}" for var in self.variables])
                if trial_key in self.eval_data:
                    with self.eval_data[trial_key]["lock"]:
                        trial_data = self.eval_data[trial_key]
                        trial_data["score"] += score
                        trial_data["visits"] += 1
                        trial_data["winrate"] = trial_data["score"]/trial_data["visits"]
                else:
                    self.eval_data[trial_key] = {
                        # Also save the trial index when this set of params was first introduced
                        "trial_index": trial_index,
                        "parameters": trial_params,
                        "score": score,
                        "visits": 1,
                        "winrate": score,
                        "lock": threading.Lock()
                    }
            return {
                "trial_index": trial_index, "winrate": score
            }
        except Exception as e:
            print(e)
            # print line at which error occurred
            traceback.print_exc()
            raise Exception(f"Error evaluating trial {trial_index}: {e}")
    
    def _create_experiment(self):
        """
        Creates an experiment to run.
        """
        steps = [
            GenerationStep(
                    model=Models.GPEI,
                    num_trials=self.bayes_trials,
                    max_parallelism=self.workers,
                    use_update=True,
            )
        ]

        generation_strategy = GenerationStrategy(
            name="Sobol+GPEI", 
            steps=steps
        )

        self.ax_client = AxClient(
            generation_strategy=generation_strategy,
            verbose_logging=self.verbose
        )

        self.ax_client.create_experiment(
            parameters=self.params,
            objectives={
                "winrate": ObjectiveProperties(minimize=False),
            }
        )

    def _get_plot(self, metric_name):
        match len(self.variables):
            case 1:
                return plot_slice(
                    self.ax_client.generation_strategy.model,
                    self.variables[0],
                    metric_name=metric_name,
                )
            case 2:
                return self.ax_client.get_contour_plot(self.variables[0], self.variables[1], metric_name=metric_name)
        # default case - return a list of contour plots for all pairs of variables
        plots = []
        for i in range(len(self.variables)):
            for j in range(i+1, len(self.variables)):
                plots.append(self.ax_client.get_contour_plot(self.variables[i], self.variables[j], metric_name=metric_name))
        return plots

    def _render_figures(self):
        wr = self._get_plot("winrate")
        if isinstance(wr, list):
            for fig in wr:
                render(fig)
        else:
            render(wr)

    def _remove_aux_jsons(self):
        path_prefix = f"{ENHANCEMENT_CONFIG_PATH_PREFIX}auxJSONs"
        # remove all directories in the auxJSONs directory
        for file in os.listdir(path_prefix):
            file_path = os.path.join(path_prefix, file)
            if os.path.isdir(file_path):
                # remove all files in the directory
                for f in os.listdir(file_path):
                    os.remove(os.path.join(file_path, f))
                # remove the directory
                os.rmdir(file_path)

    def _confidence_interval(self, winrate, visits):
        CI = proportion_confint(int(winrate * visits), visits, alpha=0.05, method='beta')
        return [round(x, 4) for x in CI]

    def run(self, plot : bool = False, render : bool = False):
        """
        Runs the experiment.

        Parameters
        ----------
        plot : bool
            Whether to plot the results of the experiment.
        render : bool
            Whether to render, i.e. display in browser, the results of the experiment.
            Note - this will render the plots for each trial.
        """
        asyncio.run(self._run(plot, render))

    async def _run(self, plot : bool = False, render : bool = False):
        """
        Helper function for running the experiment.

        Parameters
        ----------
        plot : bool
            Whether to plot the results of the experiment.
        render : bool
            Whether to render, i.e. display in browser, the results of the experiment.
            Note - this will render the plots for each trial.
        """
        self._remove_aux_jsons()

        while self.sobol_evaluations < self.init_points:
            num_points = min(self.init_points - self.sobol_evaluations, self.workers)
            print(f"Running {self.total_evaluations}/{self.init_points} initial points...")
            coroutines = self._explore_sobol_points(num_points)
            await self._complete(coroutines)

            self.save_model()

        done = False

        if plot:
            win_rate_plots = []

        while not done:
            
            num_exploit = self.workers - self.bayes_workers - self.sobol_workers
            
            bayes_coroutines, done = self._explore_bayes_points(self.bayes_workers)
            if (len(bayes_coroutines) == 0):
                # This is a necessary check for when models are reloaded from a checkpoint
                print("No more points to explore")
                break

            sobol_coroutines = self._explore_sobol_points(self.sobol_workers)
            exploit_coroutines = self._exploit_previous_points(num_exploit)

            coroutines = sobol_coroutines + bayes_coroutines + exploit_coroutines

            await self._complete(coroutines)

            # retrain ax model on any completed trial data - does not happen automatically when trials are completed
            # we don't include this in the `_complete` function because we only want to retrain the model while adding
            # bayesian points - newly added sobol/exploit points do not benefit from retraining the model on their own
            self.ax_client.fit_model()

            if plot:
                try:
                    plots = self._get_plot("winrate")
                    if isinstance(plots, list):
                        win_rate_plots.append([p.data for p in plots])
                    else:
                        win_rate_plots.append(plots.data)
                except Exception as e:
                    print("Error plotting figures")
                    print(e)
                    traceback.print_exc()
                    raise e
            if render:
                try: 
                    self._render_figures()
                except Exception as e:
                    print("Error rendering figures")
                    print(e)
                    traceback.print_exc()
                    raise e
                
            self.save_model()

        await self._run_post_tuning_phase()

        if plot:
            try:
                self._plot_figures(win_rate_plots=win_rate_plots)
            except Exception as e:
                print("Error plotting final figures")
                print(e)
                traceback.print_exc()
                raise e

        self._remove_aux_jsons()

    async def _run_post_tuning_phase(self):

        print(f"Running post-tuning phase...")

        N = self.required_num_games

        # extract the evaluation data to a list
        points = list(self.eval_data.values())

        found_best = False

        while not found_best:
            # sort the data by LCB of winrate
            # if UCB or winrate is used, a point with one visit and one win will be selected
            points.sort(key=lambda x: self._confidence_interval(x["winrate"], x["visits"])[0], reverse=True)

            print("Current best point according to LCB of winrate:")
            print(f"winrate: {points[0]['winrate']}, visits: {points[0]['visits']}, " + \
                  f"CI: {self._confidence_interval(points[0]['winrate'], points[0]['visits'])}")

            # find points for which the UCB is greater than the best point's winrate
            # and the number of evaluations is less than N
            candidate_points = []
            # if the best point has less than N evaluations, we need to evaluate it to ensure statistical significance
            if (points[0]["visits"] < N):
                candidate_points.append(points[0])
            ucb_to_beat = self._confidence_interval(points[0]["winrate"], points[0]["visits"])[1]
            for point in points:
                ucb = self._confidence_interval(point["winrate"], point["visits"])[1]
                if ucb > ucb_to_beat and point["visits"] < N:
                    candidate_points.append(point)
                
            # if there are no candidate points, we have found the best point
            found_best = len(candidate_points) == 0

            if not found_best:
                print(f"Evaluating {min(len(candidate_points), self.workers)} candidate points...")
                # if there are candidate points, we need to evaluate them, so we allocate coroutines equal to the number
                # of workers round-robin style and run them
                coroutines = []
                index = 0
                for _ in range(self.workers):
                    parameters, trial_index = self.ax_client.attach_trial(parameters=candidate_points[index]["parameters"])
                    coroutines.append(self._eval(trial_index, parameters))
                    index = (index + 1) % len(candidate_points)

                await self._complete(coroutines)
                self.total_post_tuning_evaluations += len(coroutines)

                self.save_model()
        
        # retrain ax model on any completed trial data - we do this here to include the custom evaluation data for
        # plotting purposes
        self.ax_client.fit_model()
        
        print("Post-tuning phase complete!")
        print(f"Total evaluations: {self.total_evaluations}")
        print(f"Total post-tuning evaluations: {self.total_post_tuning_evaluations}")
        print(f"Total failed evaluations: {self.failed_evaluations}")
        
        # Sort the points by the UCB of the winrate
        points.sort(key=lambda x: self._confidence_interval(x["winrate"], x["visits"])[1], reverse=True)
        
        results = {
            "total_points_evaluated": len(self.eval_data),
            "bayes_points_evaluated": self.bayes_trials,
            "sobol_points_evaluated": len(self.eval_data) - self.bayes_trials,
            "total_evaluations": self.total_evaluations,
            "total_post_tuning_evals": self.total_post_tuning_evaluations,
            "total_failed_evaluations": self.failed_evaluations,
            "best_point(s)": {}
        }
        for point in points:
            if point["winrate"] == points[0]["winrate"]:
                results["best_point(s)"][f"trial={point['trial_index']}"] = {
                    "winrate": point["winrate"],
                    "visits": point["visits"],
                    "parameters": point["parameters"]
                }
            else:
                break

        print("trial index, winrates, visits:")
        for point in points:
            print(f"{point['trial_index']}\t\t", point["winrate"], point["visits"])

        print("Found best point(s)!")
        print("Best point(s):")
        print(json.dumps(results, indent=4))
        
        path = f"{WINRATES_PATH_PREFIX}{self.game}/{self.enhancement}{self.save_folder_postfix}" + \
            f"/{self.boardSize}_{self.turnDuration}_{self.threadsPerInstance}.json"
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))
        with open(path, "w") as f:
            f.write(json.dumps(results, indent=4))



    def _plot_figures(self, win_rate_plots):
        print("Plotting figures...")
        images_path_prefix = f"{IMAGES_PATH_PREFIX}{self.game}/{self.enhancement}{self.save_folder_postfix}/" + \
            f"{self.boardSize}_{self.turnDuration}_{self.threadsPerInstance}"
        trace_path_prefix = f"{TRACES_PATH_PREFIX}{self.game}/{self.enhancement}{self.save_folder_postfix}/" + \
            f"{self.boardSize}_{self.turnDuration}_{self.threadsPerInstance}"
        gifs_path_prefix = f"{GIFS_PATH_PREFIX}{self.game}/{self.enhancement}{self.save_folder_postfix}"
        gif_postfix = f"_bs={self.boardSize}_ms={self.turnDuration}_th={self.threadsPerInstance}"

        # create the directories if it doesn't exist
        for path in [images_path_prefix, trace_path_prefix, gifs_path_prefix]:
            if not os.path.exists(path):
                os.makedirs(path)

        # create a list of file names for the saved PNG images
        wr_file_names = [f'{images_path_prefix}/winrate_{i}' for i in range(len(win_rate_plots))]

        # save winrate plots
        for i in range(len(win_rate_plots)):
            if isinstance(win_rate_plots[i], list):
                index = 0
                for j in range(len(self.variables)):
                    v1 = self.variables[j]
                    for k in range(j + 1, len(self.variables)):
                        v2 = self.variables[k]
                        pio.write_image(win_rate_plots[i][index], f"{wr_file_names[i]}_{v1}_{v2}.png", scale=3)
                        index += 1
            else:
                pio.write_image(win_rate_plots[i], f"{wr_file_names[i]}.png", scale=3)

        # write newest (post-tuning) winrate plots to a html in trace directory
        wr_plot = self._get_plot("winrate")
        if isinstance(wr_plot, list):
            index = 0
            for i in range(len(self.variables)):
                v1 = self.variables[i]
                for j in range(i + 1, len(self.variables)):
                    v2 = self.variables[j]
                    pio.write_html(wr_plot[index].data, f'{trace_path_prefix}/winrate_{v1}_{v2}.html')
                    index += 1
        else:
            pio.write_html(self._get_plot("winrate").data, f'{trace_path_prefix}/winrate.html')

        # calculate and plot the winrate trace
        points = list(self.eval_data.values())
        points.sort(key=lambda x: self._confidence_interval(x["winrate"], x["visits"])[1], reverse=True)
        best_winrate = points[0]["winrate"]
        points.sort(key=lambda x: x["trial_index"])
        # get the best statistically significant winrate trace
        best_objectives = []
        for point in points:
            best_objectives.append(min(point["winrate"], best_winrate))
        best_objectives = np.array([best_objectives])
        # save optimization trace
        best_objective_plot = optimization_trace_single_method(
            y=np.maximum.accumulate(best_objectives, axis=1),
            title="Model performance vs. # of iterations",
            ylabel="winrate"
        ).data

        # pio.write_image(best_objective_plot, f'{trace_path_prefix}/winrate_trace.png', scale=3)
        pio.write_html(best_objective_plot, f'{trace_path_prefix}/winrate_trace.html')

        # ### save pareto front
        # objectives = self.ax_client.experiment.optimization_config.objective.objectives

        # for i in range(len(objectives)):
        #     if objectives[i].metric.name == "winrate":
        #         primary_objective = objectives[i].metric
        #     if objectives[i].metric.name == "ucb1":
        #         secondary_objective = objectives[i].metric

        # pareto_frontier = compute_posterior_pareto_frontier(
        #     experiment=self.ax_client.experiment,
        #     data=self.ax_client.experiment.fetch_data(),
        #     primary_objective=primary_objective,
        #     secondary_objective=secondary_objective,
        #     absolute_metrics=["winrate", "ucb1"],
        #     num_points=5
        # )
        
        # pareto_plot = plot_pareto_frontier(pareto_frontier, CI_level=0.95).data

        # # pio.write_image(pareto_plot, f'{trace_path_prefix}/pareto_frontier.png', scale=3)
        # pio.write_html(pareto_plot, f'{trace_path_prefix}/pareto_frontier.html')

        # create a list of image objects using the file names
        if isinstance(wr_plot, list):
            for i in range(len(self.variables)):
                v1 = self.variables[i]
                for j in range(i + 1, len(self.variables)):
                    v2 = self.variables[j]
                    wr_images = [imageio.imread(f"{fileName}_{v1}_{v2}.png") for fileName in wr_file_names]
                    # save the list of images as a gif
                    imageio.mimsave(f'{gifs_path_prefix}/winrate{gif_postfix}_{v1}_{v2}.gif', wr_images, fps=1)
        else:
            wr_images = [imageio.imread(f"{fileName}.png") for fileName in wr_file_names]
            # save the list of images as a gif
            imageio.mimsave(f'{gifs_path_prefix}/winrate{gif_postfix}.gif', wr_images, fps=1)
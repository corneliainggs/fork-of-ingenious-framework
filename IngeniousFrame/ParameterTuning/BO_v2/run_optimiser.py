import os
from utils.optimiser import Optimiser

# NOTE: Only one instance of the optimiser should be run at a time

def main():
    optimiser = Optimiser(
        workers=60,
        threadsPerInstance=1,
        bayes_trials=30,
        game="go",
        boardSize=9,
        turnDuration=1000,
        enhancement="Vanilla",
        init_points=60,
        bayes_workers=1,
        sobol_workers=6,
        verbose=True,
        required_num_games=120
    )
    optimiser.load_latest_model()
    optimiser.run(plot=True, render=False)

if __name__ == "__main__":
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    main()

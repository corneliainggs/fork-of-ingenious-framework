from __future__ import annotations
from typing_extensions import Self
from threading import Thread, Lock
from typing import Tuple
from node import node
from copy import copy, deepcopy
from time import sleep
import subprocess
import signal
import numpy as np
import json
import os
import pickle
import multiprocessing as mp

PATH_FROM_SCRIPT = "../../ParameterTuning/MCTS_HardCoded/"
PATH_TO_SCRIPT = "../../scripts/"
ENHANCEMENT_PATH = "auxJSONs/"
SAVED_TREES_PATH = "savedTrees/"

class Tree:
    """
    Tree class for Monte-Carlo Tree Search Parameter Tuning.
    """

    def __init__(self, config):
        """
        Initializes the tree with the given configuration dictionary and UCB1 value, if it is given.
        Otherwise, the UCB1 value is given the default UCB1 value of 2.

        Args:
            config: The configuration dictionary.
        """
        self.config = config
        self.enhancements = list(self.config["enhancements"].keys())
        # if config contains c field
        if ("cValue" in self.config):
            self.C = self.config["cValue"]
        else:
            self.C = 2
            self.config["cValue"] = self.C
        self.root = node(type=node.ROOT, C=self.C)
        self.RUNNING = True
        self.PAUSE = True
        for e in self.enhancements:
            self.root.addChild(node(type=node.ENHANCEMENT, name=e, C=self.C))
        self.workers = []
        self.workersArePaused = []
        self.writeLock = Lock()
        self.addWorkers(int(self.config["instances"]))

    def loadTree(self, name : str):
        """
        Loads the tree from a file.

        Args:
            name: The name of the file.
        """

        # load tree from pickle file
        with open(f"{SAVED_TREES_PATH}{name}.pickle", "rb") as f:
            T = pickle.load(f)

        self.config = T["config"]
        self.C = self.config["cValue"]
        self.root = node(type=node.ROOT, C=self.C)
        self.root.fromJSON(T["nodes"])
        self.enhancements = list(self.config["enhancements"].keys())

        print("Waiting for worker threads to finish...")

        self.waitForRunningWorkers()
        self.RUNNING = False
        self.PAUSE = False

        for w in self.workers:
            w.join()

        self.workers = []
        self.workersArePaused = []

        self.addWorkers(int(self.config["instances"]))
        self.startWorkers()

    def saveTree(self, name : str):
        """
        Saves the tree to a file.

        Args:
            name: The name of the file.
        """

        self.waitForRunningWorkers()
        
        print("Saving tree...")

        T = {
            "config": self.config,
            "nodes": self.root.toJSON(),
        }

        with open(f"{SAVED_TREES_PATH}{name}.pickle", "wb") as f:
            pickle.dump(T, f)

        print("Tree saved successfully.")

    def waitForRunningWorkers(self):
        """
        Pauses the tree and waits for all worker threads to finish their simulations.
        """
        self.pause()
        print("Pausing and waiting for all threads to finish execution...")

        for i in range(len(self.workersArePaused)):
            while not (self.workersArePaused[i]):
                sleep(0.1)
            print(f"Thread {i + 1} out of {len(self.workersArePaused)} has finished execution.")

    def addWorkers(self, num : int):
        """
        Adds num worker threads to the tree.

        Args:
            num: The number of worker threads to add.
        """
        for i in range(num):
            self.workers.append(Thread(target=self.runWorker, args=(i,)))
            self.workersArePaused.append(True)
    
    def startWorkers(self):
        """
        Starts the worker threads.
        """
        self.PAUSE = True
        self.RUNNING = True
        for i in range(len(self.workers)):
            self.workersArePaused[i] = False
            self.workers[i].start()

    def resume(self):
        """
        Resumes the worker threads of the MCTS algorithm.
        """
        self.PAUSE = False

    def pause(self):
        """
        Pauses the MCTS algorithm.
        """
        self.PAUSE = True

    def printStatistics(self, threshold : int, minWR : float):
        """
        Prints the visit counts of each enhancement.

        Args:
            threshold: The visit threshold.
            minWR: The minimum winrate.
        """
        for i in range(len(self.root.children)):
            print("=======================================")
            print(f"{self.root.children[i].name}:")
            print(f"\tTotal number of visits = {self.root.children[i].visitCount}")
            self.winRateStatistics(i, threshold, minWR)
        print("=======================================")
        print(f"Total number of visits = {self.root.visitCount}")
        print("=======================================")
        print("")

    def winRateStatistics(self, index : int, threshold : int, minWR : float):
        """
        Prints the ordered winrate statistics for nodes that have been visited more than threshold times, without
        their children having been visited more than threshold times.

        Args:
            index: The index of the enhancement in the list of enhancements.
            threshold: The visit threshold.
            minWR: The minimum winrate.
        """
        print("=======================================")
        results = []
        self.getMostVisitedNodes(self.root.children[index], threshold, minWR, results)
        if (len(results) == 0):
            print("\tNo nodes with sufficient statistics.")
        # sort results based on winrate
        results.sort(key=lambda x: (x.score/x.visitCount), reverse=True)
        # print results
        for r in results:
            print(f"\t{round(100*r.score/r.visitCount, 3)}% winrate over {r.visitCount} games with parameter ranges = [ {r.bucketsToStr()} ] ")
        print("=======================================")
        print("")

    def getMostVisitedNodes(self, N : node, visitThreshold : int, minWR : float, results : list[node]) -> None:
        """
        Appends all node-children that have been visited more than threshold times, without
        their children having been visited more than threshold times, to the results list.

        Args:
            N: The node to start the search from.
            visitThreshold: The visit threshold.
            minWR: The minimum winrate.
            results: The list of nodes to append to.
        """
        if (N.type != node.ENHANCEMENT):
            if (N.visitCount < visitThreshold and N.parent.visitCount >= visitThreshold or len(N.children) == 0 and N.visitCount >= visitThreshold):
                if (N.parent.score/N.parent.visitCount >= minWR and N.parent not in results):
                    results.append(N.parent)
                if (len(N.children) == 0 and N.visitCount >= visitThreshold and N.score/N.visitCount >= minWR and N not in results):
                    results.append(N)
                return
        for c in N.children:
            self.getMostVisitedNodes(c, visitThreshold, minWR, results)


    def runWorker(self, id : int):
        """
        Starts a worker thread that will search the tree by applying the MCTS algorithm.

        Args:
            id: The id of the worker thread.
        """
        # Pause
        while (self.PAUSE):
            self.workersArePaused[id] = True
            sleep(0.1)
        self.workersArePaused[id] = False

        while (self.RUNNING):
            # Traversal
            leaf, enhancement = self.traverse()

            # if the depth limit has not been reached, expand the node
            if (leaf.depth - 1 < self.config["maxParamSplitDepth"]*len(self.config["enhancements"][enhancement]["params"])):
                # Attempt to lock the leaf node and continue to node expansion if acquired
                if not (leaf.lock.acquire(blocking=False)):
                    # If the leaf node is locked, wait for node expansion to complete
                    leaf.lock.acquire()
                    # node expansion completed
                    leaf.lock.release()
                    # unlock and continue in order to find a new leaf node
                    continue

                originParent = leaf
                # apply virtual loss
                originParent.visitCount += 1
                tempParent = originParent.parent
                while (tempParent != None):
                    tempParent.lock.acquire()
                    tempParent.visitCount += 1
                    tempParent.lock.release()
                    tempParent = tempParent.parent

                # Node expansion
                if (len(leaf.children) == 0 and not leaf.isTerminal and leaf.visitCount > 0):
                    # Lock expansion of nodes to prevent tree from printing changing tree structure
                    self.writeLock.acquire()
                    leaf.nodeExpansion(self.config["bucketSplitCount"], self.C, self.config["enhancements"][enhancement]["params"])
                    self.writeLock.release()
                    # release the lock after expanding the node
                    leaf.lock.release()
                    leaf = leaf.children[0]
                else:
                    leaf.lock.release()
            else:
                
                originParent = leaf
                # apply virtual loss
                originParent.visitCount += 1
                tempParent = originParent.parent
                while (tempParent != None):
                    tempParent.lock.acquire()
                    tempParent.visitCount += 1
                    tempParent.lock.release()
                    tempParent = tempParent.parent
            
            # Evaluate simulation result
            score = float(self.eval(leaf, enhancement, id))

            if not (score == -1.0):
                # Back propagation
                self.backPropagation(leaf, score)

                originParent.lock.acquire()
                # remove virtual loss
                originParent.visitCount -= 1
                originParent.lock.release()
                originParent = originParent.parent
                while (originParent != None):
                    originParent.lock.acquire()
                    originParent.visitCount -= 1
                    originParent.lock.release()
                    originParent = originParent.parent
        
            # Pause
            while (self.PAUSE):
                self.workersArePaused[id] = True
                sleep(0.1)        
            self.workersArePaused[id] = False
        
    def backPropagation(self, leaf : node, score : float) -> None:
        """
        Back propagates the score of a simulation result to the parent nodes.

        Args:
            leaf: The leaf node to back propagate from.
            score: The score of the simulation result.
        """
        while (leaf.parent != None):
            leaf.lock.acquire()
            leaf.visitCount += 1
            leaf.score += score
            leaf.lock.release()
            leaf = leaf.parent
            
        leaf.lock.acquire()
        leaf.visitCount += 1
        leaf.score += score
        leaf.lock.release()
    
    def traverse(self) -> Tuple[node, str]:
        """
        Traverses the tree from its root node and returns the leaf node to simulate based on the UCB1 score evaluations at each step.
        """
        parent = copy(self.root)

        enhancement = ""

        if (len(parent.children) == 0):
            return parent
        n = parent.children[0]
        while (len(parent.children) > 0):
            obj = -1
            for i in range(len(parent.children)):
                child = parent.children[i]
                if (child.type == node.ENHANCEMENT and child.visitCount < 300):
                    n = child
                    break
                if (child.UCB1() > obj):
                    obj = child.UCB1()
                    n = child
            if (enhancement == ""):
                enhancement = n.name
            parent = n
        return n, enhancement

    def printTree(self):
        """
        Prints the tree in text format from the root node.
        """
        self.writeLock.acquire()
        self.printTreeRecur(copy(self.root))
        self.writeLock.release()

    def printTreeRecur(self, N : node, prefix = "", depth = 0):
        """
        Recursive helper function for printing the tree.

        Args:
            N: The node to print.
            prefix: The prefix generated by the parent nodes.
            depth: The depth of the node.
        """
        prefix = list(prefix)
        for s in range(len(prefix) - 5):
            if (prefix[s] == "├" or prefix[s] == "│"):
                prefix[s] = "│"
            else:
                prefix[s] = " "
        prefix = "".join(prefix)
        if (N.type == node.ROOT):
            print("ROOT")
        elif (N.type == node.ENHANCEMENT):
            print(f"{prefix}{N.name}")
        elif (N.type == node.BUCKET):
            print(f"{prefix}{N.name}:{N.bucket}")
        depth += 1
        for i in range(len(N.children)):
            if (N.type == node.BUCKET):
                if (i != len(N.children) - 1):
                    self.printTreeRecur(N.children[i], prefix + "├────", depth)
                else: 
                    self.printTreeRecur(N.children[i], prefix + "└────", depth)
            else:
                if (i != len(N.children) - 1):
                    self.printTreeRecur(N.children[i], prefix + "├────", depth)
                else:
                    self.printTreeRecur(N.children[i], prefix + "└────", depth)

    def eval(self, leaf, enhancement, id) -> float:
        """
        Evaluates the simulation result of a leaf node using the buckets chosen up and to the leaf node.

        Args:
            leaf: The leaf node to evaluate.
            enhancement: The enhancement used to generate the leaf node.
            id: The id of the worker thread.
        """
        enhancementDict = self.config["enhancements"][enhancement]
        buckets = leaf.buckets
        params = leaf.params

        for i in range(len(self.config['enhancements'][enhancement]['params'])):
            if (i < len(params)):
                sample = np.random.uniform(buckets[i][0], buckets[i][1])
                key = self.config["boundMapping"][params[i]]
            else:
                sample = np.random.uniform(0.0, 1.0)
                key = self.config["boundMapping"][self.config['enhancements'][enhancement]['params'][i]]
            upper = enhancementDict[f"{key}Upper"]
            lower = enhancementDict[f"{key}Lower"]
            enhancementDict[key] = sample*(upper - lower) + lower
        
        if ("cValue" not in enhancementDict.keys()):
            enhancementDict["cValue"] = self.config["tunedC"]
        
        # Make directory for thread if it doesn't exist
        if not os.path.exists(f"{ENHANCEMENT_PATH}{id}"):
            os.makedirs(f"{ENHANCEMENT_PATH}{id}")
        # print json of enhancementDict
        with open(f"{ENHANCEMENT_PATH}{id}/EnhancementChoice{enhancement}.json", "w") as outfile:
            json.dump(enhancementDict, outfile, indent=4)

        command = [f"cd {self.config['scriptPath']} ; " + \
            f"./{self.config['scriptName']} 1 {self.config['boardSize']} 1 {self.config['threadsPerInstance']} {self.config['turnDuration']} " + \
            f"{PATH_FROM_SCRIPT}{ENHANCEMENT_PATH}{id}/EnhancementChoice{enhancement}.json {enhancement} {id}"]
        
        try:
            result = subprocess.run(command, shell = True, capture_output = True)
            processed = result.stdout.decode('utf-8').strip().split("\n")[-3:]
            for line in processed:
                if (line.split(" ", 1)[0] == enhancement):
                    wr = float(line.strip().split(' ')[-1].replace("%", ''))
                    return wr/100.0
            print("ERROR: No output from simulation")
            print("caused by : ", command)
            print("output : ", result.stdout.decode('utf-8').strip().split("\n")[-3:])
            sleep(1)
            # Remove auxillary json file to preserve inode space
            os.remove(f"{ENHANCEMENT_PATH}{id}/EnhancementChoice{enhancement}.json")
            return -1.0
        except Exception as e:
            print("ERROR: Simulation crashed")
            print("caused by : ", command)
            print(e)
            sleep(1)
            # Remove auxillary json file to preserve inode space
            os.remove(f"{ENHANCEMENT_PATH}{id}/EnhancementChoice{enhancement}.json")
            return 0.0
            
    def handler(self, signum, frame):
        """
        Function that handles Ctrl + C interrupts.
        """
        self.stop()

    def stop(self):
        """
        Stops the MCTS algorithm.
        """
        self.RUNNING = False
        self.PAUSE = False

        print("Waiting for worker threads to finish...")

        for w in self.workers:
            w.join()

        self.printStatistics(100, 0.0)

        exit(0)

    def updateInstances(self, instances):
        """
        Updates the number of instances to use for the MCTS algorithm.

        Args:
            instances: The number of instances to use.
        """
        self.config["instances"] = instances

if __name__ == "__main__":
    # Change working directory to directory of current python file
    path = os.path.dirname(os.path.abspath(__file__))
    os.chdir(path)
    
    # Read enhancements and ranges from config.json
    with open("config.json") as f:
        cnfg = json.load(f)

    # Initialise tree
    T = Tree(cnfg)

    # Start workers
    T.startWorkers()

    signal.signal(signal.SIGINT, T.handler)

    while (True and T.RUNNING):
        if (T.PAUSE):
            print("1. Resume")
        else:
            print("1. Pause")
        print("2. Print tree (pauses node expansion while printing)")
        print("3. Print statistics")
        print("4. Save tree")
        print("5. Load tree")
        print("6. Update number of instances")
        print("0. Exit")
        a = input()
        if (a == "1"):
            T.PAUSE = not T.PAUSE
        elif (a == "2"):
            T.printTree()
        elif (a == "3"):
            print("Specify the threshold number of games to require for printing results:")
            while True:
                try:
                    threshold = int(input())
                    if (threshold <= 0):
                        print("Threshold must be a positive integer")
                        continue
                    break
                except ValueError:
                    print("Please enter an integer.")
            print("Specify  the minimum winrate required for printing results")
            while True:
                try:
                    minWinrate = float(input())
                    # Verify float is between 0 and 1
                    if (minWinrate < 0 or minWinrate > 1):
                        print("Please enter a number between 0.0 and 1.0.")
                    else:
                        break
                except ValueError:
                    print("Please enter a value between 0.0 and 1.0.")
            T.printStatistics(threshold, minWinrate)
        elif (a == "4"):
            print("Specify the name of the file to save the tree to, e.g. 'tree1':")
            savedTrees = ["Back"]
            if not os.path.exists("savedTrees"):
                os.makedirs("savedTrees")
            for file in os.listdir("savedTrees"):
                if (file.endswith(".pickle")):
                    savedTrees.append(file.replace(".pickle", ""))
            if (len(savedTrees) > 0):
                print("----")
                print("Saved trees:")
                for i in range(len(savedTrees)):
                    print(f"{i}. {savedTrees[i]}")
                print("----")

            while True:
                try:
                    filename = input()
                    if (filename in savedTrees and filename != "0" and filename != "Back"):
                        print("Tree already exists. Overwrite? (y/n)")
                        if (input() == "y"):
                            break
                    break
                except ValueError:
                    print("Please enter a string.")
            if (filename == "0" or filename == "Back"):
                print("Tree not saved.")
                continue
            T.saveTree(filename)
        elif (a == "5"):
            savedTrees = ["Back"]
            for file in os.listdir("savedTrees"):
                if (file.endswith(".pickle")):
                    savedTrees.append(file.replace(".pickle", ""))
            if (len(savedTrees) == 1):
                print("No saved trees found.")
                continue
            print("Specify the name of the file to load the tree from:")
            print("\t*Note that this will overwrite the current tree and load the other tree's configuration .json.")
            while True:
                try:
                    print("----")
                    print("Saved trees:")
                    for i in range(len(savedTrees)):
                        print(f"{i}. {savedTrees[i]}")
                    print("----")
                    filename = input()
                    if (filename not in savedTrees and filename != "0" and filename != "Back"):
                        print("Please enter a valid filename.")
                    else:
                        break
                except ValueError:
                    print("Please enter a string matching one of the filenames.")
            if (filename == "0" or filename == "Back"):
                print("Tree not loaded.")
                continue
            T.loadTree(filename)
        elif (a == "6"):
            print("Current number of instances: ", T.config["instances"])
            print("Specify the number of instances to run:")
            print("\t*Note that the tree will have to be reloaded after this change. Input 0 to cancel. Reload by saving and loading the tree.")
            while True:
                try:
                    instances = int(input())
                    if (instances < 0):
                        print("Please enter a positive integer.")
                    # check there are enough cores to run the specified number of instances
                    elif (instances*T.config["threadsPerInstance"] > mp.cpu_count()):
                        print(f"Please enter a number of instances that can be run on this machine. (Max: {mp.cpu_count()//T.config['threadsPerInstance']})")
                    else:
                        break
                except ValueError:
                    print("Please enter an integer.")
            if not (instances == 0):
                T.updateInstances(instances)
        elif (a == "0"):
            T.stop()

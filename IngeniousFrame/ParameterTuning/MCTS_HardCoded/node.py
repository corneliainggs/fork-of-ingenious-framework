from __future__ import annotations
from copy import deepcopy
from threading import Lock
import math
import numpy as np


class node:
    """
    Node class for Monte-Carlo Tree Search Parameter Tuning.
    """

    # Impermanent nodes are used to indicate the node is being read from a file.
    IMPERMANENT = -1
    ROOT = 0
    ENHANCEMENT = 1
    BUCKET = 2


    SplitThreshold = 2

    def __init__(self, type, C=2, name="", bucket=[0.0, 1.0], index=0, splitCount=0):
        """
        Initialises the node with the given parameters.

        Args:
            Type: The type of the node, [0, 1, 2] corresponding to [ROOT, ENHANCEMENT, BUCKET]. *A fourth node-type is added for IMPERMANENT nodes,
            which are used internally to represent nodes that are being read from a file.
            C: The constant for UCB1 (default = 2).
            Name: The name of the node, if it is an ENHANCEMENT node, else defaults to "".
            Bucket: The bucket of the node, if it is a BUCKET node, else defaults to [0.0, 1.0].
            Index: The parameter index of the node, else defaults to 0.
            SplitCount: The number of times the parameter corresponding to this node has been split, defaults to 0.
        """
        self.parent = None
        self.depth = 0
        self.isTerminal = False
        self.children = []
        self.lock = Lock()
        if (type == node.IMPERMANENT):
            return
        self.type = type
        if (type == node.ENHANCEMENT):
            self.name = name
            self.bucket = bucket
        elif (type == node.BUCKET):
            self.bucket = bucket
        self.score = 0
        self.visitCount = 0
        self.C = C
        self.params = []
        self.buckets = []
        self.splitCount = splitCount
        self.index = index


    def fromJSON(self, data) -> node | None:
        """
        Initialises the node with the given data.

        Args:
            data: The data to initialise the node with.
        """
        # Add node data
        self.type = data["type"]
        self.depth = data["depth"]
        self.score = data["score"]
        self.visitCount = data["visitCount"]
        self.C = data["C"]
        self.params = data["params"]
        self.buckets = data["buckets"]
        self.splitCount = data["splitCount"]
        self.index = data["index"]
        if (self.type == node.ENHANCEMENT):
            self.name = data["name"]
            self.bucket = data["bucket"]
        elif (self.type == node.BUCKET):
            self.name = data["name"]
            self.bucket = data["bucket"]
        # Add children data
        for child in data["children"]:
            childNode = node(type=node.IMPERMANENT)
            childNode.fromJSON(child)
            childNode.parent = self
            self.children.append(childNode)
            childNode.depth = self.depth + 1

    def toJSON(self):
        """
        Returns the node and (recursively) its children as a JSON object.
        """
        data = {
            "type": self.type,
            "score": self.score,
            "visitCount": self.visitCount,
            "C": self.C,
            "children": [child.toJSON() for child in self.children],
            "params": self.params,
            "buckets": self.buckets,
            "splitCount": self.splitCount,
            "index": self.index,
            "depth": self.depth
        }
        if (self.type == node.ENHANCEMENT):
            data["name"] = self.name
            data["bucket"] = self.bucket
        elif (self.type == node.BUCKET):
            data["name"] = self.name
            data["bucket"] = self.bucket
    
        return data
    

            

    def UCB1(self):
        """
        Returns the UCB1 value of the node.
        """
        if (self.visitCount == 0 or self.parent.visitCount == 0):
            return 1000000
        return self.score/self.visitCount + math.sqrt(self.C*math.log(self.parent.visitCount)/self.visitCount)
        
    def addChild(self, child):
        """
        Adds the given child to the node.

        Args:
            child: The child to add.
        """
        child.parent = self
        child.depth = self.depth + 1
        child.params = deepcopy(self.params)
        child.buckets = deepcopy(self.buckets)
        if (child.type == node.BUCKET):
            if (self.index + 1 == len(self.params)):
                child.splitCount += 1
                child.index = 0
            else:
                child.index = self.index + 1
            child.buckets[child.index] = child.bucket
            child.name = child.params[child.index]
        self.children.append(child)

    def nodeExpansion(self, n, C, params=[]):
        """
        Expands the node by creating n new children.

        Args:
            n: The number of children to split the current parameter into.
            C: The constant for UCB1.
            params: The list of all params.
        """
        # Add params
        if (self.type == node.ENHANCEMENT):
            self.params = deepcopy(params)
            self.buckets = [[0.0, 1.0]]*len(self.params)
            # self.addChild(node(type=node.BUCKET, C=C, name=params[0]))
            # return

        # Add buckets
        if (len(self.params) == len(params)):
            pos = ((self.index + 1) % len(self.params))
            l = (self.buckets[pos][1] - self.buckets[pos][0])/n
            for i in range(n):
                if (self.splitCount < node.SplitThreshold):
                    self.addChild(node(type=node.BUCKET, C=C, bucket=(self.buckets[pos][0] + l*i, self.buckets[pos][0] + l*(i+1))))
                else:
                    l = 1.0/n
                    self.addChild(node(type=node.BUCKET, C=C, bucket=(l*i, l*(i+1))))

    def bucketsToStr(self) -> str:
        """
        Returns the bucket of the node's enhancement as a string.
        """
        s = ""
        for i in range(len(self.params)):
            s += f"{self.params[i]}: [{self.buckets[i][0]}, {self.buckets[i][1]}]"
            if (i != len(self.params) - 1):
                s += ", "
        return s


    def printBuckets(self, prefix=None) -> None:
        """
        Prints the bucket of the node's enhancement.
        """
        if (prefix != None):
            print(f"{prefix}:", end=f"{' '*(20 - len(prefix))}│\t")
        for i in range(len(self.params)):
            # print enhancement, bucket pairs as comma seperated list
            print(f"{self.params[i]}: [{self.buckets[i][0]}, {self.buckets[i][1]}]", end=' ')
        print()
    

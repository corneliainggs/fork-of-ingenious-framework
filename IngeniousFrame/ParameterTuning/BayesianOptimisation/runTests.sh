#!/bin/bash

# enhancementList=(Vanilla Rave MastExpansion FPU Contextual)
enhancementList=(Vanilla)
# enhancementList=(Rave MastExpansion FPU Contextual)
# game options = {go, othello, nim}
game="go"
# constant newline character
newline=$'\n'

# print enhancement list
for enhancement in ${enhancementList[@]};
do
    # board size, instances, games per instance, threads, time per game, trials
    sh -c 'python3 tuner.py 9 24 2 1 1000 50' <<< "${enhancement}${newline}${game}" > tunedLogs/${game}/3s_50T_120G_9S_${enhancement}.out
    # sh -c 'python3 tuner.py 10 40 3 1 1000 50' <<< "${enhancement}Tuned${newline}${game}" > tunedLogs/${game}/1s_50T_120G_10S_${enhancement}Tuned.out
done

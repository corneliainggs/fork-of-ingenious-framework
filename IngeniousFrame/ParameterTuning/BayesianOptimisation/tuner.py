import os
import sys
import json
import subprocess
from typing import List, Dict
from ax.service.ax_client import AxClient

PATH_FROM_SCRIPT = "../../ParameterTuning/BayesianOptimisation/enhancementConfigurations/"
PATH_TO_SCRIPT = "../../scripts/"
ENHANCEMENT_PATH = "enhancementConfigurations/"
GAMES = ["go", "othello", "nim"]

class Tuner:

    def __init__(self, scriptPath : str, scriptName : str, enhancementName : str, \
            enhancementVariables : List[str], configJson : Dict[str, any], instances : int, boardSize : int, \
                gamesPerInstance : int, threadsPerInstance : int, turnDuration : int, \
                    benchmarkEnhancement = "Vanilla") -> float:
        """
        Parameters
        ----------
        scriptPath : Relative path to the test script to run
        scriptName : Name of the test script to run
        enhancementName : Name of the enhancement to optimise. Must correspond to the name of the enhancement json file,
        i.e. for "EnhancementChoiceVanilla.json", enhancementName = "Vanilla". The enhancement name must not contain
        spaces.
        enhancementVariables : List of the variables to optimise. Must correspond to the names of the variables in the
        enhancement json file.
        configJson : A dictionary containing the contents of the enhancement json file.
        instances : Number of game instances to run in parallel.
        boardSize : Size of the board to use.
        gamesPerInstance : Number of games to run per instance.
        threadsPerInstance : Number of threads to use per instance.
        turnDuration : Duration of each turn in milliseconds.
        benchmarkEnhancement : Name of the enhancement to use as a benchmark. Defaults to "Vanilla".
            TODO: Not yet implemented - currently only supports "Vanilla" as benchmark function.
        """
        self.scriptPath = scriptPath
        self.scriptName = scriptName
        self.enhancementName = enhancementName
        self.enhancementVariables = enhancementVariables    
        self.configJson = configJson
        self.instances = instances
        self.boardSize = boardSize
        self.gamesPerInstance = gamesPerInstance
        self.threadsPerInstance = threadsPerInstance
        self.turnDuration = turnDuration
        self.benchmarkEnhancement = benchmarkEnhancement
        self._createExperiment()
    
    def evaluate(self, values) -> None:
        with open(f"{ENHANCEMENT_PATH}EnhancementChoice{self.enhancementName}.json") as file:
            parameters = json.load(file)        
        for i in range(len(self.enhancementVariables)):
            parameters[self.enhancementVariables[i]] = values.get(self.enhancementVariables[i])
        with open(f"{ENHANCEMENT_PATH}EnhancementChoice{self.enhancementName}.json", "w") as outfile:
            json.dump(parameters, outfile, indent=4)        

        command = [f"cd {self.scriptPath} ;" + \
            f"./{self.scriptName} {self.instances} {self.boardSize} {self.gamesPerInstance} " + f"{self.threadsPerInstance} {self.turnDuration} " + \
            f"{PATH_FROM_SCRIPT}EnhancementChoice{self.enhancementName}.json {self.enhancementName}"]
        
        result = subprocess.run(command, shell = True, capture_output = True)
        processed = result.stdout.decode('utf-8').strip().split("\n")[-3:]
        for line in processed:
            if (line.split(" ", 1)[0].replace("tChoice","").replace("ice","") == self.enhancementName):
                wr = float(line.strip().split(' ')[-1].replace("%", ''))
                print("Parameters : ", values, flush = True)
                print(f"WinRate : {wr}\n", flush = True)      
                return {"WinRate" : wr}
            
    def _createExperiment(self):
        self.params = [{
            "name" : f"{enh}",
            "value_type" : "float",
            "type" : "range",
            "bounds" : [self.configJson[f"{enh}Lower"], \
                        self.configJson[f"{enh}Upper"]]
        } for enh in self.enhancementVariables]

        self.ax_client = AxClient()
        self.ax_client.create_experiment(
            name="GO",
            parameters=self.params,
            objective_name="WinRate",
            minimize=False,
        )
            
    def runExperiment(self, totalTrials, instances, gamesPerInstance):
        # Run the optimisation
        for i in range(totalTrials):
            print(f"Running trial {i+1}/{totalTrials}...")
            parameters, trial_index = self.ax_client.get_next_trial()
            self.ax_client.complete_trial(trial_index=trial_index, raw_data=self.evaluate(parameters))

        best_parameters, values = self.ax_client.get_best_parameters(use_model_predictions=False)

        print(f"{instances*gamesPerInstance*totalTrials} games played, {instances*gamesPerInstance} games per trial\n")

        print("Best parameters : ", best_parameters)
        print("Best values : ", values)

def validateThreadCount(threads):
    if (threads > os.cpu_count()):
        # Ask user for yes no input
        print("WARNING : Program requires more threads than the system has cores. Do you want to continue? (y/n)")
        choice = input()
        if (choice == "y" or choice == "Y"):
            print("Continuing...")
        else:
            exit(1)

def chooseEnhancement():
    enhancements = os.listdir(ENHANCEMENT_PATH)
    ### Determine the enhancement to optimise:
    print("Choose enhancement to optimise :")
    for i in range(0, len(enhancements)):
        print(f"\t{enhancements[i].replace('EnhancementChoice','').replace('.json','')}")
    while True:
        choice = sys.stdin.readline().strip()
        if f"EnhancementChoice{choice}.json" in enhancements:
            break
        else:
            # print("Invalid choice - Please select a choice from the list.")
            pass
    return choice

def chooseGame():
    print("Choose game to optimise :")
    for game in GAMES:
        print(f"\t{game}")
    while True:
        game = sys.stdin.readline().strip()
        if game in GAMES:
            break
        else:
            # print("Invalid choice - Please select a choice from the list.")
            pass
    return game

def getOptimisationParams(choice):
    try:
        with open(f"{ENHANCEMENT_PATH}EnhancementChoice{choice}.json") as file:
            configJson = json.load(file)
    except Exception as e:
        print("Optimisation for enhancement choice not yet implemented. Exiting...")
        exit(1)
    with open(f"{ENHANCEMENT_PATH}EnhancementChoice{choice}.json", "w") as outfile:
            json.dump(configJson, outfile, indent=4)
    exempt = [
        "Selection",
        "Expansion",
        "Simulation",
        "Backpropagation"
    ]
    variables = []
    for key in configJson.keys():
        if key not in exempt and not ("Upper" in key or "Lower" in key):
            for prefix in configJson.keys():
                if ((key + "Upper") in prefix):
                    variables.append(key)
                    break

    if (len(variables) == 0):
        print("No parameters to optimise. Exiting...")
        exit(1)
    
    return variables, configJson

if __name__ == "__main__":
    # Read # instances, # boardsize, # games per instance, # threads per instance, # turn duration from command line
    if len(sys.argv) != 7:
        print("Usage: python3 tuner.py <boardSize> <instances> <gamesPerInstance> <threadsPerInstance> <turnDuration> <totalTrials>")
        exit(1)
    boardSize = int(sys.argv[1])
    instances = int(sys.argv[2])
    gamesPerInstance = int(sys.argv[3])
    threadsPerInstance = int(sys.argv[4])
    turnDuration = int(sys.argv[5])
    totalTrials = int(sys.argv[6])

    validateThreadCount(instances*threadsPerInstance)

    print(f"Now tuning with {instances} instances, {threadsPerInstance} threads per instance, {gamesPerInstance} games per instance, {turnDuration} turn duration, {totalTrials} total trials")
    
    choice = chooseEnhancement()
    game = chooseGame()
    variables, configJson = getOptimisationParams(choice)

    print(f"Now optimising {game} for {choice}: {variables}")
    # Initialise tuner
    tune = Tuner(  scriptPath = f"{PATH_TO_SCRIPT}{game}",
                scriptName = "test.sh",
                enhancementName = choice,
                enhancementVariables = variables,
                configJson = configJson,
                instances=instances,
                boardSize=boardSize,
                gamesPerInstance=gamesPerInstance,
                threadsPerInstance=threadsPerInstance,
                turnDuration=turnDuration,
                benchmarkEnhancement = "Vanilla")
    
    tune.runExperiment(totalTrials, instances, gamesPerInstance)
    

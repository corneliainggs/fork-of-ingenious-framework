package za.ac.sun.cs.ingenious.games.diceGames.dudo.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.commandline.client.ClientArguments;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.dag.CycleException;
import za.ac.sun.cs.ingenious.core.exception.dag.StateNotFoundException;
import za.ac.sun.cs.ingenious.core.exception.probability.IncorrectlyNormalizedDistributionException;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoActionSensor;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoFinalEvaluator;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoGameState;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.network.DudoInitGameMessage;
import za.ac.sun.cs.ingenious.search.cfr.FSICFR;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.ClassUtils;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import static za.ac.sun.cs.ingenious.games.diceGames.dudo.engines.DudoCFREngine.getPrettyStrategies;

public class DudoFSICFREngine extends DudoGeneralEngine {

    private int numIterations = 1000;

    private Map<DudoGameState, Map<Action, Double>> normStrategies;
    private FSICFR<DudoGameState> fsicfr;

    private final Random rand;
    private boolean trainMore = false;
    private boolean printStrategies = false;

    public DudoFSICFREngine(EngineToServerConnection toServer, String enhancementPath,
            int threadCount, int turnLength) {
        super(toServer);

        if (!Objects.equals(enhancementPath, ClientArguments.defaultEnhancementConfigPath)) {
            try {
                MatchSetting m = new MatchSetting(enhancementPath);

                numIterations = m.getSettingAsInt("iterations", numIterations);
                Log.info("FSICFREngine", "Loaded iterations: " + numIterations);

                printStrategies = m.getSettingAsBoolean("printStrategies", false);
                Log.info("FSICFREngine", "Printing normalized strategies: " + printStrategies);

                // TODO Issue (302): Change referee to allow for different determinizers per
                // player.
                // Needed to change memory parameter
            } catch (IOException e) {
                Log.error("FSICFREngine", "Unable to load enhancement file:\t" + e.getMessage());
            }
        }

        rand = new Random();
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        currentState.setFirstPlayer(((DudoInitGameMessage) a).getFirstPlayer());

        try {
            fsicfr = new FSICFR<>(
                    currentState,
                    logic,
                    logic,
                    new DudoFinalEvaluator(),
                    new DudoActionSensor());

            Log.info("DudoFSICFREngine", "Training from scratch");
            fsicfr.solve(numIterations);
        } catch (StateNotFoundException | CycleException ex) {
            Log.error("DudoFSICFREngine", "Unable to create FSICFR object");
            ex.printStackTrace();
        }

        normStrategies = fsicfr.getNormalizedStrategy();

        if (printStrategies) {
            System.out.println(getPrettyStrategies(normStrategies, logic, currentState, this.playerID));
            Log.info("DudoFSICFREngine", "Done printing strategies");
        }
    }

    @Override
    public String engineName() {
        return "DudoCFSICFREngine";
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Map<Action, Double> strat = normStrategies.get(currentState);

        Action action;

        try {
            action = ClassUtils.chooseFromDistribution(strat, rand);
        } catch (IncorrectlyNormalizedDistributionException e) {
            Log.error("DudoFSICFREngine", "Failed to sample action from strategy. Player first action");
            action = (Action) strat.keySet().toArray()[0];
        }

        Log.info("DudoFSICFREngine", "Playing move: " + currentState.getPrettyClaim(
                ((DiscreteAction) action).getActionNumber()));

        return new PlayActionMessage(action);
    }
}

package za.ac.sun.cs.ingenious.core.network.lobby;

import com.esotericsoftware.minlog.Log;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;

/**
 * Class to hold and manage all open lobbies, client handlers and started
 * threads.
 * 
 * @author Stephan Tietz
 *
 */
public class LobbyManager {

	private final Map<String, LobbyHost> openLobbies;
	private final Map<String, ClientHandler> clientHandlers;
	private final List<Thread> startedReferees;

	/**
	 * Creates a new lobby manager without any open lobbies.
	 */
	public LobbyManager() {
		openLobbies = new ConcurrentHashMap<String, LobbyHost>();
		clientHandlers = new ConcurrentHashMap<String, ClientHandler>();
		startedReferees = new CopyOnWriteArrayList<Thread>();
	}

	/**
	 * @return List of all referee threads started by this game server.
	 */
	public synchronized List<Thread> getStartedReferees() {
		return startedReferees;
	}

	/**
	 * @param t Thread that contains a newly started referee.
	 */
	public synchronized void registerStartedReferee(Thread t) {
		startedReferees.add(t);
	}

	/**
	 * @param name of the lobby for which settings are requested.
	 * @return The match settings of the specified lobby.
	 */
	public synchronized MatchSetting getMatchSettings(String name) {
		return openLobbies.get(name).getMatchSetting();
	}

	/**
	 * @param lobbyName
	 * @return A LobbyHost for the specified lobby.
	 */
	public synchronized LobbyHost getLobby(String lobbyName) {
		return openLobbies.get(lobbyName);
	}

	/**
	 * @return The names of the open lobbies.
	 */
	public synchronized String[] getLobbyNames() {
		return openLobbies.keySet().toArray(new String[openLobbies.size()]);
	}

	/**
	 * Add a new lobby.
	 * 
	 * @param lobbyName   name of the lobby. Must be unique.
	 * @param lobbyHoster
	 */
	public synchronized boolean addLobby(String lobbyName, LobbyHost lobbyHoster) {
		if (!openLobbies.containsKey(lobbyName)) {
			openLobbies.put(lobbyName, lobbyHoster);
			return true;
		} else {
			Log.error("Lobbyname already taken.");
			return false;
		}
	}

	/**
	 * @return True if no lobby with the given name exists yet.
	 */
	public synchronized boolean isLobbyNameAvailable(String name) {
		return !openLobbies.containsKey(name);
	}

	/**
	 * @return String[] a list of names for all clientHandlers currently connected
	 *         to this GameServer
	 */
	public synchronized String[] getPlayerNames() {
		return clientHandlers.keySet().toArray(new String[clientHandlers.size()]);
	}

	/**
	 * Store a connection to a player in the clientHandlers HashMap
	 */
	public synchronized void addClientHandler(String playerName, ClientHandler handler) {
		Log.info("Added player name " + playerName);
		clientHandlers.put(playerName, handler);
	}

	/**
	 * remove a previously stored ClientHandler
	 */
	public synchronized void removeClientHandler(String playerName) {
		Log.info("Removing player name " + playerName);
		if (clientHandlers.remove(playerName) == null) {
			Log.error("Player name " + playerName + " not found in clientHandlers. Could not be removed.");
		}
	}

	/**
	 * check the clientHandlers hashmap for a previous connection
	 * using the same name.
	 * 
	 * @param playerName
	 * @return boolean
	 */
	public synchronized boolean checkDuplicateClientHandlerName(String playerName) {
		return clientHandlers.containsKey(playerName);
	}

	/**
	 * Removes a lobby from the list of open lobbies.
	 * 
	 * @param name the lobby to be removed from the open lobbies list.
	 */
	public synchronized void unregisterLobby(String name) {
		Log.info("Removed lobby with name " + name);
		openLobbies.remove(name);
	}
}

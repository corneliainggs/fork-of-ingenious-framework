package za.ac.sun.cs.ingenious.games.diceGames.dudo;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.network.DudoInitGameMessage;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.network.DudoMatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.referee.GeneralReferee;
import za.ac.sun.cs.ingenious.games.diceGames.actions.DiceRollAction;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;

public class DudoReferee extends GeneralReferee<DudoGameState, DudoLogic, DudoFinalEvaluator> {

    public DudoReferee(MatchSetting match, PlayerRepresentation[] players)
            throws MissingSettingException, IncorrectSettingTypeException {

        super(match, players,
                new DudoGameState(
                        2,
                        match.getSettingAsInt("num_sides", 6),
                        match.getSettingAsInt("num_dice", 2)),
                new DudoLogic(),
                new DudoFinalEvaluator(),
                new DudoActionSensor());
    }

    @Override
    protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
        return new DudoInitGameMessage(this.matchSettings, currentState.getFirstPlayer());
    }

    @Override
    protected MatchResetMessage createMatchResetMessage(PlayerRepresentation player) {
        return new DudoMatchResetMessage(eval.getScore(currentState));
    }

    @Override
    protected void updateServerState(PlayActionMessage m) {
        if (m.getAction() instanceof DiceRollAction) {
            Log.info(sensor.fromPointOfView(m.getAction(), currentState, -1));
        } else {
            DiscreteAction action = (DiscreteAction) m.getAction();
            Log.info("Player " + m.getAction().getPlayerID() + " claimed: "
                    + currentState.getPrettyClaim(action.getActionNumber()));
        }

        logic.makeMove(currentState, sensor.fromPointOfView(m.getAction(), currentState, -1)); // update server
                                                                                               // gamestate
    }

    @Override
    protected void reactToInvalidAction(int player, PlayActionMessage m) {

        if (m.getAction() instanceof DiceRollAction) {
            DiceRollAction action = (DiceRollAction) m.getAction();
            Log.info("Player " + player + " made an illegal dice roll: " + action.getRoll());
        } else if (m.getAction() instanceof DiscreteAction) {
            DiscreteAction action = (DiscreteAction) m.getAction();
            Log.info("Player " + player + " made an illegal claim: "
                    + currentState.getPrettyClaim(action.getActionNumber()));
        } else {
            Log.info("Player " + player + " made an illegal action: " + m.getAction());
        }
    }
}
package za.ac.sun.cs.ingenious.core;

import java.util.List;
import java.util.Set;
import java.util.Map;

import za.ac.sun.cs.ingenious.core.util.selector.ActionSelector;
import za.ac.sun.cs.ingenious.core.util.selector.UniformActionSelector;

/**
 * Common members and operations of any game. Every game added to the framework
 * should have a class implementing this. In your implementation, make sure to
 * separate game state from game logic that operates on states.
 * 
 * @author Michael Krause
 * @param <S> The type of GameState that this logic operates on.
 */
public interface GameLogic<S extends GameState> {

	ActionSelector selector = UniformActionSelector.getInstance();

	/**
	 * Checks whether the given move can be played in the given state
	 */
	public boolean validMove(S fromState, Move move);

	/**
	 * Applies the given move to the given state; returns false if the move could not be applied
	 */
	public boolean makeMove(S fromState, Move move);

	/**
	 * Reverts the given move from the given state; the move need not be that last one that was applied
	 */
	public default void undoMove(S fromState, Move move) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Return a list of actions that the given player can execute in the given state
	 */
	public List<Action> generateActions(S fromState, int forPlayerID);

	/**
	 * Return true if the given game configuration is terminal
	 */
	public boolean isTerminal(S state);

	/**
	 * Returns a set of player IDs containing all players who can act in the given state
	 */
	public Set<Integer> getCurrentPlayersToAct(S fromState);

	/**
	 * Returns whether the game requires there to be stochastic events played by a stochastic player
	 */
	public default boolean requiresStochasticPlayer() {
		return false;
	}

	public default Map<Action, Double> generateStochasticActions(S fromState) {
		throw new UnsupportedOperationException();
	}
}
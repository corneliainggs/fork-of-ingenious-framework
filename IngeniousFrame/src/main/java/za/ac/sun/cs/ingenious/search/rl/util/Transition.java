package za.ac.sun.cs.ingenious.search.rl.util;

import za.ac.sun.cs.ingenious.core.Action;

/**
 * Class that stores a transition of (phi(state), action, reward, phi(nextState)).
 *
 * @author Steffen Jacobs
 */
public class Transition {
    private double[][][] transformedState;
    private Action action;
    private double reward;
    private double[][][] transformedNextState;

    public Transition(double[][][] transformedState, Action action, double reward, double[][][] transformedNextState) {
        this.transformedState = transformedState;
        this.action = action;
        this.reward = reward;
        this.transformedNextState = transformedNextState;
    }

    public Transition(double[][] transformedState, Action action, double reward, double[][] transformedNextState) {
        this.transformedState = new double[][][]{transformedState};
        this.action = action;
        this.reward = reward;
        if (transformedNextState == null) {
            this.transformedNextState = null;
        } else {
            this.transformedNextState = new double[][][]{transformedNextState};
        }
    }

    public Transition(double[] transformedState, Action action, double reward, double[] transformedNextState) {
        this.transformedState = new double[][][]{new double[][]{transformedState}};
        this.action = action;
        this.reward = reward;

        if (transformedNextState == null) {
            this.transformedNextState = null;
        } else {
            this.transformedNextState = new double[][][]{new double[][]{transformedNextState}};
        }
    }

    public double[][][] getTransformedState() {
        return transformedState;
    }

    public void setTransformedState(double[][][] transformedState) {
        this.transformedState = transformedState;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public double getReward() {
        return reward;
    }

    public void setReward(double reward) {
        this.reward = reward;
    }

    public double[][][] getTransformedNextState() {
        return transformedNextState;
    }

    public void setTransformedNextState(double[][][] transformedNextState) {
        this.transformedNextState = transformedNextState;
    }
}

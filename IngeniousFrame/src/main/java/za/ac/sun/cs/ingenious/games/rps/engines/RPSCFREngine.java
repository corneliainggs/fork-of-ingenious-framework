package za.ac.sun.cs.ingenious.games.rps.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.exception.dag.StateNotFoundException;
import za.ac.sun.cs.ingenious.core.exception.probability.IncorrectlyNormalizedDistributionException;
import za.ac.sun.cs.ingenious.games.rps.RPSGameState;
import za.ac.sun.cs.ingenious.games.rps.RPSLogic;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.ClassUtils;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.move.UnobservedMove;
import za.ac.sun.cs.ingenious.search.cfr.CFR;

import java.util.Map;
import java.util.Random;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class RPSCFREngine extends Engine {
    private final int numIterations = 10000;

    private final RPSLogic logic;
    private final RPSGameState currentState;
    private final Map<RPSGameState, Map<Action, Double>> aveStrategies;
    private final Random rand;

    /**
     * @param toServer An established connection to the GameServer
     */
    public RPSCFREngine(EngineToServerConnection toServer) {
        super(toServer);

        logic = new RPSLogic();
        currentState = new RPSGameState(2);
        rand = new Random();

        CFR<RPSGameState> cfr = new CFR<RPSGameState>(
                currentState,
                logic,
                logic,
                logic,
                logic);

        try {
            cfr.solve(numIterations);
        } catch (StateNotFoundException ex) {
            Log.error("RPSCFREngine", "Failed to train CFR\n" + ex);
        }

        aveStrategies = cfr.getNormalizedStrategy();
        Log.info(getPrettyStrategies(aveStrategies, logic, playerID));
    }

    public static String getPrettyStrategies(Map<RPSGameState, Map<Action, Double>> aveStrategies, RPSLogic logic,
            int playerID) {
        StringBuilder stringBuilder = new StringBuilder();

        for (RPSGameState state : aveStrategies.keySet()) {
            if (aveStrategies.get(state).keySet().isEmpty()
                    || (int) (logic.getCurrentPlayersToAct(state).toArray())[0] != playerID) {
                continue;
            }

            stringBuilder.append(state.getPretty());
            stringBuilder.append("=================\nStrategy Profile:\n");
            for (Action action : aveStrategies.get(state).keySet()) {
                stringBuilder.append(RPSLogic.getPrettyRPSAction((DiscreteAction) action))
                        .append(" : ")
                        .append(aveStrategies.get(state).get(action))
                        .append("\n");
            }
            stringBuilder.append("=================\n\n");
        }

        return stringBuilder.toString();
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String engineName() {
        return "RPS CFR Engine";
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        Log.info("Game over");
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        Log.info("Player " + a.getMove().getPlayerID() + " played " +
                ((a.getMove() instanceof UnobservedMove) ? "a hidden move"
                        : RPSLogic.getPrettyRPSAction(((DiscreteAction) a.getMove()))));

        logic.makeMove(currentState, a.getMove());
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Map<Action, Double> strat = aveStrategies.get(currentState);

        Action action;
        try {
            action = ClassUtils.chooseFromDistribution(strat, rand);
        } catch (IncorrectlyNormalizedDistributionException ex) {
            action = (Action) strat.keySet().toArray()[0];
        }

        return new PlayActionMessage(action);
    }

    /**
     * Creates a strategy profile with an initial cumulative strategy table. For
     * demonstrating the
     * convergence of the CFR algorithm.
     * 
     * @return
     */
    public static Map<RPSGameState, Map<Action, Double>> getRandomStrategy(RPSLogic logic, RPSGameState state,
            Random rand) {
        Map<RPSGameState, Map<Action, Double>> strategy = new HashMap<>();

        List<RPSGameState> queue = new LinkedList<>();
        queue.add(state);

        while (!queue.isEmpty()) {
            RPSGameState firstState = queue.remove(0);

            double[] strat = new double[3];
            for (int i = 0; i < 3; i++) {
                strat[i] = rand.nextDouble();
            }

            initTable(logic, strat, firstState, strategy);
            List<Action> actions = logic.generateActions(firstState,
                    (int) logic.getCurrentPlayersToAct(firstState).toArray()[0]);

            for (Action a : actions) {
                RPSGameState newState = firstState.deepCopy();
                logic.makeMove(newState, logic.fromPointOfView(a, newState, -1));
                queue.add(newState);
            }
        }
        return strategy;
    }

    /**
     * Initializes the strategy table for an information set. Copied logic from
     * {@link CFR}
     */
    public static void initTable(RPSLogic logic, double[] cumStrat, RPSGameState state,
            Map<RPSGameState, Map<Action, Double>> strategy) {
        int activePlayerID = (int) logic.getCurrentPlayersToAct(state).toArray()[0];
        RPSGameState infoSet = logic.observeState(state, activePlayerID);

        if (strategy.containsKey(infoSet) || logic.isTerminal(state)) {
            return;
        }

        List<Action> actions = logic.generateActions(infoSet, activePlayerID);
        Map<Action, Double> strategyTable = new HashMap<>();
        for (int i = 0; i < actions.size(); i++) {
            strategyTable.put(actions.get(i), cumStrat[i]);
        }

        strategy.put(infoSet, strategyTable);
    }
}
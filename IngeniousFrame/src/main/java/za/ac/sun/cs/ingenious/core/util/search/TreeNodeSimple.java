package za.ac.sun.cs.ingenious.core.util.search;

import za.ac.sun.cs.ingenious.core.GameState;
import java.util.List;

public class TreeNodeSimple<S extends GameState> implements TreeNode<S, TreeNodeSimple<S>, TreeNodeSimple<S>> {

	SearchNode<S> searchNode;
	TreeNodeSimple<S> parent;
	List<TreeNodeSimple<S>> children;

	public TreeNodeSimple(S state, TreeNodeSimple<S> parent, List<TreeNodeSimple<S>> children) {
		searchNode = SearchNodeSimple.newInstance(state);
		this.parent = parent;
		this.children = children;
	}

	// SearchNode interface methods:
	public S getState(boolean getDefensiveCopy) {
		return searchNode.getState(getDefensiveCopy);
	}

	public double getValue() {
		return searchNode.getValue();
	}

	public void addValue(double add) {
		searchNode.addValue(add);
	}

	// TODO: change?
	public String toString() {
		String parentString = (this.getParent() == null) ? "null" : this.getParent().getState(false).toString();
		return ("TreeNodeNode:\n\tState = " + getState(false).toString() + "\n\tvalue = " + getValue() +
				"\n\tParent (state) = " + parentString + "\n\tChildren = " + getChildren());
	}

	// TreeSearchNode interface methods:
	public TreeNodeSimple<S> getParent() {
		return this.parent;
	}

	public void setParent(TreeNodeSimple<S> parent) {
		this.parent = parent;
	}

	public List<TreeNodeSimple<S>> getChildren() {
		return this.children;
	}

	public void setChildren(List<TreeNodeSimple<S>> children) {
		this.children = children;
	}

	public void addChild(TreeNodeSimple<S> child) {
		children.add(child);
	}

	public void addChildren(List<TreeNodeSimple<S>> newChildren) {
		for (TreeNodeSimple<S> child : newChildren) {
			children.add(child);
		}
	}

}

package za.ac.sun.cs.ingenious.search.mcts.legacy;

import za.ac.sun.cs.ingenious.core.GameState;

/**
 * A playout policy describes how the simulations in MCTS are run once the best
 * node in the search TreeEngine has been found and expanded. A standard
 * implementation is
 * {@link RandomPolicy}.
 * 
 * @author steve
 */
public interface PlayoutPolicy<S extends GameState> {

	/**
	 * The result of a playout is the payoff vector for all players + the total
	 * number of actions that
	 * were taken from the start state of the playout till the terminal state.
	 */
	public final class PlayoutResult {
		private final double[] payoffs;
		private final int numSteps;

		public PlayoutResult(double[] payoffs, int numSteps) {
			this.payoffs = payoffs;
			this.numSteps = numSteps;
		}

		public double[] getPayoffs() {
			return payoffs;
		}

		public int getNumSteps() {
			return numSteps;
		}
	}

	/**
	 * Performs a playout.
	 * 
	 * @param node The game state from which to start the playout.
	 * @return Scores per player after the playout + number of steps from state
	 *         until terminal state was reached.
	 */
	public PlayoutResult playout(S state);

}

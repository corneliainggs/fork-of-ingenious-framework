# Ingenious Reinforcement Learning

Reinforcement learning algorithms and domains added to the framework.

This readme serves as informal project documentation to help getting the RL algorithms up and running. Notes are also 
provided to hopefully help with future work. Example level files for MDP and Gridworld environments are also provided.

## Getting started

### Python environment
Ensure that you are in the `IngeniousFrame` directory. Create a virtual environment:
```
python3 -m venv ./src/main/python/venv && cd ./src/main/python/
```
Active `venv`
```
source venv/bin/activate
```
Ensure you're in the virtual environment:
```
which python
```
Upgrade `pip` and install the required packages:
```
pip install -U pip
pip install numpy==1.19.2
pip install matplotlib==3.2.2
pip install torch==1.7.0
pip install tensorboard
```
   
### Usage 

#### General Notes
* Clear any `.alg` files between runs if you want to remove the existing agents.
* DQN agents can be found in `python/src/trained_models`. To use from these it is necessary to copy the model over to
`python/src/backup_networks` with the appropriate identifier. This identifier is typically `<engine_name>_<player_id>`.
* To view Tensorboard data, the browser should automatically be opened. Ensure that 'Scalars' is chosen in the
top-right menu. It may take a while before data is shown here, due to training not beginning immediately while the replay
buffer fills up.

#### MDP
The MDP level to be run should be specified in `./scripts/mdp/mdp.json`.

Switch to MDP scripts directory:
```
cd ./scripts/mdp
```

Start server:
```
./start_server.sh
```

##### Value Iteration
```
./run_value_iteration.sh
```

##### Policy Iteration
```
./run_policy_iteration.sh
```

#### Gridworld
The Gridworld level to be run should be specified in `./scripts/gridWorld/gridWorld.json`.
Switch to Gridworld scripts directory:
```
cd ./scripts/gridWorld
```

Start server:
```
./start_server.sh
```

##### Tabular Q-Learning

Before training, be sure to remove existing agents: `./scripts/gridWorld/*.alg`

Train an agent with the following:
```
./train_tabular_qlearning.sh
```

The agent can then be run with the following:
```
./run_tabular_qlearning.sh
```

##### DQN

Before training, ensure that `src/python/backup_networks` is clear of saved models if you do not want to continue 
training an existing agent. Not doing so much result in errors if the weights of an existing network are loaded for a
different Gridworld (due to different board dimensions).

Train an agent with the following:
```
./train_dqn.sh
```

The agent can then be run with the following:
```
./run_dqn.sh
```


#### MNK
Specify the MNK settings in `./scripts/mnk/mnk.json`

Switch to MNK scripts directory:
```
cd ./scripts/mnk
```

Start server:
```
./start_server.sh
```

##### Tabular Q-Learning

Before training, be sure to remove existing agents: `./scripts/mnk/*.alg`

Train an agent with the following:
```
./train_tabular_qlearning.sh
```

The agent can then be run with the following:
```
./run_tabular_qlearning.sh
```

And you can play against it by running:
```
./run_human_player.sh
```

##### DQN

Before training, ensure that `src/python/backup_networks` is clear of saved models if you do not want to continue 
training an existing agent.

Train an agent against a random MNK agent with the following:
```
./train_dqn.sh
```

Or with self play with:
```
./train_dqn_self_play.sh
```

The agent can then be run with the following:
```
./run_dqn.sh
```

And you can play against it by running:
```
./run_human_player.sh
```

#### Snake
Switch to Snake scripts directory:
```
cd ./scripts/snake
```

Start server:
```
./start_server.sh
```

Train an agent:
```
./train_dqn.sh
```

The agent can then be run with the following:
```
./run_dqn.sh
```

To use the trained agent, it can be copied over using the following example:
```
cd ./scripts/snake
cp ../../src/main/python/src/trained_models/ExternalDQNBackup_36_9_9.pt ../../src/main/python/src/backup_networks/SnakeExternalDQNEngine_0.pt
```
Note that this model requires a GPU compatible with CUDA.
   
## Adding domains / algorithms / engines

### Training from pixels
Any new games added to be train with DQN using pixels can refer to Snake to see how it was handled there. It's a bit
hacky, so hopefully a nice pattern for this can be found in the future.

An image buffer is maintained for each `SnakeState` instance, containing the four latest frames leading up to it i.e.
images for the previous three states, and an image for the current state. This buffer size can be be changed with a
constant. Note that these images objects are not duplicated across states, so the same `BufferedImage` object references 
are used across multiple state instances. Because of this, be sure not to mutate the `BufferedImage` objects for
whatever reason.

At various points in the game, the `SnakeEngine` will trigger updates to the image buffer of the current state by 
invoking helper methods in the `SnakePanel`. These helper methods in `SnakePanel` could probably be moved somewhere 
else.

### RL Instrumentation
Any new RL algorithms could benefit by extending `ReinforcementLearningInstrumentation`. This class needs to be
notified of the following events:
* When the agent receives a new reward value, the `addReward` method should be invoked.
* When an action is chosen by the agent, invoke the `incrementStep` method.
* When a score is observed in the game (if applicable), `notifyScore` may optionally be invoked.
* When an episode ends, invoke `endEpisode`.
* When the game terminates, invoke `close` to close the `PrintWriter` object used for logging.

These can be done either by the algorithm or an engine, it's quite problem dependent. Try to hook as many of these up
with the algorithm, if possible.

For an example, see `TabularSarsa` together with `GridWorldTabularSarsaEngine`. Also have a look at 
`SnakeExternalDQNEngine` to see how some of the instrumentation parameters can be changed instead of using defaults.

### Rewards
When adding rewards to a domain, it is highly recommended to create an implementation of `GameRewardEvaluator<S>`.
Instantiate this evaluator in your referee, and override the `calculatePlayerRewards` method to rather invoke the
reward evaluator. See `SnakeReferee` and `SnakeRewardEvaluator` as an example.

### Adversarial domains
`MNKTabularQLearningEngine` is probably the best example for RL in an adversarial setting. 

The main difference to a single agent domain is that the agent should be trained after the opposing player has chosen a 
move to ensure that the agent learns from the 'consequences' of its choices. If this weren't the case, the agent would 
never realize it has even lost a game.

This means that, instead of training when a reward is received, the agent instead trains before choosing its action in
the next turn (since the opponents' moves for the last round will have been observed by this point).
The agent also trains when the episode terminates (since the agent won't choose another action in the episode).

### Basic UI examples
Gridworld and Snake use a basic UI that could be a useful starting point to adding a UI in other domains.

## Other

### MDP File
```json5
{
    "initialState": 2,  // Starting state.
    "states": 5,        // Defines the state numbers in the MDP e.g. {0, 1, 2, 3, 4}.
    "actions": 2,       // Defines the action numbers in the MDP e.g. {0, 1}.
    "terminalStates": [0, 4], // State numbers of terminal states.
    "reward": [         // Reward for reaching S' from S, indexed as R[S][S'].
        [-5, -1, -1, -1, 5],
        [-5, -1, -1, -1, 5],
        [-5, -1, -1, -1, 5],
        [-5, -1, -1, -1, 5],
        [-5, -1, -1, -1, 5]
    ],
    "validActions": [   // Valid action numbers for each state, indexed as A[S].
        [],
        [0, 1],
        [0, 1],
        [0, 1],
        []
    ],
    "dynamics": [       // Distributions of resulting states, indexed as P[S][A][S'].
        [],
        [
            [1.0, 0, 0, 0, 0],
            [0, 0, 1.0, 0, 0]
        ],
        [
            [0, 1.0, 0, 0, 0],
            [0, 0, 0, 1.0, 0]
        ],
        [
            [0, 0, 1.0, 0, 0],
            [0, 0, 0, 0, 1.0]
        ],
        []
    ]
}
```

### Gridworld File

See documented methods in `GridWorldLevelParser`.

```
Rewards:
W = 1.0 1
L = -1.0 1
Wind:
Column 1 1 1
Row 3 1 1
Board:
-0.1 0
X X X X X
X L   W X
X   S   X
X       X
X X X X X
```
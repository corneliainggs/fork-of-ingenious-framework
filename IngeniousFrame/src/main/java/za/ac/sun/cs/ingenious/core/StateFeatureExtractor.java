package za.ac.sun.cs.ingenious.core;

/**
 * Interface for extracting features or channels from a state. Used for constructing features used by linear function
 * approximation in reinforcement learning algorithms, or for constructing channels used by nonlinear function
 * approximation such as DQN.
 *
 * @author Steffen Jacobs
 */
public interface StateFeatureExtractor<T extends GameState, L extends GameLogic<T>> {
    /**
     * Build an array of features for the given state, using the provided game logic.
     *
     * @param state State for which to build features.
     * @param logic Logic class that contains the dynamics of the game.
     * @return An array of values representing the set of features.
     */
    default double[] buildFeatures(T state, L logic) {
        throw new UnsupportedOperationException();
    }

    /**
     * Build an array of features for the given state-action pair.
     *
     * @param state State for which to build features.
     * @param action Action applied to state.
     * @param logic Logic class that contains the dynamics of the game.
     * @return An array of values representing the set of features.
     */
    default double[] buildFeatures(T state, Action action, L logic) {
        throw new UnsupportedOperationException();
    }

    /**
     * Build a matrix of size (channels, height, width) for the given state.
     *
     * @param state State for which to build channels.
     * @param logic Logic class that contains the dynamics of the game.
     * @return A matrix containing the channels, each representing some aspect of the game state.
     */
    default double[][][] buildTensor(T state, L logic) {
        throw new UnsupportedOperationException();
    }
}

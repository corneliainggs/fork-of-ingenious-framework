package za.ac.sun.cs.ingenious.games.othello.gamestate;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedRectangleBoard;

/**
 * The GameState implementation for the Othello board game.
 *
 * @author Rudolf Stander
 */
public class OthelloBoard extends TurnBasedRectangleBoard {

	/* Definitions of board block states and player IDs. DO NOT CHANGE */
	public static final byte EMPTY = -1;
	public static final byte BLACK = 0;
	public static final byte WHITE = 1;

	public static final String EMPTY_STRING = "*";
	public static final String BLACK_STRING = "0";
	public static final String WHITE_STRING = "1";

	protected int blackScore;
	protected int whiteScore;

	/**
	 * Constructs a new OthelloBoard object and sets up the board with specified
	 * dimensions.
	 *
	 * @param size the board's height, should be a multiple of 2
	 */
	public OthelloBoard(int size) {
		super(size, size, BLACK, 2);
		this.boardSetup(size, size);
	}

	/**
	 * Constructs a new OthelloBoard object and sets up the board with specified
	 * dimensions.
	 * 
	 * @param boardWidth  The width of the board
	 * @param boardHeight The height of the board
	 */
	public OthelloBoard(int boardWidth, int boardHeight) {
		super(boardWidth, boardHeight, BLACK, 2);
		this.boardSetup(boardWidth, boardHeight);
	}

	private void boardSetup(int boardWidth, int boardHeight) {
		for (int i = 0; i < this.getNumCells(); i++) {
			this.board[i] = EMPTY;
		}

		this.board[xyToIndex(boardHeight / 2 - 1, boardWidth / 2 - 1)] = WHITE;
		this.board[xyToIndex(boardHeight / 2, boardWidth / 2)] = WHITE;
		this.board[xyToIndex(boardHeight / 2 - 1, boardWidth / 2)] = BLACK;
		this.board[xyToIndex(boardHeight / 2, boardWidth / 2 - 1)] = BLACK;

		/* Initialize the player scores */
		this.blackScore = 2;
		this.whiteScore = 2;
	}

	/**
	 * Returns a new OthelloBoard object based on the match settings.
	 * 
	 * @param matchSetting The match settings to use
	 * @return A new OthelloBoard object
	 * @throws IncorrectSettingTypeException If the match settings have incorrect
	 *                                       types
	 */
	public static OthelloBoard getNewBoardFromMatchSetting(MatchSetting matchSetting)
			throws IncorrectSettingTypeException {
		try {
			int boardSize = matchSetting.getSettingAsInt("boardSize");
			return new OthelloBoard(boardSize);
		} catch (MissingSettingException e) {
			try {
				int boardWidth = matchSetting.getSettingAsInt("boardWidth");
				int boardHeight = matchSetting.getSettingAsInt("boardHeight");
				return new OthelloBoard(boardWidth, boardHeight);
			} catch (MissingSettingException e1) {
				Log.error("Could not find [boardSize] or [boardWidth and boardHeight] in match settings");
			}
		}
		throw new RuntimeException("Could not create OthelloBoard from match settings");
	}

	/**
	 * Constructs a new OthelloBoard object that is a carbon copy of the given
	 * OthelloBoard.
	 *
	 * @param copy the board to copy
	 */
	public OthelloBoard(OthelloBoard copy) {
		super(copy);
		this.blackScore = copy.blackScore;
		this.whiteScore = copy.whiteScore;
	}

	@Override
	public OthelloBoard deepCopy() {
		return new OthelloBoard(this);
	}

	/**
	 * Returns the board's current grid.
	 *
	 * @return the board's grid
	 */
	public byte[] getGrid() {
		return board;
	}

	/**
	 * Returns a copy of the board's current grid.
	 *
	 * @return a copy of the board's grid
	 */
	public byte[] getGridCopy() {
		byte[] copy = new byte[board.length];
		System.arraycopy(board, 0, copy, 0, board.length);
		return copy;
	}

	/**
	 * Returns the number (ID) of the player who will play next.
	 *
	 * @return the next player's number (ID)
	 */
	public int getCurrentPlayer() {
		return this.nextMovePlayerID;
	}

	/**
	 * Updates the nextMovePlayerID to the next player's ID
	 */
	public void updateCurrentPlayer() {
		nextMovePlayerID = nextMovePlayerID == BLACK ? WHITE : BLACK;
	}

	/**
	 * Returns the score of each of the players.
	 *
	 * @return an array with the players' scores as entries
	 */
	public double[] getScore() {
		double scores[] = new double[2];
		scores[0] = blackScore;
		scores[1] = whiteScore;

		return scores;
	}

	/**
	 * Returns a string representation of the current board state
	 *
	 * @return a string representation of the current board state
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		StringBuilder singleColumnDashedLine = new StringBuilder();
		StringBuilder dashedLine = new StringBuilder("\n");
		int columnCharWidth = 1 + (int) Math.log10(getBoardWidth() - 1);
		String formattedEmpty = String.format("%" + columnCharWidth + "s", EMPTY_STRING);
		String formattedBlack = String.format("%" + columnCharWidth + "s", BLACK_STRING);
		String formattedWhite = String.format("%" + columnCharWidth + "s", WHITE_STRING);

		/* Generate the scores */
		sb.append("Black Score: ");
		sb.append(blackScore);
		sb.append("\n");
		sb.append("White Score: ");
		sb.append(whiteScore);
		sb.append("\n");

		/* Generate next player */
		sb.append("Next player: ");

		if (nextMovePlayerID == BLACK) {
			sb.append("black\n");
		} else {
			sb.append("white\n");
		}

		/* Generate the board */
		sb.append("Board:\n");

		/* Add spacer before line of column numbers */
		for (int i = 0; i < columnCharWidth; i++) {
			sb.append(" ");
			singleColumnDashedLine.append("-");
		}

		sb.append("|");

		/* Print column numbers */
		for (int i = 0; i < this.getBoardWidth(); i++) {
			sb.append(String.format("%" + columnCharWidth + "d", i));
			dashedLine.append(singleColumnDashedLine);
		}

		dashedLine.append(singleColumnDashedLine);
		dashedLine.append("-");
		sb.append(dashedLine);
		sb.append("\n");

		for (int i = 0; i < this.getBoardWidth(); i++) {
			sb.append(String.format("%" + columnCharWidth + "d", i));
			sb.append("|");

			for (int j = 0; j < this.getBoardHeight(); j++) {
				if (board[this.xyToIndex(j, i)] == EMPTY) {
					sb.append(formattedEmpty);
				} else if (board[this.xyToIndex(j, i)] == BLACK) {
					sb.append(formattedBlack);
				} else {
					sb.append(formattedWhite);
				}
			}

			sb.append("\n");
		}

		return sb.toString();
	}

	/**
	 * Prints a representation of the current board state.
	 */
	@Override
	public void printPretty() {
		Log.info("\n" + toString());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + blackScore;
		result = prime * result + whiteScore;
		return result;
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}

		if (!super.equals(that)) {
			return false;
		}

		if (getClass() != that.getClass()) {
			return false;
		}

		OthelloBoard thatBoard = (OthelloBoard) that;

		if (blackScore != thatBoard.blackScore) {
			return false;
		}

		if (whiteScore != thatBoard.whiteScore) {
			return false;
		}

		return true;
	}
}

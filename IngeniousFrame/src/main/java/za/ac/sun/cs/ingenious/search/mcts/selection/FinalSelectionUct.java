package za.ac.sun.cs.ingenious.search.mcts.selection;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedRectangleBoard;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

import java.util.List;

/**
 * A standard implementation of the TreeEngine selection strategy
 * {@link TreeSelection},
 * that calculates UCT values to make a decision of which child node to select.
 * This strategy is specifically for TreeEngine selection and makes use of read
 * locks
 * when viewing node information that is used in the calculations.
 *
 * @author Karen Laubscher
 *
 * @param <S> The game state type.
 * @param <N> The type of the mcts node.
 */
public class FinalSelectionUct<S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, ?>>
		implements TreeSelectionFinal<N> {

	private GameLogic<S> logic;

	/**
	 * Constructor to create a TreeEngine UCT selection object, since the constant C
	 * is not supplied, the default value for C is assigned as square root of 2.
	 *
	 * @param logic The game logic
	 */
	private FinalSelectionUct(GameLogic<S> logic) {
		this.logic = logic;
	}

	/**
	 * The method that decides which child node to traverse to next, based on
	 * calculating the UCT value for each child and then selecting the child
	 * with the highest UCT value. Since the TreeEngine structure is shared in
	 * TreeEngine
	 * parallelisation, nodes are (read) locked when information for
	 * the calculations are viewed.
	 *
	 * @param node The current node whose children nodes are considered for the
	 *             next node to traverse to.
	 *
	 * @return The selected child node with the highest UCT value.
	 */
	public N select(N node, ZobristHashing zobrist) {

		if (logic.isTerminal(node.getState(false))) {
			Log.error("FinalSelection", "Node is terminal. Cannot select child of terminal node.");
			return null;
		}

		List<N> children = node.getChildren();

		if (children.size() == 1) {
			return children.get(0);
		}

		N highestUctChild = null;
		double tempVal;
		node.getState(false).printPretty();
		double highestUct = Double.NEGATIVE_INFINITY;
		for (N child : children) {
			if (zobrist != null) {
				long hashForChildBoard = zobrist.hashBoard((TurnBasedRectangleBoard) child.getState(false));
				if (!zobrist.hashes.containsKey(hashForChildBoard)) {
					// choose robust child
					tempVal = child.getVisitCount();
					if (Double.compare(tempVal, highestUct) > 0) {
						highestUctChild = child;
						highestUct = tempVal;
					}
				}
			} else {
				// choose robust child
				tempVal = child.getVisitCount();
				if (Double.compare(tempVal, highestUct) > 0) {
					highestUctChild = child;
					highestUct = tempVal;
				}
			}
		}
		if (highestUctChild == null) {
			Log.warn("FinalSelection", "Highest UCT child is null");
			if (!children.isEmpty()) {
				StringBuilder sb = new StringBuilder();
				sb.append("Children list is not empty. " + children.size()
						+ " children found. This is likely due to move-loop that occurred. Returning random child.");
				for (N child : children) {
					sb.append("\n");
					sb.append(child.getVisitCount());
					long hashForChildBoard = zobrist.hashBoard((TurnBasedRectangleBoard) child.getState(false));
					if (zobrist.hashes.containsKey(hashForChildBoard)) {
						sb.append(" (hash found)");
					} else {
						sb.append(" (hash not found)");
					}
				}
				Log.warn("FinalSelection", sb.toString());
				return children.get((int) (Math.random() * children.size()));
			} else {
				Log.error("FinalSelection",
						"Children list is empty. Cannot select child of empty list." + node.toString());
			}
		}
		return highestUctChild;
	}

	/**
	 * A static factory method, which creates a TreeEngine UCT selection object that
	 * uses the default value of square root 2 for the constant C in the UCTS
	 * calculation.
	 *
	 * This factory method is also a generic method, which uses Java type
	 * inferencing to encapsulate the complex generic type capturing required
	 * by the TreeEngine UCT selection constructor.
	 *
	 * @param logic The game logic
	 */
	public static <SS extends GameState, NN extends MctsNodeTreeParallelInterface<SS, NN, ?>> TreeSelectionFinal<NN> newFinalSelectionUct(
			GameLogic<SS> logic) {
		return new FinalSelectionUct<SS, NN>(logic);
	}

	/**
	 * A static factory method, which creates a TreeEngine UCT selection object that
	 * uses the specified value for the constant C in the UCT value calculation.
	 *
	 * This factory method is also a generic method, which uses Java type
	 * inferencing to encapsulate the complex generic type capturing required
	 * by the TreeEngine UCT selection constructor.
	 *
	 * @param logic The game logic
	 * @param c     The constant value for C (in the UCT calculation).
	 */
	public static <SS extends GameState, NN extends MctsNodeTreeParallelInterface<SS, NN, ?>> TreeSelectionFinal<NN> newTreeSelectionUct(
			GameLogic<SS> logic, double c) {
		return new FinalSelectionUct<SS, NN>(logic);
	}

}

package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import za.ac.sun.cs.ingenious.core.Action;

public class MastStoredAction {

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private final ReadLock readLock = lock.readLock();
    private final WriteLock writeLock = lock.writeLock();

    private final Action action;
    private int visitCount = 0;
    private double score = 0.0;

    private boolean cached = false;
    private double cachedValue;

    /**
     * constructor to initialise the action
     * 
     * @param action
     */
    public MastStoredAction(Action action) {
        this.action = action;
    }

    /**
     * increment the number of playouts throughout the game for which this move was
     * played
     */
    public void addResult(double delta) {
        writeLock();
        score += delta;
        visitCount++;
        cached = false;
        writeUnlock();
    }

    /**
     * @return the action for which this wrapper holds values
     */
    public Action getAction() {
        return action;
    }

    /**
     * @return get the average score of the action
     */
    public double getAverageScore() {
        readLock();
        try {
            return score / visitCount;
        } finally {
            readUnlock();
        }
    }

    /**
     * @return get the number of times the action was played
     */
    public int getVisitCount() {
        readLock();
        try {
            return visitCount;
        } finally {
            readUnlock();
        }
    }

    /**
     * @return the score of the action
     */
    public double getValue() {
        readLock();
        try {
            return score;
        } finally {
            readUnlock();
        }
    }

    /**
     * @return get the Mast value of the action
     */
    public double getMastValue(double tau, ConcurrentHashMap<Integer, Double> cachedMastValues) {
        readLock();
        try {
            if (cached) {
                return cachedValue;
            }
            int hash = visitCount << 2 + (int) (score * 2);
            Double globalCachedValue = cachedMastValues.get(hash);
            if (globalCachedValue != null) {
                cachedValue = globalCachedValue;
                cached = true;
                return globalCachedValue;
            }
            cachedValue = Math.exp(score / (visitCount * tau));
            cached = true;
            return cachedValue;
        } finally {
            readUnlock();
        }
    }

    public void readLock() {
        readLock.lock();
    }

    public void readUnlock() {
        readLock.unlock();
    }

    public void writeLock() {
        writeLock.lock();
    }

    public void writeUnlock() {
        writeLock.unlock();
    }
}

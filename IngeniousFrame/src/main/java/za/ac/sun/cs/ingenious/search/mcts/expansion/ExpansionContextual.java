package za.ac.sun.cs.ingenious.search.mcts.expansion;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.CMCTable;

public class ExpansionContextual<S extends GameState, C, N extends MctsNodeCompositionInterface<S, C, ?>>
        implements ExpansionThreadSafe<C, N> {

    private CMCTable cmcTable;
    private GameLogic<S> logic;
    private ThreadLocalRandom randomGen = ThreadLocalRandom.current();

    private ExpansionContextual(GameLogic<S> logic, CMCTable cmcTable) {
        this.logic = logic;
        this.cmcTable = cmcTable;
    }

    public C expand(N node) {
        List<Action> unexploredActions = node.getUnexploredActions();

        if (unexploredActions.size() == 0) {
            return node.getSelfAsChildType();
        }
        Action action;
        N parent = (N) node.getParent();
        // get a contextual random action

        if (parent != null) {
            action = cmcTable.getCMCAction(unexploredActions, randomGen, parent.getPrevAction());
        } else {
            action = cmcTable.getCMCAction(unexploredActions, randomGen, null);
        }
        // get the index of the action
        int actionIndex = unexploredActions.indexOf(action);
        // remove gibbs action from unexplored actions
        C expandedChild = node.popUnexploredChild(logic, actionIndex);
        // If the child is null, it means that the action has already been explored, so
        // we return the node as a child
        if (expandedChild == null) {
            Log.warn("ExpansionMast", "Action already explored, returning node as child");
            return node.getSelfAsChildType();
        }
        // add the child to the node
        node.addChild(expandedChild);
        return expandedChild;
    }

    public static <SS extends GameState, CC, NN extends MctsNodeCompositionInterface<SS, CC, ?>> ExpansionThreadSafe<CC, NN> newExpansionContextual(
            GameLogic<SS> logic, CMCTable cmcTable) {
        return new ExpansionContextual<>(logic, cmcTable);
    }

}

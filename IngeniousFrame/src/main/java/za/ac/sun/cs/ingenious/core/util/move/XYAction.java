package za.ac.sun.cs.ingenious.core.util.move;

import java.util.Objects;

import za.ac.sun.cs.ingenious.core.Action;

/**
 * An action that consists of x and y coordinates and the player that acted.
 * 
 * @author Michael Krause
 */
public class XYAction implements Action {

	private static final long serialVersionUID = 1L;

	private final int x, y;
	private final int player;

	public XYAction(int x, int y, int player) {
		this.x = x;
		this.y = y;
		this.player = player;
	}

	@Override
	public int getPlayerID() {
		return player;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public String toString() {
		return "(" + x + "|" + y + ") --> " + player;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj != null && this.getClass() == obj.getClass())) {
			return false;
		} else {
			return (this.hashCode() == obj.hashCode());
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y, player);
	}

	@Override
	public XYAction deepCopy() {
		return new XYAction(x, y, player);
	}
}

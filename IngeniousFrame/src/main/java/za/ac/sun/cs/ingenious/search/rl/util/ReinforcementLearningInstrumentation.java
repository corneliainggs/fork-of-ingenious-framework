package za.ac.sun.cs.ingenious.search.rl.util;

import com.esotericsoftware.minlog.Log;
import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.style.markers.SeriesMarkers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

/**
 * Instrumentation used for monitoring the performance of reinforcement learning algorithms and includes a chart that
 * shows average rewards.
 *
 * @author Steffen Jacobs
 */
public class ReinforcementLearningInstrumentation {
    /**
     * Total number of steps plotted as the X axis.
     */
    private static final int MAX_HISTORY = 100000;

    /**
     * Default number of steps for moving averages.
     */
    private static final int DEFAULT_RECENT_STEP_COUNT = 20000;

    /**
     * Number of steps between each chart update.
     */
    private int chartUpdateFreq = 50;

    /**
     * Number of steps between each update to the log file.
     */
    private int logFileUpdateFreq = 50;

    /**
     * Used for identifying plot and log file.
     */
    protected String identifier;

    /**
     * Episodes that have elapsed.
     */
    protected int totalEpisodes;

    /**
     * Steps that have elapsed.
     */
    protected int totalSteps;

    /**
     * Number of steps used for the moving average.
     */
    private int recentStepCount = DEFAULT_RECENT_STEP_COUNT;

    /**
     * Total sum of all rewards received.
     */
    protected double cumulativeReward;

    /**
     * Sum of rewards earned in the last `recentStepCount` steps.
     */
    protected double recentCumulativeReward;

    /**
     * Highest reward value received.
     */
    protected double maxReward;

    /**
     * Number of times the current maximum reward was received.
     */
    protected int maxRewardFreq;

    /**
     * Lowest reward value earned.
     */
    protected double minReward;

    /**
     * Highest game score achieved. The score can be different depending on the game, and so it is up an engine to
     * update this value using the `notifyScore` method.
     */
    protected double maxScore;

    /**
     * Number of times the current maximum score was achieved.
     */
    protected int maxScoreFreq;

    /**
     * Indicates whether the chart is shown and refreshed.
     */
    private boolean useChart;

    private XYChart chart;
    private SwingWrapper<XYChart> sw;
    private FileWriter fileWriter;

    LinkedList<Double> cumulativeRewardAtStep;
    LinkedList<Double> overallAverageRewardAtStep;
    LinkedList<Double> recentAverageRewardAtStep;
    LinkedList<Double> steps;

    public ReinforcementLearningInstrumentation() {
        this.init();

        initLogger();
    }


    public ReinforcementLearningInstrumentation(String identifier) {
        this.init();
        this.identifier = identifier;
        chart.setTitle(identifier);

        initLogger();
    }

    /**
     * Initialize values and set up chart.
     */
    public void init() {
        identifier = "UnnamedRLAgent";
        totalEpisodes = 0;
        totalSteps = 0;
        cumulativeReward = 0.0;
        recentCumulativeReward = 0.0;
        maxRewardFreq = 0;
        maxScore = -Double.MAX_VALUE;
        maxScoreFreq = 0;

        chart = QuickChart.getChart(identifier, "Steps", "Average Reward", new String[] {}, new double[]{}, new double[][]{});

        chart.addSeries("Recent", new double[]{0}, new double[]{0}).setMarker(SeriesMarkers.NONE);
        chart.addSeries("Overall", new double[]{0}, new double[]{0}).setMarker(SeriesMarkers.NONE);

        sw = new SwingWrapper<>(chart);
        useChart = false;

        cumulativeRewardAtStep = new LinkedList<>();
        overallAverageRewardAtStep = new LinkedList<>();
        recentAverageRewardAtStep = new LinkedList<>();
        steps = new LinkedList<>();
    }

    public void initLogger() {
        try {
            File dir = new File("./Logs/");
            dir.mkdirs();

            File file = new File(dir, identifier + ".csv");
            fileWriter = new FileWriter(file);

            fileWriter.write("Episodes" +
                    ", Steps" +
                    ", Cumulative Reward" +
                    ", Cumulative Reward (recent)" +
                    ", Avg Reward Overall" +
                    ", Avg Reward (recent)\n");
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void displayChart() {
        useChart = true;
        sw.displayChart();
    }

    public int getTotalEpisodes() {
        return totalEpisodes;
    }

    public int getTotalSteps() {
        return totalSteps;
    }

    public double getCumulativeReward() {
        return cumulativeReward;
    }

    public void endEpisode() {
        totalEpisodes++;
    }

    /**
     * Notifies the instrumentation that an episode step has occurred. Values of the different metrics are updated /
     * recalculated.
     */
    public void incrementStep() {
        totalSteps++;

        steps.add((double) totalSteps);
        cumulativeRewardAtStep.add(cumulativeReward);
        overallAverageRewardAtStep.add(cumulativeReward / (double) totalSteps);

        int numberOfRecentSteps = Math.min(totalSteps, recentStepCount);
        double[] cumulativeReward = cumulativeRewardAtStep.stream()
                .skip(cumulativeRewardAtStep.size() - numberOfRecentSteps)
                .mapToDouble(x -> x)
                .toArray();

        recentCumulativeReward = cumulativeReward[cumulativeReward.length - 1] - cumulativeReward[0];
        recentAverageRewardAtStep.add(recentCumulativeReward / numberOfRecentSteps);

        if (totalSteps >= MAX_HISTORY) {
            steps.removeFirst();
            cumulativeRewardAtStep.removeFirst();
            overallAverageRewardAtStep.removeFirst();
            recentAverageRewardAtStep.removeFirst();
        }

        refreshChart();
        appendToLog();
    }

    /**
     * Notifies that the agent received a reward value.
     *
     * @param reward The reward value received by the agent.
     */
    public void addReward(double reward) {
        cumulativeReward += reward;

        if (reward > maxReward) {
            maxReward = reward;
            maxRewardFreq = 1;
        } else if (reward == maxReward) {
            maxRewardFreq++;
        }

        if (reward < minReward) minReward = reward;
    }

    /**
     * Notifies the instrumentation what the current game score is.
     *
     * @param score The current score of the game, as determined by the engine.
     */
    public void notifyScore(double score) {
        if (score > maxScore) {
            maxScore = score;
            maxScoreFreq = 1;
        } else if (score == maxScore) {
            maxScoreFreq++;
        }
    }

    public void setRecentStepCount(int recentStepCount) {
        this.recentStepCount = recentStepCount;
    }

    public void setChartUpdateFreq(int chartUpdateFreq) {
        this.chartUpdateFreq = chartUpdateFreq;
    }

    public void setLogFileUpdateFreq(int logFileUpdateFreq) {
        this.logFileUpdateFreq = logFileUpdateFreq;
    }

    /**
     * Output the current instrumentation values.
     */
    public void printMetrics() {
        StringBuilder sb = new StringBuilder();

        sb.append("\n\t");
        sb.append(identifier).append("\n");
        sb.append("==================================================\n");
        sb.append(String.format("%30s: %10.2f", "Episodes", (double) totalEpisodes)).append("\n");
        sb.append(String.format("%30s: %10.2f", "Steps", (double) totalSteps)).append("\n");
        sb.append(String.format("%30s: %10.2f", "Cumulative Reward", cumulativeReward)).append("\n");
        sb.append(String.format("%30s: %10.2f", "Avg Reward Overall", cumulativeReward / totalSteps)).append("\n");
        sb.append(String.format("%30s: %10.2f", "Cumulative Reward (recent)", recentCumulativeReward)).append("\n");
        sb.append(String.format("%30s: %10.2f", "Avg Reward Recent", recentCumulativeReward / Math.min(totalSteps, recentStepCount))).append("\n");
        sb.append(String.format("%30s: %10.2f (%d)", "Max Reward", maxReward, maxRewardFreq)).append("\n");
        sb.append(String.format("%30s: %10.2f", "Min Reward", minReward)).append("\n");
        sb.append(String.format("%30s: %10.2f (%d)", "Max Score", maxScore, maxScoreFreq)).append("\n");

        Log.info(sb);
    }

    /**
     * Refreshes the chart UI. The frequency of this update is determined by `CHART_UPDATE_FREQ`.
     */
    private void refreshChart() {
        if (!useChart) return;

        if (totalSteps % chartUpdateFreq == 0) {
            double[] stepsArray = steps.stream().mapToDouble(x -> x).toArray();

            double[] averageRewardPerStepRecentArray = recentAverageRewardAtStep.stream().mapToDouble(x -> x).toArray();
            chart.updateXYSeries("Recent", stepsArray, averageRewardPerStepRecentArray, null);

            double[] averageRewardPerStepOverallArray = overallAverageRewardAtStep.stream().mapToDouble(x -> x).toArray();

            chart.updateXYSeries("Overall", stepsArray, averageRewardPerStepOverallArray, null);

            sw.repaintChart();
        }
    }

    /**
     * Appends a new row to the CSV logfile.
     */
    private void appendToLog() {
        if (fileWriter == null) return;

        if (totalSteps % logFileUpdateFreq == 0) {
            try {
                fileWriter.write(totalEpisodes +
                        ", " + totalSteps +
                        ", " + cumulativeReward +
                        ", " + recentCumulativeReward +
                        ", " + cumulativeReward / totalSteps +
                        ", " + recentCumulativeReward / Math.min(totalSteps, recentStepCount) +
                        "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Gracefully close any streams that may be open. Currently, this is only the case with the file writer.
     */
    public void close() {
        try {
            if (fileWriter != null)
                fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

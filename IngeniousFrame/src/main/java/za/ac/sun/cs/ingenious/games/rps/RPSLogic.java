package za.ac.sun.cs.ingenious.games.rps;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.move.UnobservedMove;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

public class RPSLogic implements GameLogic<RPSGameState>, GameFinalEvaluator<RPSGameState>, ActionSensor<RPSGameState>,
        InformationSetDeterminizer<RPSGameState> {
    public final static int ROCK = 0, PAPER = 1, SCISSORS = 2;
    static List<Action>[] possibleActions;
    static Move[] unobservedMoves = new Move[] { new UnobservedMove(0), new UnobservedMove(1) };
    static double[][] possibleScores = { { 0, 0 }, { 1, -1 }, { -1, 1 } };
    static Set<Integer>[] playerSets;

    static {
        possibleActions = new ArrayList[3];

        for (int i = 0; i < 2; i++) {
            possibleActions[i] = new ArrayList<Action>(3);
            for (int j = 0; j < 3; j++) {
                possibleActions[i].add(new DiscreteAction(i, j));
            }
        }
        possibleActions[2] = new ArrayList<>(0);

        playerSets = new Set[3];

        playerSets[0] = new HashSet<Integer>() {
            {
                add(0);
            }
        };
        playerSets[1] = new HashSet<Integer>() {
            {
                add(1);
            }
        };
        playerSets[2] = new HashSet<Integer>() {
            {
                add(-1);
            }
        };
    }

    private final Random rand;

    public RPSLogic() {
        rand = new Random();
    }

    @Override
    public boolean validMove(RPSGameState fromState, Move move) {
        if (!(move instanceof DiscreteAction)) {
            return false;
        }

        DiscreteAction action = (DiscreteAction) move;

        return action.getActionNumber() == ROCK ||
                action.getActionNumber() == PAPER ||
                action.getActionNumber() == SCISSORS;
    }

    @Override
    public boolean makeMove(RPSGameState fromState, Move move) {
        fromState.playerActions[move.getPlayerID()] = move;

        return true;
    }

    @Override
    public List<Action> generateActions(RPSGameState fromState, int forPlayerID) {
        if (isTerminal(fromState)) {
            return possibleActions[2];
        } else {
            return possibleActions[forPlayerID];
        }
    }

    @Override
    public boolean isTerminal(RPSGameState state) {
        return state.playerActions[0] != null && state.playerActions[1] != null;
    }

    @Override
    public Set<Integer> getCurrentPlayersToAct(RPSGameState fromState) {
        if (fromState.playerActions[0] == null) {
            return playerSets[0];
        } else if (fromState.playerActions[1] == null) {
            return playerSets[1];
        } else {
            return playerSets[2];
        }
    }

    @Override
    public double[] getScore(RPSGameState forState) {

        if (forState.playerActions[0] == null || forState.playerActions[1] == null) {
            return possibleScores[0];
        }

        int[] actionNumbers = new int[2];
        actionNumbers[0] = ((DiscreteAction) forState.playerActions[0]).getActionNumber();
        actionNumbers[1] = ((DiscreteAction) forState.playerActions[1]).getActionNumber();

        if (actionNumbers[0] == actionNumbers[1]) {
            return possibleScores[0];
        } else {
            if (actionNumbers[0] == ROCK && actionNumbers[1] == SCISSORS ||
                    actionNumbers[0] == PAPER && actionNumbers[1] == ROCK ||
                    actionNumbers[0] == SCISSORS && actionNumbers[1] == PAPER) {
                return possibleScores[1];
            } else {
                return possibleScores[2];
            }
        }
    }

    @Override
    public Move fromPointOfView(Action originalAction, RPSGameState fromGameState, int pointOfViewPlayerID) {
        if (originalAction.getPlayerID() == pointOfViewPlayerID || pointOfViewPlayerID == -1) {
            return originalAction;
        } else {
            return unobservedMoves[Math.abs(pointOfViewPlayerID - 1)];
        }
    }

    @Override
    public RPSGameState determinizeUnknownInformation(RPSGameState forState, int forPlayerID) {
        RPSGameState newState = forState.deepCopy();

        for (int i = 0; i < 2; i++) {
            if (newState.playerActions[i] != null && newState.playerActions[i] instanceof UnobservedMove) {
                newState.playerActions[i] = possibleActions[newState.playerActions[i].getPlayerID()]
                        .get(rand.nextInt(3));
            }
        }

        return newState;
    }

    @Override
    public RPSGameState observeState(RPSGameState state, int forPlayerID) {
        RPSGameState newState = state.deepCopy();

        for (int i = 0; i < 2; i++) {
            if (forPlayerID != -1 && newState.playerActions[i] != null && i != forPlayerID) {
                newState.playerActions[i] = unobservedMoves[i];
            }
        }

        return newState;
    }

    public static String getPrettyRPSAction(DiscreteAction action) {
        return ((action.getActionNumber() == RPSLogic.ROCK) ? "Rock"
                : (action.getActionNumber() == RPSLogic.PAPER) ? "Paper" : "Scissors");
    }
}
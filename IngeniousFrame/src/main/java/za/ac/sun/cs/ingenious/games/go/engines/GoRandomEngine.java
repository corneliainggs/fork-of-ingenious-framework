package za.ac.sun.cs.ingenious.games.go.engines;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.*;
import za.ac.sun.cs.ingenious.games.go.GoEngine;

import java.io.IOException;
import java.util.List;

public class GoRandomEngine extends GoEngine {

    private static final int TURN_TIME = 100;

    /**
     * @param toServer An established connection to the GameServer
     */
    public GoRandomEngine(EngineToServerConnection toServer)
            throws MissingSettingException, IncorrectSettingTypeException, IOException {
        super(toServer);
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        List<Action> actions = logic.generateActions(currentState, currentState.nextMovePlayerID);
        return new PlayActionMessage(actions.get((int) (Math.random() * actions.size())));
    }
}

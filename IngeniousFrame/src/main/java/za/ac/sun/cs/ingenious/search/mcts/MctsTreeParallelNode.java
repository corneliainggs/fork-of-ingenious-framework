package za.ac.sun.cs.ingenious.search.mcts;

import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsNode;
import za.ac.sun.cs.ingenious.core.GameState;

public interface MctsTreeParallelNode<S extends GameState, C, P> extends MctsNode<S, C, P> {

	double getVirtualLoss();

	void applyVirtualLoss();

	void restoreVirtualLoss();

	void incVisitVirtualLoss(double lossValue);

	void addValueVirtualLoss(double lossValue, double add);

	void readLock();

	void readUnlock();

	void writeLock();

	void writeUnlock();

	int getDepth();

	void logPossibleMoves();

}

// TODO: comments!

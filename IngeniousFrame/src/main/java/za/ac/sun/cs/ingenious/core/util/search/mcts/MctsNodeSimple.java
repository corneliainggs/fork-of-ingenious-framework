package za.ac.sun.cs.ingenious.core.util.search.mcts;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.util.search.TreeNode;
import za.ac.sun.cs.ingenious.core.util.search.TreeNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsRootNode;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class MctsNodeSimple<S extends GameState> implements
		MctsNodeCompositionInterface<S, MctsNodeSimple<S>, MctsNodeSimple<S>>, MctsRootNode<MctsNodeSimple<S>> {

	TreeNode<S, MctsNodeSimple<S>, MctsNodeSimple<S>> treeNode;
	int visitCount;
	List<Action> unexploredActions, allPossibleActions;
	Action prevAction;
	private int playerID;

	public MctsNodeSimple(S state, Action prevAction, MctsNodeSimple<S> parent, List<MctsNodeSimple<S>> children,
			GameLogic<S> logic, int playerID) {
		this.playerID = playerID;
		treeNode = new TreeNodeComposition<S, MctsNodeSimple<S>, MctsNodeSimple<S>>(state, parent, children);
		this.prevAction = prevAction;
		// Log.info(this.prevAction);
		this.visitCount = 0;
		for (int i : logic.getCurrentPlayersToAct(state)) {
			if (allPossibleActions == null) {
				allPossibleActions = logic.generateActions(state, i);
			} else {
				allPossibleActions.addAll(logic.generateActions(state, i));
			}
		}
		this.allPossibleActions = Collections.unmodifiableList(allPossibleActions);
		this.unexploredActions = new ArrayList<Action>(allPossibleActions);
	}

	public int getPlayerID() {
		return playerID;
	}

	public S getState(boolean getDefensiveCopy) {
		return treeNode.getState(getDefensiveCopy);
	}

	public double getValue() {
		return treeNode.getValue();
	}

	public void addValue(double add) {
		treeNode.addValue(add);
	}

	// TODO: change?
	public String toString() {
		String parentString = (this.getParent() == null) ? "null" : this.getParent().getState(false).toString();

		return ("TreeNodeNode:\n\tState = " + getState(false).toString() + "\nprevAction = " + getPrevAction()
				+ "\n\tvalue = " + getValue() +
				"\n\tParent (state) = " + parentString + "\n\tChildren size = " + getChildren().size() +
				"\n\tvisitCount = " + getVisitCount() + "\n\tUnexploredActions = " + getUnexploredActions());
	}

	// TreeNode interface:
	public MctsNodeSimple<S> getParent() {
		return treeNode.getParent();
	}

	public void setParent(MctsNodeSimple<S> parent) {
		treeNode.setParent(parent);
	}

	public List<MctsNodeSimple<S>> getChildren() {
		return treeNode.getChildren();
	}

	public void setChildren(List<MctsNodeSimple<S>> children) {
		treeNode.setChildren(children);
		for (MctsNodeSimple<S> child : children) {
			this.unexploredActions.remove(child.getPrevAction());
		}
	}

	// FIXME: when a child is added, need to check that child is not already a
	// child, if it is, merge info, if it's not add and remove prevAction from
	// unexploredactions...
	public void addChild(MctsNodeSimple<S> child) {
		treeNode.addChild(child);
		this.unexploredActions.remove(child.getPrevAction());
	}

	public void addChildren(List<MctsNodeSimple<S>> newChildren) {
		treeNode.addChildren(newChildren);
	}

	// MctsNodeCompositionInterface interface:
	public double getVisitCount() {
		return this.visitCount;
	}

	public void incVisitCount() {
		visitCount++;
	}

	public void decVisitCount() {
		visitCount--;
	}

	public double getChildVisitCount(MctsNodeSimple<S> child) {
		return child.getVisitCount();
	}

	public double getChildValue(MctsNodeSimple<S> child) {
		return child.getValue();
	}

	public List<Action> getUnexploredActions() {
		return Collections.unmodifiableList(unexploredActions);
	}

	public boolean unexploredActionsEmpty() {
		return unexploredActions.isEmpty();
	}

	/**
	 * Removes an unexplored action and returns the resulting child node. Note that
	 * this child node will have to be added to the tree manually.
	 */
	public MctsNodeSimple<S> popUnexploredChild(GameLogic<S> logic) {
		return popUnexploredChild(logic, (int) (Math.random() * unexploredActions.size()));
	}

	/**
	 * Removes an unexplored action and returns the resulting child node. Note that
	 * this child node will have to be added to the tree manually.
	 */
	public MctsNodeSimple<S> popUnexploredChild(GameLogic<S> logic, int index) {
		if (unexploredActionsEmpty()) {
			return null;
		}
		// getState(true) returns defensive copy, so this is allowed/not alias:
		S childState = getState(true);
		Action unexploredAction = unexploredActions.remove(index);
		logic.makeMove(childState, unexploredAction);

		int alternatePlayer = alternatePlayer(this.getPlayerID());
		return new MctsNodeSimple<S>(childState, unexploredAction, this, new ArrayList<MctsNodeSimple<S>>(), logic,
				alternatePlayer);
	}

	public int alternatePlayer(int player) {
		if (player == 0) {
			return 1;
		} else if (player == 1) {
			return 0;
		} else {
			Log.error("MctsNodeSimple", "Shouldn't get here, MctsNodeTreeParallel invalid player : id=" + player);
			return -1;
		}
	}

	public Action getPrevAction() {
		return this.prevAction;
	}

	public MctsNodeSimple<S> getSelfAsChildType() {
		return this;
	}

	public void merge(MctsNodeSimple<S> other) {
		mergeHelper(other, false);
	}

	private boolean mergeHelper(MctsNodeSimple<S> other, boolean isChild) {

		if (!this.getState(false).equals(other.getState(false))) {
			return false;
		}

		this.addValue(other.getValue());
		for (int i = 0; i < other.getVisitCount(); i++) {
			this.incVisitCount();
		}

		if (isChild)
			return true;

		List<MctsNodeSimple<S>> thisChildren = this.getChildren();
		List<MctsNodeSimple<S>> otherChildren = other.getChildren();
		if (thisChildren.isEmpty()) {
			this.setChildren(otherChildren);
			return true;
		}

		for (MctsNodeSimple<S> otherChild : otherChildren) {
			boolean childMerged = false;
			for (MctsNodeSimple<S> thisChild : thisChildren) {
				if (thisChild.getState(false).equals(otherChild.getState(false))) {
					thisChild.mergeHelper(otherChild, true);
					childMerged = true;
					break;
				}
			}
			if (!childMerged)
				this.addChild(otherChild);
		}

		return true;
	}

	public List<Action> getAllPossibleActions() {
		return this.allPossibleActions;
	}
}

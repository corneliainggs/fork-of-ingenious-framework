package za.ac.sun.cs.ingenious.games.diceGames.dudo;

import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.games.diceGames.actions.DudoClaimMove;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.move.UnobservedMove;
import za.ac.sun.cs.ingenious.games.diceGames.actions.DiceRollAction;

public class DudoActionSensor implements ActionSensor<DudoGameState> {

    @Override
    public Move fromPointOfView(Action originalAction, DudoGameState fromGameState,
            int pointOfViewPlayerID) {

        if (originalAction instanceof DiceRollAction) {
            if (pointOfViewPlayerID == -1
                    || pointOfViewPlayerID == fromGameState.getNumPlayers()
                    || originalAction.getPlayerID() == pointOfViewPlayerID) {
                return originalAction;
            }
        } else if (originalAction instanceof DiscreteAction
                && ((DiscreteAction) originalAction).getActionNumber() == fromGameState.dudo) {
            // Distribute number of remaining dice at end of round
            return new DudoClaimMove((DiscreteAction) originalAction,
                    fromGameState.getNumPlayerDice().clone());
        } else {
            return originalAction;
        }

        return new UnobservedMove(originalAction.getPlayerID());
    }
}
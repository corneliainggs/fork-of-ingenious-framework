package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.ReentrantLock;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.games.go.GoBoard;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;

public class CMCTable {

    private final Map<Action, CMCStoredActions> moveCombinationScores = new HashMap<>();
    private ReentrantLock combinationScoresLock = new ReentrantLock();

    private int windowSize;
    private double gamma, beta;

    /**
     * refresh the hashmap to a clean empty hashmap
     */
    public void resetVisitedMoves() {
        moveCombinationScores.clear();
    }

    public CMCTable(double gamma, double beta, int windowSize) {
        this.gamma = gamma;
        this.beta = beta;
        this.windowSize = windowSize;
    }

    public void updateVisitedMovesTable(List<Action> moves, double[] scores) {
        if (moves.size() < 3 || windowSize == 0) {
            return;
        }
        Action firstAction, secondAction;
        int lowerIndex, upperIndex;
        double result;
        for (int i = 0; i < moves.size(); i++) {
            // first move of move pair for a simulation
            firstAction = moves.get(i);
            lowerIndex = i + 2;
            if (windowSize == -1) {
                // find every pair of moves for each player and use these to update the table
                upperIndex = moves.size();
            } else {
                // if window size is not -1, then only consider a sliding window for each move
                // made by each player.
                upperIndex = Math.min(moves.size(), lowerIndex + 2 * windowSize);
            }
            for (int j = lowerIndex; j < upperIndex; j += 2) {
                // second move of move pair for a simulation
                secondAction = moves.get(j);
                // ensure that both moves are from the same player
                if (firstAction.getPlayerID() != secondAction.getPlayerID()) {
                    Log.error("SimulationContextual", "Error during simulation: move pair does not have same playerID");
                    throw new RuntimeException("Error during simulation: move pair does not have same playerID");
                }
                result = scores[firstAction.getPlayerID()];
                // update table with both a1->a2 and a2->a1 if the result is 1, it means that
                // the player who played these two actions won; otherwise, they lost
                updateVisitedMovesTableEntry(firstAction, secondAction, result);
                updateVisitedMovesTableEntry(secondAction, firstAction, result);
            }
        }
    }

    private void updateVisitedMovesTableEntry(Action a1, Action a2, double score) {
        // lock the table to prevent two threads from adding the same action
        combinationScoresLock.lock();
        CMCStoredActions storedActions = moveCombinationScores.get(a1);
        // if there are no stored actions for the given action
        if (storedActions == null) {
            // create new stored action and update memory table
            storedActions = new CMCStoredActions();
            moveCombinationScores.put(a1, storedActions);
        }
        // release the lock as the new action has been added
        combinationScoresLock.unlock();
        // update the stored action
        storedActions.addResult(a2, score);
    }

    /**
     * Attempts to return a contextual action based on the data available in lookup
     * tables.
     *
     * @param logic
     * @param state
     * @param playerId
     * @param randomGen
     * @param previousAction
     * @return
     */
    public <S extends GameState> Action getCMCAction(GameLogic<S> logic, S state, int playerId,
            ThreadLocalRandom randomGen, Action previousAction) {
        // if a random double is greater than gamma or the previous action is null then
        // return a random action
        if (randomGen.nextDouble() > gamma || previousAction == null) {
            if (state instanceof GoBoard) {
                return (Action) ((GoBoard) state).getSingleMove(playerId);
            } else {
                List<Action> possibleActions = logic.generateActions(state, playerId);
                return possibleActions.get(randomGen.nextInt(possibleActions.size()));
            }
        }
        return getCMCAction(logic.generateActions(state, playerId), randomGen, previousAction);
    }

    /**
     * Attempts to return a contextual action based on the data available in lookup
     * tables.
     *
     * @param actions
     * @param randomGen
     * @param previousAction
     * @return
     */
    public Action getCMCAction(List<Action> actions, ThreadLocalRandom randomGen, Action previousAction) {
        if (previousAction == null) {
            return actions.get(randomGen.nextInt(actions.size()));
        }
        CMCStoredActions storedActions = moveCombinationScores.get(previousAction);
        // if there are no stored actions for the given action, return a random action
        if (storedActions == null) {
            return actions.get(randomGen.nextInt(actions.size()));
        }
        // use arrays to allow for lambda expression to update the values
        double[] bestCmcScore = { -1.0 };
        Action[] bestAction = { null };
        // get the best action based on the CMC value
        actions.stream().forEach(action -> {
            double cmcValue = storedActions.getCMCValue(action);
            if (cmcValue > bestCmcScore[0] && cmcValue > beta) {
                bestCmcScore[0] = cmcValue;
                bestAction[0] = action;
            }
        });
        return (bestAction[0] == null) ? actions.get(randomGen.nextInt(actions.size())) : bestAction[0];
    }

    public <S extends GameState> void logTableStatistics(MctsNodeTreeParallel<S> node) {
        if (moveCombinationScores.size() == 0 || !Log.DEBUG) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\n===================");
        node.getChildren().stream().sorted((c1, c2) -> {
            return Double.compare(getAverageScore(c2), getAverageScore(c1));
        }).forEach(child -> {
            CMCStoredActions storedActions = moveCombinationScores.get(child.getPrevAction());
            if (storedActions == null) {
                Log.debug("No stored actions for this action");
            } else {
                sb.append("\n\t" + child.getPrevAction() + "  CMC average winrate: " + " ("
                        + Math.round(getAverageScore(child) * 10_000.0) / 100.0 + "%) mcts wins/visits : "
                        + child.getValue() + "/" + child.getVisitCount() + " ("
                        + Math.round(child.getValue() / child.getVisitCount() * 10_000.0) / 100.0 + "%)");
            }
        });
        sb.append("\n===================");
        Log.debug("CMC stats", sb.toString());
    }

    private <S extends GameState> double getAverageScore(MctsNodeTreeParallel<S> child) {
        double[] totValue = { 0.0 };
        CMCStoredActions storedActions = moveCombinationScores.get(child.getPrevAction());
        if (storedActions != null) {
            storedActions.getActions().stream().forEach(action -> {
                double cmcValue = storedActions.getCMCValue(action);
                totValue[0] += cmcValue;
            });
        }
        return totValue[0] / (storedActions == null ? 1 : storedActions.getActions().size());
    }
}

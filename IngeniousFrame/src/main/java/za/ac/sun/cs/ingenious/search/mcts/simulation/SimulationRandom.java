package za.ac.sun.cs.ingenious.search.mcts.simulation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.games.go.GoBoard;

public class SimulationRandom<S extends GameState> extends SimulationThreadSafe<S> {

	// Evaluator in general usually evaluates to 0.0 for loss, 0.5 for draw, 1.0 for
	// win (zero sum games).
	private ThreadLocalRandom randomGen = ThreadLocalRandom.current();
	MctsGameFinalEvaluator<S> evaluator;
	ActionSensor<S> obs;
	boolean recordMoves;

	/**
	 * Constructor.
	 *
	 * @param logic     The game logic used during the simulation.
	 * @param evaluator The game evaluator used to determine the results of the
	 *                  simulation.
	 * @param obs       Object to process actions made by the players during
	 *                  playouts.
	 *                  Actions are observed from the point of view of the
	 *                  environment player.
	 */
	public SimulationRandom(GameLogic<S> logic, MctsGameFinalEvaluator<S> evaluator, ActionSensor<S> obs,
			boolean recordMoves) {
		this.logic = logic;
		this.evaluator = evaluator;
		this.obs = obs;
		this.recordMoves = recordMoves;
	}

	/**
	 * The method that performs random simulation playout.
	 *
	 * @param state The game state from which to start the simulation playout.
	 * @return The result scores
	 */
	public SimulationTuple simulate(List<Action> selectionMoves, S state) {
		Action randomAction;
		ArrayList<Action> moves = new ArrayList<>(selectionMoves);
		while (!logic.isTerminal(state)) {
			for (int playerId : logic.getCurrentPlayersToAct(state)) {
				if (state instanceof GoBoard) {
					randomAction = (Action) ((GoBoard) state).getSingleMove(playerId);
				} else {
					List<Action> possibleActions = logic.generateActions(state, playerId);
					randomAction = possibleActions.get(randomGen.nextInt(possibleActions.size()));
				}
				logic.makeMove(state, obs.fromPointOfView(randomAction, state, playerId));
				if (recordMoves) {
					moves.add(randomAction);
				}
			}
		}
		double[] scores = evaluator.getMctsScore(state);
		return new SimulationTuple(moves, scores);
	}

	public static <SS extends GameState> SimulationThreadSafe<SS> newSimulationRandom(GameLogic<SS> logic,
			MctsGameFinalEvaluator<SS> evaluator, ActionSensor<SS> obs, boolean recordMoves) {
		return new SimulationRandom<SS>(logic, evaluator, obs, recordMoves);
	}
}

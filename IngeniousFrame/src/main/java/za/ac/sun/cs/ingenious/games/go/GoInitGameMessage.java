package za.ac.sun.cs.ingenious.games.go;

import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;

/**
 * @author Michael Krause
 */
public class GoInitGameMessage extends InitGameMessage {

	private static final long serialVersionUID = 1L;

	private int boardWidth;

	public GoInitGameMessage(int boardWidth) {
		this.boardWidth = boardWidth;
	}

	public int getBoardWidth() {
		return boardWidth;
	}
}

package za.ac.sun.cs.ingenious.games.mdp;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.util.message.MatchSettingsInitGameMessage;
import za.ac.sun.cs.ingenious.core.util.referee.FullyObservableMovesReferee;
import za.ac.sun.cs.ingenious.games.mdp.util.MDPSetting;

/**
 * Referee for the MDP game that handles game initialization from the MDP setting JSON file.
 *
 * @author Steffen Jacobs
 */
public class MDPReferee extends FullyObservableMovesReferee<MDPState<Long>, MDPLogic<Long>, MDPFinalEvaluator<Long>> {
    private MDPRewardEvaluator<Long> rewardEval;

    public MDPReferee(MatchSetting match, PlayerRepresentation[] players) throws MissingSettingException, IncorrectSettingTypeException {
        super(
                match,
                players,
                MDPState.fromSetting(
                        MDPSetting.fromFile(match.getSettingAsString("mdpGameFileName"))
                ),
                MDPLogic.fromSetting(
                        MDPSetting.fromFile(match.getSettingAsString("mdpGameFileName"))
                ),
                MDPFinalEvaluator.fromSetting(
                        MDPSetting.fromFile(match.getSettingAsString("mdpGameFileName"))
                )
        );

        currentState.setGameState(logic.getInitialGameState());
        rewardEval = MDPRewardEvaluator.fromSetting(
                MDPSetting.fromFile(match.getSettingAsString("mdpGameFileName"))
        );
    }

    @Override
    protected void beforeGameStarts(){
        StringBuilder s = new StringBuilder();
        s.append("\nMDP From: ");
        try {
            s.append(matchSettings.getSettingAsString("mdpGameFileName")).append("\n");
        } catch (MissingSettingException | IncorrectSettingTypeException e) {
            Log.error("Could not read settings from received MatchSetting object, message: " + e.getMessage());
        }

        Log.info(s);
    }

    @Override
    protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
        return new MatchSettingsInitGameMessage(matchSettings);
    }

    @Override
    protected double[] calculatePlayerRewards() {
        return rewardEval.getReward(currentState);
    }
}

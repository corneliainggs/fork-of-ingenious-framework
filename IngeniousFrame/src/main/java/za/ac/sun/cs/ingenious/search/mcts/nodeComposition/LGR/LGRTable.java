package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.LGR;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;

import java.util.concurrent.ConcurrentHashMap;

import com.esotericsoftware.minlog.Log;

public class LGRTable {

    public final ConcurrentHashMap<Action, Action> moveCombinationScores = new ConcurrentHashMap<>();

    /**
     * refresh the hashmap to a clean empty hashmap
     */
    public void resetVisitedMoves() {
        moveCombinationScores.clear();
    }

    public <S extends GameState> void logTableStatistics(MctsNodeTreeParallel<S> node) {
        if (moveCombinationScores.size() == 0 || !Log.DEBUG) {
            return;
        }
    }
}

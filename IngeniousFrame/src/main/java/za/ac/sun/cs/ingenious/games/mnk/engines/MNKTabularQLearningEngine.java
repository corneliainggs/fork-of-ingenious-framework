package za.ac.sun.cs.ingenious.games.mnk.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.mnk.MNKEngine;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;
import za.ac.sun.cs.ingenious.search.rl.qlearning.TabularQLearning;

import java.io.*;
import java.util.List;

public class MNKTabularQLearningEngine extends MNKEngine {
    private final double ALPHA = 0.01;
    private final double EPSILON = 1.0;
    private final double EPSILON_STEP = 1.0 / 100000;
    private final double EPSILON_MIN = 0.01;
    private final double GAMMA = 1.0;
    private final boolean DOUBLE_LEARNING = true;
    private final int ALG_SAVE_FREQ = 5000;

    private String algorithmFileName = null;
    private MNKState previousBoard = null;
    private Action previousAction = null;
    private ScalarReward previousReward = null;
    private TabularQLearning alg;
    private boolean enableTraining;

    public MNKTabularQLearningEngine(EngineToServerConnection toServer) throws IOException, ClassNotFoundException {
        super(toServer);

        enableTraining = false;
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "MNKTabularQLearningEngine";
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        // Learn from previous move if match hasn't ended. This value should typically be 0.
        if (previousBoard != null)
            alg.update(previousBoard, previousAction, (ScalarReward) previousReward, board);

        // Record board, then choose an action.
        previousBoard = board.deepCopy();

        Action choice = null;
        List<Action> availableActions = logic.generateActions(board, playerID);
        choice = alg.chooseAction(board, availableActions);

        previousAction = choice;

        return new PlayActionMessage(choice);
    }

    @Override
    public void receiveMatchResetMessage(MatchResetMessage a) {
        // Learn from previous move if match has ended. This value should either be -1 or 1 for loss and win respectively.
        alg.update(previousBoard, previousAction, previousReward, board);
        alg.endEpisode();

        previousBoard = null;
        previousAction = null;
        previousReward = null;
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        ScalarReward rewardReceived = (ScalarReward) a.getReward();

        previousReward = new ScalarReward(rewardReceived.getReward());
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        if (alg == null) {
            try {
                algorithmFileName = "MNKTabularQLearning_Player" + playerID + ".alg";
                alg = TabularQLearning.fromFile(algorithmFileName);
            } catch (Exception e) {
                alg = new TabularQLearning(
                        "MNKExternalDQNEngine_" + playerID,
                        enableTraining,
                        ALPHA,
                        EPSILON,
                        EPSILON_STEP,
                        EPSILON_MIN,
                        GAMMA,
                        DOUBLE_LEARNING
                );
            }

            alg.displayChart();
        }

        if (alg.getTotalSteps() % ALG_SAVE_FREQ == 0)
            alg.save(algorithmFileName);
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        alg.save(algorithmFileName);

        alg.printMetrics();
        alg.close();
    }

    public TabularQLearning getAlg() {
        return alg;
    }

    public void setTraining() {
        this.enableTraining = true;
    }
}

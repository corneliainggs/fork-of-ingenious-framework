package za.ac.sun.cs.ingenious.core.network.external.messages.linearFunctionApproximation;

import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalFeatures;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

import java.util.HashMap;
import java.util.Map;

/**
 * Used to communicate an action value to linear function approximation in the Python application.
 *
 * @author Steffen Jacobs
 */
public class TrainLinearFeaturesMessage extends ExternalMessage {
    Map<String, Object> payload;

    public TrainLinearFeaturesMessage(double[] featureSet, double value) {
        super(ExternalMessageType.TRAIN_LINEAR_FEATURES);

        payload = new HashMap<>();
        payload.put("state", new ExternalFeatures(featureSet));
        payload.put("value", value);
    }

    public Map<String, Object> getPayload() {
        return payload;
    }

    public void setPayload(Map<String, Object> payload) {
        this.payload = payload;
    }
}

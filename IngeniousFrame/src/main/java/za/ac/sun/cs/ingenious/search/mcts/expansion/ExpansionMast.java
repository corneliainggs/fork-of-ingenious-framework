package za.ac.sun.cs.ingenious.search.mcts.expansion;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST.MastTable;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import com.esotericsoftware.minlog.Log;

public class ExpansionMast<S extends GameState, C, N extends MctsNodeCompositionInterface<S, C, ?>>
		implements ExpansionThreadSafe<C, N> {

	private GameLogic<S> logic;
	private ThreadLocalRandom randomGen = ThreadLocalRandom.current();
	private MastTable mastTable;

	private ExpansionMast(GameLogic<S> logic, MastTable mastTable) {
		this.logic = logic;
		this.mastTable = mastTable;
	}

	/**
	 * Create a new child node from the current node and add it to the search tree
	 * according to the MAST move ordering.
	 * 
	 * @param node the current node for which a child must be added
	 * @return
	 */
	public C expand(N node) {
		List<Action> unexploredActions = node.getUnexploredActions();

		if (unexploredActions.size() == 0) {
			return node.getSelfAsChildType();
		}

		// get a gibbs-biased random action
		Action gibbsAction = mastTable.getMastAction(unexploredActions, randomGen);
		// get index of gibbs action in unexplored actions
		int gibbsActionIndex = unexploredActions.indexOf(gibbsAction);
		// remove gibbs action from unexplored actions
		C expandedChild = node.popUnexploredChild(logic, gibbsActionIndex);
		// If the child is null, it means that the action has already been explored, so
		// we return the node as a child
		if (expandedChild == null) {
			Log.warn("ExpansionMast", "Action already explored, returning node as child");
			return node.getSelfAsChildType();
		}
		// add the child to the node
		node.addChild(expandedChild);
		return expandedChild;
	}

	public static <SS extends GameState, CC, NN extends MctsNodeCompositionInterface<SS, CC, ?>> ExpansionThreadSafe<CC, NN> newExpansionMast(
			GameLogic<SS> logic, MastTable mastTable) {
		return new ExpansionMast<>(logic, mastTable);
	}
}

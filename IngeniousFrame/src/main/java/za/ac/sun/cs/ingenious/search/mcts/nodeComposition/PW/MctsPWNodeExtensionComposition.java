package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.PW;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;

import java.util.ArrayList;
import java.util.List;

public class MctsPWNodeExtensionComposition<S extends GameState, C, P>
        implements MctsPWNodeExtensionCompositionInterface {

    public static String id = "PW";

    private ArrayList<C> unprunedChildren = new ArrayList<>();
    private ArrayList<C> PrunedChildren = new ArrayList<>();

    /**
     * empty constructor
     */
    public MctsPWNodeExtensionComposition() {
    }

    /**
     * empty set up since no set of relevant for this type of node extension
     * 
     * @param node
     */
    public void setUp(MctsNodeComposition node) {
    }

    /**
     * @return the id of the next player to play for the node object for which this
     *         extension relates
     */
    public String getID() {
        return id;
    }

    /**
     * @return the list of children objects relating to children which are pruned
     *         and explored
     */
    public ArrayList<C> getUnprunedChildren() {
        return unprunedChildren;
    }

    /**
     * @return the size of the list of children objects relating to children which
     *         are pruned and explored
     */
    public int getUnprunedChildrenSize() {
        return unprunedChildren.size();
    }

    /**
     * Adds a child to the list of children objects relating to children which are
     * pruned and explored
     * 
     * @param child
     */
    public void addChildToUnprunedChildren(C child) {
        unprunedChildren.add(child);
    }

    /**
     * @return the list of children relating to children pruned and unexplored
     */
    public ArrayList<C> getPrunedChildren() {
        return PrunedChildren;
    }

    /**
     * @return the size of the list of children relating to children pruned and
     *         unexplored
     */
    public int getPrunedChildrenSize() {
        return PrunedChildren.size();
    }

    /**
     * returns the list of children relating to children pruned and unexplored
     * 
     * @param index
     * @return
     */
    public C getPrunedChild(int index) {
        return PrunedChildren.get(index);
    }

    /**
     * Adds the child to the list of pruned, explored children relating to this node
     * 
     * @param child
     */
    public void addChildToPrunedChildren(C child) {
        PrunedChildren.add(child);
    }

    /**
     * removes the child at the index into the list of pruned, explored children
     * relating to this node
     * 
     * @param index
     */
    public void removeChildFromPrunedChildren(int index) {
        PrunedChildren.remove(index);
    }

    /**
     * adds the list of children to the list of pruned children from which children
     * are then removed
     * during the unpruning process
     * 
     * @param children
     */
    public void instantiatePrunedChildren(List<MctsNodeComposition> children) {
        this.PrunedChildren = new ArrayList<>();
        int size = children.size();
        for (int i = 0; i < size; i++) {
            this.PrunedChildren.add((C) children.get(i));
        }
    }

}

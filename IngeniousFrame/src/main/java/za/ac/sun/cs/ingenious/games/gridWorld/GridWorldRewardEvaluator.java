package za.ac.sun.cs.ingenious.games.gridWorld;

import za.ac.sun.cs.ingenious.core.GameRewardEvaluator;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;

import java.util.Map;

/**
 * Class containing the evaluator for rewards in GridWorld.
 *
 * @author Steffen Jacobs
 */
public class GridWorldRewardEvaluator implements GameRewardEvaluator<GridWorldState> {
    private Map<Coord, Double> rewards;
    private double stepReward;

    public GridWorldRewardEvaluator(Map<Coord, Double> rewards, double stepReward) {
        this.rewards = rewards;
        this.stepReward = stepReward;
    }

    @Override
    public double[] getReward(GridWorldState forState) {
        double[] reward = new double[forState.getNumPlayers()];

        for (int i = 0; i < forState.getNumPlayers(); i++) {
            reward[i] = stepReward;
            Coord playerCoords = forState.findPlayerCoords(i);

            if (rewards.containsKey(playerCoords)) {
                reward[i] = rewards.get(playerCoords);
            }
        }

        return reward;
    }
}

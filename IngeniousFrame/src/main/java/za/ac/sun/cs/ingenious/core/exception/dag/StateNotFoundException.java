package za.ac.sun.cs.ingenious.core.exception.dag;

public class StateNotFoundException extends Exception {

    public StateNotFoundException(String msg) {
        super(msg);
    }
}

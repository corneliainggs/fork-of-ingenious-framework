package za.ac.sun.cs.ingenious.search.mcts.selection;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

import java.util.List;

/**
 * A standard implementation of the TreeEngine selection strategy
 * {@link TreeSelection},
 * that calculates UCT values to make a decision of which child node to select.
 * This strategy is specifically for TreeEngine selection and makes use of read
 * locks
 * when viewing node information that is used in the calculations.
 *
 * @author Karen Laubscher
 *
 * @param <S> The game state type.
 * @param <N> The type of the mcts node.
 */
public class TreeSelectionUctTunedFPU<S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, ?>>
		implements TreeSelection<N> {

	private static double startPriority;
	private static double c;
	public int player;

	/**
	 * Constructor to create a TreeEngine UCT selection object with a specified
	 * value
	 * for the constant C used in the UCT value calculation.
	 *
	 * @param player        The player for which the UCT value is calculated
	 * @param startPriority The reward value of each node before it is explored for
	 *                      the first time
	 */
	private TreeSelectionUctTunedFPU(int player) {
		this.player = player;
	}

	/**
	 * The method that decides which child node to traverse to next, based on
	 * calculating the UCT value for each child and then selecting the child
	 * with the highest UCT value. Since the TreeEngine structure is shared in
	 * TreeEngine
	 * parallelisation, nodes are (read) locked when information for
	 * the calculations are viewed.
	 * 
	 * @param node The current node whose children nodes are considered for the
	 *             next node to traverse to.
	 * 
	 * @return The selected child node with the highest UCT value.
	 */
	public N select(N node) {

		List<N> children = node.getChildren();
		double currentVisitCount = node.getVisitCount();
		N highestUctChild = null;

		double tempVal, avgChildScore, childValue, childVisits, V;
		double highestUct = Double.NEGATIVE_INFINITY;
		for (N child : children) {

			childValue = child.getValue();
			childVisits = child.getVisitCount() + child.getVirtualLoss();
			avgChildScore = childValue / childVisits;

			V = avgChildScore - Math.pow(avgChildScore, 2) + Math.sqrt((2 * Math.log(currentVisitCount) / childVisits));

			tempVal = avgChildScore
					+ c * (Math.sqrt(Math.log(currentVisitCount) * Math.min(0.25, V) / childVisits));

			if (Double.compare(tempVal, highestUct) > 0) {
				highestUctChild = child;
				highestUct = tempVal;
			}
		}

		if (highestUctChild == null || (!node.unexploredActionsEmpty() && highestUct < startPriority)) {
			return null;
		}

		return highestUctChild;
	}

	/**
	 * A static factory method, which creates a TreeEngine UCT selection object that
	 * uses the specified value for the constant C in the UCT value calculation.
	 *
	 * This factory method is also a generic method, which uses Java type
	 * inferencing to encapsulate the complex generic type capturing required
	 * by the TreeEngine UCT selection constructor.
	 *
	 * @param logic The game logic
	 * @param c     The constant value for C (in the UCT calculation).
	 */
	public static <SS extends GameState, NN extends MctsNodeTreeParallelInterface<SS, NN, ?>> TreeSelection<NN> newTreeSelectionFPUTuned(
			int player, double c, double startPriority) {
		TreeSelectionUctTunedFPU.c = c;
		TreeSelectionUctTunedFPU.startPriority = startPriority;
		return new TreeSelectionUctTunedFPU<SS, NN>(player);
	}

}

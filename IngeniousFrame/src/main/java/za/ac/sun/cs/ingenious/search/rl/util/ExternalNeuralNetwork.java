package za.ac.sun.cs.ingenious.search.rl.util;

import com.esotericsoftware.minlog.Log;
import com.google.gson.Gson;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.external.ExternalComms;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExitMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.neuralNetwork.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class that abstracts communication detail with the Python application away from the external DQN algorithm.
 *
 * @author Steffen Jacobs
 */
public class ExternalNeuralNetwork {
    private boolean initialized;
    private final ExternalComms externalComms;
    private final Map<Action, Integer> actionMap;
    private final Map<Integer, Action> reverseActionMap;
    private int nextActionNumber;

    public ExternalNeuralNetwork() {
        // TODO (#289): Would work nicely if the external Python app could somehow be launched from Java.

        externalComms = new ExternalComms();
        initialized = false;
        actionMap = new HashMap<>();
        reverseActionMap = new HashMap<>();
        nextActionNumber = 0;
    }

    /**
     * Initializes the neural network by sending the number of channels, height and width of the feature matrix to the
     * python app. When using an existing model, it is important that the ordering of provided list of actions is the
     * same.
     *
     * @param playerIdentifier
     * @param network
     * @param channels
     * @param height
     * @param width
     * @param actions
     * @param bufferMemory
     * @param sampleSize
     * @param sampleThreshold
     * @param sampleFrequency
     * @param doubleLearning
     * @param prioritizedSampling
     * @param bufferAlpha
     * @param learningRate
     */
    public void init(
            String playerIdentifier,
            String network,
            int channels,
            int height,
            int width,
            List<Action> actions,
            int bufferMemory,
            int sampleSize,
            int sampleThreshold,
            int sampleFrequency,
            int targetNetworkUpdateFrequency,
            boolean doubleLearning,
            boolean prioritizedSampling,
            double bufferAlpha,
            double learningRate,
            double gamma
    ) {
        Gson gson = new Gson();

        InitNeuralNetMessage initMessage = new InitNeuralNetMessage(
                playerIdentifier,
                network,
                channels,
                height,
                width,
                actions.size(),
                bufferMemory,
                sampleSize,
                sampleThreshold,
                sampleFrequency,
                targetNetworkUpdateFrequency,
                doubleLearning,
                prioritizedSampling,
                bufferAlpha,
                learningRate,
                gamma
        );

        String message = gson.toJson(initMessage);

        externalComms.send(message);
        externalComms.receive();
        initialized = true;

        ensureActionIsMapped(actions);
    }

    public void ensureInitialized() {
        if (!initialized) {
            throw new IllegalStateException("External network has not been initialized by invoking init(...).");
        }
    }

    private void ensureActionIsMapped(Action action) {
        if (!actionMap.containsKey(action)) {
            actionMap.put(action, nextActionNumber);
            reverseActionMap.put(nextActionNumber++, action);
        }
    }

    private void ensureActionIsMapped(List<Action> actions) {
        for (Action action : actions) {
            if (!actionMap.containsKey(action)) {
                actionMap.put(action, nextActionNumber);
                reverseActionMap.put(nextActionNumber++, action);
            }
        }
    }

    public Map<Action, Double> getActionValues(double[][][] state) {
        ensureInitialized();

        Gson gson = new Gson();
        String message = gson.toJson(new ReqNeuralNetActionValuesMessage(state));
        externalComms.send(message);

        String map = externalComms.receive();
        NeuralNetActionValuesMessage response = gson.fromJson(map, NeuralNetActionValuesMessage.class);
        Map<Integer, Double> payload = response.getPayload();

        Map<Action, Double> actionValues = new HashMap<>();

        for (int actionNumber : payload.keySet()) {
            actionValues.put(reverseActionMap.get(actionNumber), payload.get(actionNumber));
        }

        return actionValues;
    }

    public Map<Action, Double> getActionValues(double[][] state) {
        return getActionValues(new double[][][]{state});
    }

    public Map<Action, Double> getActionValues(double[] state) {
        return getActionValues(new double[][]{state});
    }

    public void put(Transition transition) {
        ensureInitialized();
        ensureActionIsMapped(transition.getAction());

        Gson gson = new Gson();
        String message = gson.toJson(new NeuralNetPutTransitionMessage(
                transition.getTransformedState(),
                actionMap.get(transition.getAction()),
                transition.getReward(),
                transition.getTransformedNextState()
        ));

        externalComms.send(message);
        externalComms.receive();
    }

    public void createBackup(String identifier) {
        ensureInitialized();

        Gson gson = new Gson();
        String message = gson.toJson(new NeuralNetCreateBackupMessage(identifier));

        externalComms.send(message);
        externalComms.receive();
    }

    public void close() {
        Gson gson = new Gson();
        String message = gson.toJson(new ExitMessage());
        Log.info("Exit message: " + message);
        externalComms.send(message);

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        externalComms.close();
    }
}

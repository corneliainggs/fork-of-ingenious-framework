package za.ac.sun.cs.ingenious.games.diceGames.dudo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.games.diceGames.actions.DiceRollAction;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.Move;

public class DudoGameState extends GameState implements Serializable {

    public static int numPlayers = 0;
    public static int numSides = 0;
    public static int numDice = 0;
    public static List<List<DiceRollAction>> possibleRolls = new ArrayList<List<DiceRollAction>>(numPlayers);
    public static List<List<DiscreteAction>> possibleActions = new ArrayList<>(numPlayers);

    public static int prettyPrintSpacing = 2;

    public static void initializeStaticObjects(int numPlayers, int numSides, int numDice) {
        DudoGameState.numSides = numSides;
        DudoGameState.numDice = numDice;
        DudoGameState.numPlayers = numPlayers;

        possibleRolls = new ArrayList<>(numPlayers);
        possibleActions = new ArrayList<>(numPlayers);

        for (int i = 0; i < numPlayers; i++) {
            List playerRolls = new ArrayList<DiceRollAction>(numSides);
            for (int j = 1; j <= numSides; j++) {
                playerRolls.add(new DiceRollAction(i, j));
            }

            possibleRolls.add(playerRolls);
        }

        for (int id = 0; id < numPlayers; id++) {
            List<DiscreteAction> playerActions = new ArrayList<>(numPlayers * numDice * numSides + 1);
            for (int i = 0; i < numPlayers * numDice * numSides + 1; i++) {
                playerActions.add(new DiscreteAction(id, i));
            }
            possibleActions.add(playerActions);
        }
    }

    public static boolean[][][] diceDots = {
            {
                    { false, false, false },
                    { false, true, false },
                    { false, false, false } },
            {
                    { false, false, true },
                    { false, false, false },
                    { true, false, false }
            },
            {
                    { false, false, true },
                    { false, true, false },
                    { true, false, false }
            },
            {
                    { true, false, true },
                    { false, false, false },
                    { true, false, true }
            },
            {
                    { true, false, true },
                    { false, true, false },
                    { true, false, true }
            },
            {
                    { true, true, true },
                    { false, false, false },
                    { true, true, true }
            },
    };

    protected int numActions; // Total number of actions possible in a game
    public int dudo; // Action number for the claim 'Dudo'

    private int round;
    private int movesFromRound;

    private List<Move> previousMoves;
    private Move[][] playerRolls;
    private int[] numPlayerDice; // Number of dice each player has remaining
    private int firstPlayer; // First player of a round

    public DudoGameState(int numPlayers, int numSides, int numDice) {
        super(numPlayers);

        previousMoves = new ArrayList<>();

        if (DudoGameState.numPlayers == 0 ||
                DudoGameState.numSides != numSides || DudoGameState.numDice != numDice) {
            initializeStaticObjects(numPlayers, numSides, numDice);
        }

        numActions = numSides * (numDice * numPlayers) + 1;
        dudo = numActions - 1;

        playerRolls = new Move[numPlayers][numDice];
        numPlayerDice = new int[numPlayers];

        for (int i = 0; i < numPlayers; i++) {
            numPlayerDice[i] = numDice;
        }

        round = 0;
        movesFromRound = 0;

        // firstPlayer = (int)(numPlayers * Math.random());
        firstPlayer = 1;
        // firstPlayer = 0;
    }

    @Override
    public String getPretty() {
        StringBuilder builder = new StringBuilder();

        // builder.append(" Previous hash: " + this.prevStateHash + "\n");
        // builder.append(" Hashcode: " + this.hashCode() + "\n");
        // builder.append("Prevous actions: " + this.previousMoves).append("\n");

        for (int i = 0; i < numPlayers; i++) {
            builder.append(" Player ").append(i + 1).append(":\n");
            buildPlayerDice(builder, numPlayerDice[i], playerRolls[i]);
        }

        int cells = remainingDice() * numSides + 1;
        int perCell = 5 + (prettyPrintSpacing - 1);
        builder.append(' ').append("-".repeat(cells * perCell + (cells - 1))).append("\n");

        for (int n = 0; n < remainingDice(); n++) {
            for (int r = 1; r < numSides; r++) {
                builder.append("| ")
                        .append(getPrettyClaim(n * numSides + r))
                        .append(" ")
                        .append(" ".repeat(prettyPrintSpacing - 1));
            }
            builder.append("| ")
                    .append(getPrettyClaim(n * numSides))
                    .append(" ")
                    .append(" ".repeat(prettyPrintSpacing - 1));
        }
        builder.append("| Dudo").append(" ".repeat(prettyPrintSpacing - 1)).append("|\n");
        builder.append(' ').append("=".repeat(cells * perCell + (cells - 1))).append("\n");

        String trueChar = "✓";
        String falseChar = " ";

        boolean[] claimHistory = getClaimHistory();

        for (int n = 0; n < remainingDice(); n++) {
            for (int r = 1; r < numSides; r++) {
                builder.append("|  ").append((claimHistory[n * numSides + r])
                        ? trueChar
                        : falseChar).append(" ".repeat(1 + prettyPrintSpacing));
            }
            builder.append("|  ").append((claimHistory[n * numSides])
                    ? trueChar
                    : falseChar).append(" ".repeat(1 + prettyPrintSpacing));
        }
        builder.append("|  ").append((claimHistory[numActions - 1])
                ? trueChar
                : falseChar).append(" ".repeat(1 + prettyPrintSpacing)).append("|\n");

        builder.append(' ').append("=".repeat(cells * perCell + (cells - 1))).append("\n");

        return builder.toString();
    }

    private void buildPlayerDice(StringBuilder builder, int numDice, Move[] playerRolls) {
        if (numDice == 0) {
            return;
        }

        for (int i = 0; i < numDice; i++) {
            builder.append("\t _______  ");
        }
        builder.append("\n");

        for (int r = 0; r < 3; r++) {
            for (int d = 0; d < numDice; d++) {

                builder.append("\t| ");

                if (playerRolls[d] instanceof DiceRollAction) {
                    int roll = ((DiceRollAction) playerRolls[d]).getRoll();
                    for (int c = 0; c < 3; c++) {
                        builder.append((diceDots[roll - 1][r][c]) ? '•' : ' ').append(" ");
                    }
                } else if (playerRolls[d] == null || r != 1) {
                    builder.append("      ");
                } else {
                    builder.append("  ?   ");
                }

                builder.append("| ");
            }
            builder.append("\n");
        }

        for (int i = 0; i < numDice; i++) {
            builder.append("\t ‾‾‾‾‾‾‾  ");
        }

        builder.append("\n");
    }

    public String getPrettyClaim(int i) {
        if (i == dudo) {
            return "Dudo";
        }

        int[] numRank = toNumRank(i);
        return numRank[0] + "x" + numRank[1];
    }

    public int remainingDice() {
        int sum = 0;
        for (int i = 0; i < numPlayerDice.length; i++) {
            sum += numPlayerDice[i];
        }
        return sum;
    }

    /**
     * Returns a tuple of the previous claim number and the claim rank
     */
    public int[] toNumRank(int i) {
        return new int[] {
                Math.floorDiv(i, numSides) + 1,
                i % numSides + 1
        };
    }

    public void resetRound() {
        for (int i = 0; i < numPlayers; i++) {
            for (int j = 0; j < numDice; j++) {
                playerRolls[i][j] = null;
            }
        }

        this.round++;
        this.movesFromRound = 0;
    }

    public int getRemainingPlayers() {
        int count = 0;

        for (int i : numPlayerDice) {
            if (i > 0)
                count++;
        }

        return count;
    }

    protected int toActionNumber(int n, int r) {
        n = n - 1;
        r = r - 1;

        return (n * numSides) + r;
    }

    public Move[][] getPlayerRolls() {
        return playerRolls;
    }

    public int[] getNumPlayerDice() {
        return numPlayerDice;
    }

    protected boolean[] getClaimHistory() {
        boolean[] claimHistory = new boolean[numActions];

        // Only include actions from current round
        for (int i = previousMoves.size() - movesFromRound; i < previousMoves.size(); i++) {
            Move move = previousMoves.get(i);

            if (move instanceof DiscreteAction) {
                claimHistory[((DiscreteAction) move).getActionNumber()] = true;
            }
        }

        return claimHistory;
    }

    protected void makeMove(Move move) {
        this.previousMoves.add(move);
        this.movesFromRound++;
    }

    protected List<Move> getPreviousMoves() {
        return previousMoves;
    }

    protected void setPreviousMoves(List<Move> moves) {
        this.previousMoves = moves;
    }

    protected Move getPreviousMove() {
        if (previousMoves.size() <= 1) {
            return null;
        }

        return this.previousMoves.get(this.previousMoves.size() - 2);

    }

    protected Move getLatestMove() {
        if (previousMoves.size() == 0) {
            return null;
        }
        return this.previousMoves.get(this.previousMoves.size() - 1);
    }

    public int getFirstPlayer() {
        return firstPlayer;
    }

    public void setFirstPlayer(int firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    public void setNumPlayerDice(int[] numPlayerDice) {
        this.numPlayerDice = numPlayerDice;
    }

    @Override
    public DudoGameState deepCopy() {
        DudoGameState newGameState = new DudoGameState(this.numPlayers, numSides, numDice);

        newGameState.previousMoves.addAll(this.previousMoves);

        newGameState.round = this.round;
        newGameState.movesFromRound = this.movesFromRound;

        for (int i = 0; i < numPlayers; i++) {
            System.arraycopy(this.playerRolls[i], 0, newGameState.playerRolls[i], 0, numDice);
        }
        newGameState.numPlayerDice = this.numPlayerDice.clone();

        newGameState.setFirstPlayer(this.getFirstPlayer());

        return newGameState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }

        DudoGameState state = (DudoGameState) o;
        return this.previousMoves.equals(state.previousMoves);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + this.previousMoves.hashCode();

        return result;
    }

    @Override
    public String toString() {
        return getPretty();
    }
}
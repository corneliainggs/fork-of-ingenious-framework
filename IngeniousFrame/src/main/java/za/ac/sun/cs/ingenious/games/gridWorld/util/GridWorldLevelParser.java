package za.ac.sun.cs.ingenious.games.gridWorld.util;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldLogic;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Parser that creates GridWorld from a file.
 *
 * @author Steffen Jacobs
 */
public class GridWorldLevelParser {
    /**
     * Indicates the separator between each column. Primarily used so that the level is readable in plaintext.
     */
    public static final char LEVEL_TOKEN_SPACER = ' ';
    public static final char LEVEL_TOKEN_EMPTY = ' ';
    public static final char LEVEL_TOKEN_UNREACHABLE = 'X';
    public static final char LEVEL_TOKEN_START = 'S';
    public static final char LEVEL_TOKEN_PORTAL = 'P';
    public static final Set<Character> RESERVED_TOKENS = new HashSet<>(
            Arrays.asList(
                    LEVEL_TOKEN_SPACER,
                    LEVEL_TOKEN_EMPTY,
                    LEVEL_TOKEN_UNREACHABLE,
                    LEVEL_TOKEN_START,
                    LEVEL_TOKEN_PORTAL
            )
    );

    public static final String LEVEL_TOKEN_REWARD_REGEX = "[A-Z]";

    public static final String LEVEL_SECTION_REGEX = "[a-zA-Z]+:";
    public static final String LEVEL_SECTION_BOARD = "Board:";
    public static final String LEVEL_SECTION_REWARDS = "Rewards:";
    public static final String LEVEL_SECTION_WIND = "Wind:";

    private int width;
    private int height;
    private double stepReward;
    private int[][] level;
    private int navigationType;

    private Set<Character> terminalDefinitions;
    private Set<Coord> terminalCoordinates;

    private Map<Character, Double> rewardDefinitions;
    private Map<Coord, Double> rewards;

    private Set<Coord> portalCoordinates;

    private Map<Integer, WindDefinition> columnWindIntensity;
    private Map<Integer, WindDefinition> rowWindIntensity;

    public GridWorldLevelParser() {
        terminalDefinitions = new HashSet<>();
        terminalCoordinates = new HashSet<>();
        rewardDefinitions = new HashMap<>();
        rewards = new HashMap<>();
        portalCoordinates = new HashSet<>();
        rowWindIntensity = new HashMap<>();
        columnWindIntensity = new HashMap<>();
    }

    public GridWorldLevelParser(MatchSetting matchSetting) throws MissingSettingException, IncorrectSettingTypeException {
        terminalDefinitions = new HashSet<>();
        terminalCoordinates = new HashSet<>();
        rewardDefinitions = new HashMap<>();
        rewards = new HashMap<>();
        portalCoordinates = new HashSet<>();
        rowWindIntensity = new HashMap<>();
        columnWindIntensity = new HashMap<>();

        try {
            if (matchSetting.hasSetting("levelFile")) {
                String levelFilePath = matchSetting.getSettingAsString("levelFile");
                createFromFile(levelFilePath);
            } else if (matchSetting.hasSetting("levelContent")) {
                /*
                 * Specifying the level content raw in the match setting JSON is not recommended. This initialization is
                 * intended only to support hardcoded level strings used by JUnit tests.
                 * See: search.rl.testData.ReinforcementLearningTestData.cliffLevel
                 */
                String levelContent = matchSetting.getSettingAsString("levelContent");
                createFromString(levelContent);
            }
        } catch (MissingSettingException | IncorrectSettingTypeException e) {
            Log.error("Could not read settings from received MatchSetting object, message: " + e.getMessage());

            throw e;
        }
    }

    public GridWorldLevelParser usingFile(String filePath) {
        this.createFromFile(filePath);
        return this;
    }

    public GridWorldLevelParser usingString(String level) {
        this.createFromString(level);
        return this;
    }

    private void createFromFile(String filePath) {
        try {
            Scanner scanner = new Scanner(Paths.get(filePath));

            // Force locale so that floating point values are parsed with "." as the separator.
            scanner.useLocale(Locale.ENGLISH);

            this.parse(scanner);

            scanner.close();
        } catch (IOException e) {
            fatalError("GridWorldLevelParser.createFromFile: GridWorld level could not be read from " + filePath + ".", e);
        }
    }

    private void createFromString(String level) {
        Scanner scanner = new Scanner(level);

        // Force locale so that floating point values are parsed with "." as the separator.
        scanner.useLocale(Locale.ENGLISH);

        this.parse(scanner);

        scanner.close();
    }

    /**
     * Parses the level using the provided scanner instance.
     * @param scanner
     */
    private void parse(Scanner scanner) {
        while (scanner.hasNext(LEVEL_SECTION_REGEX)) {
            String section = scanner.next(LEVEL_SECTION_REGEX);

            switch (section) {
                case LEVEL_SECTION_REWARDS:
                    readRewardIdentifiers(scanner);
                    break;
                case LEVEL_SECTION_BOARD:
                    readBoard(scanner);
                    break;
                case LEVEL_SECTION_WIND:
                    readWind(scanner);
                    break;
            }
        }
    }

    /**
     * Reads in identifiers that represent different rewards and terminal states. This follows the following format:
     * <pre>[identifier] = [rewardValue] [isTerminal]</pre>
     *
     * For example two reward identifiers, the first having 15.0 reward and defined as terminal, and with the second
     * having -10000.0 reward and defined as terminal:
     * <pre>
     * Rewards:
     * A = 15.0 1
     * D = -10000.0 1
     * </pre>
     *
     * @param scanner
     */
    private void readRewardIdentifiers(Scanner scanner) {
        while (scanner.hasNext() && !scanner.hasNext(LEVEL_SECTION_REGEX)) {
            if (scanner.hasNext(LEVEL_TOKEN_REWARD_REGEX)) {
                char identifier = scanner.next(LEVEL_TOKEN_REWARD_REGEX).charAt(0);
                scanner.next("=");

                double reward = scanner.nextDouble();
                boolean isTerminal = scanner.nextInt() > 0;

                if (RESERVED_TOKENS.contains(identifier)) {
                    fatalError("GridWorldLevelParser.readRewards: Unable to use reserved identifier `" + identifier + "`.", null);
                }

                rewardDefinitions.put(identifier, reward);
                if (isTerminal) terminalDefinitions.add(identifier);
            } else {
                fatalError("GridWorldLevelParser.readRewards: Expected reward descriptor `" + LEVEL_TOKEN_REWARD_REGEX + "`.", null);
            }
        }
    }

    /**
     * Reads the board of the level, with header information containing step reward and navigation type. Width and
     * height are determined automatically.
     *
     * For example, a board with -1.0 step reward and cardinal navigation:
     * <pre>
     * Board:
     * -1.0 0
     * X X X X X X X X
     * X             X
     * X             X
     * X             X
     * X S X D D X A X
     * X X X D D X X X
     * </pre>
     * @param scanner
     */
    private void readBoard(Scanner scanner) {
        stepReward = scanner.nextDouble();
        navigationType = scanner.nextInt();

        if (navigationType < 0 || navigationType >= GridWorldLogic.NavigationType.values().length) {
            fatalError("GridWorldLevelParser.readBoard: Invalid navigation type `" + navigationType + "`.", null);
        }

        ArrayList<Integer> cells = new ArrayList<>();

        int row = 1;    // Starting on the first row, keep track of number of rows parsed.
        int col = 0;    // Column in current row.
        int parseWidth = 0;  // Width derived from max value of col.

        // Move scanner onto the first row of the board to parse.
        scanner.nextLine();

        // Continue parsing the board until EOF or next section.
        while (scanner.hasNext() && !scanner.hasNext(LEVEL_SECTION_REGEX)) {
            // Take first character in the row. If end of line, this is null.
            String token = scanner.findInLine(".");

            if (token != null) { // Convert the single character string to a character.
                char nextChar = token.charAt(0);

                if (nextChar == LEVEL_TOKEN_EMPTY) {
                    cells.add(GridWorldState.CELL_REACHABLE);
                } else if (nextChar == LEVEL_TOKEN_UNREACHABLE) {
                    cells.add(GridWorldState.CELL_UNREACHABLE);
                } else if (nextChar == LEVEL_TOKEN_START) {
                    cells.add(GridWorldState.DEFAULT_FIRST_PLAYER);
                } else if (nextChar == LEVEL_TOKEN_PORTAL) {
                    cells.add(GridWorldState.CELL_REACHABLE);
                    portalCoordinates.add(new Coord(col, row - 1));
                } else if (rewardDefinitions.containsKey(nextChar)) {
                    Coord coord = new Coord(col, row - 1);

                    cells.add(GridWorldState.CELL_REACHABLE);
                    rewards.put(coord, rewardDefinitions.get(nextChar));

                    if (terminalDefinitions.contains(nextChar))
                        terminalCoordinates.add(coord);
                }

                // Get the next character and ignore it, as its the spacer between columns.
                scanner.findInLine(".");

                // Maintain column count.
                col++;
            } else {
                // Move scanner to the next row.
                scanner.nextLine();
                // Keep track of how many columns were in the previous row, before col is reset.
                parseWidth = col;
                // Reset col to 0 for new row.
                col = 0;
                // Bump row count.
                row++;
            }
        }

        height = row;
        width = parseWidth;

        // Convert linear cell list into the board matrix.
        level = new int[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                level[i][j] = cells.get((i * width) + j);
            }
        }
    }

    /**
     * Reads the wind dynamics of the board. This follows the following format:
     * <pre>[Row | Column] [index] [intensity] [isStochastic]</pre>
     *
     * Stochastic wind alters the default behaviour by increasing the intensity by 1 with probability 0.25, or
     * decreasing the intensity with probability 0.25. This probability is not currently dynamic, and is instead
     * hardcoded.
     *
     * For example, column index 4 with intensity -2 (upwards with intensity 2) non-stochastic, and row index 0 with
     * intensity 3 to the right with stochastic behaviour:
     * <pre>
     * Wind:
     * Column 4 -2 0
     * Row 0 3 1
     * </pre>
     * @param scanner
     */
    private void readWind(Scanner scanner) {
        while (scanner.hasNext() && !scanner.hasNext(LEVEL_SECTION_REGEX)) {
            String direction = scanner.next();
            Map<Integer, WindDefinition> windIntensity = new HashMap<>();

            if (direction.equalsIgnoreCase("row")) {
                windIntensity = this.rowWindIntensity;
            } else if (direction.equalsIgnoreCase("column")) {
                windIntensity = this.columnWindIntensity;
            } else {
                fatalError("GridWorldLevelParser.readWind: Invalid direction `" + direction + "` specified.", null);
            }

            int directionIndex = scanner.nextInt();
            int intensity = scanner.nextInt();
            int stochasticBehaviour = scanner.nextInt();
            windIntensity.put(directionIndex, new WindDefinition(intensity, stochasticBehaviour != 0));
        }
    }

    /**
     * Prints the error message and exception trace (if applicable), then exits the client.
     *
     * @param message Error message to log.
     * @param e Exception resulting in the error (if applicable).
     */
    public void fatalError(String message, Exception e) {
        Log.error(message);
        if (e != null) e.printStackTrace();

        System.exit(-1);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public double getStepReward() {
        return stepReward;
    }

    public int[][] getLevel() {
        return level;
    }

    public Set<Coord> getTerminalCoordinates() {
        return terminalCoordinates;
    }

    public Map<Coord, Double> getRewards() {
        return rewards;
    }

    public int getNavigationType() {
        return navigationType;
    }

    public Set<Coord> getPortalCoordinates() {
        return portalCoordinates;
    }

    public Map<Integer, WindDefinition> getColumnWindIntensity() {
        return columnWindIntensity;
    }

    public Map<Integer, WindDefinition> getRowWindIntensity() {
        return rowWindIntensity;
    }

    public static class WindDefinition {
        private int intensity;
        private boolean hasStochasticBehaviour;

        public WindDefinition(int intensity, boolean hasStochasticBehaviour) {
            this.intensity = intensity;
            this.hasStochasticBehaviour = hasStochasticBehaviour;
        }

        public int getIntensity() {
            return intensity;
        }

        public boolean hasStochasticBehaviour() {
            return hasStochasticBehaviour;
        }
    }
}

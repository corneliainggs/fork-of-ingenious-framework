package za.ac.sun.cs.ingenious.search.mcts.backpropagation;

import java.util.List;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

public class BackpropagationSimple<N extends MctsNodeTreeParallelInterface<?, ?, ?>>
		implements BackpropagationThreadSafe<N> {

	public int player;

	public BackpropagationSimple(int player) {
		this.player = player;
	}

	/**
	 * Update the fields relating to this backpropagation strategy for the current
	 * node.
	 * 
	 * @param node
	 * @param results the win/loss or draw outcome for the simulation relating to
	 *                the current playout.
	 */
	public void propagate(N node, double[] results, List<Action> moves) {
		node.incVisitCount();
		double result = results[((N) node.getParent()).getPlayerID()];
		node.addValue(result);
	}

	public static <NN extends MctsNodeTreeParallelInterface<?, ?, ?>> BackpropagationSimple<NN> newBackpropagationSimple(
			int player) {
		return new BackpropagationSimple(player);
	}

}
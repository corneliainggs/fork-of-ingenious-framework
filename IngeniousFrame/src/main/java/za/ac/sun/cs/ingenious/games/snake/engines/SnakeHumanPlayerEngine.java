package za.ac.sun.cs.ingenious.games.snake.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.snake.SnakeEngine;
import za.ac.sun.cs.ingenious.games.snake.SnakeFeatureExtractor;
import za.ac.sun.cs.ingenious.games.snake.SnakeState;

import java.util.List;
import java.util.Scanner;

/**
 * Human player engine for Snake.
 *
 * @author Steffen Jacobs
 */
public class SnakeHumanPlayerEngine extends SnakeEngine {
    private Scanner scanner;
    private SnakeState previousState = null;
    private Action previousAction = null;

    public SnakeHumanPlayerEngine(EngineToServerConnection toServer) {
        super(toServer);
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "SnakeHumanPlayerEngine";
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        scanner = new Scanner(System.in);
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        List<Action> availableActions = logic.generateActions(state, playerID);

        StringBuilder promptStringBuilder = new StringBuilder();

        promptStringBuilder.append("\nAvailable actions:\n");

        for (int i = 0; i < availableActions.size(); i++) {
            promptStringBuilder.append(i).append(" ").append(availableActions.get(i)).append("\n");
        }

        promptStringBuilder.append("Choose an action: ");

        System.out.println(promptStringBuilder);

        try {
            int actionNumberChosen = scanner.nextInt();

            while (actionNumberChosen < 0 || actionNumberChosen >= availableActions.size()) {
                System.out.println("Invalid action.");
                actionNumberChosen = scanner.nextInt();
            }

            Action choice = availableActions.get(actionNumberChosen);

            return new PlayActionMessage(choice);
        } catch (Exception e) {
            Log.error("An error occurred while reading user input.");
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        scanner.close();
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        super.receiveRewardMessage(a);
        ScalarReward reward = (ScalarReward) a.getReward();
        Log.info("Received reward: " + reward);
    }
}

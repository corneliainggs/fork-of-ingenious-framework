package za.ac.sun.cs.ingenious.core.util.search;

import za.ac.sun.cs.ingenious.core.GameState;

class SearchNodeSimple<S extends GameState> implements SearchNode<S> {

	private final S state;
	private double value;

	private SearchNodeSimple(S state) { // private because it should be uninstantiable
		this.state = stateDeepCopy(state);
		this.value = 0;
	}

	public static <SS extends GameState> SearchNodeSimple<SS> newInstance(SS state) { // Since private constructor => //
																						// factory method
		return new SearchNodeSimple<SS>(state);
	}

	public final S getState(boolean getDefensiveCopy) {
		if (getDefensiveCopy) {
			return stateDeepCopy(this.state);
		} else {
			return state;
		}
	}

	public final double getValue() {
		return value;
	}

	public final void addValue(double add) {
		this.value += add;
	}

	public String toString() {
		return this.state.toString();
	}

	private final S stateDeepCopy(S state) {
		return (S) state.deepCopy();
	}
}

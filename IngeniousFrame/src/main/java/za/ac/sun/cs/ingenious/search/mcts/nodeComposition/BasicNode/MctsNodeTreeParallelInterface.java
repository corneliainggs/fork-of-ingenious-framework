package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;

import java.util.Hashtable;

public interface MctsNodeTreeParallelInterface<S extends GameState, C, P>
		extends MctsNodeCompositionInterface<S, C, P> {

	double getVirtualLoss();

	void applyVirtualLoss();

	void restoreVirtualLoss();

	void readLockEnhancementClassesArrayList();

	void readUnlockEnhancementClassesArrayList();

	Hashtable<String, MctsNodeExtensionParallelInterface> getEnhancementClasses();

	void writeLock();

	void writeUnlock();

	int getDepth();

	double getScore();

	void logPossibleMoves();

	int getPlayerID();

}

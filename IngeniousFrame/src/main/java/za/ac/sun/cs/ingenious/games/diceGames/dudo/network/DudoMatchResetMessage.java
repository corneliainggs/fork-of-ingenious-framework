package za.ac.sun.cs.ingenious.games.diceGames.dudo.network;

import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;

/**
 * Message to be sent to players to indicate that the game is done and that the
 * state on the
 * server is going to be reset
 *
 * Contains the scores for each player from the game that just ended.
 */
public class DudoMatchResetMessage extends MatchResetMessage {

    private double[] scores;

    public DudoMatchResetMessage(double[] scores) {
        this.scores = scores;
    }

    public double[] getScores() {
        return scores;
    }
}

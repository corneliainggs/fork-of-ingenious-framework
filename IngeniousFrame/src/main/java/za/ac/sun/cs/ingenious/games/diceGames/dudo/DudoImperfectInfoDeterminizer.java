package za.ac.sun.cs.ingenious.games.diceGames.dudo;

import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;

import java.util.ArrayList;
import java.util.List;

public class DudoImperfectInfoDeterminizer implements InformationSetDeterminizer<DudoGameState> {
    private DudoLogic logic;
    private int m; // Number of previous moves to remember

    public DudoImperfectInfoDeterminizer(int m, DudoLogic logic) {
        this.m = m;
        this.logic = logic;
    }

    @Override
    public DudoGameState determinizeUnknownInformation(DudoGameState forState, int forPlayerID) {
        return logic.determinizeUnknownInformation(forState, forPlayerID);
    }

    @Override
    public DudoGameState observeState(DudoGameState state, int forPlayerID) {
        DudoGameState obState = logic.observeState(state, forPlayerID);

        List<Move> previousMoves = state.getPreviousMoves();
        List<Move> moves = new ArrayList<>();

        for (int i = Math.max(0, previousMoves.size() - m); i < previousMoves.size(); i++) {
            moves.add(previousMoves.get(i));
        }

        obState.setPreviousMoves(moves);

        return obState;
    }
}

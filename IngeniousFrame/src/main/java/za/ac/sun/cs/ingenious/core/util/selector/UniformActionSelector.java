package za.ac.sun.cs.ingenious.core.util.selector;

import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;

/**
 * Action selector that chooses an action uniformly from a list of stochastic
 * actions.
 *
 * Implements the Singleton pattern.
 */
public class UniformActionSelector implements ActionSelector {
    private static UniformActionSelector singleton = null;
    private Random rand;

    private UniformActionSelector() {
        rand = new Random();
    }

    @Override
    public <S extends GameState, L extends GameLogic> Action getAction(S fromGameState, L logic) {
        Set<Action> actions = logic.generateStochasticActions(fromGameState).keySet();

        int randI = rand.nextInt(actions.size());

        Iterator<Action> it = actions.iterator();
        Action a = it.next();
        for (int i = 0; i < randI; i++) {
            a = it.next();
        }

        return a;
    }

    public static UniformActionSelector getInstance() {
        if (singleton == null) {
            singleton = new UniformActionSelector();
        }

        return singleton;
    }
}
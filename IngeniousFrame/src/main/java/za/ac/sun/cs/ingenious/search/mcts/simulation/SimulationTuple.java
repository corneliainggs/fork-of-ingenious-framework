package za.ac.sun.cs.ingenious.search.mcts.simulation;

import za.ac.sun.cs.ingenious.core.Action;
import java.util.List;

public class SimulationTuple {

    private final List<Action> moves;
    private final double[] scores;

    public SimulationTuple(List<Action> moves, double[] scores) {
        this.moves = moves;
        this.scores = scores;
    }

    public double[] getScores() {
        return scores;
    }

    public List<Action> getMoves() {
        return moves;
    }

}

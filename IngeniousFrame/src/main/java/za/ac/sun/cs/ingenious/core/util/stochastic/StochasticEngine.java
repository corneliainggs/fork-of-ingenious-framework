package za.ac.sun.cs.ingenious.core.util.stochastic;

import com.esotericsoftware.minlog.Log;

import java.util.concurrent.ThreadLocalRandom;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.selector.ActionSelector;

/**
 * Engine that runs alongside the referee and is used produce stochastic moves
 * for the game
 * environment
 */
public class StochasticEngine<S extends GameState, L extends GameLogic<S>> extends Engine {
    // Used for checking whether an engine in the lobby is the stochastic player.
    public static final String engineName = "StochasticPlayer1";

    protected L logic;
    protected S state;
    private ThreadLocalRandom rand = ThreadLocalRandom.current();
    private ActionSelector selector;

    public StochasticEngine(EngineToServerConnection toServer, L logic, S state, ActionSelector selector) {
        super(toServer);
        this.logic = logic;
        this.state = (S) state.deepCopy();
        this.selector = selector;
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String engineName() {
        return engineName;
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        this.logic.makeMove(this.state, a.getMove());
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        return new PlayActionMessage(selector.getAction(state, logic));
    }

    public void serverDisconnected() {
        Log.error("Engine", "Server disconnected!");
    }
}
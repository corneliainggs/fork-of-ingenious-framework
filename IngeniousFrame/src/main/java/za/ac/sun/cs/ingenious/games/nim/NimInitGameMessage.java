package za.ac.sun.cs.ingenious.games.nim;

import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;

public class NimInitGameMessage extends InitGameMessage {

    private static final long serialVersionUID = 1L;

    private int numRows;

    public NimInitGameMessage(int numRows) {
        this.numRows = numRows;
    }

    public int getNumRows() {
        return numRows;
    }
}
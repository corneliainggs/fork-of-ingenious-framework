package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.games.go.GoBoard;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.games.ingenious.Constants;

public class MastTable {

    // The maximum number of entries in the cache was found experimentally by
    // running for different powers of 2
    private final Map<Action, MastStoredAction> moveCombinationScores = new HashMap<>();
    private final ConcurrentHashMap<Integer, Double> cachedMastValues = new ConcurrentHashMap<>(
            (int) (Constants.MAST_MAX_CACHE_SIZE / 0.75), (float) 0.75);
    private ReentrantLock combinationScoresLock = new ReentrantLock();
    private double tau, gamma;

    public MastTable(double gamma, double tau) {
        this.gamma = gamma;
        this.tau = tau;
    }

    public void updateVisitedMovesTable(List<Action> moveList, double[] scores) {
        for (Action action : moveList) {
            double score = scores[action.getPlayerID()];
            // lock the table to prevent two threads from adding the same action
            combinationScoresLock.lock();
            MastStoredAction actionFromTable = moveCombinationScores.get(action);
            // Add new action to table
            if (actionFromTable == null) {
                MastStoredAction newStoredAction = new MastStoredAction(action);
                // update the action
                newStoredAction.addResult(score);
                moveCombinationScores.put(action, newStoredAction);
                // release the lock as the new action has been added
                combinationScoresLock.unlock();
            } else {
                // the action is already in the table so we can release the lock
                combinationScoresLock.unlock();
                // lock the action to prevent two threads from updating the same action
                actionFromTable.writeLock();
                // update the action
                actionFromTable.addResult(score);
                // release the lock as the action has been updated
                actionFromTable.writeUnlock();
            }
        }
    }

    /**
     * Returns a random action, biased towards actions with a higher win to visit
     * ratio based on the Gibbs distribution
     * 
     * @param actions
     * @return a random action
     */
    public <S extends GameState> Action getMastAction(GameLogic<S> logic, S state, int playerId,
            ThreadLocalRandom randomGen) {
        // if a random double is greater than gamma return a random action
        if (randomGen.nextDouble() > gamma) {
            if (state instanceof GoBoard) {
                return (Action) ((GoBoard) state).getSingleMove(playerId);
            } else {
                List<Action> possibleActions = logic.generateActions(state, playerId);
                return possibleActions.get(randomGen.nextInt(possibleActions.size()));
            }
        }
        // otherwise return a biased random action according to the MAST algorithm
        return getMastAction(logic.generateActions(state, playerId), randomGen);
    }

    public Action getMastAction(List<Action> actions, ThreadLocalRandom randomGen) {
        if (actions.isEmpty()) {
            Log.warn("MastTable", "No actions to choose from");
            return null;
        }

        double[] mastValues = new double[actions.size()];
        double cumulativeMastValue = 0.0;
        for (int i = 0; i < actions.size(); i++) {
            Action action = actions.get(i);
            MastStoredAction actionFromTable = moveCombinationScores.get(action);
            // if the action has not yet been visited, give it a probability of 1.0, i.e.
            // return the action
            if (actionFromTable == null) {
                return action;
            }
            // increment the rolling value
            cumulativeMastValue += actionFromTable.getMastValue(tau, cachedMastValues);
            // add the rolling value to the list of probabilities
            mastValues[i] = cumulativeMastValue;
        }
        // return a randomly selected action, biased based on the probabilities
        // we multiply by the cumulativeMastValue as this has the same effect as
        // normalising the mast values we calculated above
        double randomValue = randomGen.nextDouble() * cumulativeMastValue;
        for (int i = 0; i < mastValues.length; i++) {
            if (randomValue <= mastValues[i]) {
                return actions.get(i);
            }
        }
        // It should not be possible to get to this point in the code
        Log.error("MastTable", "No action selected");
        throw new RuntimeException("No action selected");
    }

    /**
     * refresh the hashmap to a clean empty hashmap
     */
    public void resetVisitedMoves() {
        // moveCombinationScores = new ConcurrentHashMap<>(16, (float) 0.75, 16);
        // changed to clear for efficiency
        moveCombinationScores.clear();
    }

    /**
     * @return the tau value parameter specified in the engine
     */
    protected double getTau() {
        return tau;
    }

    public <S extends GameState> void logTableStatistics(MctsNodeTreeParallel<S> node) {
        if (moveCombinationScores.size() == 0 || !Log.DEBUG) {
            return;
        }
        double[] cumulativeMastValues = { 0.0 };

        List<MctsNodeTreeParallel<S>> actions = node.getChildren().stream().map(c -> {
            Action previousAction = c.getPrevAction();
            MastStoredAction storedAction = moveCombinationScores.get(previousAction);
            // if there is no entry for the action, return null - we will filter these out
            // and not consider them
            if (storedAction == null) {
                return null;
            }
            cumulativeMastValues[0] += storedAction.getMastValue(tau, cachedMastValues);
            return c;
            // filter out null values and collect the actions into a list
        }).filter(Objects::nonNull).collect(Collectors.toList());
        StringBuilder sb = new StringBuilder();
        sb.append("\n===================");
        actions.stream().sorted((c1, c2) -> {
            // we don't have to account for null values here as we have already filtered
            // them
            return Double.compare(moveCombinationScores.get(c2.getPrevAction()).getMastValue(tau, cachedMastValues),
                    moveCombinationScores.get(c1.getPrevAction()).getMastValue(tau, cachedMastValues));
        }).forEach(c -> {
            Action action = c.getPrevAction();
            MastStoredAction storedAction = moveCombinationScores.get(action);
            double gibbsProbability = storedAction.getMastValue(tau, cachedMastValues) / cumulativeMastValues[0];
            double winrate = storedAction.getAverageScore();
            sb.append("\n\t" + action + " Mast winrate: " + storedAction.getValue() + "/" + storedAction.getVisitCount()
                    + " (" + Math.round(winrate * 10_000.0) / 100.0 + "%) with gibbs probability = "
                    + Math.round(gibbsProbability * 1_000_000.0) / 10_000.0 + "% ; mcts wins/visits : " + c.getValue()
                    + "/" + c.getVisitCount() + " (" + Math.round(c.getValue() / c.getVisitCount() * 10_000.0) / 100.0
                    + "%)");
        });
        sb.append("\n===================");
        Log.debug("Mast stats:", sb.toString());
    }
}

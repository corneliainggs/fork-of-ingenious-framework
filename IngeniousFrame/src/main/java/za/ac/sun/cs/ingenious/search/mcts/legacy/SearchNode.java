package za.ac.sun.cs.ingenious.search.mcts.legacy;

import com.esotericsoftware.minlog.Log;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.ismcts.ISMCTSNode;

/**
 * Describes nodes in an MCTS search TreeEngine. Can be extended for different
 * search algorithms. For example,
 * plain MCTS uses {@link MCTSNode}. SOISMCTS uses {@link ISMCTSNode}.
 *
 * @param <S> The type of TurnBasedGameState to operate on.
 */
public abstract class SearchNode<S extends TurnBasedGameState, N extends SearchNode<S, N>> {

	protected int depth;
	protected S currentState;
	protected List<N> expandedChildren;
	protected Move parentToThisMove;
	protected N parent;
	protected int visits;
	protected double[] rewards;
	protected List<Move> unexpandedMoves;

	protected GameLogic<S> logic;

	/**
	 * Creates a new search TreeEngine node.
	 * 
	 * @param thisNodeState The GameState in this node.
	 * @param logic         Object containing the game logic.
	 * @param toThisNode    The move that leads to this node.
	 * @param parent        The parent node, null if this node is root.
	 */
	public SearchNode(S thisNodeState, GameLogic<S> logic, Move toThisNode, N parent, List<Move> unexpandedMoves) {
		this.logic = logic;
		this.parent = parent;
		if (parent != null) {
			this.depth = parent.getDepth() + 1;
		} else {
			this.depth = 0;
		}
		this.visits = 0;
		this.parentToThisMove = toThisNode;
		this.currentState = thisNodeState;
		this.expandedChildren = new LinkedList<N>();
		this.rewards = new double[currentState.getNumPlayers()];
		this.unexpandedMoves = unexpandedMoves;
	}

	/**
	 * @return Depth of this node in the search TreeEngine. 0 is the depth of the
	 *         TreeEngine root.
	 */
	public int getDepth() {
		return depth;
	}

	/**
	 * @return The game state / information set that this node represents.
	 */
	public S getGameState() {
		return currentState;
	}

	/**
	 * @return The player that is going to act in this node.
	 */
	public int getCurrentPlayer() {
		return currentState.nextMovePlayerID;
	}

	/**
	 * @return True if this node contains a terminal game state.
	 */
	public boolean isTerminal() {
		return logic.isTerminal(currentState);
	}

	/**
	 * @return The already expanded children of this node.
	 */
	public List<N> getExpandedChildren() {
		return expandedChildren;
	}

	/**
	 * @return The move that lead to this node.
	 */
	public Move getMove() {
		return parentToThisMove;
	}

	/**
	 * @return The parent of this node in the search TreeEngine. 'null' indicates
	 *         that this is the
	 *         root node.
	 */
	public N getParent() {
		return parent;
	}

	/**
	 * @return The amount of times that this node has been visited.
	 */
	public int getVisits() {
		return visits;
	}

	/**
	 * Increase the value of getVisits() by 1
	 */
	public void increaseVisits() {
		this.visits++;
	}

	/**
	 * @return The total reward scored by choosing this node per player.
	 */
	public double[] getReward() {
		return rewards;
	}

	/**
	 * Increases the reward per player by the corresponding entry in d.
	 * 
	 * @param d Array of values by which to increase the reward of the players.
	 */
	public void addReward(double[] d) {
		if (d.length != rewards.length) {
			Log.error("search.mcts.SimpleNode",
					"Cannot add reward array with " + d.length + " players; expected " + rewards.length + " players.");
			return;
		}
		for (int i = 0; i < rewards.length; i++) {
			rewards[i] += d[i];
		}
	}

	/**
	 * @return A list of moves. This is empty after construction and can be filled
	 *         with moves that are note yet expanded from the current node.
	 */
	public List<Move> getUnexpandedMoves() {
		return unexpandedMoves;
	}

	/**
	 * Adds the given child to the list of expanded children.
	 * 
	 * @param node Child to add.
	 */
	public void addChild(N node) {
		expandedChildren.add(node);
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder("SearchNode - D: " + depth + ",	P: " + getCurrentPlayer());
		if (parentToThisMove != null) {
			s.append(",	M: " + parentToThisMove.toString());
		}
		s.append(",	Stat: (" + Arrays.toString(rewards) + "	|	" + visits + ")");
		return s.toString();
	}
}

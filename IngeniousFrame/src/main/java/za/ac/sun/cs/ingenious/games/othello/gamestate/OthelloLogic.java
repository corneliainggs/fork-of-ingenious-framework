package za.ac.sun.cs.ingenious.games.othello.gamestate;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.move.ForfeitAction;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The GameLogic implementation for the Othello board game.
 *
 * @author Rudolf Stander
 */
public class OthelloLogic implements TurnBasedGameLogic<OthelloBoard> {

	public static OthelloLogic defaultOthelloLogic = new OthelloLogic();

	/**
	 * Checks whether the given move can be played in the given state
	 */
	@Override
	public boolean validMove(OthelloBoard fromState, Move move) {
		if (move == null) {
			return false;
		}

		if (move instanceof XYAction) {
			XYAction xyMove = (XYAction) move;
			int x = xyMove.getX();
			int y = xyMove.getY();

			if (xyMove.getPlayerID() != fromState.getCurrentPlayer()) {
				return false;
			}

			/* Check if the move position is valid */
			if (x < 0 || x >= fromState.getBoardWidth() || y < 0 || y >= fromState.getBoardHeight()) {
				return false;
			}

			/* Cannot place a disk on a non-empty position */
			if (fromState.getGrid()[fromState.xyToIndex(x, y)] != OthelloBoard.EMPTY) {
				return false;
			}

			/*
			 * Check if there is a line of the opponent's disks between the
			 * position and any other disk of the player
			 */
			return formsLine(fromState, xyMove.getPlayerID(), y, x);
		} else if (move instanceof IdleAction) {
			if (((IdleAction) move).getPlayerID() != fromState.getCurrentPlayer()) {
				return false;
			}

			/* A player may only remain idle if there are no possible moves */
			return !canMove(fromState, fromState.getCurrentPlayer());
		} else if (move instanceof ForfeitAction) {
			return true;
		}

		return false;
	}

	/**
	 * If the move is valid for the given game state, performs the move and
	 * updates the state of the board.
	 *
	 * @param move the move to be made
	 *
	 * @return true if the move was performed, false otherwise
	 */
	@Override
	public boolean makeMove(OthelloBoard fromState, Move move) {
		return makeMove(fromState, move, true);
	}

	/**
	 * If the check argument is true, performs a validity check on the move and
	 * performs
	 * the move only if the move is valid; otherwise, performs the move regardless.
	 *
	 * @param move  the move to be made
	 * @param check whether to perform validity checks or not
	 *
	 * @return true if the move was performed, false otherwise
	 */
	public boolean makeMove(OthelloBoard fromState, Move move, boolean check) {
		if (move == null) {
			return false;
		}

		if (check && !validMove(fromState, move)) {
			return false;
		}

		if (move instanceof IdleAction) {
			fromState.updateCurrentPlayer();

			return true;
		} else if (move instanceof ForfeitAction) {
			return true;
		}

		XYAction xyMove = (XYAction) move;
		int x = xyMove.getX();
		int y = xyMove.getY();
		int player = xyMove.getPlayerID();

		/* Check if the move position is valid */
		if (x < 0 || x >= fromState.getBoardWidth() || y < 0 || y > fromState.getBoardHeight()) {
			return false;
		}

		byte board[] = fromState.getGrid();

		/* Get all the opponent's disks that should be flipped and flip them */
		int flippedDisks = flipDisks(fromState, player, y, x);

		if (flippedDisks == 0) {
			return false;
		}

		board[fromState.xyToIndex(x, y)] = (byte) player;

		/* Update other board state info */
		fromState.updateCurrentPlayer();

		if (player == OthelloBoard.BLACK) {
			fromState.blackScore += flippedDisks + 1;
			fromState.whiteScore -= flippedDisks;
		} else {
			fromState.whiteScore += flippedDisks + 1;
			fromState.blackScore -= flippedDisks;
		}

		return true;
	}

	/**
	 * Reverts the given move from the given state; the move need not be that last
	 * one that was applied
	 */
	@Override
	public void undoMove(OthelloBoard fromState, Move move) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Generates a list of all the moves that are possible (and valid) for a
	 * specific player given the current board state.
	 *
	 * @param player the player for who moves are generated
	 *
	 * @return a list of all valid moves the specific player can
	 *         perform next
	 */
	@Override
	public List<Action> generateActions(OthelloBoard fromState, int player) {
		List<Action> possibleActions = new ArrayList<Action>();
		return (List) generateActions(fromState, player, possibleActions);
	}

	/**
	 * Generates a list of all the moves that are possible (and valid) for a
	 * specific player given the current board state, or returns true if a valid
	 * move is found.
	 *
	 * @param player the player for who moves are generated
	 *
	 * @return a list of all valid moves the specific player can
	 *         perform next; or true if a valid move is found, or false
	 *         otherwise
	 */
	private Object generateActions(OthelloBoard fromState, int player, Object obj) {
		byte board[] = fromState.getGrid();

		boolean checkedForLines[][] = new boolean[fromState.getBoardHeight()][fromState.getBoardWidth()];

		for (int y = 0; y < fromState.getBoardHeight(); y++) {
			for (int x = 0; x < fromState.getBoardWidth(); x++) {
				if (board[fromState.xyToIndex(x, y)] != (byte) (player ^ 1)) {
					continue;
				}

				/* Test all the empty spots around an opponent's disk */
				for (int i = -1; i <= 1; i++) {
					if (y + i < 0 || y + i >= fromState.getBoardHeight()) {
						continue;
					}

					for (int j = -1; j <= 1; j++) {
						if (x + j < 0 || x + j >= fromState.getBoardWidth()) {
							continue;
						}

						if (i == 0 && j == 0) {
							continue;
						}

						if (board[fromState.xyToIndex(x + j, y + i)] != OthelloBoard.EMPTY) {
							continue;
						}

						if (!checkedForLines[y + i][x + j]) {
							if (formsLine(fromState, player, y + i, x + j)) {
								if (obj instanceof List) {
									((List) obj).add(new XYAction(x + j, y + i, player));
								} else {
									return Boolean.TRUE;
								}
							}
							checkedForLines[y + i][x + j] = true;
						}
					}
				}
			}
		}

		if (obj instanceof List) {
			if (((List) obj).isEmpty()) {
				((List) obj).add(new IdleAction((byte) player));
			}
			return obj;
		} else {
			return Boolean.FALSE;
		}
	}

	/**
	 * Determines if the board is in a final (terminal) state, thus no more moves
	 * can be made by either player.
	 *
	 * @return true if the board is in a terminal state,
	 *         false otherwise
	 */
	@Override
	public boolean isTerminal(OthelloBoard fromState) {
		byte board[] = fromState.getGrid();
		boolean filled = true;

		/* Check if all blocks are filled */
		for (int i = 0; i < board.length; i++) {
			if (board[i] != OthelloBoard.EMPTY) {
				filled = false;
				break;
			}
		}

		if (filled) {
			return true;
		}

		/* Check if the black player can move */
		if (canMove(fromState, OthelloBoard.BLACK)) {
			return false;
		}

		/* Check if the white player can move */
		if (canMove(fromState, OthelloBoard.WHITE)) {
			return false;
		}

		return true;
	}

	/*************************************************************************
	 * Helper Functions *
	 ************************************************************************/

	/**
	 * Determines if the specific player can make a move by checking if placing
	 * a disk on any empty block around the opposing player's disks will form
	 * a line to another of the specified player's disks.
	 *
	 * @param player the player whose move possibilities are checked
	 *
	 * @return true if the player can make a move, false otherwise
	 */
	public boolean canMove(OthelloBoard fromState, int player) {
		return (Boolean) generateActions(fromState, player, Boolean.TRUE);
	}

	/**
	 * Checks if there is a line of the opponent's disks between the given
	 * position and any other disk of the specified player.
	 *
	 * @param player the player whose position is evaluated
	 * @param y      the y position to evaluate
	 * @param x      the x position to evaluate
	 *
	 * @return true if there is a line, false otherwise
	 */
	private boolean formsLine(OthelloBoard fromState, int player, int y, int x) {
		byte board[] = fromState.getGrid();
		boolean foundOpponentDisk = false;

		/* Check upwards from the position */
		for (int i = y - 1; i >= 0; i--) {
			if (board[fromState.xyToIndex(x, i)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(x, i)] != ((byte) player)) {
				foundOpponentDisk = true;
			} else if (foundOpponentDisk) {
				return true;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		foundOpponentDisk = false;

		/* Check downwards from the position */
		for (int i = y + 1; i < fromState.getBoardHeight(); i++) {
			if (board[fromState.xyToIndex(x, i)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(x, i)] != ((byte) player)) {
				foundOpponentDisk = true;
			} else if (foundOpponentDisk) {
				return true;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		foundOpponentDisk = false;

		/* Check leftwards from the position */
		for (int i = x - 1; i >= 0; i--) {
			if (board[fromState.xyToIndex(i, y)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(i, y)] != ((byte) player)) {
				foundOpponentDisk = true;
			} else if (foundOpponentDisk) {
				return true;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		foundOpponentDisk = false;

		/* Check rightwards from the position */
		for (int i = x + 1; i < fromState.getBoardWidth(); i++) {
			if (board[fromState.xyToIndex(i, y)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(i, y)] != ((byte) player)) {
				foundOpponentDisk = true;
			} else if (foundOpponentDisk) {
				return true;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		foundOpponentDisk = false;

		/* Check diagonally upwards left from the position */
		for (int i = 1; i < fromState.getNumCells(); i++) {
			int row = y - i;
			int col = x - i;

			if (row < 0 || col < 0) {
				break;
			}

			if (board[fromState.xyToIndex(col, row)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(col, row)] != ((byte) player)) {
				foundOpponentDisk = true;
			} else if (foundOpponentDisk) {
				return true;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		foundOpponentDisk = false;

		/* Check diagonally upwards right from the position */
		for (int i = 1; i < fromState.getNumCells(); i++) {
			int row = y - i;
			int col = x + i;

			if (row < 0 || col >= fromState.getBoardWidth()) {
				break;
			}

			if (board[fromState.xyToIndex(col, row)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(col, row)] != ((byte) player)) {
				foundOpponentDisk = true;
			} else if (foundOpponentDisk) {
				return true;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		foundOpponentDisk = false;

		/* Check diagonally downwards left from the position */
		for (int i = 1; i < fromState.getNumCells(); i++) {
			int row = y + i;
			int col = x - i;

			if (row >= fromState.getBoardHeight() || col < 0) {
				break;
			}

			if (board[fromState.xyToIndex(col, row)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(col, row)] != ((byte) player)) {
				foundOpponentDisk = true;
			} else if (foundOpponentDisk) {
				return true;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		foundOpponentDisk = false;

		/* Check diagonally downwards right from the position */
		for (int i = 1; i < fromState.getNumCells(); i++) {
			int row = y + i;
			int col = x + i;

			if (row >= fromState.getBoardHeight() || col >= fromState.getBoardWidth()) {
				break;
			}

			if (board[fromState.xyToIndex(col, row)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(col, row)] != (byte) player) {
				foundOpponentDisk = true;
			} else if (foundOpponentDisk) {
				return true;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		return false;
	}

	/**
	 * Flips all the disks of the opposing player that forms a line between
	 * the given position and any other disk of the specified player.
	 * NOTE: The position must be empty, otherwise the action is invalid and no
	 * disks will be flipped.
	 *
	 * @param player the player whose position is evaluated
	 * @param y      the y position
	 * @param x      the x position
	 *
	 * @return the number of disks flipped
	 */
	private int flipDisks(OthelloBoard fromState, int player, int y, int x) {
		byte board[] = fromState.getGrid();

		if (board[fromState.xyToIndex(x, y)] != OthelloBoard.EMPTY) {
			return 0;
		}

		int flippedDisks = 0;
		boolean foundOpponentDisk = false;
		boolean foundPlayerDisk = false;
		ArrayList<int[]> lineDisks = new ArrayList<int[]>();

		/* Check upwards from the position */
		for (int i = y - 1; i >= 0; i--) {
			if (board[fromState.xyToIndex(x, i)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(x, i)] != (byte) player) {
				foundOpponentDisk = true;
				lineDisks.add(new int[] { i, x });
			} else if (foundOpponentDisk) {
				foundPlayerDisk = true;
				break;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		/* Flip the disks */
		if (foundPlayerDisk) {
			for (int[] disk : lineDisks) {
				board[fromState.xyToIndex(disk[1], disk[0])] = (byte) player;
				flippedDisks++;
			}
		}

		foundPlayerDisk = false;
		foundOpponentDisk = false;
		lineDisks.clear();

		/* Check downwards from the position */
		for (int i = y + 1; i < fromState.getBoardHeight(); i++) {
			if (board[fromState.xyToIndex(x, i)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(x, i)] != (byte) player) {
				foundOpponentDisk = true;
				lineDisks.add(new int[] { i, x });
			} else if (foundOpponentDisk) {
				foundPlayerDisk = true;
				break;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		/* Flip the disks */
		if (foundPlayerDisk) {
			for (int[] disk : lineDisks) {
				board[fromState.xyToIndex(disk[1], disk[0])] = (byte) player;
				flippedDisks++;
			}
		}

		foundPlayerDisk = false;
		foundOpponentDisk = false;
		lineDisks.clear();

		/* Check leftwards from the position */
		for (int i = x - 1; i >= 0; i--) {
			if (board[fromState.xyToIndex(i, y)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(i, y)] != (byte) player) {
				foundOpponentDisk = true;
				lineDisks.add(new int[] { y, i });
			} else if (foundOpponentDisk) {
				foundPlayerDisk = true;
				break;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		/* Flip the disks */
		if (foundPlayerDisk) {
			for (int[] disk : lineDisks) {
				board[fromState.xyToIndex(disk[1], disk[0])] = (byte) player;
				flippedDisks++;
			}
		}

		foundPlayerDisk = false;
		foundOpponentDisk = false;
		lineDisks.clear();

		/* Check rightwards from the position */
		for (int i = x + 1; i < fromState.getBoardWidth(); i++) {
			if (board[fromState.xyToIndex(i, y)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(i, y)] != (byte) player) {
				foundOpponentDisk = true;
				lineDisks.add(new int[] { y, i });
			} else if (foundOpponentDisk) {
				foundPlayerDisk = true;
				break;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		/* Flip the disks */
		if (foundPlayerDisk) {
			for (int[] disk : lineDisks) {
				board[fromState.xyToIndex(disk[1], disk[0])] = (byte) player;
				flippedDisks++;
			}
		}

		foundPlayerDisk = false;
		foundOpponentDisk = false;
		lineDisks.clear();

		/* Check diagonally upwards left from the position */
		for (int i = 1; i < fromState.getNumCells(); i++) {
			int row = y - i;
			int col = x - i;

			if (row < 0 || col < 0) {
				break;
			}

			if (board[fromState.xyToIndex(col, row)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(col, row)] != (byte) player) {
				foundOpponentDisk = true;
				lineDisks.add(new int[] { row, col });
			} else if (foundOpponentDisk) {
				foundPlayerDisk = true;
				break;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		/* Flip the disks */
		if (foundPlayerDisk) {
			for (int[] disk : lineDisks) {
				board[fromState.xyToIndex(disk[1], disk[0])] = (byte) player;
				flippedDisks++;
			}
		}

		foundPlayerDisk = false;
		foundOpponentDisk = false;
		lineDisks.clear();

		/* Check diagonally upwards right from the position */
		for (int i = 1; i < fromState.getNumCells(); i++) {
			int row = y - i;
			int col = x + i;

			if (row < 0 || col >= fromState.getBoardWidth()) {
				break;
			}

			if (board[fromState.xyToIndex(col, row)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(col, row)] != (byte) player) {
				foundOpponentDisk = true;
				lineDisks.add(new int[] { row, col });
			} else if (foundOpponentDisk) {
				foundPlayerDisk = true;
				break;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		/* Flip the disks */
		if (foundPlayerDisk) {
			for (int[] disk : lineDisks) {
				board[fromState.xyToIndex(disk[1], disk[0])] = (byte) player;
				flippedDisks++;
			}
		}

		foundPlayerDisk = false;
		foundOpponentDisk = false;
		lineDisks.clear();

		/* Check diagonally downwards left from the position */
		for (int i = 1; i < fromState.getNumCells(); i++) {

			int row = y + i;
			int col = x - i;

			if (row >= fromState.getBoardHeight() || col < 0) {
				break;
			}

			if (board[fromState.xyToIndex(col, row)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(col, row)] != (byte) player) {
				foundOpponentDisk = true;
				lineDisks.add(new int[] { row, col });
			} else if (foundOpponentDisk) {
				foundPlayerDisk = true;
				break;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		/* Flip the disks */
		if (foundPlayerDisk) {
			for (int[] disk : lineDisks) {
				board[fromState.xyToIndex(disk[1], disk[0])] = (byte) player;
				flippedDisks++;
			}
		}

		foundPlayerDisk = false;
		foundOpponentDisk = false;
		lineDisks.clear();

		/* Check diagonally downwards right from the position */
		for (int i = 1; i < fromState.getNumCells(); i++) {
			int row = y + i;
			int col = x + i;

			if (row >= fromState.getBoardHeight() || col >= fromState.getBoardWidth()) {
				break;
			}

			if (board[fromState.xyToIndex(col, row)] == OthelloBoard.EMPTY) {
				break;
			} else if (board[fromState.xyToIndex(col, row)] != (byte) player) {
				foundOpponentDisk = true;
				lineDisks.add(new int[] { row, col });
			} else if (foundOpponentDisk) {
				foundPlayerDisk = true;
				break;
			} else { /* A player's disk was found before an opponnent's disk */
				break;
			}
		}

		/* Flip the disks */
		if (foundPlayerDisk) {
			for (int[] disk : lineDisks) {
				board[fromState.xyToIndex(disk[1], disk[0])] = (byte) player;
				flippedDisks++;
			}
		}

		return flippedDisks;
	}
}

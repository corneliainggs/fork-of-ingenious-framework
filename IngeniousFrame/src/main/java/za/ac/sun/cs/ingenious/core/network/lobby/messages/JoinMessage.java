package za.ac.sun.cs.ingenious.core.network.lobby.messages;

import za.ac.sun.cs.ingenious.core.network.Message;


/**
 * Sent by the client while still in lobby phase to join into a lobby with specific name.
 * 
 * @author Stephan Tietz
 */
public class JoinMessage extends Message {

	private static final long serialVersionUID = 1L;

	private String lobbyName;
	
	/**
	 * Creates a new JoinMessage for the specified lobby
	 */
	public JoinMessage(String lobbyName) {
		this.lobbyName = lobbyName;
	}
	
	public String getLobbyName() {
		return lobbyName;
	}
}

package za.ac.sun.cs.ingenious.search.mcts.MctsAlgorithm;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.go.GoBoard;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelection;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionFinal;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationTuple;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 * Implements a general-purpose TreeEngine parallelism version of the MCTS
 * algorithm. This implementation uses Threads
 * and is for multicore systems with shared memory. Due to the shared memory
 * nature of this algorithm, the MCTS node
 * type that can be used for this algorithm should store the child and parent of
 * the node as references to the node
 * structure.
 *
 * @author Karen Laubscher
 *
 * @param <S> The game state type
 * @param <N> The MCTS node type
 */
public class MctsTree<S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> {

	private TreeSelection<N> selection;
	private ExpansionThreadSafe<N, N> expansion;
	private SimulationThreadSafe<S> simulation;
	private ArrayList<BackpropagationThreadSafe<N>> backpropagationEnhancements;
	private TreeSelectionFinal<N> finalSelection;
	private GameLogic<S> logic;
	private int threadCount;
	int[] playoutCounts;
	private int playerId;
	private long totalPlayoutsOverGame = 0;
	private int numberTurns = 0;
	// Most of the time the overhead is around 20-40ms (based on the game,
	// boardsize, etc. and found experimentally on a fast processor), but to prevent
	// the couple of outliers from skewing results, we use a conservative estimate
	// of 50ms
	// Note that with exceedingly high load the overhead may take upward of 150ms
	private static final int OVERHEAD_TIME_MS = 50;

	/**
	 * Constructor that takes in the 4 basic strategies as well as a final selection
	 * strategy, that may differ from the
	 * selection strategy used when traversing the TreeEngine during the MCTS
	 * algorithm.
	 *
	 * @param selection                   The selection strategy. A common strategy
	 *                                    to use is {@link TreeSelection},
	 *                                    which calculates UCT values.
	 * @param expansion                   The expansion strategy. A common strategy
	 *                                    to use is {@link ExpansionSingle},
	 *                                    which adds one unexplored node
	 * @param simulation                  The simulation strategy. A basic play out
	 *                                    policy is to use
	 *                                    {@link SimulationRandom}, which do random
	 *                                    playout.
	 * @param backpropagationEnhancements The backpropagation strategy. A common
	 *                                    strategy to use is
	 *                                    {@link BackpropagationAverage}, just
	 *                                    adding the result
	 * @param finalSelection              The selection strategy used to make the
	 *                                    final selection to determine the best
	 *                                    move to be returned.
	 * @param logic                       The game logic, used to check for ternimal
	 *                                    gamestates.
	 * @param threadCount                 The number of TreeEngine threads that
	 *                                    should be created.
	 */
	public MctsTree(TreeSelection<N> selection, ExpansionThreadSafe<N, N> expansion, SimulationThreadSafe<S> simulation,
			ArrayList<BackpropagationThreadSafe<N>> backpropagationEnhancements, TreeSelectionFinal<N> finalSelection,
			GameLogic<S> logic, int threadCount, int playerId) {
		this.selection = selection;
		this.expansion = expansion;
		this.simulation = simulation;
		this.backpropagationEnhancements = backpropagationEnhancements;
		this.finalSelection = finalSelection;
		this.logic = logic;
		this.threadCount = threadCount;
		this.playerId = playerId;
		playoutCounts = new int[this.threadCount];
	}

	/**
	 * This method generates an action that represents the best move choice found.
	 * The method forms part of the template
	 * operation that drives the TreeEngine parallelisation MCTS algorithm, in
	 * particular, it is the main thread
	 * operation, that creates the TreeEngine threads, and at the end, performs the
	 * final selection from the shared root
	 * node.
	 *
	 * @param root       The root node from which the search should start.
	 * @param turnlength Time in ms within which the move needs to be decided.
	 *
	 * @return The best move action that was found by the algorithm during the time
	 *         of the turn length.
	 */

	public N doSearch(N root, long turnlength, ZobristHashing zobrist, int simsPerPlayout) {
		Log.set(Log.LEVEL_DEBUG);
		long endTime = System.nanoTime() + Math.max(turnlength - OVERHEAD_TIME_MS, turnlength) * 1_000_000;
		List<TreeThread> threads = new ArrayList<TreeThread>(threadCount);
		for (int i = 0; i < threadCount; i++) {
			TreeThread th = new TreeThread(root, endTime, simsPerPlayout);
			threads.add(th);
			th.start();
		}

		for (Thread th : threads) {
			try {
				long timeRemaining = endTime - System.nanoTime();
				long timeRemainingMillis = timeRemaining / 1_000_000;
				if (timeRemaining < 0) {
					Log.warn("MctsTree", "Not all TreeThreads returned in time, continuing with final selection phase");
					break;
				}
				th.join(timeRemainingMillis);
			} catch (InterruptedException e) {
				Log.warn("MctsTree",
						"Master thread interrupted while waiting for workers to finish, continuing with final selection phase");
			}
		}

		N finalChoice = finalSelection.select(root, zobrist);

		// print some statistics via a thread to decrease the time it takes to return
		// the move
		new Thread(() -> {
			logTreeStatistics(root, finalChoice, threads);
		}).start();

		return finalChoice;
	}

	/**
	 * This method prints some post-search statistics.
	 * 
	 * @param root
	 * @param finalChoice
	 * @param threads
	 */
	public void logTreeStatistics(N root, N finalChoice, List<TreeThread> threads) {

		printTreeOverview(root, 10);
		printPossibleMoves(root, playerId);
		printTree(root, 3);

		Action bestMove = finalChoice.getPrevAction();

		if (bestMove != null) {
			Log.debug("MctsTree", "Best action found: " + bestMove.toString() + " "
					+ Math.round(finalChoice.getScore() * 10_000.0) / 100.0 + "%");
		} else {
			Log.debug("MctsTree", "No best action found.");
		}

		int totalPlayoutCount = 0;
		for (Thread th : threads) {
			int currentThreadPlayoutCount = ((TreeThread) th).getPlayoutCount();
			totalPlayoutCount += currentThreadPlayoutCount;
			Log.debug("Number of playouts over thread with id " + th.getId() + " is: " + currentThreadPlayoutCount);
		}
		Log.debug("Total number of playouts over all threads: " + totalPlayoutCount);
		totalPlayoutsOverGame += totalPlayoutCount;
		numberTurns += 1;
		Log.debug("numTurns: " + numberTurns);
		Log.debug("Average number of playouts per turn to this point in the game: "
				+ (totalPlayoutsOverGame / numberTurns));

		Log.debug("Best Move: " + bestMove + "\n===================");
	}

	/**
	 * Print the TreeEngine up to the specified depth. This generates excessive
	 * printout.
	 *
	 * @param root     The root of the TreeEngine.
	 * @param maxDepth Depth at which to cut off the TreeEngine printout.
	 */
	public static <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> void printTree(N root,
			int maxDepth) {
		if (!Log.TRACE) {
			return;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("\n===================");
		sb.append("\nCurrent Search Tree:");
		sb.append("\n===================");

		Queue<N> list = new LinkedList<>();
		list.add(root);
		while (!list.isEmpty()) {
			N n = list.poll();

			if (n.getDepth() >= maxDepth) {
				break;
			}

			sb.append("\n" + n.toString());
			sb.append("\nDepth: " + n.getDepth());

			// print board
			sb.append("\nBoard state:");
			sb.append("\n" + n.getState(false).getPretty());

			list.addAll(n.getChildren());
		}
		sb.append("\n===================");
		Log.trace("MCTS Tree", sb.toString());
	}

	/**
	 * Helper method to nicely print the first level of the game TreeEngine.
	 *
	 * @param root The root of the TreeEngine. All moves leading out from this
	 *             TreeEngine will be printed.
	 */
	public static <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> void printPossibleMoves(
			N root, int playerId) {
		if (Log.DEBUG) {
			root.logPossibleMoves();
		}
	}

	/**
	 * Prints an overview over the amount of nodes in the search TreeEngine to the
	 * specified depth.
	 *
	 * @param root     The root of the TreeEngine.
	 * @param maxDepth Depth at which to cut off the TreeEngine overview.
	 */
	public static <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> void printTreeOverview(N root,
			int maxDepth) {
		if (!Log.DEBUG) {
			return;
		}

		StringBuilder sb = new StringBuilder();

		sb.append("\n===================");
		sb.append("\nCurrent search TreeEngine overview:");
		sb.append("\n===================");

		int[] count = new int[maxDepth];

		Stack<N> stack = new Stack<>();
		stack.add(root);
		while (!stack.isEmpty()) {
			N n = stack.pop();

			if (n.getDepth() >= maxDepth) {
				// don't add children to stack if we're at/past the max depth
				continue;
			}

			count[n.getDepth()]++;
			stack.addAll(n.getChildren());
		}

		for (int i = 0; i < count.length; i++) {
			sb.append("\n\tdepth " + i + ": " + count[i] + " nodes");
		}

		sb.append("\n===================");

		Log.debug("MCTS Tree Overview", sb.toString());
	}

	/**
	 * The TreeEngine thread that performs MCTS on the shared TreeEngine, This forms
	 * part of the template operation that
	 * drives the TreeEngine parallelisation.
	 */
	private class TreeThread extends Thread {
		// root is shared
		private final N myRoot;
		private final long myEndTime;
		private int simsPerPlayout;
		int numberOfPlayouts;

		public TreeThread(N root, long endTime, int simsPerPlayout) {
			this.myRoot = root;
			this.myEndTime = endTime;
			this.numberOfPlayouts = 0;
			this.simsPerPlayout = simsPerPlayout;
		}

		/**
		 * @return number of playouts made by this thread
		 */
		public int getPlayoutCount() {
			return numberOfPlayouts;
		}

		@Override
		public void run() {
			SimulationTuple resultsTuple;
			double[] results;
			List<Action> moves, fullMovePath;
			while (myEndTime > System.nanoTime()) {
				moves = new ArrayList<>();
				numberOfPlayouts++;
				N currentNode = myRoot;
				currentNode.writeLock();
				while (!logic.isTerminal(currentNode.getState(false))) {
					N previousNode = currentNode;
					currentNode = selection.select(previousNode);
					if (currentNode == null) {
						currentNode = previousNode;
						break;
					} else {
						moves.add(currentNode.getPrevAction());
						currentNode.applyVirtualLoss();
					}
					currentNode.writeLock();
					previousNode.writeUnlock();
				}
				N addedNode = expansion.expand(currentNode);
				// if the expansion phase returns the same node, the state is terminal/there are
				// no more unexplored
				// actions. In this case, we don't want to apply virtual loss twice.
				if (addedNode != currentNode) {
					moves.add(addedNode.getPrevAction());
					addedNode.applyVirtualLoss();
				}
				currentNode.writeUnlock();
				for (int sim = 0; sim < simsPerPlayout; sim++) {
					S state = addedNode.getState(true);
					// set the simulation property if the state is a GoBoard to prevent looping
					// during simulation
					if (state instanceof GoBoard) {
						((GoBoard) state).setSimulation();
					}
					resultsTuple = simulation.simulate(moves, state);
					results = resultsTuple.getScores();
					// get the moves from the simulation
					fullMovePath = resultsTuple.getMoves();
					// verify that no aliasing took place
					if (fullMovePath == moves) {
						Log.error("TreeThread", "MoveList-alias detected in simulation phase");
						throw new RuntimeException("MoveList-alias detected in simulation phase");
					}
					// perform backpropagation step
					currentNode = addedNode;
					while (currentNode != myRoot) { // Pseudo: while (currentNode element of TreeEngine)
						if (sim == simsPerPlayout - 1) {
							currentNode.restoreVirtualLoss();
						}
						for (BackpropagationThreadSafe<N> backpropEnhancement : backpropagationEnhancements) {
							backpropEnhancement.propagate(currentNode, results, fullMovePath);
						}
						currentNode = currentNode.getParent();
					}
					myRoot.incVisitCount();
				}
			}
		}
	}
}

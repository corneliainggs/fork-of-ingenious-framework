package za.ac.sun.cs.ingenious.games.diceGames.dudo;

import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.games.diceGames.actions.DiceRollAction;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.selector.UniformActionSelector;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.util.move.UnobservedMove;

import java.util.TreeSet;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class DudoLogic implements InformationSetDeterminizer<DudoGameState>, GameLogic<DudoGameState> {

    // TODO - Issue #295: Make general for n players
    static Set<Integer>[] playerIDSets = new Set[] {
            new TreeSet<Integer>(List.of(0)), // player 1
            new TreeSet<Integer>(List.of(1)), // player 2
            new TreeSet<Integer>(List.of(2)), // stochastic player
            new TreeSet<Integer>(List.of(-1)), // no player. For terminal nodes
    };

    UniformActionSelector selector = UniformActionSelector.getInstance();

    @Override
    public DudoGameState determinizeUnknownInformation(DudoGameState forState, int forPlayerID) {
        DudoGameState newState = forState.deepCopy();

        Move[][] rolls = newState.getPlayerRolls();
        for (int i = 0; i < rolls.length; i++) {
            for (int j = 0; j < rolls[i].length; j++) {
                if (rolls[i][j] != null && rolls[i][j] instanceof UnobservedMove) {
                    rolls[i][j] = selector.getAction(newState, this);
                }
            }
        }
        return newState;
    }

    @Override
    public DudoGameState observeState(DudoGameState state, int forPlayerID) {
        DudoGameState newState = state.deepCopy();

        if (forPlayerID == state.getNumPlayers()) {
            return newState;
        }

        List<Move> newMoves = new ArrayList<>();

        for (Move move : newState.getPreviousMoves()) {
            if (move instanceof DiceRollAction && move.getPlayerID() != forPlayerID) {
                newMoves.add(new UnobservedMove(move.getPlayerID()));
            } else {
                newMoves.add(move);
            }
        }

        newState.setPreviousMoves(newMoves);

        Move[][] rolls = newState.getPlayerRolls();
        for (int i = 0; i < rolls.length; i++) {
            for (int j = 0; j < rolls[i].length; j++) {
                if (rolls[i][j] != null && rolls[i][j].getPlayerID() != forPlayerID &&
                        rolls[i][j] instanceof DiceRollAction) {
                    rolls[i][j] = new UnobservedMove(rolls[i][j].getPlayerID());
                }
            }
        }

        return newState;
    }

    @Override
    public boolean validMove(DudoGameState fromState, Move move) {
        if (move instanceof DiceRollAction) {
            int rollNum = ((DiceRollAction) move).getRoll();

            return rollNum >= 1 && rollNum <= DudoGameState.numSides;
        } else if (move instanceof UnobservedMove) {
            return true;
        } else {
            DiscreteAction action = (DiscreteAction) move;

            // If beginning of the game without at least two dice rolls
            if (fromState.getPreviousMove() == null || fromState.getLatestMove() == null) {
                return false;
            }

            // If beginning of a round without at least two dice rolls
            if (fromState.getPreviousMove() instanceof DiscreteAction
                    && ((DiscreteAction) fromState.getPreviousMove()).getActionNumber() == fromState.dudo) {
                return false;
            }

            // If first player node, is action within correct range
            if (fromState.getPreviousMove() instanceof DiceRollAction
                    && fromState.getLatestMove() instanceof DiceRollAction) {
                return action.getActionNumber() >= 0 && action.getActionNumber() < fromState.dudo;
            }

            // If not the first player node and a claim of dudo.
            if (fromState.getLatestMove() instanceof DiscreteAction
                    && action.getActionNumber() == fromState.dudo) {
                return true;
            }

            int[] previousNumRank = fromState.toNumRank(
                    ((DiscreteAction) fromState.getLatestMove()).getActionNumber());
            int[] currentNumRank = fromState.toNumRank(action.getActionNumber());

            if (Arrays.equals(currentNumRank, previousNumRank)) {
                return false;
            }

            if (currentNumRank[0] == previousNumRank[0]) {
                if (currentNumRank[1] == 1 || currentNumRank[1] > previousNumRank[1]) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return currentNumRank[0] > previousNumRank[0];
            }
        }
    }

    @Override
    public boolean makeMove(DudoGameState fromState, Move move) {
        fromState.makeMove(move);

        if (move instanceof DiceRollAction || move instanceof UnobservedMove) {
            Move[][] rolls = fromState.getPlayerRolls();

            for (int i = 0; i < rolls[move.getPlayerID()].length; i++) {
                if (rolls[move.getPlayerID()][i] == null) {
                    rolls[move.getPlayerID()][i] = move;
                    break;
                }
            }

        } else if (move instanceof DiscreteAction) {
            DiscreteAction latestMove = (DiscreteAction) move;

            fromState.getClaimHistory()[latestMove.getActionNumber()] = true;

            // End of a round
            if (latestMove.getActionNumber() == fromState.dudo) {
                determineResults(fromState, latestMove);
                fromState.resetRound();
                return true;
            }
        }
        return true;
    }

    /**
     * Determines the result at the end of a round. The loser(s) have dice removed
     * and the round
     * winner is set in the game state
     */
    private void determineResults(DudoGameState fromState, DiscreteAction latestMove) {
        DiscreteAction previousMove = (DiscreteAction) fromState.getPreviousMove();

        int numOnes = 0;
        int[] previousNumRank = fromState.toNumRank(previousMove.getActionNumber());
        int rankCount = 0;

        Move[][] playerRolls = fromState.getPlayerRolls();
        for (int i = 0; i < playerRolls.length; i++) {
            Move[] rolls = playerRolls[i];
            for (int j = 0; j < fromState.getNumPlayerDice()[i]; j++) {
                if (rolls[j] == null || rolls[j] instanceof UnobservedMove) {
                    continue;
                }
                DiceRollAction roll = (DiceRollAction) rolls[j];

                if (roll.getRoll() == 1) {
                    numOnes++;
                } else if (roll.getRoll() == previousNumRank[1]) {
                    // Don't need to double count the ones
                    rankCount++;
                }
            }
        }

        if (previousNumRank[1] == 1) {
            rankCount = numOnes;
        } else {
            rankCount += numOnes;
        }

        int[] numPlayerDice = fromState.getNumPlayerDice();
        int winner;
        if (rankCount == previousNumRank[0]) {
            winner = previousMove.getPlayerID();
            for (int i = 0; i < fromState.getNumPlayers(); i++) {
                if (i != winner) { // All but the challenged lose a die
                    numPlayerDice[i] = Math.max(0, numPlayerDice[i] - 1);
                }
            }
        } else if (rankCount > previousNumRank[0]) { // Challenger loses
            winner = previousMove.getPlayerID();
            numPlayerDice[latestMove.getPlayerID()] = Math.max(0, numPlayerDice[latestMove.getPlayerID()] -
                    (rankCount - previousNumRank[0]));
        } else { // Challenged loser
            winner = latestMove.getPlayerID();
            numPlayerDice[previousMove.getPlayerID()] = Math.max(0, numPlayerDice[previousMove.getPlayerID()] -
                    (previousNumRank[0] - rankCount));
        }

        fromState.setFirstPlayer(winner);
    }

    @Override
    public List<Action> generateActions(DudoGameState fromState, int forPlayerID) {
        if (isTerminal(fromState)) {
            return new ArrayList<>();
        }

        List<Action> genActions = new ArrayList(fromState.numActions);

        Move latestMove = fromState.getLatestMove();

        if (latestMove instanceof DiscreteAction) {
            DiscreteAction latestAction = (DiscreteAction) latestMove;

            if (latestAction.getActionNumber() == fromState.dudo) {
                return genActions;
            }

            int[] latestNumRank = fromState.toNumRank(latestAction.getActionNumber());

            int n = latestNumRank[0];
            // One is a wild card and no claim with same number of dice can be played after
            if (latestNumRank[1] != 1) {
                for (int r = latestNumRank[1] + 1; r <= DudoGameState.numSides; r++) {
                    genActions.add(DudoGameState.possibleActions.get(forPlayerID).get(
                            fromState.toActionNumber(n, r)));
                }
                genActions.add(DudoGameState.possibleActions.get(forPlayerID).get(
                        fromState.toActionNumber(n, 1)));
            }

            // Add all claims with higher number of dice
            for (n = n + 1; n <= fromState.remainingDice(); n++) {
                for (int r = 1; r <= fromState.numSides; r++) {
                    genActions.add(DudoGameState.possibleActions.get(forPlayerID).get(
                            fromState.toActionNumber(n, r)));
                }
            }
            genActions.add(DudoGameState.possibleActions.get(forPlayerID).get(fromState.dudo));
        } else {
            for (int n = 1; n <= fromState.remainingDice(); n++) {
                for (int r = 1; r <= fromState.numSides; r++) {
                    genActions.add(DudoGameState.possibleActions.get(forPlayerID).get(
                            fromState.toActionNumber(n, r)));
                }
            }
        }

        return genActions;
    }

    @Override
    public boolean isTerminal(DudoGameState state) {
        for (int i : state.getNumPlayerDice()) {
            if (i == 0)
                return true;
        }
        return false;
    }

    @Override
    public Set<Integer> getCurrentPlayersToAct(DudoGameState fromState) {
        if (isTerminal(fromState)) {
            return playerIDSets[3];
        } else if (getNextStochasticPlayID(fromState) != -1) { // Beginning of round, play stochastic player
            return playerIDSets[2];
        } else {
            if (fromState.getLatestMove() instanceof DiceRollAction
                    || fromState.getLatestMove() instanceof UnobservedMove) { // Play first player at beginner of the
                                                                              // game. Otherwise, the round winner
                return playerIDSets[fromState.getFirstPlayer()];
            } else { // Middle of a round
                return playerIDSets[(fromState.getLatestMove().getPlayerID() + 1) % fromState.getNumPlayers()];
            }
        }
    }

    @Override
    public boolean requiresStochasticPlayer() {
        return true;
    }

    @Override
    public Map<Action, Double> generateStochasticActions(DudoGameState fromState) {
        if (isTerminal(fromState)) {
            return new HashMap<>();
        }

        Map<Action, Double> actionsMap = new HashMap<Action, Double>(fromState.numSides);

        int id = getNextStochasticPlayID(fromState);

        List<DiceRollAction> rolls = DudoGameState.possibleRolls.get(id);

        for (DiceRollAction roll : rolls) {
            actionsMap.put(roll, (1.0 / fromState.numSides));
        }

        return actionsMap;
    }

    /**
     * Returns the ID of a player that has yet to receive a die roll.
     */
    private int getNextStochasticPlayID(DudoGameState fromState) {
        if (isTerminal(fromState)) {
            return -1;
        }

        Move[][] playerRolls = fromState.getPlayerRolls();
        int[] numPlayerDice = fromState.getNumPlayerDice();
        for (int i = 0; i < playerRolls.length; i++) {
            for (int j = 0; j < numPlayerDice[i]; j++) {
                if (playerRolls[i][j] == null) {
                    return i;
                }
            }
        }

        return -1;
    }
}
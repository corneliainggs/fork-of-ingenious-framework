package za.ac.sun.cs.ingenious.core.util.misc;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

/**
 * An enum for representing points on the 16-wind compass rose.  The 16 clasically named points
 * are represented by their traditional abbreviations in clockwise order beginning
 * from north.  For more background, see https://en.wikipedia.org/wiki/Points_of_the_compass .
 *
 * The enum also provides functionality for dealing with certain direction groups as subsets
 * of this collection.
 *
 * @author Steve Kroon
 */
public enum CompassDirection {
    N("north"),
    NNE("north-northeast"),
    NE("northeast"),
    ENE("east-northeast"),
    E("east"),
    ESE("east-southeast"),
    SE("southeast"),
    SSE("south-southeast"),
    S("south"),
    SSW("south-southwest"),
    SW("southwest"),
    WSW("west-southwest"),
    W("west"),
    WNW("west-northwest"),
    NW("northwest"),
    NNW("north-northwest");

    // Consider using ImmutableEnumSet for these - see http://stackoverflow.com/questions/3603810/are-there-plans-for-immutableenumset-in-java-7
    private static Set<CompassDirection> cardinals = Collections.unmodifiableSet(EnumSet.of(N,E,S,W));
    private static Set<CompassDirection> ordinals = Collections.unmodifiableSet(EnumSet.of(NE,SE,SW,NW));
    private static Set<CompassDirection> principals = Collections.unmodifiableSet(EnumSet.of(N,E,S,W, NE,SE,SW,NW));
    
    // Whole name for toString() method.
    private String toStringName;
    
    private CompassDirection(String toStringName){
    	this.toStringName = toStringName;
    }

    /**
     * Returns an unmodifiable set containing the enum elements corresponding to the cardinal directions.
     */
    public static Set<CompassDirection> cardinalDirections() {
        return cardinals;
    }

    /**
     * Returns an unmodifiable set containing the enum elements corresponding to the ordinal directions.
     */
    public static Set<CompassDirection> ordinalDirections() {
        return ordinals;
    }

    /**
     * Returns an unmodifiable set containing the enum elements corresponding to the principal directions.
     */
    public static Set<CompassDirection> principalDirections() {
        return principals;
    }

    /**
     * @return true if the specified direction is a cardinal direction, and false otherwise.
     */
    public boolean isCardinal() {
        return cardinalDirections().contains(this);
    }
    
    /**
     * @return true if the specified direction is an ordinal direction, and false otherwise.
     */
    public boolean isOrdinal() {
        return ordinalDirections().contains(this);
    }
    
    /**
     * @return true if the specified direction is a principal direction, and false otherwise.
     */
    public boolean isPrincipal() {
        return principalDirections().contains(this);
    }

    public CompassDirection getOppositeDirection() {
        if (this.equals(CompassDirection.N)) {
            return S;
        } else if (this.equals(NNE)) {
            return SSW;
        } else if (this.equals(NE)) {
            return SW;
        } else if (this.equals(ENE)) {
            return WSW;
        } else if (this.equals(E)) {
            return W;
        } else if (this.equals(ESE)) {
            return WNW;
        } else if (this.equals(SE)) {
            return NW;
        } else if (this.equals(SSE)) {
            return NNW;
        } else if (this.equals(S)) {
            return N;
        } else if (this.equals(SSW)) {
            return NNE;
        } else if (this.equals(SW)) {
            return NE;
        } else if (this.equals(WSW)) {
            return ENE;
        } else if (this.equals(W)) {
            return E;
        } else if (this.equals(WNW)) {
            return ESE;
        } else if (this.equals(NW)) {
            return SE;
        } else if (this.equals(NNW)) {
            return SSE;
        }

        return null;
    }
    
    /**
     * @return the full name of a direction
     */
    public String toString(){
    	return this.toStringName;
    }

    public String toStringPretty() {
        switch (this) {
            case N:
                return "↑";
            case NE:
                return "↗";
            case E:
                return "→";
            case SE:
                return "↘";
            case S:
                return "↓";
            case SW:
                return "↙";
            case W:
                return "←";
            case NW:
                return "↖";
        }
        return "";
    }
}

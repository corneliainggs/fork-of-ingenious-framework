package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MctsRaveNodeExtensionParallel<S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>>
        implements MctsNodeExtensionParallelInterface {

    // lock for setUp
    private final ReadWriteLock locksetUp = new ReentrantReadWriteLock();
    private final Lock writeLocksetUp = locksetUp.writeLock();
    private final Lock readLocksetUp = locksetUp.readLock();

    // lock for child
    private final ReadWriteLock lockChild = new ReentrantReadWriteLock();
    private final Lock writeLockChild = lockChild.writeLock();
    private final Lock readLockChild = lockChild.readLock();

    MctsRaveNodeExtensionComposition raveNodeExtensionBasic;

    /**
     * Constructor which initialises the extension composition object for which this
     * class is a wrapper
     */
    public MctsRaveNodeExtensionParallel() {
        raveNodeExtensionBasic = new MctsRaveNodeExtensionComposition();
    }

    /**
     * perform the set up of respective fields relating to the type of node for
     * which this class is a wrapper
     * 
     * @param node
     */
    public void setUp(MctsNodeComposition node) {
        writeLocksetUp.lock();
        try {
            raveNodeExtensionBasic.setUp(node);
        } finally {
            writeLocksetUp.unlock();
        }
    }

    /**
     * @return the id of the next player to play for the node object for which this
     *         extension relates
     */
    public String getID() {
        return raveNodeExtensionBasic.getID();
    }

    /**
     * Prints to log the win to visit rate of all children as well as the value
     * attributed to that move according
     * to the relevant implementation (This value would be used during selection to
     * choose an action)
     */
    public <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> String getMoveLogString(N child) {
        return "\n\t\tRave winrate: " + this.getRaveWins(child.getPrevAction()) + "/"
                + this.getRaveVisits(child.getPrevAction()) + "    ("
                + Math.round(100 * this.getRaveWins(child.getPrevAction()) / this.getRaveVisits(child.getPrevAction())
                        * 100.0) / 100.0
                + "%)";
    }

    /**
     * set the read lock for this node extension
     */
    public void setReadLockChild() {
        readLockChild.lock();
    }

    /**
     * unset the read lock for this node extension
     */
    public void unsetReadLockChild() {
        readLockChild.unlock();
    }

    /**
     * @param action
     * @return the visit count rave value for the child relating to the action given
     *         as parameter
     */
    public int getRaveVisits(Action action) {
        return raveNodeExtensionBasic.getRaveVisits(action);
    }

    /**
     * @param action
     * @return the win count rave value for the child relating to the action given
     *         as parameter
     */
    public double getRaveWins(Action action) {
        return raveNodeExtensionBasic.getRaveWins(action);
    }

    /**
     * updates the rave win and visit count fields for children of the current
     * nodes
     * 
     * @param win
     * @param movesOnPath
     */
    public void updateRaveValues(double score, List<Action> movesOnPath, int startIndex) {
        raveNodeExtensionBasic.updateRaveValues(score, movesOnPath, startIndex);
    }

    /**
     * @return action to node mapping for use in the selection phase of the rave
     *         implementation
     */
    public ConcurrentHashMap<Action, MctsNodeTreeParallelInterface<?, ?, ?>> getActionToNodeMapping() {
        return raveNodeExtensionBasic.getActionToNodeMapping();
    }

    /**
     * Update the action to node mapping for use in the selection phase of the rave
     * implementation
     * 
     * @param child
     */
    public <S extends GameState> void addChild(MctsNodeTreeParallel<S> child) {
        raveNodeExtensionBasic.addChild(child);
    }

    /**
     * Update the action to node mapping for use in the selection phase of the rave
     * implementation
     * 
     * @param newChildren
     */
    public <S extends GameState> void addChildren(List<MctsNodeTreeParallel<S>> newChildren) {
        raveNodeExtensionBasic.addChildren(newChildren);
    }
}

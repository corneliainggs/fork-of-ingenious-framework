package za.ac.sun.cs.ingenious.search.cfr;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.exception.dag.StateNotFoundException;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;
import za.ac.sun.cs.ingenious.core.GameState;

import static za.ac.sun.cs.ingenious.search.cfr.Helper.getNextPlayerID;
import static za.ac.sun.cs.ingenious.search.cfr.Helper.getNormalizedDistribution;

/**
 * This implements counterfactual regret minimization for any game. This
 * implementation follows the
 * terminology and pseudocode given in "An Introduction to Counterfactual Regret
 * Minimization" by
 * Todd W. Neller and Marc Lanctot.
 *
 * Note that this algorithm needs to represent all information sets in the game
 * and is therefore only
 * feasible with games with a small number of different information sets.
 *
 * @param <S> TurnBasedGameState this algorithm works with
 */
public class CFR<S extends GameState> {
	private final double exploitThreshold = 0.001;

	private final boolean loadedStrategy;

	private Map<S, Map<Action, Double>> regretTables; // Cumulative regret per information set
	private Map<S, Map<Action, Double>> strategyTables; // The cumulative strategy profile per information set
	private Map<S, Map<Action, Double>> profileT; // The strategy profile (strategies per information set for all
													// players) for this iteration
	private Map<S, Map<Action, Double>> profileTp1; // The strategy profile (strategies per information set for all
													// players) for the next iteration
	private Map<S, Map<Action, Double>> chanceActionCounts; // Frequency of an action being sampled. Double so that it
															// can be normalized

	private S root;
	private InformationSetDeterminizer<S> det;
	private GameLogic<S> logic;
	private GameFinalEvaluator<S> eval;
	private ActionSensor<S> sensor;

	private int chanceSamples;
	private int outcomeSamples;
	private int externalSamples;

	private int trainingIterations;

	private boolean collectExploitability;
	private BestResponse<S> bestResponse;
	private double[] exploitability;

	private boolean collectTrainingTimes;
	private double[] trainingTimes;
	private double totalTrainingTime;

	public CFR(S root, InformationSetDeterminizer<S> det, GameLogic<S> logic,
			GameFinalEvaluator<S> eval, ActionSensor<S> sensor,
			int... numSamples) {
		this.root = root;
		this.det = det;
		this.logic = logic;
		this.eval = eval;
		this.sensor = sensor;

		strategyTables = new HashMap<S, Map<Action, Double>>();
		regretTables = new HashMap<S, Map<Action, Double>>();
		profileT = new HashMap<S, Map<Action, Double>>(); // Time t (initially, t=0)
		profileTp1 = new HashMap<S, Map<Action, Double>>(); // Time t+1

		loadedStrategy = false;

		initSamplingValues(numSamples);

		Log.info("CFR", "Initializing data structures...");
		long startTime = System.nanoTime();
		initTables();
		Log.info("CFR", "Initializing DONE. Time elapsed: " +
				TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - startTime) +
				"s. Number of info sets: " + profileT.size());
	}

	/**
	 * @param strategyTables is stored cumulative strategy profiles. This does not
	 *                       set an initial
	 *                       strategy profile to continue playing from. The strategy
	 *                       profile that
	 *                       is started with is uniform.
	 */
	public CFR(S root, InformationSetDeterminizer<S> det, GameLogic<S> logic,
			GameFinalEvaluator<S> eval, ActionSensor<S> sensor,
			Map<S, Map<Action, Double>> strategyTables, int... numSamples) {

		this.root = root;
		this.det = det;
		this.logic = logic;
		this.eval = eval;
		this.sensor = sensor;

		this.strategyTables = strategyTables;
		regretTables = new HashMap<S, Map<Action, Double>>();
		profileT = new HashMap<S, Map<Action, Double>>(); // Time t (initially, t=0)
		profileTp1 = new HashMap<S, Map<Action, Double>>(); // Time t+1

		loadedStrategy = true;

		Log.info("CFR", "Loading data from saved cumulative strategies");
		// TODO: Get flag from method arguments to determine whether the strategy
		// profiles are initialized to the normalized cumulative strategy.
		initTables(this.strategyTables, false);
		Log.info("CFR", "Loaded " + strategyTables.size() + " information sets");

		initSamplingValues(numSamples);
	}

	public void setCollectExploitability(boolean collectExploitability) {
		this.collectExploitability = collectExploitability;
	}

	public void setCollectTrainingTimes(boolean collectTrainingTimes) {
		this.collectTrainingTimes = collectTrainingTimes;
	}

	private void initSamplingValues(int... numSamples) {
		try {
			chanceSamples = numSamples[0];
			if (chanceSamples <= 0)
				chanceSamples = Integer.MAX_VALUE;
		} catch (IndexOutOfBoundsException | NullPointerException ex) {
			chanceSamples = Integer.MAX_VALUE;
		}

		// TODO: Incorporate outcome sampling into CFR algorithm
		try {
			outcomeSamples = numSamples[1];
			if (outcomeSamples <= 0)
				outcomeSamples = Integer.MAX_VALUE;
		} catch (IndexOutOfBoundsException | NullPointerException ex) {
			outcomeSamples = Integer.MAX_VALUE;
		}

		// TODO: Incorporate external sampling into CFR algorithm
		try {
			externalSamples = numSamples[2];
			if (externalSamples <= 0)
				externalSamples = Integer.MAX_VALUE;
		} catch (IndexOutOfBoundsException | NullPointerException ex) {
			externalSamples = Integer.MAX_VALUE;
		}

		// TODO Issie #301: Include outcome and external sampling
		if (chanceSamples != Integer.MAX_VALUE) { // Visits only necessary with sampling approaches
			chanceActionCounts = new HashMap<>();
		}

		Log.info("CFR", "Sampling a max of " + chanceSamples + " chance node(s)");
		Log.info("CFR", "Sampling a max of " + outcomeSamples + " outcome node(s)");
		Log.info("CFR", "Sampling a max of " + externalSamples + " external node(s)");
	}

	/**
	 * Method used when cumulative strategy profile is loaded from disk and strategy
	 * profiles are to
	 * be initialized with normalized cumulative strategy profile. Initializes the
	 * regretTable and
	 * strategy profiles for the information set that the given state belongs to.
	 */
	private void initTable(S infoSetForActivePlayer, int activePlayerID,
			Map<Action, Double> normProfile) {
		List<Action> actions = logic.generateActions(infoSetForActivePlayer, activePlayerID);

		Map<Action, Double> regretTable = new HashMap<>();

		for (Action a : actions) {
			regretTable.put(a, 0.0);
		}

		regretTables.put(infoSetForActivePlayer, regretTable);
		profileT.put(infoSetForActivePlayer, normProfile);
		profileTp1.put(infoSetForActivePlayer, normProfile);
	}

	/**
	 * Initializes the regretTable, strategyTable etc. for the information set that
	 * the given state belongs to.
	 * The cumulative regret and cumulative strategy are initialized to 0, while the
	 * strategies at this information
	 * set are initialized to a uniform distribution over actions. If the cumulative
	 * regret was loaded from disk,
	 * the new cumulative strategy profile is not used.
	 */
	private void initTable(S infoSetForActivePlayer, int activePlayerID) {
		List<Action> actions = logic.generateActions(infoSetForActivePlayer, activePlayerID);

		Map<Action, Double> regretTable = new HashMap<Action, Double>();
		Map<Action, Double> strategyTable = new HashMap<Action, Double>();
		Map<Action, Double> distT = new HashMap<Action, Double>();
		Map<Action, Double> distTp1 = new HashMap<Action, Double>();
		double initialActionProb = 1.0 / actions.size();
		for (Action a : actions) {
			regretTable.put(a, 0.0);
			strategyTable.put(a, 0.0);
			distT.put(a, initialActionProb);
		}

		regretTables.put(infoSetForActivePlayer, regretTable);
		if (!loadedStrategy) { // If loaded from disk
			strategyTables.put(infoSetForActivePlayer, strategyTable);
		}
		profileT.put(infoSetForActivePlayer, distT);
		profileTp1.put(infoSetForActivePlayer, distTp1);
	}

	/**
	 * Builds the complete game TreeEngine and calls initTable for each encountered
	 * state.
	 * After this method call regretTable, strategyTable etc. exist for each
	 * possible information set in the game.
	 */
	private void initTables() {
		strategyTables = new HashMap<S, Map<Action, Double>>();

		List<S> queue = new LinkedList<S>();
		queue.add(root);

		while (!queue.isEmpty()) {
			S firstState = queue.remove(0);

			List<Action> actions;

			int id = getNextPlayerID(firstState, logic);

			if (id == firstState.getNumPlayers()) {
				actions = new ArrayList<Action>(logic.generateStochasticActions(firstState).keySet());

				if (chanceSamples != Integer.MAX_VALUE) {
					HashMap<Action, Double> actionCounts = new HashMap<Action, Double>();

					for (Action a : actions) {
						actionCounts.put(a, 0.0);
					}

					chanceActionCounts.put(firstState, actionCounts);
				}
			} else {
				S infoSetForActivePlayer = det.observeState(firstState, id);

				if (!(this.regretTables.containsKey(infoSetForActivePlayer) || logic.isTerminal(firstState))) {
					initTable(infoSetForActivePlayer, id);
				}

				actions = logic.generateActions(firstState, id);
			}

			for (Action a : actions) {
				@SuppressWarnings("unchecked")
				S newState = (S) firstState.deepCopy();
				logic.makeMove(newState, sensor.fromPointOfView(a, newState, -1));
				queue.add(newState);
			}
		}
	}

	/**
	 * Initializes the regret and profile tables using the information sets from
	 * stored cumulative
	 * strategies table.
	 *
	 * @param loadedStrategy The loaded cumulative strategy profile.
	 * @param loadProfile    Whether to load the normalized cumulative strategy as
	 *                       the initial strategy
	 *                       profile.
	 */
	private void initTables(Map<S, Map<Action, Double>> loadedStrategy, boolean loadProfile) {
		if (loadProfile) {
			Map<S, Map<Action, Double>> normalizedStrategy = getNormalizedStrategy();

			for (S infoSet : loadedStrategy.keySet()) {
				initTable(infoSet, getNextPlayerID(infoSet, logic),
						normalizedStrategy.get(infoSet));
			}
		} else {
			for (S infoSet : loadedStrategy.keySet()) {
				initTable(infoSet, getNextPlayerID(infoSet, logic));
			}
		}
	}

	/**
	 * The cfr iteration, line numbers correspond to the pseudocode on page 12 of
	 * the above-mentioned paper
	 */
	private double cfr(S h, int i, int t, double pi1, double pi2) {
		int activePlayerID = getNextPlayerID(h, logic);

		// Lines 6-7
		if (logic.isTerminal(h)) {
			double score = eval.getScore(h)[i];
			return score;
		} else if (activePlayerID == h.getNumPlayers()) {
			Map<Action, Double> actionsMap = logic.generateStochasticActions(h);

			double val = 0;

			if (chanceSamples != Integer.MAX_VALUE && chanceSamples < actionsMap.size()) {// Sampling with replacement
				Map<Action, Double> sampledActionMap = new HashMap<>();
				for (int j = 0; j < chanceSamples; j++) {
					Action a = Helper.sampleFromStrategy(actionsMap);

					chanceActionCounts.get(h).put(a, chanceActionCounts.get(h).get(a) + 1);

					sampledActionMap.put(a, chanceActionCounts.get(h).get(a));
				}

				actionsMap = getNormalizedDistribution(sampledActionMap);
			}

			for (Action a : actionsMap.keySet()) {

				@SuppressWarnings("unchecked")
				S ha = (S) h.deepCopy();
				logic.makeMove(ha, sensor.fromPointOfView(a, ha, -1));

				double next_pi1 = pi1;
				double next_pi2 = pi2;

				if (i == 1) {
					next_pi1 = actionsMap.get(a) * pi1;
				} else {
					next_pi2 = actionsMap.get(a) * pi2;
				}

				val += actionsMap.get(a) * cfr(ha, i, t, next_pi1, next_pi2);
			}

			return val;
		}

		// Line 12
		S I = det.observeState(h, activePlayerID);
		// Line 13
		double vSig = 0.0;
		// Line 14
		Map<Action, Double> vSigIToA = new HashMap<Action, Double>();
		Set<Action> actionsAtI = profileT.get(I).keySet();
		for (Action a : actionsAtI) {
			vSigIToA.put(a, 0.0);
		}

		// Lines 15-22
		for (Action a : actionsAtI) {
			@SuppressWarnings("unchecked")
			S ha = (S) h.deepCopy();
			logic.makeMove(ha, sensor.fromPointOfView(a, ha, -1));
			if (activePlayerID == 0) {
				vSigIToA.put(a, cfr(ha, i, t, profileT.get(I).get(a) * pi1, pi2));
			} else if (activePlayerID == 1) {
				vSigIToA.put(a, cfr(ha, i, t, pi1, profileT.get(I).get(a) * pi2));
			}

			vSig = vSig + profileT.get(I).get(a) * vSigIToA.get(a);
		}

		// Lines 23-29
		if (activePlayerID == i) {
			double sumCumulativeRegrets = 0;

			for (Action a : actionsAtI) {
				regretTables.get(I).put(a,
						regretTables.get(I).get(a) + (i == 0 ? pi2 : pi1) * (vSigIToA.get(a) - vSig));
				strategyTables.get(I).put(a,
						strategyTables.get(I).get(a) + (i == 0 ? pi1 : pi2) * profileT.get(I).get(a));
				sumCumulativeRegrets += Math.max(regretTables.get(I).get(a), 0.0);
			}
			double uniformProb = 1.0 / actionsAtI.size();
			for (Action a : actionsAtI) {
				if (sumCumulativeRegrets > 0) {
					profileTp1.get(I).put(a, Math.max(regretTables.get(I).get(a), 0.0) / sumCumulativeRegrets);
				} else {
					profileTp1.get(I).put(a, uniformProb);
				}
			}
		}

		if (I.hashCode() == -1915122128) {
			System.out.println("FOUND it");
			System.out.println(vSig);
		}
		// Line 30
		return vSig;
	}

	/**
	 * First, initializes the data structures for each information set in the game
	 * if necessary.
	 * Then runs the counterfactual regret minimization algorithm for the given
	 * number of iterations.
	 * No return value, instead use getCumulativeStrategy and normalize using
	 * {@link Helper#getNormalizedDistribution}
	 */
	public void solve(int numIterations) throws StateNotFoundException {
		trainingIterations = numIterations;

		if (collectExploitability) {
			exploitability = new double[numIterations];
			bestResponse = new BestResponse<>(root, det, logic, eval, sensor);
		}
		if (collectTrainingTimes) {
			totalTrainingTime = 0.0;
			trainingTimes = new double[numIterations];
		}
		double[] finalEvals = new double[2];
		double trainingTime = 0.0;
		for (int t = 0; t < numIterations; t++) {
			long iterStart = System.nanoTime();
			for (int i = 0; i < 2; i++) {
				double v = cfr(root, i, t, 1, 1);
				finalEvals[i] += v;
			}
			swopStrategyProfiles(); // After each iteration, the profile for t+1 becomes t
			double iterTime = System.nanoTime() - iterStart;
			trainingTime += iterTime;
			Log.info("CFR", "Iteration " + t + " took " +
					TimeUnit.NANOSECONDS.toSeconds((long) iterTime) + "s.");
			totalTrainingTime += iterTime;

			if (collectTrainingTimes) {
				trainingTimes[t] = iterTime;
			}

			if (this.collectExploitability) {
				Map<S, Map<Action, Double>> normStrat = getNormalizedStrategy();
				double brvP1 = bestResponse.getBestResponseValue(normStrat, 0);
				double brvP2 = bestResponse.getBestResponseValue(normStrat, 1);

				exploitability[t] = (brvP1 + brvP2) / 2.0;

				if (exploitability[t] < exploitThreshold) {
					trainingIterations = t + 1;
					break;
				}
			}
		}

		Log.info("CFR", "Total training took " +
				TimeUnit.NANOSECONDS.toSeconds((long) trainingTime) + "s" + "\t(" + trainingTime + " ns)");
		Log.info("CFR", "Final expected payoffs:");
		Log.info("CFR", "P1: " + finalEvals[0] / numIterations);
		Log.info("CFR", "P2: " + finalEvals[1] / numIterations);
	}

	/**
	 * @param I Some information set.
	 * @return Cumulative strategy at this information set.
	 */
	public Map<Action, Double> getCumulativeStrategy(S I) {
		return this.strategyTables.get(I);
	}

	public Map<S, Map<Action, Double>> getCumulativeStrategyProfile() {
		return strategyTables;
	}

	public Map<S, Map<Action, Double>> getNormalizedStrategy() {
		Map<S, Map<Action, Double>> normStrategy = new HashMap<>();

		for (Map.Entry<S, Map<Action, Double>> entry : strategyTables.entrySet()) {
			normStrategy.put(entry.getKey(), getNormalizedDistribution(entry.getValue()));
		}

		return normStrategy;
	}

	private void swopStrategyProfiles() {
		Map<S, Map<Action, Double>> oldStrategyProfile = profileTp1;
		profileTp1 = profileT;
		profileT = oldStrategyProfile;
	}

	public double[] getExploitability() {
		if (exploitability.length > trainingIterations) {
			exploitability = Arrays.copyOfRange(exploitability, 0, trainingIterations);
		}

		return exploitability;
	}

	public double[] getTrainingTimes() {
		if (trainingTimes.length > trainingIterations) {
			trainingTimes = Arrays.copyOfRange(trainingTimes, 0, trainingIterations);
		}

		return trainingTimes;
	}

	public double getTotalTrainingTime() {
		return totalTrainingTime;
	}

}
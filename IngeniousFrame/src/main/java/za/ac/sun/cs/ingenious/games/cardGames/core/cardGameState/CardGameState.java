package za.ac.sun.cs.ingenious.games.cardGames.core.cardGameState;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature;
import za.ac.sun.cs.ingenious.games.cardGames.core.deck.CardDeck;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.KeyNotFoundException;
import za.ac.sun.cs.ingenious.games.cardGames.core.exceptions.LocationNotFoundException;

/**
 * The class CardGameState.
 * 
 * A card game state is mainly represented by a TreeMap with with Card as the
 * key and ArrayList of locations as according value.
 * 
 * Generic types are the two features for the card and also an **enum** with all
 * possible locations of a game in it.
 * (e. g. players or roles, draw piles, discard piles, ect...).
 *
 * The constructor takes a deck as an argument, as well as the initial location
 * for all cards (e. g. draw pile) and the ID of the first player.
 * 
 * *Please note:* in a card game state a Card&lt;?,?&gt; is no longer a proper
 * representation for an actual card. It is only a key for the map.
 * Now the number of locations in the ArrayList indicate how many cards of a
 * type are in the game and even more importantly, where.
 * If the ArrayList of locations for a specific card type is empty this card
 * doesn't exist in the game.
 * By adding a new type Card&lt;?,?&gt; this class will add a new location in
 * the ArrayList of the key *Card&lt;?,?&gt;* (or create a new entry with this
 * new card type).
 *
 * *Also important:* as long as roles are not introduced to this framework
 * consider:
 * listing all player locations first and in a row in the location enum.
 * This saves time and effort when implementing the logic of a game. You can
 * easily access a specific location by '<YourEnum>.values()[indexOfLocation]'.
 * So player *0* would correspond to &lt;YourEnum&gt;.values()[*0*].
 * This might change when *roles* are introduced.
 *
 * @param <F1>           Generic type of first feature (implements CardFeature).
 * @param <F2>           Generic type of second feature (implements
 *                       CardFeature).
 * @param <LocationEnum> Enum for all possible locations of a game (e. g.
 *                       players or roles, draw piles, discard piles, ect...).
 * 
 * @author Joshua Wiebe
 */
public abstract class CardGameState<F1 extends CardFeature, F2 extends CardFeature, LocationEnum extends Enum<LocationEnum>>
		extends TurnBasedGameState {

	/** The deck. */
	protected CardDeck<F1, F2> deck;

	/** The TreeMap. */
	protected TreeMap<Card<F1, F2>, ArrayList<LocationEnum>> map;

	/**
	 * Instantiates a new card game state.
	 *
	 * @param deck                    The card deck which is supposed to be
	 *                                transformed into a TreeMap.
	 * @param initLocationForAllCards The initial location for all cards.
	 * @param firstPlayerID           The first player ID
	 */
	public CardGameState(CardDeck<F1, F2> deck, LocationEnum initLocationForAllCards, int numPlayers) {
		super(0, numPlayers);
		this.deck = deck;
		map = new TreeMap<>();
		initCardLocationMap(initLocationForAllCards);
	}

	/**
	 * Initializes the card-location-map.
	 *
	 * @param initialLocationForAllCards is a single initial location where all
	 *                                   cards will be at the beginning of a game
	 *                                   (e. g. draw pile)
	 */
	public void initCardLocationMap(LocationEnum initialLocationForAllCards) {
		for (Card<F1, F2> card : this.deck.getAllCards()) {
			// By calling addCardType() only new card types get a new entry.
			this.addCardType(card);
			try {
				// In the ArrayList of locations another initial card location will be added.
				this.addLocation(card, initialLocationForAllCards);
			} catch (KeyNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Adds the card type if it doesn't exist already.
	 *
	 * @param newCardType the potential new card type
	 */
	protected void addCardType(Card<F1, F2> newCardType) {
		if (!map.containsKey(newCardType)) {
			map.put(newCardType, new ArrayList<LocationEnum>());
		}
	}

	/**
	 * Adds a location if the Card exists as a key in the TreeMap.
	 *
	 * @param card        Is the key for the TreeMap.
	 * @param newLocation The new location.
	 * @throws KeyNotFoundException Raised if the key (Card) could not be found.
	 */
	public void addLocation(Card<F1, F2> card, LocationEnum newLocation) throws KeyNotFoundException {
		if (map.containsKey(card)) {
			map.get(card).add(newLocation);
		} else {
			throw new KeyNotFoundException("Invalid key! This deck doesn't contain " + card.toString() + ". ");
		}
	}

	/**
	 * Change location.
	 *
	 * @param cardType    The card type.
	 * @param oldLocation The old location of the card type.
	 * @param newLocation The new location of the card type.
	 * @throws KeyNotFoundException      Is raised if no such card type could be
	 *                                   found as a key in the TreeMap.
	 * @throws LocationNotFoundException Is raised if no such old location for a
	 *                                   card type could be found.
	 */
	public void changeLocation(Card<F1, F2> cardType, LocationEnum oldLocation, LocationEnum newLocation)
			throws KeyNotFoundException, LocationNotFoundException {

		// Check if cardType exists as a key in TreeMap.
		if (!map.containsKey(cardType)) {
			throw new KeyNotFoundException("Invalid key! This deck doesn't contain " + cardType.toString() + ". ");
		}

		// Get the ArrayList<Location> for a certain card type.
		ArrayList<LocationEnum> listOfLoc = map.get(cardType);

		// Iterate through ArrayList and if the old location exists change it to the new
		// location.
		int i = 0;
		boolean locationExists = false;
		for (LocationEnum currLoc : listOfLoc) {
			if (currLoc.equals(oldLocation)) {
				// Change location
				listOfLoc.set(i, newLocation);
				// Keep in mind that old location exists
				locationExists = true;
				break;
			}
			i++;
		}

		// If old location could not be found raise LocationNotFoundException.
		if (!locationExists) {
			throw new LocationNotFoundException(
					"Apparently the location " + oldLocation + " does not exist for " + cardType.toString());
		}
	}

	/**
	 * @see za.ac.sun.cs.ingenious.gamestate#printPretty()
	 * 
	 *      Print the game state (TreeMap) pretty.
	 * 
	 *      Print each card type and list all locations of this card type vertically
	 *      and indented under it.
	 */
	@Override
	public void printPretty() {

		// Using a StringBuilder for Log.info().
		StringBuilder s = new StringBuilder();
		s.append("\nCardtypes and their locations:");
		s.append("\n");

		// For each card type.
		for (Map.Entry<Card<F1, F2>, ArrayList<LocationEnum>> entry : map.entrySet()) {
			s.append(entry.getKey().toString() + ": \n");

			// For each location of the card type.
			for (LocationEnum location : entry.getValue()) {
				s.append("    " + location + "\n");
			}
			s.append("\n");
		}
		Log.info(s);
	}

	/**
	 * Gets the map.
	 *
	 * @return The map
	 */
	public TreeMap<Card<F1, F2>, ArrayList<LocationEnum>> getMap() {
		// consider a deep copy with Cloner (see super class GameState). But this would
		// be very slow.
		return (TreeMap<Card<F1, F2>, ArrayList<LocationEnum>>) map.clone();
	}

	/**
	 * Sets the next player.
	 *
	 * @param nextPlayer The new next player
	 */
	public void setCurrentPlayer(int nextPlayer) {
		this.nextMovePlayerID = nextPlayer;
	}

	/**
	 * Gets the current player.
	 *
	 * @return The current player
	 */
	public int getCurrentPlayer() {
		return this.nextMovePlayerID;
	}
}

package za.ac.sun.cs.ingenious.core.util.move;

import za.ac.sun.cs.ingenious.core.Action;

/**
 * An Action indicating a player has forfeited.
 *
 * @author Rudolf Stander
 */
public class ForfeitAction implements Action {

	/* For serialization. */
	private static final long serialVersionUID = 1L;
	/* The player who has forfeited. */
	private byte player;

	/**
	 * Constructs a new ForfeitAction object.
	 *
	 * @param player The player who has forfeited.
	 */
	public ForfeitAction(byte player) {
		this.player = player;
	}

	/**
	 * Get the player who has forfeit.
	 *
	 * @return The player who has forfeited.
	 */
	@Override
	public int getPlayerID() {
		return player;
	}

	public String toString() {
		return "Action: ForfeitAction, Player: " + player;
	}

	@Override
	public ForfeitAction deepCopy() {
		return new ForfeitAction(player);
	}
}

package za.ac.sun.cs.ingenious.search.cfr;

import com.esotericsoftware.minlog.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;

/**
 * Provides common methods for all CFR based search algorithms.
 */
public final class Helper {

	// Tolerance with which to check double values for equality.
	public static final double floatingTolerance = 0.0000001;

	private Helper() {
	}

	/**
	 * @param dist Probability distribution over actions for some information set.
	 * @return An action sampled according to the given probability distribution
	 */
	public static Action sampleFromStrategy(Map<Action, Double> dist) {
		Action sampledAction = null;
		double sampleValue = Math.random();
		double cumulativeProbs = 0.0;
		Set<Action> actions = dist.keySet();
		for (Action a : actions) {
			cumulativeProbs += dist.get(a);
			if (sampledAction == null && cumulativeProbs > sampleValue) {
				sampledAction = a;
			}
		}
		if (!(1.0 - floatingTolerance < cumulativeProbs && cumulativeProbs < 1.0 + floatingTolerance)) {
			Log.error("Helper.sampleFromStrategy", "Cumulative probability is: " + cumulativeProbs);
		}
		return sampledAction;
	}

	/**
	 * @param unnormalizedTable An unnormalized probability "distribution" over
	 *                          actions for some information set.
	 * @return The input table normalized with the sum of all "probabilities" over
	 *         all actions.
	 */
	public static Map<Action, Double> getNormalizedDistribution(Map<Action, Double> unnormalizedTable) {
		double sum = 0.0;
		for (Double d : unnormalizedTable.values()) {
			sum += (d < floatingTolerance) ? 0.0 : d;
		}

		Map<Action, Double> normalizedTable = new HashMap<Action, Double>();
		for (Entry<Action, Double> e : unnormalizedTable.entrySet()) {
			if (sum == 0) {
				normalizedTable.put(e.getKey(), 1.0 / unnormalizedTable.size());
			} else {
				normalizedTable.put(e.getKey(),
						(e.getValue() < floatingTolerance) ? 0.0 : e.getValue() / sum);
			}
		}

		return normalizedTable;
	}

	/**
	 * Replaces the reliance on the depreciated {@link TurnBasedGameState}
	 * that used a nextPlayerID instance variable. Continues on the assumption that
	 * exactly one
	 * player will be making a move at a given time.
	 */
	public static <S extends GameState> int getNextPlayerID(S fromState, GameLogic<S> logic) {
		Set<Integer> nextPlayers = logic.getCurrentPlayersToAct(fromState);

		if (nextPlayers.size() > 1) {
			Log.error("CFR", "More than one player expected to play when only one should be expected.");
		}

		return (int) nextPlayers.toArray()[0];
	}
}
package za.ac.sun.cs.ingenious.games.nim;

import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.search.minimax.GameEvaluator;

/**
 * Final evaluator is included for compatibility with the framework.
 */
public class NimMctsFinalEvaluator implements GameEvaluator<NimBoard>, MctsGameFinalEvaluator<NimBoard> {

    private static final double WIN_VALUE = 1.0;
    private static final double LOSE_VALUE = 0.0;
    private static final double DRAW_VALUE = 0.5;

    public double[] getMctsScore(NimBoard forState) {
        double[] scores = getScore(forState);
        if (scores[0] > scores[1]) {
            scores[0] = WIN_VALUE;
            scores[1] = LOSE_VALUE;
        } else if (scores[0] < scores[1]) {
            scores[0] = LOSE_VALUE;
            scores[1] = WIN_VALUE;
        } else {
            scores[0] = DRAW_VALUE;
            scores[1] = DRAW_VALUE;
        }
        return scores;
    }

    /**
     * Getter for the value of a win (For the MCTS result)
     *
     * @return The win value used in MCTS.
     */
    public double getWinValue() {
        return (double) WIN_VALUE;
    }

    /**
     * Getter for the value of a loss (For the MCTS result).
     *
     * @return The loss value used in MCTS.
     */
    public double getLossValue() {
        return (double) LOSE_VALUE;
    }

    /**
     * Getter for the value of a draw (For the MCTS result).
     *
     * @return The draw value used in MCTS.
     */
    public double getDrawValue() {
        return (double) DRAW_VALUE;
    }

    /**
     * @param forState Some terminal state
     * @return The ID of the player who won in that state
     */
    public int getWinner(NimBoard forState) {
        return forState.nextMovePlayerID;

    }

    /**
     * We normalize the players' scores here, so 0 is a complete loss, 1 a perfect
     * win
     * and 0.5 a draw.
     */
    public double[] getScore(NimBoard forState) {
        double[] scores = new double[2];
        int winner = getWinner(forState);
        scores[winner] = 1;
        scores[(winner + 1) % 2] = 0;
        return scores;
    }

}

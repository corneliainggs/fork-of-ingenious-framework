package za.ac.sun.cs.ingenious.core.util.move;

import java.util.Objects;

import za.ac.sun.cs.ingenious.core.Action;

/**
 * An Action indicating a player did nothing that turn.
 *
 * @author Rudolf Stander
 */
public class IdleAction implements Action {

	/* For serialization */
	private static final long serialVersionUID = 1L;
	/* The default player ID */
	private final int DEFAULT_ID = -1;
	/*
	 * The player who was idle, with 0 representing black and
	 * 1 representing white.
	 */
	private int player;

	/**
	 * Constructs a new IdleAction object.
	 *
	 * @param player the player who made the move.
	 */
	public IdleAction(int player) {
		this.player = player;
	}

	/**
	 * Constructs a new IdleAction object with a default playerID.
	 */
	public IdleAction() {
		this.player = DEFAULT_ID;
	}

	/**
	 * Get the player who made the move.
	 *
	 * @return the player who placed the disk with 0 representing black and
	 *         1 representing white
	 */
	@Override
	public int getPlayerID() {
		return player;
	}

	/**
	 * Returns true if the given object is an IdleAction object with the same
	 * playerID.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof IdleAction)) {
			return false;
		}
		return (this.hashCode() == obj.hashCode());
	}

	@Override
	public int hashCode() {
		// Constructed like this to represent two dummy 'X' and 'Y' coordinates to
		// prevent clashes with hashes from XYAction, making the assumption that no
		// coordinate will ever be -1.
		return Objects.hash(-1, -1, player);
	}

	public String toString() {
		return "Action: IdleAction, Player: " + player;
	}

	@Override
	public IdleAction deepCopy() {
		return new IdleAction(player);
	}
}

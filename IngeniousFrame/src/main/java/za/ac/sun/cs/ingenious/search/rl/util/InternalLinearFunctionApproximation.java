package za.ac.sun.cs.ingenious.search.rl.util;

import com.esotericsoftware.minlog.Log;

import java.text.DecimalFormat;
import java.util.Arrays;

/**
 * Implementation of LinearFunctionApproximation that performs the weight updates.
 *
 * @author Steffen Jacobs
 */
public class InternalLinearFunctionApproximation implements LinearFunctionApproximation {
    private boolean initialized;
    private double[] weights;
    private double bias;
    private double learningRate = 0.001;
    private boolean useBias;

    public InternalLinearFunctionApproximation() {
        initialized = false;
    }

    public void init(int size) {
        weights = new double[size];
        initialized = true;
        useBias = true;
    }

    public void ensureInitialized(double[] featureSet) {
        if (initialized) return;

        init(featureSet.length);
    }

    public double[] get(double[][] featureSets) {
        if (featureSets.length < 1) return new double[0];

        ensureInitialized(featureSets[0]);

        double[] values = new double[featureSets.length];

        for (int i = 0; i < featureSets.length; i++) {
            values[i] = this.get(featureSets[i]);
        }

        return values;
    }

    private double get(double[] featureSet) {
        double predictedValue = 0;

        if (useBias)
            predictedValue += bias;

        for (int i = 0; i < featureSet.length; i++) {
            predictedValue += weights[i] * featureSet[i];
        }

        return predictedValue;
    }

    public void put(double[] featureSet, double estimatedValue) {
        double predictedValue = get(featureSet);

        double error = estimatedValue - predictedValue;
//        double loss = error * error;

//        Log.info("Error: " + error);

        StringBuilder featureSetSb = new StringBuilder().append(" [ ");
        for (int i = 0; i < featureSet.length; i++) {
            featureSetSb.append(featureSet[i]).append(", ");
        }
        featureSetSb.append(" ]");

//        Log.info("Feature set: " + featureSetSb);
//        Log.info("Weights Before:");
//        printWeights();

        for (int i = 0; i < weights.length; i++) {
            weights[i] += learningRate * error * featureSet[i];
        }

        if (useBias)
            bias += learningRate * error * 1;

//        Log.info("Weights After:");
//        printWeights();
//        Log.info("=====================================================");
    }

    public void printWeights() {
        printWeights(weights.length);
    }

    public void printWeights(int width) {
        DecimalFormat df = new DecimalFormat("0.00");
        StringBuilder sb = new StringBuilder();
        sb.append("Bias: ").append(bias);
        sb.append("\t[\n\t");
        for (int i = 0; i < weights.length; i++) {
            sb.append(df.format(weights[i])).append(", ");
            if ((i + 1) % (width) == 0 && i != weights.length - 1)
                sb.append("\n\t");
        }
        sb.append("\n]");

        Log.info(sb);
    }
}

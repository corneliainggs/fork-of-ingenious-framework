package za.ac.sun.cs.ingenious.games.othello.network;

import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;

/**
 * The message sent to Engines when a game has started.
 * Holds the player's ID.
 *
 * @author Rudolf Stander
 *
 */
public class OthelloInitGameMessage extends InitGameMessage {

	private static final long serialVersionUID = 1L;

	private int boardWidth, boardHeight;

	public OthelloInitGameMessage(int boardWidth) {
		this.boardWidth = boardWidth;
		this.boardHeight = boardWidth;
	}

	public OthelloInitGameMessage(int boardWidth, int boardHeight) {
		this.boardWidth = boardWidth;
		this.boardHeight = boardHeight;
	}

	public int getBoardWidth() {
		return boardWidth;
	}

	public int getBoardHeight() {
		return boardHeight;
	}

	public boolean isSquareBoard() {
		return boardWidth == boardHeight;
	}

}

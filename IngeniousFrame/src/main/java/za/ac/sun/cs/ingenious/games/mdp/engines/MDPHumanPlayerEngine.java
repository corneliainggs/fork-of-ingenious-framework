package za.ac.sun.cs.ingenious.games.mdp.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.games.mdp.MDPEngine;

import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Human player for MDP, prompted for action choices and receives rewards.
 *
 * @author Steffen Jacobs
 */
public class MDPHumanPlayerEngine extends MDPEngine {
    private Scanner scanner;

    public MDPHumanPlayerEngine(EngineToServerConnection toServer) {
        super(toServer);
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "MDPHumanPlayerEngine";
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        scanner = new Scanner(System.in);
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        List<Action> availableActions = logic.generateActions(state, playerID);

        Set<Integer> availableDiscreteActions = availableActions.stream()
                .map(x -> ((DiscreteAction)x).getActionNumber())
                .collect(Collectors.toSet());

        StringBuilder promptStringBuilder = new StringBuilder();

        promptStringBuilder.append("Current state: ")
                .append(state.getGameState())
                .append("\nAvailable actions:\n");

        for (int actionNumber : availableDiscreteActions) {
            promptStringBuilder.append(actionNumber).append("\n");
        }

        promptStringBuilder.append("Choose an action: ");

        System.out.println(promptStringBuilder);

        try {
            int actionChoice = scanner.nextInt();

            while (!availableDiscreteActions.contains(actionChoice)) {
                System.out.println("Invalid action.");
                actionChoice = scanner.nextInt();
            }

            return new PlayActionMessage(availableActions.get(actionChoice));
        } catch (Exception e) {
            Log.error("An error occurred while reading user input.");
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        scanner.close();
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        Log.info("Reward received: " + a.getReward());
    }
}

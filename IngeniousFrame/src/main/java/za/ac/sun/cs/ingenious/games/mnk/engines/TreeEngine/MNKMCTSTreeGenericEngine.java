package za.ac.sun.cs.ingenious.games.mnk.engines.TreeEngine;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.move.ForfeitAction;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.search.mcts.MctsAlgorithm.MctsTree;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.CMCTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.LGR.LGRTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST.MastTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.PW.MctsPWNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE.MctsRaveNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelection;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionFinal;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;
import za.ac.sun.cs.ingenious.games.mnk.engines.MNKMctsFinalEvaluator;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Hashtable;

import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage.newBackpropagationAverage;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationRave.newBackpropagationRave;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationSimple.newBackpropagationSimple;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionMast.newExpansionMast;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionContextual.newExpansionContextual;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle.newExpansionSingle;
import static za.ac.sun.cs.ingenious.search.mcts.selection.FinalSelectionUct.newFinalSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionPW.newTreeSelectionProgressiveWidening;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionRave.newTreeSelectionRave;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUct.newTreeSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUctFPU.newTreeSelectionFPU;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUctTunedFPU.newTreeSelectionFPUTuned;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationContextual.newSimulationContextual;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrfFull.newSimulationLgrFull;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrTree.newSimulationLgrTree;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrFull.newSimulationLgrfFull;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationMast.newSimulationMast;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom.newSimulationRandom;

public class MNKMCTSTreeGenericEngine extends MNKMctsEngine {

    MctsTree<MNKState, MctsNodeTreeParallel<MNKState>> mcts;

    ArrayList<BackpropagationThreadSafe<MctsNodeTreeParallel<MNKState>>> backpropagationEnhancements;

    Hashtable<String, MctsNodeExtensionParallelInterface> enhancementExtensionClasses;

    String enhancementConfigName;

    MatchSetting engineConfiguration;

    MastTable mastTable;
    LGRTable lgrTable;
    CMCTable cmcTable;

    protected final int TURN_LENGTH; // in milliseconds
    protected final int THREAD_COUNT;

    boolean recordMoves;

    /**
     * @param toServer An established connection to the GameServer
     */
    public MNKMCTSTreeGenericEngine(EngineToServerConnection toServer, String enhancementConfig, int threadCount,
            int turnLength) throws IOException, MissingSettingException, IncorrectSettingTypeException {
        super(toServer);

        backpropagationEnhancements = new ArrayList<>();
        enhancementExtensionClasses = new Hashtable<>();

        THREAD_COUNT = threadCount;
        TURN_LENGTH = turnLength;

        enhancementConfigName = enhancementConfig.substring(enhancementConfig.indexOf("Choice") + 6,
                enhancementConfig.length() - 5);

        engineConfiguration = new MatchSetting(enhancementConfig);

        recordMoves = true;

        // Selection strategy object
        TreeSelection<MctsNodeTreeParallel<MNKState>> selection = getSelectionClass();

        // Final Selection strategy object
        TreeSelectionFinal<MctsNodeTreeParallel<MNKState>> finalSelection = newFinalSelectionUct(logic);

        SimulationThreadSafe<MNKState> simulation = getSimulationClass();

        // Expansion strategy object
        ExpansionThreadSafe<MctsNodeTreeParallel<MNKState>, MctsNodeTreeParallel<MNKState>> expansion = getExpansionClass();

        // Backpropagation strategy objects
        for (String backprop : engineConfiguration.getSettingAsString("Backpropagation").split(",")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<MNKState>> backpropagationEnhancement = getBackpropagationClass(
                    backprop);

            if (backpropagationEnhancement != null) {
                backpropagationEnhancements.add(backpropagationEnhancement);
            } else {
                Log.error("MNKMCTSTreeGenericEngine", "Backpropagation class " + backprop + " does not exist");
                throw new RuntimeException("Backpropagation class " + backprop + " does not exist");
            }
        }

        this.mcts = new MctsTree<>(selection, expansion, simulation, backpropagationEnhancements, finalSelection, logic,
                THREAD_COUNT, playerID);
    }

    @Override
    public String engineName() {
        return "MNKMCTSTreeGenericEngine";
    }

    @Override
    public void receiveMatchResetMessage(MatchResetMessage a) {
        MNKMctsFinalEvaluator eval = new MNKMctsFinalEvaluator();
        Log.info("Game has terminated");
        Log.info("Final scores:");
        double[] score = eval.getScore(currentState);
        for (int i = 0; i < score.length; i++) {
            if (i == playerID) {
                Log.info(enhancementConfigName + ": " + score[i]);
            } else {
                Log.info("Opponent : " + score[i]);
            }
        }
        Log.info("Final state:");
        currentState.printPretty();
    }

    @Override
    /**
     * Use MCTS to compute the best action to play on this player's next move in the
     * game
     */
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {

        resetEnhancementMemory();

        MctsNodeTreeParallel<MNKState> root = new MctsNodeTreeParallel<MNKState>(currentState, null, null,
                new ArrayList<>(), logic, enhancementExtensionClasses, playerID);

        MctsNodeTreeParallel<MNKState> finalChoice = mcts.doSearch(root, TURN_LENGTH, zobrist,
                engineConfiguration.getSettingAsInt("simsPerPlayout", 1));

        new Thread(() -> {
            logTableStatistics(root);
        }).start();

        Action action = finalChoice.getPrevAction();
        if (action == null) {
            return new PlayActionMessage(new ForfeitAction((byte) playerID));
        }

        return new PlayActionMessage(action);
    }

    private void resetEnhancementMemory() {
        if (mastTable != null) {
            mastTable.resetVisitedMoves();
        }
        if (lgrTable != null) {
            lgrTable.resetVisitedMoves();
        }
        if (cmcTable != null) {
            cmcTable.resetVisitedMoves();
        }

        Hashtable<String, MctsNodeExtensionParallelInterface> newEnhancementExtensionClasses = new Hashtable<>();

        for (MctsNodeExtensionParallelInterface newExtension : enhancementExtensionClasses.values()) {
            try {
                MctsNodeExtensionParallelInterface newEnhancementExtension = newExtension.getClass()
                        .getDeclaredConstructor().newInstance();
                newEnhancementExtensionClasses.put(newExtension.getID(), newEnhancementExtension);
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException
                    | InvocationTargetException e) {
                Log.error("MNKMCTSTreeGenericEngine", e);
            }
        }

        enhancementExtensionClasses = newEnhancementExtensionClasses;

        // drop a hint to the garbage collector
        System.gc();
    }

    private void logTableStatistics(MctsNodeTreeParallel<MNKState> root) {
        if (mastTable != null) {
            mastTable.logTableStatistics(root);
        }
        if (lgrTable != null) {
            lgrTable.logTableStatistics(root);
        }
        if (cmcTable != null) {
            cmcTable.logTableStatistics(root);
        }
    }

    /**
     * Create object for the selection class specified in the .json file.
     *
     * @param selectionClass
     * @return
     */
    public TreeSelection<MctsNodeTreeParallel<MNKState>> getSelectionClass()
            throws MissingSettingException, IncorrectSettingTypeException {
        String selectionClass = engineConfiguration.getSettingAsString("Selection");
        if (selectionClass.equals("Uct")) {
            return newTreeSelectionUct(
                    playerID,
                    engineConfiguration.getSettingAsDouble("cValue"));
        } else if (selectionClass.equals("UctFPU")) {
            return newTreeSelectionFPU(
                    playerID,
                    engineConfiguration.getSettingAsDouble("cValue"),
                    engineConfiguration.getSettingAsDouble("startPriorityValue"));
        } else if (selectionClass.equals("UctTunedFPU")) {
            return newTreeSelectionFPUTuned(
                    playerID,
                    engineConfiguration.getSettingAsDouble("cValue"),
                    engineConfiguration.getSettingAsDouble("startPriorityValue"));
        } else if (selectionClass.equals("Rave")) {
            return newTreeSelectionRave(
                    playerID,
                    engineConfiguration.getSettingAsDouble("cValue"),
                    engineConfiguration.getSettingAsInt("vValue"));
        } else if (selectionClass.equals("PW")) {
            // TODO : Add configurable parameters for PW
            MctsPWNodeExtensionParallel parallelPWNodeExtension = new MctsPWNodeExtensionParallel();
            enhancementExtensionClasses.put("PW", parallelPWNodeExtension);
            return newTreeSelectionProgressiveWidening();
        } else {
            Log.error("Invalid selection strategy defined in the <...>EnhancementChoice.json file.");
            throw new RuntimeException("Invalid selection strategy defined in the <...>EnhancementChoice.json file.");
        }
    }

    /**
     * Create object for the expansion class specified in the .json file.
     *
     * @param expansionClass
     * @param enhancements
     * @return
     */
    public ExpansionThreadSafe<MctsNodeTreeParallel<MNKState>, MctsNodeTreeParallel<MNKState>> getExpansionClass()
            throws MissingSettingException, IncorrectSettingTypeException {
        String expansionClass = engineConfiguration.getSettingAsString("Expansion");
        if (expansionClass.equals("Single")) {
            return newExpansionSingle(logic);
        } else if (expansionClass.equals("Mast")) {
            assert mastTable != null : "Mast table is null";
            return newExpansionMast(logic, mastTable);
        } else if (expansionClass.equals("Contextual")) {
            assert cmcTable != null : "CMC table is null";
            return newExpansionContextual(logic, cmcTable);
        } else {
            Log.error("Invalid expansion strategy defined in the <...>EnhancementChoice.json file.");
            throw new RuntimeException("Invalid expansion strategy defined in the <...>EnhancementChoice.json file.");
        }
    }

    /**
     * Create object for the simulation class specified in the .json file.
     *
     * @param simulationClass
     * @param recordMoves
     * @param enhancements
     * @return
     */
    public SimulationThreadSafe<MNKState> getSimulationClass()
            throws MissingSettingException, IncorrectSettingTypeException {
        String simulationClass = engineConfiguration.getSettingAsString("Simulation");
        if (simulationClass.equals("Random")) {
            return newSimulationRandom(
                    logic, new MNKMctsFinalEvaluator(), new PerfectInformationActionSensor<MNKState>(),
                    recordMoves);
        } else if (simulationClass.equals("Mast")) {
            mastTable = new MastTable(
                    engineConfiguration.getSettingAsDouble("gamma"),
                    engineConfiguration.getSettingAsDouble("tau"));
            return newSimulationMast(
                    logic, new MNKMctsFinalEvaluator(), new PerfectInformationActionSensor<MNKState>(),
                    recordMoves,
                    engineConfiguration.getSettingAsBoolean("treeOnly"),
                    mastTable);
        } else if (simulationClass.equals("Contextual")) {
            cmcTable = new CMCTable(
                    engineConfiguration.getSettingAsDouble("gamma"),
                    engineConfiguration.getSettingAsDouble("threshold"),
                    engineConfiguration.getSettingAsInt("window"));
            return newSimulationContextual(
                    logic, new MNKMctsFinalEvaluator(), new PerfectInformationActionSensor<MNKState>(),
                    recordMoves,
                    cmcTable);
        } else if (simulationClass.equals("LgrTree")) {
            return newSimulationLgrTree(
                    logic, new MNKMctsFinalEvaluator(), new PerfectInformationActionSensor<MNKState>(),
                    recordMoves,
                    lgrTable);
        } else if (simulationClass.equals("LgrFull")) {
            lgrTable = new LGRTable();
            return newSimulationLgrFull(logic, new MNKMctsFinalEvaluator(),
                    new PerfectInformationActionSensor<MNKState>(),
                    recordMoves,
                    lgrTable);
        } else if (simulationClass.equals("LgrfTree")) {
            lgrTable = new LGRTable();
            return newSimulationLgrTree(
                    logic, new MNKMctsFinalEvaluator(), new PerfectInformationActionSensor<MNKState>(),
                    recordMoves,
                    lgrTable);
        } else if (simulationClass.equals("LgrfFull")) {
            lgrTable = new LGRTable();
            return newSimulationLgrfFull(
                    logic, new MNKMctsFinalEvaluator(), new PerfectInformationActionSensor<MNKState>(),
                    recordMoves,
                    lgrTable);
        } else {
            Log.error("Invalid simulation strategy defined in the <...>EnhancementChoice.json file.");
            throw new RuntimeException("Invalid simulation strategy defined in the <...>EnhancementChoice.json file.");
        }
    }

    /**
     * Create object for the backpropagation class specified in the .json file.
     *
     * @param backpropagationClass
     * @return
     */
    public BackpropagationThreadSafe<MctsNodeTreeParallel<MNKState>> getBackpropagationClass(
            String backpropagationClass) {
        if (backpropagationClass.equals("Average")) {
            return newBackpropagationAverage();
        } else if (backpropagationClass.equals("Simple")) {
            return newBackpropagationSimple(playerID);
        } else if (backpropagationClass.equals("Rave")) {
            MctsRaveNodeExtensionParallel parallelRaveNodeExtension = new MctsRaveNodeExtensionParallel();
            enhancementExtensionClasses.put("Rave", parallelRaveNodeExtension);
            return newBackpropagationRave();
        } else {
            Log.error("Invalid backpropagation strategy defined in the <...>EnhancementChoice.json file.");
            throw new RuntimeException(
                    "Invalid backpropagation strategy defined in the <...>EnhancementChoice.json file.");
        }
    }
}

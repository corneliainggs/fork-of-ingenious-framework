package za.ac.sun.cs.ingenious.core.util.state;

import java.util.HashSet;
import java.util.Set;

import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;

/**
 * Common logic of any turn based game (games where exactly one player can act in each state).
 * 
 * @author Michael Krause
 * @param <S> The type of TurnBasedGameState this logic operates on
 */
public interface TurnBasedGameLogic<S extends TurnBasedGameState> extends GameLogic<S> {

	/**
	 * For turn based games, the player to act is stored in the nextMovePlayerID variable of the
	 * game state. Therefore, this default implementation of getCurrentPlayersToAct simply returns 
	 * a set containing that variable.
	 */
	@Override
	public default Set<Integer> getCurrentPlayersToAct(S fromState) {
		Set<Integer> retVal = new HashSet<Integer>();
		retVal.add(fromState.nextMovePlayerID);
		return retVal;
	}

	/**
	 * Set fromState.nextMovePlayerID to the next player to act after the given move has
	 * been applied. Use this e.g. at the end of your makeMove method.
	 * 
	 * The default implementation cycles through players 0,1,2,...,fromState.getNumPlayers()-1
	 */
	public default void nextTurn(S fromState, Move move) {
		fromState.nextMovePlayerID = (fromState.nextMovePlayerID+1) % fromState.getNumPlayers();	
	}
}

package za.ac.sun.cs.ingenious.search.mcts.simulation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.LGR.LGRTable;

public class SimulationLgrfFull<S extends GameState> extends SimulationThreadSafe<S> {

    // Evaluator in general usually evaluates to 0.0 for loss, 0.5 for draw, 1.0 for
    // win (zero sum games).
    private LGRTable lgrTable;
    private ThreadLocalRandom randomGen = ThreadLocalRandom.current();
    MctsGameFinalEvaluator<S> evaluator;
    ActionSensor<S> obs;
    boolean recordMoves;

    /**
     * Constructor.
     *
     * @param logic             The game logic used during the simulation.
     * @param evaluator         The game evaluator used to determine the results of
     *                          the simulation.
     * @param obs               Object to process actions made by the players during
     *                          playouts.
     *                          Actions are observed from the point of view of the
     *                          environment player.
     * @param visitedMovesTable This is a hashtable to store the last good reply for
     *                          each move played
     */
    public SimulationLgrfFull(GameLogic<S> logic, MctsGameFinalEvaluator<S> evaluator, ActionSensor<S> obs,
            boolean recordMoves, LGRTable lgrTable) {
        this.logic = logic;
        this.evaluator = evaluator;
        this.obs = obs;
        this.recordMoves = recordMoves;
        this.lgrTable = lgrTable;
    }

    /**
     * The method that performs random simulation playout.
     *
     * @param state The game state from which to start the simulation playout.
     * @return The result scores
     */
    public SimulationTuple simulate(List<Action> selectionMoves, S state) {
        List<Action> moves = new ArrayList<>();

        // TODO : Generalise for more than two players. See issue #327
        Action[] previousActions = new Action[2];
        previousActions[0] = null;
        previousActions[1] = null;

        while (!logic.isTerminal(state)) {
            for (int playerId : logic.getCurrentPlayersToAct(state)) {
                Action nextAction;
                if (playerId == 0) {
                    nextAction = getNextAction(playerId, previousActions[1], state);
                    previousActions[0] = nextAction;
                } else {
                    nextAction = getNextAction(playerId, previousActions[0], state);
                    previousActions[1] = nextAction;
                }
                logic.makeMove(state, obs.fromPointOfView(nextAction, state, playerId));
                if (recordMoves) {
                    moves.add(nextAction);
                }
            }
        }
        double[] scores = evaluator.getMctsScore(state);
        updateVisitedMovesTable(moves, scores);
        return new SimulationTuple(moves, scores);
    }

    /**
     * @param previousAction
     * @param state
     * @return the next action as chosen by the LGRF algorithm
     */
    public Action getNextAction(int player, Action previousAction, S state) {
        Action currentLgr = null;
        if (previousAction != null) {
            currentLgr = lgrTable.moveCombinationScores.get(previousAction);
        }
        // stochastic distribution to prevent looping
        if (currentLgr != null && randomGen.nextInt(100) <= 90 && logic.validMove(state, currentLgr)) {
            return currentLgr;
        } else {
            List<Action> possibleActions = logic.generateActions(state, player);

            if (!possibleActions.isEmpty()) {
                return possibleActions.get(randomGen.nextInt(possibleActions.size()));
            }
        }
        return null;
    }

    /**
     * backpropagation phase for the simulation phase in which values in the lgr
     * table are updated
     * with simulation move information
     * 
     * @param moveList
     * @param scores
     */
    public void updateVisitedMovesTable(List<Action> moveList, double[] scores) {
        if (moveList.size() < 2) {
            return;
        }

        int firstActionPlayer = moveList.get(0).getPlayerID();
        int secondActionPlayer = moveList.get(1).getPlayerID();
        int lowerIndex, upperIndex;

        if (scores[secondActionPlayer] == 1) {
            lowerIndex = 0;
            upperIndex = moveList.size() - 2;
        } else if (scores[firstActionPlayer] == 1) {
            lowerIndex = 1;
            upperIndex = moveList.size() - 3;
        } else {
            return;
        }
        for (int i = lowerIndex; i < upperIndex; i = i + 2) {
            Action firstAction = moveList.get(i);
            Action secondAction = moveList.get(i + 1);
            // TODO : Check if IdleActions should be ignored. See issue #329
            if (firstAction instanceof IdleAction)
                continue;
            Action currentLgr = lgrTable.moveCombinationScores.get(firstAction);
            if (currentLgr == null) {
                lgrTable.moveCombinationScores.put(firstAction, secondAction);
            } else {
                lgrTable.moveCombinationScores.replace(firstAction, secondAction);
            }

            firstAction = secondAction;
            secondAction = moveList.get(i + 2);
            // TODO : Check if IdleActions should be ignored. See issue #329
            if (firstAction instanceof IdleAction)
                continue;
            currentLgr = lgrTable.moveCombinationScores.get(firstAction);
            if (currentLgr != null && currentLgr.equals(secondAction)) {
                lgrTable.moveCombinationScores.remove(firstAction);
            }
        }
    }

    public static <SS extends GameState> SimulationThreadSafe<SS> newSimulationLgrFull(GameLogic<SS> logic,
            MctsGameFinalEvaluator<SS> evaluator, ActionSensor<SS> obs, boolean recordMoves, LGRTable lgrTable) {
        return new SimulationLgrfFull<SS>(logic, evaluator, obs, recordMoves, lgrTable);
    }

}

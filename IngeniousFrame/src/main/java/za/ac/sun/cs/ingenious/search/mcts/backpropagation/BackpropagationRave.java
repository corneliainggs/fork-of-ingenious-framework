package za.ac.sun.cs.ingenious.search.mcts.backpropagation;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE.MctsRaveNodeExtensionParallel;

import java.util.List;
import java.util.Hashtable;

import com.esotericsoftware.minlog.Log;

public class BackpropagationRave<N extends MctsNodeTreeParallelInterface<?, ?, ?>>
        implements BackpropagationThreadSafe<N> {

    public BackpropagationRave() {
    }

    // TODO : Only apply Rave backpropagation if the parent node has fewer visits
    // than the Rave visits - nice to have for efficiency

    /**
     * Update the fields relating to this backpropagation strategy for the current
     * node.
     * 
     * @param node
     * @param results the win/loss or draw outcome for the simulation relating to
     *                the current playout.
     */
    public void propagate(N node, double[] results, List<Action> moves) {
        N parentNode = (N) node.getParent();
        double result = results[parentNode.getPlayerID()];

        Hashtable<String, MctsNodeExtensionParallelInterface> enhancementsParentNode = parentNode
                .getEnhancementClasses();
        node.readLockEnhancementClassesArrayList();
        MctsRaveNodeExtensionParallel raveEnhancementExtensionParentNode = (MctsRaveNodeExtensionParallel) enhancementsParentNode
                .get("Rave");
        node.readUnlockEnhancementClassesArrayList();

        if (raveEnhancementExtensionParentNode == null) {
            Log.error("No rave enhancement to backprop from in Rave backpropagation class");
        }

        // Get the start index for the sequence of moves contained in the subtree of
        // this node
        int subTreeStartIndex = node.getDepth();

        // Update the RAVE values for this node's parent
        if (!moves.isEmpty() && subTreeStartIndex != moves.size()) {
            raveEnhancementExtensionParentNode.updateRaveValues(result, moves, subTreeStartIndex);
        }
    }

    public static <NN extends MctsNodeTreeParallelInterface<?, ?, ?>> BackpropagationRave<NN> newBackpropagationRave() {
        return new BackpropagationRave<>();
    }

}
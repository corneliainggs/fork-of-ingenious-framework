package za.ac.sun.cs.ingenious.games.diceGames.dudo.network;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.util.message.MatchSettingsInitGameMessage;

/**
 * Message to be sent to all engines when a game has started.
 * Contains the ID of the player who is to play first.
 */
public class DudoInitGameMessage extends MatchSettingsInitGameMessage {
    private int firstPlayer;

    public DudoInitGameMessage(MatchSetting settings, int firstPlayer) {
        super(settings);
        this.firstPlayer = firstPlayer;
    }

    public int getFirstPlayer() {
        return firstPlayer;
    }
}

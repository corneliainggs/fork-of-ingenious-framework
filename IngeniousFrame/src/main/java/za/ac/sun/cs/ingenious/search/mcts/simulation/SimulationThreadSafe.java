package za.ac.sun.cs.ingenious.search.mcts.simulation;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;

import java.util.List;

/**
 * The simulation strategy determines how a MCTS self-play simulation playout is
 * run.
 *
 * @author Karen Laubscher
 * 
 * @param <S> The game state type
 */
public abstract class SimulationThreadSafe<S extends GameState> {

	protected GameLogic<S> logic;

	/**
	 * The method that performs a simulation playout.
	 *
	 * @param state The game state from which to start the simulation playout.
	 *
	 * @return The result scores
	 */
	public abstract SimulationTuple simulate(List<Action> selectionMoves, S state);

	/**
	 * Game logic object getter.
	 *
	 * @return The game logic
	 */
	public GameLogic<S> getLogic() {
		return logic;
	}

}

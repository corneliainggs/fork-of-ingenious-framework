package za.ac.sun.cs.ingenious.games.diceGames.dudo.engines;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.commandline.client.ClientArguments;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.dag.StateNotFoundException;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.exception.probability.IncorrectlyNormalizedDistributionException;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoActionSensor;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoFinalEvaluator;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoGameState;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.DudoLogic;
import za.ac.sun.cs.ingenious.games.diceGames.dudo.network.DudoInitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.util.ClassUtils;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.search.cfr.CFR;

public class DudoCFREngine extends DudoGeneralEngine {

    private int numIterations = 1000;

    private Map<DudoGameState, Map<Action, Double>> normStrategies;
    private Map<DudoGameState, Map<Action, Double>> loadedStrategy;
    private CFR<DudoGameState> cfr;

    private final Random rand;
    private String saveStrategyPath;
    private boolean trainMore = false;
    private boolean printStrategies = false;
    private int chanceSampling;
    private int outcomeSampling;
    private int externalSampling;

    public DudoCFREngine(EngineToServerConnection toServer, String enhancementPath,
            int threadCount, int turnLength) {
        super(toServer);

        if (!Objects.equals(enhancementPath, ClientArguments.defaultEnhancementConfigPath)) {
            try {
                MatchSetting m = new MatchSetting(enhancementPath);

                try {
                    FileInputStream cumulativeStrategyStream = new FileInputStream(
                            m.getSettingAsString("loadStrategy"));
                    ObjectInputStream in = new ObjectInputStream(cumulativeStrategyStream);
                    loadedStrategy = (Map<DudoGameState, Map<Action, Double>>) in.readObject();
                    in.close();
                    cumulativeStrategyStream.close();
                    Log.info("DudoCFREngine", "Loaded cumulative strategy profile from: "
                            + m.getSettingAsString("loadStrategy"));
                } catch (IOException | MissingSettingException | ClassNotFoundException
                        | IncorrectSettingTypeException e) {
                    Log.error("DudoCFREngine", "Failed to load CFR tables:\t" + e.getMessage());
                }

                numIterations = m.getSettingAsInt("iterations", numIterations);
                Log.info("DudoCFREngine", "Loaded iterations: " + numIterations);

                trainMore = m.getSettingAsBoolean("trainMore", false);
                Log.info("DudoCFREngine", "Train CFR: " + trainMore);

                printStrategies = m.getSettingAsBoolean("printStrategies", false);
                Log.info("DudoCFREngine", "Printing normalized strategies: " + printStrategies);

                try {
                    saveStrategyPath = m.getSettingAsString("saveStrategy");
                    Log.info("DudoCFREngine", "Loaded a save path: " + saveStrategyPath);
                } catch (MissingSettingException | IncorrectSettingTypeException e) {
                    Log.error("DudoCFREngine",
                            "Failed to find path to save the CFR tables:\t" + e.getMessage());
                }

                chanceSampling = m.getSettingAsInt("chanceSampling", Integer.MAX_VALUE);
                Log.info("DudoCFREngine",
                        "Chance sampling: " + ((chanceSampling == Integer.MAX_VALUE) ? "false" : chanceSampling));
                outcomeSampling = m.getSettingAsInt("outcomeSampling", Integer.MAX_VALUE);
                externalSampling = m.getSettingAsInt("externalSampling", Integer.MAX_VALUE);

            } catch (IOException e) {
                Log.error("DudoCFREngine", "Unable to load enhancement file:\t" + e.getMessage());
            }
        }

        rand = new Random();
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        currentState.setFirstPlayer(((DudoInitGameMessage) a).getFirstPlayer());

        if (loadedStrategy == null) {
            cfr = new CFR<DudoGameState>(
                    currentState,
                    logic,
                    logic,
                    new DudoFinalEvaluator(),
                    new DudoActionSensor(),
                    chanceSampling,
                    outcomeSampling,
                    externalSampling);

            Log.info("DudoCFREngine", "Training from scratch");
            try {
                cfr.solve(numIterations);
            } catch (StateNotFoundException ex) {
                Log.error("DudoCFREngine", "Failed to train CFR\n" + ex);
            }
        } else {
            cfr = new CFR<DudoGameState>(
                    currentState,
                    logic,
                    logic,
                    new DudoFinalEvaluator(),
                    new DudoActionSensor(),
                    loadedStrategy,
                    chanceSampling,
                    outcomeSampling,
                    externalSampling);

            if (trainMore) {
                Log.info("DudoCFREngine", "Training from loaded strategies");
                try {
                    cfr.solve(numIterations);
                } catch (StateNotFoundException ex) {
                    Log.error("DudoCFREngine", "Failed to train CFR\n" + ex);
                }
            }
        }

        normStrategies = cfr.getNormalizedStrategy();

        if (saveStrategyPath != null) {
            saveCumulativeStrategy();
        }

        if (printStrategies) {
            Log.info(getPrettyStrategies(normStrategies, logic, currentState, this.playerID));
            Log.info("CFREngine", "Done printing strategies");
        }
    }

    private void saveCumulativeStrategy() {
        try {

            FileOutputStream fileOut = new FileOutputStream(saveStrategyPath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(cfr.getCumulativeStrategyProfile());
            out.close();
            fileOut.close();
            Log.info("CFREngine", "Serialized data is saved in " + saveStrategyPath);
        } catch (IOException e) {
            Log.error("CFREngine", "Unable to save trained strategy:\t" + e.getMessage());
        }
    }

    public static String getPrettyStrategies(Map<DudoGameState, Map<Action, Double>> normStrategies, DudoLogic logic,
            DudoGameState currentState, int playerID) {
        StringBuilder stringBuilder = new StringBuilder();

        DudoGameState.prettyPrintSpacing = 3;
        int cells = DudoGameState.numPlayers * DudoGameState.numSides + 1;
        int perCell = 5 + (DudoGameState.prettyPrintSpacing - 1);

        for (DudoGameState state : normStrategies.keySet()) {
            if (normStrategies.get(state).keySet().isEmpty()
                    || (int) (logic.getCurrentPlayersToAct(state).toArray())[0] != playerID) {
                continue;
            }
            stringBuilder.append(state.getPretty());

            stringBuilder.append("|");

            for (int n = 0; n < currentState.remainingDice(); n++) {
                for (int r = 1; r < DudoGameState.numSides; r++) {
                    DiscreteAction action = DudoGameState.possibleActions
                            .get(playerID)
                            .get(n * DudoGameState.numSides + r);
                    addActionProbability(stringBuilder, normStrategies.get(state).get(action));
                }
                DiscreteAction action = DudoGameState.possibleActions
                        .get(playerID)
                        .get(n * DudoGameState.numSides);

                addActionProbability(stringBuilder, normStrategies.get(state).get(action));
            }

            DiscreteAction action = DudoGameState.possibleActions
                    .get(playerID)
                    .get(currentState.dudo);

            addActionProbability(stringBuilder, normStrategies.get(state).get(action));
            stringBuilder.append("\n");
            stringBuilder.append(' ').append("=".repeat(cells * perCell + (cells - 1))).append("\n\n");
        }

        return stringBuilder.toString();
    }

    private static void addActionProbability(StringBuilder stringBuilder, Double prob) {
        if (prob != null && prob != 0) {
            stringBuilder.append(" ").append(zeroPad(Math.round(prob *
                    Math.pow(10.0, DudoGameState.prettyPrintSpacing)) /
                    Math.pow(10.0, DudoGameState.prettyPrintSpacing),
                    2 + DudoGameState.prettyPrintSpacing));
            stringBuilder.append(" |");
        } else {
            stringBuilder.append(" ".repeat(5 + (DudoGameState.prettyPrintSpacing - 1)));
            stringBuilder.append("|");
        }
    }

    private static String zeroPad(double num, int padLength) {
        String strNum = num + "";

        if (strNum.length() < padLength) {
            strNum = strNum + "0".repeat(padLength - strNum.length());
        }

        return strNum;
    }

    @Override
    public String engineName() {
        return "DudoCFREngine";
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Map<Action, Double> strat = normStrategies.get(currentState);

        Action action;

        try {
            action = ClassUtils.chooseFromDistribution(strat, rand);
        } catch (IncorrectlyNormalizedDistributionException ex) {
            Log.error("DudoCFREngine", "Failed to sample action from strategy. Playing first action");
            action = (Action) strat.keySet().toArray()[0];
        }

        Log.info("DudoCFREngine", "Playing move: " + currentState.getPrettyClaim(
                ((DiscreteAction) action).getActionNumber()));

        return new PlayActionMessage(action);
    }
}
package za.ac.sun.cs.ingenious.games.othello.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.*;
import za.ac.sun.cs.ingenious.games.othello.gamestate.GameFinalState;
import za.ac.sun.cs.ingenious.games.othello.gamestate.OthelloBoard;
import za.ac.sun.cs.ingenious.games.othello.gamestate.OthelloFinalEvaluator;
import za.ac.sun.cs.ingenious.games.othello.gamestate.OthelloLogic;
import za.ac.sun.cs.ingenious.games.othello.network.OthelloGameTerminatedMessage;
import za.ac.sun.cs.ingenious.games.othello.network.OthelloInitGameMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;

import java.io.IOException;

/**
 * The base engine class for the Othello (Reversi) board game.
 * The engine represents a player and receives game updates and requests
 * from the Referee (server).
 */
public abstract class OthelloEngine extends Engine {

	protected OthelloBoard currentState;

	protected OthelloLogic logic;

	protected ZobristHashing zobrist;
	protected long currentBoardHash;

	protected boolean output = true;

	private int boardSize, boardWidth, boardHeight;

	/**
	 * Constructs a new engine.
	 *
	 * @param serverConnection the connection the engine uses to receive and send
	 *                         information from and to the server
	 */
	public OthelloEngine(EngineToServerConnection serverConnection)
			throws IncorrectSettingTypeException, MissingSettingException, IOException {
		super(serverConnection);
		this.logic = OthelloLogic.defaultOthelloLogic;
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {
		zobrist = zobristHashing;
	}

	@Override
	/**
	 * Returns the Engine's name
	 *
	 * @return the engine's name
	 */
	public String engineName() {
		return "OthelloEngine";
	}

	/**
	 * Determines if the player wins or loses based on the final scores sent in
	 * the termination message.
	 *
	 * @param message the game termination message sent from the Referee
	 */
	public void receiveGameTerminatedMessage(GameTerminatedMessage message) {
		toServer.closeConnection();
		System.exit(0);
	}

	@Override
	public void receiveMatchResetMessage(MatchResetMessage a) {
		OthelloFinalEvaluator eval = new OthelloFinalEvaluator();
		Log.info("Game has terminated");
		Log.info("Final scores:");
		double[] score = eval.getScore(currentState);
		for (int i = 0; i < score.length; i++) {
			Log.info("Player " + (i + 1) + ": " + score[i] + "");
		}
		Log.info("Final state:");
		currentState.printPretty();
	}

	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
		try {
			OthelloInitGameMessage message = (OthelloInitGameMessage) a;
			// create new board
			if (message.isSquareBoard()) {
				this.currentState = new OthelloBoard(message.getBoardWidth());
			} else {
				this.currentState = new OthelloBoard(message.getBoardWidth(), message.getBoardHeight());
			}
			// create new zobrist hashing
			zobrist = new ZobristHashing(currentState.getBoardWidth(), currentState.getBoardHeight());
			currentBoardHash = zobrist.hashBoard(this.currentState);
		} catch (Exception ex) {
			Log.error("GoEngine error:Problem creating board - " + ex.getLocalizedMessage());
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Requests an action from the player and determines if the action is valid.
	 * If the action is valid, it is sent to the Referee. Otherwise the player
	 * is asked to generate a new move until the player's clock runs out.
	 *
	 * @param message the request sent from the Referee
	 *
	 * @return a valid action or null if the clock ran out
	 */
	public abstract PlayActionMessage receiveGenActionMessage(GenActionMessage message);

	/**
	 * Executes a move on the current board sent from the Referee.
	 *
	 * @param message the move message sent from the Referee
	 */
	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage message) {
		logic.makeMove(currentState, message.getMove(), false);
		if (message.getMove() instanceof XYAction) {
			// TODO: Add incremental zobrist hashing support. Issue #321.
			// long hash = zobrist.hashBoard(currentState);
			zobrist.addBoardToHashtable(currentState.hashCode(), currentState);
		}
	}
}

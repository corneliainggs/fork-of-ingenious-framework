package za.ac.sun.cs.ingenious.games.go;

import com.esotericsoftware.minlog.Log;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;

/**
 * GoBoard -- A specialized game state for the game of Go
 * 
 * @author Marco Dewey
 */
public class GoBoard extends TurnBasedSquareBoard {

	private final int PASS_MOVE = -2;
	private final int SIMULATION_COUNT;
	private final int MAX_ATTEMPTS;

	public short[] chainBoard;
	public byte[] pseudoBoard;

	private int prev1;
	private int prev2;
	private int count;
	private int player1score;
	private int player2score;
	private boolean scored;
	private boolean simulation;

	/**
	 * Initialization of a GoBoard Object.
	 *
	 * @param boardWidth  An integer representing the size of the Go board
	 * @param firstPlayer An integer representing the id of the first player
	 * @param numPlayers  An integer representing the number of players in the game
	 * @return No return value.
	 */
	public GoBoard(int boardWidth, int firstPlayer, int numPlayers) {
		super(boardWidth, firstPlayer, numPlayers);
		this.chainBoard = new short[this.getNumCells()];
		this.pseudoBoard = new byte[this.getNumCells()];
		Arrays.fill(this.chainBoard, (short) -1);
		this.prev1 = -1;
		this.prev2 = -1;
		this.count = 0;
		this.player1score = 0;
		this.player2score = 0;
		this.simulation = false;
		this.scored = false;
		this.SIMULATION_COUNT = getNumCells();
		this.MAX_ATTEMPTS = getNumCells();
	}

	/**
	 * Initialization of a GoBoard Object.
	 *
	 * @param toCopy A GoBoard Object to make a deep copy of
	 * @return No return value.
	 */
	public GoBoard(GoBoard toCopy) {
		super(toCopy);
		this.prev1 = toCopy.getPrev1();
		this.prev2 = toCopy.getPrev2();
		this.count = toCopy.getCount();
		this.player1score = toCopy.getPlayer1Score();
		this.player2score = toCopy.getPlayer2Score();
		this.simulation = toCopy.getSimulationState();
		this.scored = toCopy.getScoredState();
		this.chainBoard = new short[this.getNumCells()];
		this.pseudoBoard = new byte[this.getNumCells()];
		System.arraycopy(toCopy.chainBoard, 0, this.chainBoard, 0, this.getNumCells());
		System.arraycopy(toCopy.pseudoBoard, 0, this.pseudoBoard, 0, this.getNumCells());
		this.SIMULATION_COUNT = getNumCells();
		this.MAX_ATTEMPTS = getNumCells();
	}

	/**
	 * Toggles the GoBoard Object in and out of a simulation state.
	 *
	 * @return No return value.
	 */
	public void setSimulation() {
		this.simulation = !this.simulation;
	}

	/**
	 * Getter for move one moves prior
	 *
	 * @return int of the move one moves prior
	 */
	public int getPrev1() {
		return this.prev1;
	}

	/**
	 * Getter for move two moves prior
	 *
	 * @return int of the move two moves prior
	 */
	public int getPrev2() {
		return this.prev2;
	}

	/**
	 * Getter for total move count
	 *
	 * @return int of the total move count
	 */
	public int getCount() {
		return this.count;
	}

	/**
	 * Getter for player one's score
	 *
	 * @return int of player one's score in the current game
	 */
	public int getPlayer1Score() {
		return this.player1score;
	}

	/**
	 * Getter for player two's score
	 *
	 * @return int of player two's score in the current game
	 */
	public int getPlayer2Score() {
		return this.player2score;
	}

	/**
	 * Getter for the scored state
	 *
	 * @return boolean representing he scored state of the GoBoard Object
	 */
	public boolean getScoredState() {
		return this.scored;
	}

	/**
	 * Getter for the simulation state
	 *
	 * @return boolean representing the simulation state of the GoBoard Object
	 */
	public boolean getSimulationState() {
		return this.simulation;
	}

	/**
	 * Determines if the game is in a terminal state.
	 *
	 * @return True is the board is in a terminal state, else False
	 */
	public boolean isTerminal() {
		if (this.simulation) {
			if (this.count > SIMULATION_COUNT) {
				updateScore();
				return true;
			}
		}
		if (this.prev1 == PASS_MOVE && this.prev2 == PASS_MOVE) {
			updateScore();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns a single random valid move for a given player.
	 *
	 * @param player an Integer to represent a player
	 * @return Move Object
	 */
	public Move getSingleMove(int player) {
		int x, y;
		XYAction action;
		boolean found = false;
		int i = 0;
		ThreadLocalRandom x_gen = ThreadLocalRandom.current();
		ThreadLocalRandom y_gen = ThreadLocalRandom.current();
		while (!found) {
			i++;
			x = x_gen.nextInt(this.getBoardWidth());
			y = y_gen.nextInt(this.getBoardHeight());
			action = new XYAction(x, y, player);
			if (validMove(action)) {
				return (Move) action;
			}
			if (i > MAX_ATTEMPTS) {
				break;
			}
		}
		return (Move) new IdleAction(player);
	}

	/**
	 * Returns a list of all valid moves for a given player.
	 *
	 * @param player an Integer to represent a player
	 * @return List<Action> a list of possible valid moves for a given player
	 */
	public List<Action> getFullList(int player) {
		List<Action> list = new ArrayList<Action>();
		for (int i = 0; i < this.getBoardWidth(); i++) {
			for (int j = 0; j < this.getBoardWidth(); j++) {
				int xy = xyToIndex(i, j);
				if (this.chainBoard[xy] == (short) -1) {
					XYAction action = new XYAction(i, j, player);
					if (validMove(action)) {
						list.add(action);
					}
				}
			}
		}
		if (this.prev1 != -1 && this.prev2 != -1) {
			list.add(new IdleAction(player));
		}
		return list;
	}

	/**
	 * Takes a move and makes it on the board.
	 *
	 * @param move A Move object played by a player
	 * @return True if the move was able to be made, else False
	 */
	public boolean makeMove(Move move) {
		if (this.simulation) {
			this.count++;
		}
		if (move instanceof XYAction) {
			XYAction action = (XYAction) move;
			int x = action.getX();
			int y = action.getY();
			int player = action.getPlayerID();
			int xy = xyToIndex(x, y);
			this.board[xy] = (byte) (player + 1);
			updateChains(x, y, player + 1, xy);
			this.prev2 = this.prev1;
			this.prev1 = xy;
			if (player == 0) {
				this.player1score++;
			} else {
				this.player2score++;
			}
			return true;
		} else if (move instanceof IdleAction) {
			this.prev2 = this.prev1;
			this.prev1 = PASS_MOVE;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Determines if a given move is legal in the current board state.
	 *
	 * @param move A Move object in question
	 * @return True if the given move is valid, else False
	 */
	public boolean validMove(Move move) {
		if (move instanceof XYAction) {
			XYAction action = (XYAction) move;
			int x = action.getX();
			int y = action.getY();
			int xy = xyToIndex(x, y);
			if (this.board[xy] != 0) {
				return false;
			}
			if (touchingEmpty(x, y)) {
				return true;
			}
			if (xy == this.prev2) {
				if (isSimpleKo(x, y)) {
					return false;
				}
			}
			int player = action.getPlayerID();
			int opponent;
			if (player == 0) {
				opponent = 1;
			} else {
				opponent = 0;
			}
			if (this.pseudoBoard[xy] == (((player + 1) * -1) - 1)) {
				return false;
			}
			if (isSuicide(x, y, player + 1, opponent + 1, xy)) {
				return false;
			} else {
				if (isTrueEye(x, y, player + 1)) {
					return false;
				}
				return true;
			}
		} else if (move instanceof IdleAction) {
			if (this.prev1 == -1 || this.prev2 == -1) {
				return false;
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns both players scores for a given game.
	 *
	 * @param move A Move object played by a player
	 * @return True if the move was able to be made, else False
	 */
	public double[] getScore() {
		double[] scores = new double[2];
		scores[0] = (double) this.player1score;
		scores[1] = (double) this.player2score;
		return scores;
	}

	/**
	 * Recount the score for both players.
	 *
	 * @return No return value.
	 */
	private void updateScore() {
		this.scored = true;
		for (int i : this.pseudoBoard) {
			if (this.pseudoBoard[i] == -2) {
				this.player1score++;
			} else if (this.pseudoBoard[i] == -3) {
				this.player2score++;
			}
		}
	}

	/**
	 * Looks at a given intersection and determines if it has a
	 * single liberty.
	 *
	 * @param x An Integer representing the x coordinate
	 * @param y An Integer representing the y coordinate
	 * @return True a given coordinate is next to an empty intersection,
	 *         else False
	 */
	private boolean touchingEmpty(int x, int y) {
		int xyN = xyToIndex(x, y - 1);
		if (xyN != -1) {
			if (this.board[xyN] == 0) {
				return true;
			}
		}
		int xyS = xyToIndex(x, y + 1);
		if (xyS != -1) {
			if (this.board[xyS] == 0) {
				return true;
			}
		}
		int xyE = xyToIndex(x + 1, y);
		if (xyE != -1) {
			if (this.board[xyE] == 0) {
				return true;
			}
		}
		int xyW = xyToIndex(x - 1, y);
		if (xyW != -1) {
			if (this.board[xyW] == 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Used to combine two chains into a single chain.
	 *
	 * Turn chain A and chain B into a chain C which is a composition of chain
	 * A and B.
	 *
	 * @param i An Integer representing part of chain A
	 * @param j An Integer representing part of chain B
	 * @return No return value.
	 */
	private void swap(int i, int j) {
		short a = this.chainBoard[i];
		short b = this.chainBoard[j];
		this.chainBoard[i] = b;
		this.chainBoard[j] = a;
	}

	/**
	 * Determines if a chain is going to be captured.
	 *
	 * @param xy An Integer representing part of a chain
	 * @return True if the given chain can be captured, else False
	 */
	private boolean canCapture(int xy) {
		if (this.pseudoBoard[xy] == -1) {
			return false;
		}
		int start = xy;
		while (start != this.chainBoard[xy]) {
			if (this.pseudoBoard[xy] != 0) {
				return false;
			}
			xy = this.chainBoard[xy];
		}
		if (this.pseudoBoard[xy] != 0) {
			return false;
		}
		return true;
	}

	/**
	 * Given a section of a chain, remove the entire chain.
	 *
	 * @param xy An Integer representing part of a chain
	 * @return No return value.
	 */
	private void capture(int xy) {
		int player = this.board[xy] - 1;
		int cnt = 0;
		int next = this.chainBoard[xy];
		// While part of the chain still exists
		while (next != -1) {
			// Translate the 1 dimenional index to a 2 dimenional coordinate
			int x = xy % this.getBoardWidth();
			int y = (xy - x) / this.getBoardWidth();
			// Find the adjacent intersections
			int xyN = xyToIndex(x, y - 1);
			int xyS = xyToIndex(x, y + 1);
			int xyE = xyToIndex(x + 1, y);
			int xyW = xyToIndex(x - 1, y);
			// Remove the current intersection on all boards
			this.chainBoard[xy] = (byte) -1;
			this.pseudoBoard[xy] = (byte) 0;
			this.board[xy] = (byte) 0;
			// If the north intersection exists and is not empty
			if (xyN != -1) {
				if (this.board[xyN] != 0) {
					// Update the north intersection pseudo count
					this.pseudoBoard[xyN]++;
				}
			}
			// If the south intersection exists and is not empty
			if (xyS != -1) {
				if (this.board[xyS] != 0) {
					// Update the south intersection pseudo count
					this.pseudoBoard[xyS]++;
				}
			}
			// If the east intersection exists and is not empty
			if (xyE != -1) {
				if (this.board[xyE] != 0) {
					// Update the east intersection pseudo count
					this.pseudoBoard[xyE]++;
				}
			}
			// If the west intersection exists and is not empty
			if (xyW != -1) {
				if (this.board[xyW] != 0) {
					// Update the west intersection pseudo count
					this.pseudoBoard[xyW]++;
				}
			}
			// Increase the number of stones removed
			cnt++;
			xy = next;
			next = this.chainBoard[xy];
		}
		// Update the scores
		if (player == 0) {
			player1score -= cnt;
		} else {
			player2score -= cnt;
		}
	}

	/**
	 * Given a chain, determine if the given intersection is
	 * part of that chain.
	 *
	 * @param start An Integer representing part of a chain
	 * @param xy    An Integer representing the intersection in
	 *              question
	 * @return No return value.
	 */
	private boolean contains(int start, int xy) {
		int temp = this.chainBoard[start];
		while (temp != start) {
			if (temp == xy) {
				return true;
			}
			temp = this.chainBoard[temp];
		}
		return false;
	}

	/**
	 * Given a new move, update the sourounding chains
	 *
	 * The only reason the xy coordinate is seperated from the x and y
	 * coordinates is just so you do not have to recalculate either.
	 * They do represent the same thing.
	 *
	 * @param x      An Integer representing the x coordinate of the Move
	 * @param y      An Integer representing the y coordinate of the Move
	 * @param player an Integer to represent a player
	 * @param xy     An Integer representing the new Move
	 * @return No return value.
	 */
	public void updateChains(int x, int y, int player, int xy) {
		this.chainBoard[xy] = (short) xy;
		this.pseudoBoard[xy] = 0;
		// Find the adjacent intersections
		int xyN = xyToIndex(x, y - 1);
		int xyS = xyToIndex(x, y + 1);
		int xyE = xyToIndex(x + 1, y);
		int xyW = xyToIndex(x - 1, y);
		if (xyN != -1) {
			if (this.board[xyN] != 0) {
				this.pseudoBoard[xyN]--;
				// If the north intersection contains the current players chain
				// combine the two chains
				if (this.board[xyN] == player) {
					swap(this.chainBoard[xy], this.chainBoard[xyN]);
				} else {
					// If its pseudo count is zero and the north intersection
					// can be captured, then capture it
					if (this.pseudoBoard[xyN] == 0) {
						if (canCapture(xyN)) {
							capture(xyN);
						}
					}
				}
			} else {
				this.pseudoBoard[xy]++;
			}
		}
		if (xyS != -1) {
			if (this.board[xyS] != 0) {
				this.pseudoBoard[xyS]--;
				if (this.board[xyS] == player) {
					// If the chain at the south intersetion does not contain
					// the current intersetion already, combine them
					if (!contains(xyS, xy)) {
						swap(this.chainBoard[xy], this.chainBoard[xyS]);
					}
				} else {
					// If its pseudo count is zero and the south intersection
					// can be captured, then capture it
					if (this.pseudoBoard[xyS] == 0) {
						if (canCapture(xyS)) {
							capture(xyS);
						}
					}
				}
			} else {
				this.pseudoBoard[xy]++;
			}
		}
		if (xyE != -1) {
			if (this.board[xyE] != 0) {
				this.pseudoBoard[xyE]--;
				if (this.board[xyE] == player) {
					// If the chain at the east intersetion does not contain
					// the current intersetion already, combine them
					if (!contains(xyE, xy)) {
						swap(this.chainBoard[xy], this.chainBoard[xyE]);
					}
				} else {
					// If its pseudo count is zero and the east intersection
					// can be captured, then capture it
					if (this.pseudoBoard[xyE] == 0) {
						if (canCapture(xyE)) {
							capture(xyE);
						}
					}
				}
			} else {
				this.pseudoBoard[xy]++;
			}
		}
		if (xyW != -1) {
			if (this.board[xyW] != 0) {
				this.pseudoBoard[xyW]--;
				if (this.board[xyW] == player) {
					// If the chain at the west intersetion does not contain
					// the current intersetion already, combine them
					if (!contains(xyW, xy)) {
						swap(this.chainBoard[xy], this.chainBoard[xyW]);
					}
				} else {
					// If its pseudo count is zero and the west intersection
					// can be captured, then capture it
					if (this.pseudoBoard[xyW] == 0) {
						if (canCapture(xyW)) {
							capture(xyW);
						}
					}
				}
			} else {
				this.pseudoBoard[xy]++;
			}
		}
	}

	/**
	 * Given a potential move, check if it would be a suicide
	 *
	 * The only reason the xy coordinate is separated from the x and y
	 * coordinates is just so you do not have to recalculate either.
	 * They do represent the same thing.
	 *
	 * @param x        An Integer representing the x coordinate of the Move
	 * @param y        An Integer representing the y coordinate of the Move
	 * @param player   an Integer to represent a player
	 * @param opponent an Integer to represent the opponent
	 * @param xy       An Integer representing the new Move
	 * @return True if the move is suicide, else False
	 */
	public boolean isSuicide(int x, int y, int player, int opponent, int xy) {
		// Find the adjacent intersections
		int xyN = xyToIndex(x, y - 1);
		int xyS = xyToIndex(x, y + 1);
		int xyE = xyToIndex(x + 1, y);
		int xyW = xyToIndex(x - 1, y);
		// Flags to indicate if an intersection is a players stone or opponent
		boolean pN = false;
		boolean pS = false;
		boolean pE = false;
		boolean pW = false;
		boolean oN = false;
		boolean oS = false;
		boolean oE = false;
		boolean oW = false;
		// Flag to indicate if move will result in suicide
		boolean suicide = true;
		// Flag to indicate if move will result in capture
		boolean capture = false;
		int opponentCount = 0;
		// We know that the Move does not have a liberty but we do not know
		// what the make up of the 4 sourounding intersetions are.
		if (xyN != -1) {
			if (this.board[xyN] == player) {
				pN = true;
			} else {
				opponentCount++;
				oN = true;
			}
		} else {
			opponentCount++;
		}
		if (xyS != -1) {
			if (this.board[xyS] == player) {
				pS = true;
			} else {
				opponentCount++;
				oS = true;
			}
		} else {
			opponentCount++;
		}
		if (xyE != -1) {
			if (this.board[xyE] == player) {
				pE = true;
			} else {
				opponentCount++;
				oE = true;
			}
		} else {
			opponentCount++;
		}
		if (xyW != -1) {
			if (this.board[xyW] == player) {
				pW = true;
			} else {
				opponentCount++;
				oW = true;
			}
		} else {
			opponentCount++;
		}
		// Simulate a move by editing the pseudo liberty board
		if (pN || oN) {
			this.pseudoBoard[xyN]--;
		}
		if (pS || oS) {
			this.pseudoBoard[xyS]--;
		}
		if (pE || oE) {
			this.pseudoBoard[xyE]--;
		}
		if (pW || oW) {
			this.pseudoBoard[xyW]--;
		}
		// Determine if the move will result in suicide by looking at
		// intersections that contain the same color stones
		if (pN) {
			if (canCapture(xyN)) {
				suicide = true;
			} else {
				suicide = false;
			}
		}
		if (suicide) {
			if (pS) {
				if (canCapture(xyS)) {
					suicide = true;
				} else {
					suicide = false;
				}
			}
		}
		if (suicide) {
			if (pE) {
				if (canCapture(xyE)) {
					suicide = true;
				} else {
					suicide = false;
				}
			}
		}
		if (suicide) {
			if (pW) {
				if (canCapture(xyW)) {
					suicide = true;
				} else {
					suicide = false;
				}
			}
		}
		// Determine if the move will result in capture by looking at
		// intersections that contain the opponents stones
		if (oN && canCapture(xyN)) {
			capture = true;
		}
		if (!capture) {
			if (oS && canCapture(xyS)) {
				capture = true;
			}
		}
		if (!capture) {
			if (oE && canCapture(xyE)) {
				capture = true;
			}
		}
		if (!capture) {
			if (oW && canCapture(xyW)) {
				capture = true;
			}
		}
		// Undo the simulated move by restoring the pseudo liberty board
		if (pN || oN) {
			this.pseudoBoard[xyN]++;
		}
		if (pS || oS) {
			this.pseudoBoard[xyS]++;
		}
		if (pE || oE) {
			this.pseudoBoard[xyE]++;
		}
		if (pW || oW) {
			this.pseudoBoard[xyW]++;
		}
		// If a move will result in a capture it can not be a suicide
		if (capture) {
			return false;
		} else {
			if (opponentCount == 4) {
				// If it is in a eye and can not capture then it is suicide
				return true;
			} else {
				// Else return the suicide boolean
				return suicide;
			}
		}
	}

	/**
	 * Given an intersection check if it is an eye.
	 *
	 * @param x        An Integer representing the x coordinate of the Move
	 * @param y        An Integer representing the y coordinate of the Move
	 * @param opponent an Integer to represent the oppoent
	 * @return True if the intersection is an eye, else False
	 */
	private boolean isEye(int x, int y, int opponent) {
		// Find the adjacent intersections
		int xyN = xyToIndex(x, y - 1);
		int xyS = xyToIndex(x, y + 1);
		int xyE = xyToIndex(x + 1, y);
		int xyW = xyToIndex(x - 1, y);
		// If all 4 intersections are opponents stones then it is an eye
		if (!isOpponent(xyN, opponent)) {
			return false;
		} else if (!isOpponent(xyS, opponent)) {
			return false;
		} else if (!isOpponent(xyE, opponent)) {
			return false;
		} else if (!isOpponent(xyW, opponent)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Given an intersection check if it is an true eye.
	 *
	 * @param x        An Integer representing the x coordinate of the Move
	 * @param y        An Integer representing the y coordinate of the Move
	 * @param opponent an Integer to represent the oppoent
	 * @return True if the intersection is an true eye, else False
	 */
	private boolean isTrueEye(int x, int y, int opponent) {
		// Can only be a true eye if it is a eye
		if (isEye(x, y, opponent)) {
			// Find the adjacent diagonal intersections
			int xyNE = xyToIndex(x + 1, y - 1);
			int xyNW = xyToIndex(x - 1, y - 1);
			int xySE = xyToIndex(x + 1, y + 1);
			int xySW = xyToIndex(x - 1, y + 1);
			// Count the number of opponents in intersections
			int count = 0;
			if (isOpponent(xyNE, opponent)) {
				count++;
			}
			if (isOpponent(xyNW, opponent)) {
				count++;
			}
			if (isOpponent(xySE, opponent)) {
				count++;
			}
			if (isOpponent(xySW, opponent)) {
				count++;
			}
			// If all 4 diagonals are opponents, then it is a true eye
			if (count > 3) {
				return true;
				// If less than 3 diagonals are opponents, then it is not a true eye
			} else if (count < 3) {
				return false;
				// The only case left is 3 of 4 diagonals are opponents
				// Corners must have 4 diagonal opponents
			} else if (isCorner(x, y)) {
				return false;
				// Edges must have 4 diagonal opponents
			} else if (isEdge(x, y)) {
				return false;
				// Center of the board can have only 3 diagonal opponents
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * Given an intersection check if it contains a opponent's stone.
	 *
	 * @param xy       An Integer representing the intersection
	 * @param opponent an Integer to represent the oppoent
	 * @return True if the intersection contains a opponent's stone, else False
	 */
	private boolean isOpponent(int xy, int opponent) {
		if (xy == -1) {
			return true;
		} else {
			return (this.board[xy] == opponent);
		}
	}

	/**
	 * Given an intersection check if it is a corner.
	 *
	 * @param x An Integer representing the x coordinate of the intersection
	 * @param y An Integer representing the y coordinate of the intersection
	 * @return True if the intersection is a corner, else False
	 */
	private boolean isCorner(int x, int y) {
		return (x * (this.getBoardWidth() - 1 - x) == 0)
				&& (y * (this.getBoardWidth() - 1 - y) == 0);
	}

	/**
	 * Given an intersection check if it is an edge.
	 *
	 * @param x An Integer representing the x coordinate of the intersection
	 * @param y An Integer representing the y coordinate of the intersection
	 * @return True if the intersection is an edge, else False
	 */
	private boolean isEdge(int x, int y) {
		return (x * (this.getBoardWidth() - 1 - x) == 0)
				|| (y * (this.getBoardWidth() - 1 - y) == 0);
	}

	/**
	 * Given an intersection check if it is in a state of simple ko.
	 *
	 * @param x An Integer representing the x coordinate of the intersection
	 * @param y An Integer representing the y coordinate of the intersection
	 * @return True if the intersection is in a state of simple ko,
	 *         else False
	 */
	private boolean isSimpleKo(int x, int y) {
		// Remove the corner case
		if (isCorner(x, y)) {
			return isSimpleKoCorner(x, y);
		}
		// Remove the edge case
		if (isEdge(x, y)) {
			return isSimpleKoEdge(x, y);
		}
		// Find the four adjacent intersections
		int xyN = xyToIndex(x, y - 1);
		int xyS = xyToIndex(x, y + 1);
		int xyE = xyToIndex(x + 1, y);
		int xyW = xyToIndex(x - 1, y);
		byte n = this.board[xyN];
		byte s = this.board[xyS];
		byte e = this.board[xyE];
		byte w = this.board[xyW];
		// The four adjacent intersections must have the same color stones
		if ((n == s) && ((n == e) && (n == w))) {
			// Find the four diagonally adjacent intersections
			int xyNE = xyToIndex(x + 1, y - 1);
			int xyNW = xyToIndex(x - 1, y - 1);
			int xySE = xyToIndex(x + 1, y + 1);
			int xySW = xyToIndex(x - 1, y + 1);
			byte ne = this.board[xyNE];
			byte nw = this.board[xyNW];
			byte se = this.board[xySE];
			byte sw = this.board[xySW];
			// If two diagonally adjacent intersections (mathcing on one
			// direction) are the same color and differ from the adjacent
			// intersection between them. Then check two intersection away in
			// that direction to see if it matches the diagonal color.
			if (((ne == nw) && (ne != 0)) && (ne != n)) {
				int xyNN = xyToIndex(x, y - 2);
				if (xyNN != -1) {
					byte nn = this.board[xyNN];
					if (nn == ne) {
						return true;
					}
				}

			}
			if (((se == sw) && (se != 0)) && (se != s)) {
				int xySS = xyToIndex(x, y + 2);
				if (xySS != -1) {
					byte ss = this.board[xySS];
					if (ss == se) {
						return true;
					}
				}

			}
			if (((ne == se) && (ne != 0)) && (ne != e)) {
				int xyEE = xyToIndex(x + 2, y);
				if (xyEE != -1) {
					byte ee = this.board[xyEE];
					if (ee == ne) {
						return true;
					}
				}

			}
			if (((nw == sw) && (nw != 0)) && (nw != w)) {
				int xyWW = xyToIndex(x - 2, y);
				if (xyWW != -1) {
					byte ww = this.board[xyWW];
					if (ww == nw) {
						return true;
					}
				}

			}
			return false;
		} else {
			return false;
		}
	}

	/**
	 * Given an intersection check if it is in a state of simple ko.
	 *
	 * This is a special case when the intersection in question is in a
	 * corner.
	 *
	 * @param x An Integer representing the x coordinate of the intersection
	 * @param y An Integer representing the y coordinate of the intersection
	 * @return True if the intersection is in a state of simple ko,
	 *         else False
	 */
	private boolean isSimpleKoCorner(int x, int y) {
		// Get north and south intersections
		int xyN = xyToIndex(x, y - 1);
		int xyS = xyToIndex(x, y + 1);
		// Eleminate two of the corners
		if (xyN != -1) {
			byte n = this.board[xyN];
			if (n == 0) {
				return false;
			}
			// Get east and west intersections
			int xyE = xyToIndex(x + 1, y);
			int xyW = xyToIndex(x - 1, y);
			// North East corner
			if (xyE != -1) {
				byte e = this.board[xyE];
				// If the two adjacent intersection are not the same color it
				// can not be simple ko
				if (e != n) {
					return false;
				}
				int xyNE = xyToIndex(x + 1, y - 1);
				byte ne = this.board[xyNE];
				// If the diagonal intersection is the same color as the
				// adjacent intersections then it can not be simple ko
				if (ne == 0 || ne == e) {
					return false;
				}
				int xyNN = xyToIndex(x, y - 2);
				int xyEE = xyToIndex(x + 2, y);
				byte nn = this.board[xyNN];
				byte ee = this.board[xyEE];
				// If another stone exist two intersections away that is the
				// same color as the diagonal stone then it is simple ko
				if (nn == ne) {
					return true;
				}
				if (ee == ne) {
					return true;
				}
				return false;
				// North West corner
			} else {
				byte w = this.board[xyW];
				// If the two adjacent intersection are not the same color it
				// can not be simple ko
				if (w != n) {
					return false;
				}
				int xyNW = xyToIndex(x - 1, y - 1);
				byte nw = this.board[xyNW];
				// If the diagonal intersection is the same color as the
				// adjacent intersections then it can not be simple ko
				if (nw == 0 || nw == w) {
					return false;
				}
				int xyNN = xyToIndex(x, y - 2);
				int xyWW = xyToIndex(x - 2, y);
				byte nn = this.board[xyNN];
				byte ww = this.board[xyWW];
				// If another stone exist two intersections away that is the
				// same color as the diagonal stone then it is simple ko
				if (nn == nw) {
					return true;
				}
				if (ww == nw) {
					return true;
				}
				return false;
			}
		} else {
			byte s = this.board[xyS];
			if (s == 0) {
				return false;
			}
			// Get east and west intersections
			int xyE = xyToIndex(x + 1, y);
			int xyW = xyToIndex(x - 1, y);
			// South East corner
			if (xyE != -1) {
				byte e = this.board[xyE];
				// If the two adjacent intersection are not the same color it
				// can not be simple ko
				if (e != s) {
					return false;
				}
				int xySE = xyToIndex(x + 1, y + 1);
				byte se = this.board[xySE];
				// If the diagonal intersection is the same color as the
				// adjacent intersections then it can not be simple ko
				if (se == 0 || se == e) {
					return false;
				}
				int xySS = xyToIndex(x, y + 2);
				int xyEE = xyToIndex(x + 2, y);
				byte ss = this.board[xySS];
				byte ee = this.board[xyEE];
				// If another stone exist two intersections away that is the
				// same color as the diagonal stone then it is simple ko
				if (ss == se) {
					return true;
				}
				if (ee == se) {
					return true;
				}
				return false;
				// South West corner
			} else {
				byte w = this.board[xyW];
				// If the two adjacent intersection are not the same color it
				// can not be simple ko
				if (w != s) {
					return false;
				}
				int xySW = xyToIndex(x - 1, y + 1);
				byte sw = this.board[xySW];
				// If the diagonal intersection is the same color as the
				// adjacent intersections then it can not be simple ko
				if (sw == 0 || sw == w) {
					return false;
				}
				int xySS = xyToIndex(x, y + 2);
				int xyWW = xyToIndex(x - 2, y);
				byte ss = this.board[xySS];
				byte ww = this.board[xyWW];
				// If another stone exist two intersections away that is the
				// same color as the diagonal stone then it is simple ko
				if (ss == sw) {
					return true;
				}
				if (ww == sw) {
					return true;
				}
				return false;
			}
		}
	}

	/**
	 * Given an intersection check if it is in a state of simple ko.
	 *
	 * This is a special case when the intersection in question is on an
	 * edge.
	 *
	 * @param x An Integer representing the x coordinate of the intersection
	 * @param y An Integer representing the y coordinate of the intersection
	 * @return True if the intersection is in a state of simple ko,
	 *         else False
	 */
	private boolean isSimpleKoEdge(int x, int y) {
		int xyN = xyToIndex(x, y - 1);
		int xyS = xyToIndex(x, y + 1);
		int xyE = xyToIndex(x + 1, y);
		int xyW = xyToIndex(x - 1, y);
		// North Edge
		if (xyN == -1) {
			byte s = this.board[xyS];
			byte e = this.board[xyE];
			byte w = this.board[xyW];
			// If the three adjacent intersection are not the same color it
			// can not be simple ko
			if (s == 0 || ((s != e) || (s != w))) {
				return false;
			}
			int xySE = xyToIndex(x + 1, y + 1);
			int xySW = xyToIndex(x - 1, y + 1);
			int xySS = xyToIndex(x, y + 2);
			int xyEE = xyToIndex(x + 2, y);
			int xyWW = xyToIndex(x - 2, y);
			byte se = this.board[xySE];
			byte sw = this.board[xySW];
			byte ee;
			byte ww;
			if (xyEE == -1) {
				// Case where the east stone is in the north east corner
				if (se != 0 && se != e) {
					return true;
				}
				ee = 0;
			} else {
				ee = this.board[xyEE];
			}
			if (xyWW == -1) {
				// Case where the west stone is in the north west corner
				if (sw != 0 && sw != w) {
					return true;
				}
				ww = 0;
			} else {
				ww = this.board[xyWW];
			}
			// Cases where the ko instance is along the edge
			if (se != 0 && (se != e && se == ee)) {
				return true;
			}
			if (sw != 0 && (sw != w && sw == ww)) {
				return true;
			}
			byte ss = this.board[xySS];
			// Case where the ko instance is away from the edge
			if ((se != 0 && se == sw) && (se != s && se == ss)) {
				return true;
			}
			return false;
			// South Edge
		} else if (xyS == -1) {
			byte n = this.board[xyN];
			byte e = this.board[xyE];
			byte w = this.board[xyW];
			if (n == 0 || ((n != e) || (n != w))) {
				return false;
			}
			int xyNE = xyToIndex(x + 1, y - 1);
			int xyNW = xyToIndex(x - 1, y - 1);
			int xyNN = xyToIndex(x, y - 2);
			int xyEE = xyToIndex(x + 2, y);
			int xyWW = xyToIndex(x - 2, y);
			byte ne = this.board[xyNE];
			byte nw = this.board[xyNW];
			byte ee;
			byte ww;
			if (xyEE == -1) {
				// Case were the east stone is in the south east corner
				if (ne != 0 && ne != e) {
					return true;
				}
				ee = 0;
			} else {
				ee = this.board[xyEE];
			}
			if (xyWW == -1) {
				// Case were the west stone is in the south west corner
				if (nw != 0 && nw != w) {
					return true;
				}
				ww = 0;
			} else {
				ww = this.board[xyWW];
			}
			// Cases where the ko instance is along the edge
			if (ne != 0 && (ne != e && ne == ee)) {
				return true;
			}
			if (nw != 0 && (nw != w && nw == ww)) {
				return true;
			}
			byte nn = this.board[xyNN];
			// Case where the ko instance is away from the edge
			if ((ne != 0 && ne == nw) && (ne != n && ne == nn)) {
				return true;
			}
			return false;
			// East Edge
		} else if (xyE == -1) {
			byte n = this.board[xyN];
			byte s = this.board[xyS];
			byte w = this.board[xyW];
			if (w == 0 || ((w != n) || (w != s))) {
				return false;
			}
			int xyNW = xyToIndex(x - 1, y - 1);
			int xySW = xyToIndex(x - 1, y + 1);
			int xyNN = xyToIndex(x, y - 2);
			int xySS = xyToIndex(x, y + 2);
			int xyWW = xyToIndex(x - 2, y);
			byte nw = this.board[xyNW];
			byte sw = this.board[xySW];
			byte nn;
			byte ss;
			if (xyNN == -1) {
				// Case were the north stone is in the north east corner
				if (nw != 0 && nw != n) {
					return true;
				}
				nn = 0;
			} else {
				nn = this.board[xyNN];
			}
			if (xySS == -1) {
				// Case were the south stone is in the south east corner
				if (sw != 0 && sw != s) {
					return true;
				}
				ss = 0;
			} else {
				ss = this.board[xySS];
			}
			// Cases where the ko instance is along the edge
			if (nw != 0 && (nw != n && nw == nn)) {
				return true;
			}
			if (sw != 0 && (sw != s && sw == ss)) {
				return true;
			}
			byte ww = this.board[xyWW];
			// Case where the ko instance is away from the edge
			if ((nw != 0 && nw == sw) && (nw != w && nw == ww)) {
				return true;
			}
			return false;
			// West Edge
		} else {
			byte n = this.board[xyN];
			byte s = this.board[xyS];
			byte e = this.board[xyE];
			if (e == 0 || ((e != n) || (e != s))) {
				return false;
			}
			int xyNE = xyToIndex(x + 1, y - 1);
			int xySE = xyToIndex(x + 1, y + 1);
			int xyNN = xyToIndex(x, y - 2);
			int xySS = xyToIndex(x, y + 2);
			int xyEE = xyToIndex(x + 2, y);
			byte ne = this.board[xyNE];
			byte se = this.board[xySE];
			byte nn;
			byte ss;
			if (xyNN == -1) {
				// Case were the north stone is in the north west corner
				if (ne != 0 && ne != n) {
					return true;
				}
				nn = 0;
			} else {
				nn = this.board[xyNN];
			}
			if (xySS == -1) {
				// Case were the south stone is in the south west corner
				if (se != 0 && se != s) {
					return true;
				}
				ss = 0;
			} else {
				ss = this.board[xySS];
			}
			// Cases where the ko instance is along the edge
			if (ne != 0 && (ne != n && ne == nn)) {
				return true;
			}
			if (se != 0 && (se != s && se == ss)) {
				return true;
			}
			byte ee = this.board[xyEE];
			// Case where the ko instance is away from the edge
			if ((ne != 0 && ne == se) && (ne != e && ne == ee)) {
				return true;
			}
			return false;
		}
	}

	/**
	 * Turn a 2D x and y coordinate into a 1D xy coordinate.
	 *
	 * @param x An Integer representing the x coordinate of the intersection
	 * @param y An Integer representing the y coordinate of the intersection
	 * @return An Integer representing the 1D coordinate
	 */
	@Override
	public int xyToIndex(int x, int y) {
		if (x < 0) {
			return -1;
		} else if (y < 0) {
			return -1;
		} else if (x >= this.getBoardWidth()) {
			return -1;
		} else if (y >= this.getBoardHeight()) {
			return -1;
		} else {
			return y * this.getBoardWidth() + x;
		}
	}

	/**
	 * Create a deep copy of this GoBoard Object.
	 *
	 * @return A GoBoard
	 */
	@Override
	public GoBoard deepCopy() {
		return new GoBoard(this);
	}

	/**
	 * Format String of the board.
	 *
	 * @return String of the board
	 */
	public String stringBoard() {
		StringBuilder s = new StringBuilder();
		s.append("\n");
		s.append("+");
		for (short i = 0; i < this.getBoardWidth(); i++) {
			s.append("----");
		}
		s.append("+");
		s.append("\n");

		for (short y = 0; y < this.getBoardWidth(); y++) {
			s.append("|");
			for (short x = 0; x < this.getBoardWidth(); x++) {
				byte b = this.board[xyToIndex(x, y)];
				if (this.board[xyToIndex(x, y)] == 1) {
					s.append("\033[0;33m");
				} else if (this.board[xyToIndex(x, y)] == 2) {
					s.append("\033[0;31m");
				}
				if (b < 0) {
					s.append("  " + b);
				} else {
					s.append("   " + b);
				}
				if (this.board[xyToIndex(x, y)] != 0) {
					s.append("\033[0m");
				}
			}
			s.append("|");
			s.append("\n");
		}

		s.append("+");
		for (short i = 0; i < this.getBoardWidth(); i++) {
			s.append("----");
		}
		s.append("+");
		return (s.toString());
	}

	/**
	 * Format String of the chain board.
	 *
	 * @return String of the chain board
	 */
	public String stringChain() {
		StringBuilder s = new StringBuilder();
		s.append("\n");
		s.append("+");
		for (short i = 0; i < this.getBoardWidth(); i++) {
			s.append("----");
		}
		s.append("+");
		s.append("\n");

		for (short y = 0; y < this.getBoardWidth(); y++) {
			s.append("|");
			for (short x = 0; x < this.getBoardWidth(); x++) {
				short b = this.chainBoard[xyToIndex(x, y)];
				if (this.board[xyToIndex(x, y)] == 1) {
					s.append("\033[0;33m");
				} else if (this.board[xyToIndex(x, y)] == 2) {
					s.append("\033[0;31m");
				}
				if (b < 0) {
					s.append("  " + b);
				} else if (b < 10) {
					s.append("   " + b);
				} else if (b < 100) {
					s.append("  " + b);
				} else {
					s.append(" " + b);
				}
				if (this.board[xyToIndex(x, y)] != 0) {
					s.append("\033[0m");
				}
			}

			s.append("|");
			s.append("\n");
		}

		s.append("+");
		for (short i = 0; i < this.getBoardWidth(); i++) {
			s.append("----");
		}
		s.append("+");
		return (s.toString());
	}

	/**
	 * Format String of the pseudo board.
	 *
	 * @return String of the pseudo board
	 */
	public String stringPseudo() {
		StringBuilder s = new StringBuilder();
		s.append("\n");
		s.append("+");
		for (short i = 0; i < this.getBoardWidth(); i++) {
			s.append("----");
		}
		s.append("+");
		s.append("\n");

		for (short y = 0; y < this.getBoardWidth(); y++) {
			s.append("|");
			for (short x = 0; x < this.getBoardWidth(); x++) {
				byte b = this.pseudoBoard[xyToIndex(x, y)];
				if (this.board[xyToIndex(x, y)] == 1) {
					s.append("\033[0;33m");
				} else if (this.board[xyToIndex(x, y)] == 2) {
					s.append("\033[0;31m");
				}
				if (b < 0) {
					s.append("  " + b);
				} else {
					s.append("   " + b);
				}
				if (this.board[xyToIndex(x, y)] != 0) {
					s.append("\033[0m");
				}
			}
			s.append("|");
			s.append("\n");
		}

		s.append("+");
		for (short i = 0; i < this.getBoardWidth(); i++) {
			s.append("----");
		}
		s.append("+");
		return (s.toString());
	}

	/**
	 * Format printing of the board.
	 *
	 * @return No return value.
	 */
	@Override
	public void printPretty() {
		Log.info(stringBoard());
	}

	/**
	 * Format printing of the chain board.
	 *
	 * @return No return value.
	 */
	public void printChains() {
		Log.info(stringChain());
	}

	/**
	 * Format printing of the pseudo liberty board.
	 *
	 * @return No return value.
	 */
	public void printPseudo() {
		Log.info(stringPseudo());
	}

	/**
	 * Format print the entire state's information
	 *
	 * @return No return value.
	 */
	public void printState() {
		printPretty();
		printPseudo();
		printChains();
		Log.info("Player 1 score : " + this.player1score);
		Log.info("Player 2 score : " + this.player2score);
		List<Action> player1 = getFullList(0);
		List<Action> player2 = getFullList(1);
		printActionList(player1, 0);
		printActionList(player2, 1);
	}

	/**
	 * Helper function for printing a players moves
	 *
	 * @return No return value.
	 */
	private void printActionList(List<Action> list, int player) {
		StringBuilder s = new StringBuilder();
		s.append("Player " + (player + 1) + " List");
		Iterator iter = list.iterator();
		for (Action move : list) {
			if (move instanceof XYAction) {
				XYAction xyMove = (XYAction) move;
				s.append("(" + xyMove.getX() + ", " + xyMove.getY() + ")");
			} else {
				s.append("(IdleAction)");
			}
		}
		Log.info(s.toString());
	}
}

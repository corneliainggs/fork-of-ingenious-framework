package za.ac.sun.cs.ingenious.games.gridWorld.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldEngine;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldFeatureExtractor;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldPolicyStringBuilder;
import za.ac.sun.cs.ingenious.search.rl.qlearning.ExternalDQN;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * External DQN engine for GridWorld.
 *
 * @author Steffen Jacobs
 */
public class GridWorldExternalDQNEngine extends GridWorldEngine {
    private final String NETWORK = "GridWorldNN";
    private final double ALPHA = 0.001;
    private final double EPSILON = 1.0;
    private final double EPSILON_STEP = 1.0 / 100000;
    private final double EPSILON_MIN = 0.1;
    private final double GAMMA = 0.97;
    private final int BUFFER_MEMORY = 100000;
    private final int SAMPLE_SIZE = 32;
    private final int SAMPLE_THRESHOLD = 10000;
    private final int SAMPLE_FREQUENCY = 4;
    private final int TARGET_NETWORK_UPDATE_FREQUENCY = 500;
    private final boolean DOUBLE_LEARNING = true;
    private final boolean PRIORITIZED_SAMPLING = true;
    private final double BUFFER_ALPHA = 0.5;

    private GridWorldState previousState = null;
    private Action previousAction = null;
    private ExternalDQN alg;
    private boolean enableTraining;

    private List<Action> actionsAvailable;

    public GridWorldExternalDQNEngine(EngineToServerConnection toServer) throws IOException, ClassNotFoundException {
        super(toServer);

        this.enableTraining = false;
        actionsAvailable = Arrays.asList(
                new CompassDirectionAction(playerID, CompassDirection.N),
                new CompassDirectionAction(playerID, CompassDirection.E),
                new CompassDirectionAction(playerID, CompassDirection.S),
                new CompassDirectionAction(playerID, CompassDirection.W)
        );
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        if (alg == null) {
            try {
                double[][][] sampleChannelSet = new GridWorldFeatureExtractor().buildTensor(state, logic);
                alg = new ExternalDQN(
                        "GridWorldExternalDQNEngine_" + playerID,
                        enableTraining,
                        logic,
                        new GridWorldFeatureExtractor(),
                        NETWORK,
                        sampleChannelSet.length,
                        sampleChannelSet[0].length,
                        sampleChannelSet[0][0].length,
                        actionsAvailable,
                        ALPHA,
                        EPSILON,
                        EPSILON_STEP,
                        EPSILON_MIN,
                        GAMMA,
                        BUFFER_MEMORY,
                        SAMPLE_SIZE,
                        SAMPLE_THRESHOLD,
                        SAMPLE_FREQUENCY,
                        TARGET_NETWORK_UPDATE_FREQUENCY,
                        DOUBLE_LEARNING,
                        PRIORITIZED_SAMPLING,
                        BUFFER_ALPHA
                );

                alg.displayChart();
                alg.setChartUpdateFreq(1000);
                alg.setLogFileUpdateFreq(1000);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String policyString = GridWorldPolicyStringBuilder.buildUsingExternalNeuralNetwork(
                state,
                alg.getNeuralNetwork(),
                alg.getFeatureExtractor(),
                logic
        );
        Log.info(policyString);

        alg.printMetrics();
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "GridWorldExternalDQNEngine";
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Action choice = null;
        List<Action> availableActions = logic.generateActions(state, playerID);

        choice = alg.chooseAction(state, availableActions);

        previousAction = choice.deepCopy();
        previousState = state.deepCopy();

        return new PlayActionMessage(choice);
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        ScalarReward reward = (ScalarReward) a.getReward();

        alg.update(previousState, previousAction, reward, state);
    }

    @Override
    public void receiveMatchResetMessage(MatchResetMessage a) {
        super.receiveMatchResetMessage(a);
        alg.endEpisode();

        if (alg.getTotalEpisodes() % 1000 == 0) {
            String policyString = GridWorldPolicyStringBuilder.buildUsingExternalNeuralNetwork(
                    state,
                    alg.getNeuralNetwork(),
                    alg.getFeatureExtractor(),
                    logic
            );
            Log.info(policyString);
        }
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        String policyString = GridWorldPolicyStringBuilder.buildUsingExternalNeuralNetwork(
                state,
                alg.getNeuralNetwork(),
                alg.getFeatureExtractor(),
                logic
        );
        Log.info(policyString);

        alg.printMetrics();
        alg.close();
    }

    public void setTraining() {
        this.enableTraining = true;
    }

    public ExternalDQN getAlg() {
        return alg;
    }
}

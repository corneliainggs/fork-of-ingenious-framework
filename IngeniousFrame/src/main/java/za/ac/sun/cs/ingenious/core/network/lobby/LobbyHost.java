package za.ac.sun.cs.ingenious.core.network.lobby;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Referee;
import za.ac.sun.cs.ingenious.core.RefereeFactory;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.ServerToEngineConnection;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.JoinedLobbyMessage;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.SendNameMessage;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.JoinMessage;
import za.ac.sun.cs.ingenious.core.util.referee.GeneralReferee;
import za.ac.sun.cs.ingenious.core.util.stochastic.StochasticEngine;

/**
 * Handles all actions specific to a lobby on the server side. Can accept new
 * joining
 * players and will start the game once enough players have joined.
 */
public class LobbyHost {

    protected final MatchSetting matchSettings;
    protected volatile int numberOfEngines;
    private int maxPlayers;
    protected ServerToEngineConnection[] engineConns;

    private Referee referee; // null if game is not yet started

    private LobbyManager lobbyManager;

    public LobbyHost(MatchSetting match, LobbyManager lobbyManager) {
        this.matchSettings = match;
        this.numberOfEngines = 0;
        this.lobbyManager = lobbyManager;
        this.maxPlayers = match.getNumPlayers();
        this.engineConns = new ServerToEngineConnection[matchSettings.getNumPlayers()];
        this.referee = null; // Game is not yet started
    }

    /**
     * Adds the given player to the lobby. Performs a handshake wherein the new
     * player is assigned an ID
     * Once the last player has joined the game is started immediately.
     * 
     * @return ID of the newly joined player or -1 if there was an error.
     */
    public synchronized int acceptJoiningPlayers(Socket client, ObjectInputStream is,
            ObjectOutputStream os) throws IOException, ClassNotFoundException {
        int id = getFreeID();
        Log.info("Player " + id + " joined lobby, initiating handshake");

        // Perform Handshake with newly connected player
        String playerName = "NO_NAME_RECEIVED";
        os.writeObject(new JoinedLobbyMessage(id));
        playerName = ((SendNameMessage) is.readObject()).getPlayerName();
        Log.info("Player name for id " + id + ": " + playerName);

        ServerToEngineConnection engine = new ServerToEngineConnection(id, this, client, is, os, playerName);

        if (engineConns[numberOfEngines] == null) {
            engineConns[numberOfEngines] = engine;
            numberOfEngines++;
            Log.info("The number of engines has just been increased to : " + numberOfEngines);
        } else {
            Log.error("Cannot accept new joining player, need last player to disconnect first");
            return -1;
        }
        if (numberOfEngines == matchSettings.getNumPlayers()) {
            if (createReferee()) {
                startGame();
            }
        }

        // TODO: Game will stall if stochastic player fails to connect
        if (engine.getPlayerName().equals(StochasticEngine.engineName)) {
            Log.info("Starting game from stochastic entry point");
            startGame();
        }

        return id;
    }

    private int getFreeID() {
        for (int i = 0; i < engineConns.length; i++) {
            if (engineConns[i] == null) {
                return i;
            }
        }
        Log.error("LobbyHost", "No free ID available");
        throw new RuntimeException("No free ID available");
    }

    /**
     * First checks if a stochastic player is required for the domain. If so, the
     * stochastic player
     * is created. Otherwise the game starts by unregistering this lobby and then
     * starting the
     * referee thread. A reference to the newly started referee is kept in the lobby
     * manager (so
     * that the {@link GameServer} can keep track of all the threads it started).
     */
    protected void startGame() throws IOException {
        if (referee instanceof GeneralReferee
                && ((GeneralReferee) referee).getLogic().requiresStochasticPlayer()
                && !engineConns[engineConns.length - 1].getPlayerName().equals(StochasticEngine.engineName)) {
            connectStochasticPlayer();
        } else {
            Log.info("Starting game");
            if (lobbyManager != null) {
                lobbyManager.unregisterLobby(matchSettings.getLobbyName());
                for (ServerToEngineConnection engine : engineConns) {
                    if (engine != null) {
                        lobbyManager.removeClientHandler(engine.getPlayerName());
                    }
                }
            }

            Thread newThread = new Thread(referee);
            newThread.start();
            lobbyManager.registerStartedReferee(newThread);
        }
    }

    /**
     * Creates a referee if one has not yet been created for this lobby by using the
     * {@link RefereeFactory}.
     * 
     * @return Successfully created the referee.
     */
    private boolean createReferee() {
        if (referee != null) {
            Log.error("LobbyHost",
                    "The lobby " + matchSettings.getLobbyName() + " for game " + matchSettings.getGameName()
                            + " has already started the game once. Can not start the game again.");
            return false;
        }

        referee = RefereeFactory.getInstance().getReferee(matchSettings, engineConns);

        return true;
    }

    public MatchSetting getMatchSetting() {
        return this.matchSettings;
    }

    /**
     * Called whenever an engine disconnects.
     */
    public void engineDisconnected(int id) {
        Log.info("Player " + id + " disconnected.");
        numberOfEngines--;

        if (referee == null) { // Game not yet started
            engineConns[id] = null;
            if (numberOfEngines == 0) {
                lobbyManager.unregisterLobby(matchSettings.getLobbyName());
                Log.info("Lobby " + matchSettings.getLobbyName() + " unregistered due to no more players");
            }
        } else {
            referee.engineDisconnected(id);
        }
    }

    public int getNumberOfEngines() {
        return numberOfEngines;
    }

    public int getMaxPlayers() {
        return matchSettings.getNumPlayers();
    }

    protected void connectStochasticPlayer() throws IOException {
        ObjectInputStream inputStream;
        ObjectOutputStream outputStream;
        Socket socket;
        String hostName;
        int port;

        ServerToEngineConnection[] temp = new ServerToEngineConnection[engineConns.length + 1];
        System.arraycopy(engineConns, 0, temp, 0, matchSettings.getNumPlayers());
        engineConns = temp;

        hostName = matchSettings.getSettingAsString("hostname", "localhost");
        port = matchSettings.getSettingAsInt("port", 61234);
        Log.info("Connecting to " + hostName + ":" + port);

        socket = new Socket(hostName, port);
        outputStream = new ObjectOutputStream(socket.getOutputStream());
        inputStream = new ObjectInputStream(socket.getInputStream());

        Log.info("Registering: " + StochasticEngine.engineName);
        outputStream.writeObject(new String(StochasticEngine.engineName));

        String ack;
        try {
            ack = (String) inputStream.readObject();
            if (!ack.equals("Acknowledged")) {
                Log.error("Player name StochasticPlayer1 already taken.");
                System.exit(-1);
            }
        } catch (ClassNotFoundException e2) {
            Log.error(e2.getMessage());
            System.exit(-1);
        }

        outputStream.writeObject(new JoinMessage(matchSettings.getLobbyName()));
        JoinedLobbyMessage a = new JoinedLobbyMessage(matchSettings.getNumPlayers());
        outputStream.writeObject(new SendNameMessage(StochasticEngine.engineName));
        EngineToServerConnection conn = new EngineToServerConnection(socket, inputStream, outputStream,
                matchSettings.getNumPlayers());

        referee.setStochasticEngine(conn);
    }
}
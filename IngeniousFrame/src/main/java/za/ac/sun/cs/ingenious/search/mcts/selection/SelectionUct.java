package za.ac.sun.cs.ingenious.search.mcts.selection;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.esotericsoftware.minlog.Log;

/**
 * A standard implementation of the SelectionThreadSafe strategy
 * {@link SelectionThreadSafe}, that
 * calculates UCT values to make a decision of which child node to select.
 *
 * @author Karen Laubscher
 *
 * @param <S> The game state type.
 * @param <C> The type that a child in the mcts node is stored as.
 * @param <N> The type of the mcts node.
 */
public class SelectionUct<S extends GameState, C, N extends MctsNodeCompositionInterface<S, C, ?>>
		implements SelectionThreadSafe<C, N> {

	private GameLogic<S> logic;
	private double c;
	private final ReadWriteLock lock = new ReentrantReadWriteLock();
	private final Lock readLock = lock.readLock();

	/**
	 * Constructor to create a UCT selection object, since the constant C is not
	 * supplied, the default value for C is assigned as square root of 2.
	 *
	 * @param logic The game logic
	 */
	private SelectionUct(GameLogic<S> logic) {
		this.logic = logic;
		this.c = Math.sqrt(2);
	}

	/**
	 * Constructor to create a UCT selection object with a specified value for
	 * the constant C used in the UCT value calculation.
	 *
	 * @param logic The game logic
	 * @param c     The constant value for C (in the UCT calculation).
	 */
	private SelectionUct(GameLogic<S> logic, double c) {
		this.logic = logic;
		this.c = c;
	}

	/**
	 * The method that decides which child node to traverse to next, based on
	 * calculating the UCT value for each child and then selecting the child
	 * with the highest UCT value.
	 * 
	 * @param node The current node whose children nodes are considered for the
	 *             next node to traverse to.
	 * 
	 * @return The selected child node with the highest UCT value.
	 */
	public C select(N node) {
		List<C> children = node.getChildren();

		// Terminal state - no further selection takes place, therefore return node as
		// selected node
		readLock.lock();
		try {
			if (logic.isTerminal(node.getState(false))) {
				return node.getSelfAsChildType();
			}
		} finally {
			readLock.unlock();
		}

		if (!node.unexploredActionsEmpty()) {
			return null;
		}

		// Node is fully expanded, choose suitable child
		C highestUctChild = null;
		try {
			highestUctChild = children.get(0);
		} catch (Exception ex) {
			Log.error("SelectionUCT", "No children found");
		}

		double highestUct;
		if (node.getVisitCount() == 0) {
			highestUct = Double.POSITIVE_INFINITY;
		} else {
			highestUct = node.getChildValue(highestUctChild)
					+ (c * Math.sqrt(2 * Math.log(node.getVisitCount())
							/ ((MctsNodeComposition) highestUctChild).getVisitCount()));
		}
		for (C child : children) {
			double tempVal;
			if (node.getVisitCount() == 0) {
				tempVal = Double.POSITIVE_INFINITY;
			} else {
				tempVal = node.getChildValue(child)
						+ (c * Math.sqrt(2 * Math.log(node.getVisitCount())
								/ ((MctsNodeComposition) highestUctChild).getVisitCount()));
			}
			if (tempVal > highestUct) {
				highestUctChild = child;
				highestUct = tempVal;
			}
		}
		if (highestUctChild == null) {
			Log.error("SelectionUCT", "Highest UCT child is null");
		}
		return highestUctChild;
	}

	/**
	 * A static factory method, which creates a UCT selection object that uses
	 * the default value of square root 2 for the constant C in the UCTS
	 * calculation.
	 * 
	 * This factory method is also a generic method, which uses Java type
	 * inferencing to encapsulate the complex generic type capturing required
	 * by the UCT selection constructor.
	 *
	 * @param logic The game logic
	 */
	public static <SS extends GameState, CC, NN extends MctsNodeCompositionInterface<SS, CC, ?>> SelectionThreadSafe<CC, NN> newSelectionUct(
			GameLogic<SS> logic) {
		return new SelectionUct<>(logic);
	}

	/**
	 * A static factory method, which creates a UCT selection object that uses
	 * the specified value for the constant C in the UCT value calculation.
	 * 
	 * This factory method is also a generic method, which uses Java type
	 * inferencing to encapsulate the complex generic type capturing required
	 * by the UCT selection constructor.
	 *
	 * @param logic The game logic
	 * @param c     The constant value for C (in the UCT calculation).
	 */
	public static <SS extends GameState, CC, NN extends MctsNodeCompositionInterface<SS, CC, ?>> SelectionThreadSafe<CC, NN> newSelectionUct(
			GameLogic<SS> logic, double c) {
		return new SelectionUct<>(logic, c);
	}

}

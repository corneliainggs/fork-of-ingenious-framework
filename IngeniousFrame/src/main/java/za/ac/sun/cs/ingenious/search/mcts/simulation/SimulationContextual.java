package za.ac.sun.cs.ingenious.search.mcts.simulation;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.CMCTable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class SimulationContextual<S extends GameState> extends SimulationThreadSafe<S> {

    private CMCTable cmcTable;
    private ThreadLocalRandom randomGen = ThreadLocalRandom.current();
    MctsGameFinalEvaluator<S> evaluator;
    ActionSensor<S> obs;
    boolean recordMoves;

    /**
     * Constructor.
     *
     * @param logic             The game logic used during the simulation.
     * @param evaluator         The game evaluator used to determine the results of
     *                          the simulation.
     * @param obs               Object to process actions made by the players during
     *                          playouts. Actions are observed from
     *                          the point of view of the environment player.
     * @param visitedMovesTable The object representing all two-move tiles and their
     *                          corresponding visit counts and win
     *                          counts.
     */
    public SimulationContextual(GameLogic<S> logic, MctsGameFinalEvaluator<S> evaluator, ActionSensor<S> obs,
            boolean recordMoves, CMCTable cmcTable) {
        this.logic = logic;
        this.evaluator = evaluator;
        this.obs = obs;
        this.recordMoves = recordMoves;
        this.cmcTable = cmcTable;
    }

    /**
     * The method that performs a simulation playout based on the CMC enhancement
     * algorithm.
     *
     * @param state The game state from which to start the simulation playout.
     * @return The result scores
     */
    public SimulationTuple simulate(List<Action> selectionMoves, S state) {
        // Only add selectionMoves at the end of this function as only moves in the
        // simulation are used to update the CMCTable
        Action nextAction;
        List<Action> simulationMoves = new ArrayList<>();

        // TODO : Generalise for more than two players..? This might require some
        // consideration for how the logic will work. See issue #327
        Action[] previousActions = new Action[2];
        previousActions[0] = null;
        previousActions[1] = null;

        while (!logic.isTerminal(state)) {
            for (int playerId : logic.getCurrentPlayersToAct(state)) {
                nextAction = cmcTable.getCMCAction(logic, state, playerId, randomGen, previousActions[playerId]);
                previousActions[playerId] = nextAction;
                logic.makeMove(state, obs.fromPointOfView(nextAction, state, playerId));
                if (recordMoves) {
                    simulationMoves.add(nextAction);
                }
            }
        }
        double[] scores = evaluator.getMctsScore(state);
        cmcTable.updateVisitedMovesTable(simulationMoves, scores);
        // add simulation moves to selection moves
        List<Action> fullMovePath = new ArrayList<>(selectionMoves);
        fullMovePath.addAll(simulationMoves);
        return new SimulationTuple(fullMovePath, scores);
    }

    public static <SS extends GameState> SimulationThreadSafe<SS> newSimulationContextual(GameLogic<SS> logic,
            MctsGameFinalEvaluator<SS> evaluator, ActionSensor<SS> obs, boolean recordMoves, CMCTable cmcTable) {
        return new SimulationContextual<SS>(logic, evaluator, obs, recordMoves, cmcTable);
    }
}

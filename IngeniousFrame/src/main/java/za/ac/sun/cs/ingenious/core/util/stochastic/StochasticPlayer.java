package za.ac.sun.cs.ingenious.core.util.stochastic;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;

public class StochasticPlayer implements PlayerRepresentation {

	protected Engine engine;

	public StochasticPlayer(Engine engine) {
		this.engine = engine;
	}

	public int getID() {
		return engine.getPlayerID();
	}

	public void initGame(InitGameMessage a) {
		engine.receiveInitGameMessage(a);
	}

	public void playMove(PlayedMoveMessage a) {
		engine.receivePlayedMoveMessage(a);
	}

	public PlayActionMessage genAction(GenActionMessage a) {
		return engine.receiveGenActionMessage(a);
	}

	public void terminateGame(GameTerminatedMessage a) {
		engine.receiveGameTerminatedMessage(a);
	}

	public void resetMatch(MatchResetMessage a) {
		engine.receiveMatchResetMessage(a);
	}

	public void reward(RewardMessage a) {
		engine.receiveRewardMessage(a);
	}
}
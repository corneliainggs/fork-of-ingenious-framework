import numpy as np
import torch

from src.core.util.constants import Constants
from src.core.util.transition.transition import Transition


class TransitionBatch(object):
    """
    Class that wraps (S,A,R,S') tuples zipped together.
    """

    def __init__(self, transitions: [Transition]):
        temp_state_batch = []
        self.action_batch = []
        self.reward_batch = []

        for i in range(len(transitions)):
            temp_state_batch.append(transitions[i].state)
            self.action_batch.append(transitions[i].action)
            self.reward_batch.append(transitions[i].reward)

        self.state_batch = torch.stack(temp_state_batch)

    def __str__(self) -> str:
        return f"TransitionBatch: " \
               f"\nStates: {str(self.state_batch)}" \
               f"\nActions: {str(self.action_batch)}" \
               f"\nRewards: {str(self.reward_batch)}" \
               f"\nNext states: {str(self.next_state_batch)}"




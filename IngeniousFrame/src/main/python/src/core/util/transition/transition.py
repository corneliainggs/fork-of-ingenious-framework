import sys


class Transition(object):
    """
    Class that wraps a (S,A,R,S') tuple.
    """

    def __init__(self, state, action: int, reward: float, next_state):
        self.state = state
        self.action = action
        self.reward = reward
        self.next_state = next_state
        self.priority = 1000

    def __str__(self) -> str:
        return f"({str(self.state)}, {str(self.action)}, {str(self.reward)}, {str(self.next_state)})"


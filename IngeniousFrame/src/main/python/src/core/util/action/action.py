class Action:
    """
    Simple class that represents an action. Currently, the simplest way to handle actions is to represent them with an
    action number. The translation between actions and action numbers are handled in the Ingenious Framework, ideally in
    a layer between the algorithm and communication classes.
    """
    def __init__(self, action_number: int):
        self.action_number = action_number

    def to_dict(self):
        return self.__dict__

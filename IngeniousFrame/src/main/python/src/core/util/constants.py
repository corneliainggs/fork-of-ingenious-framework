import torch


class Constants:
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Random;
import za.ac.sun.cs.ingenious.games.go.GoBoard;
import za.ac.sun.cs.ingenious.games.go.GoLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import java.lang.reflect.Method;

public class GoTests {

	/*
	 * This test plays a random 19 by 19 Go game and prints the entire state
	 * after each move. This is done to verify these results by hand.
	 *
	 * This is also a simulation of the MCTS simulation phase
	 *
	 * This test has no assertions
	 */
	@Test
	public void test() {
		GoBoard state = new GoBoard(19, 0, 2);
		GoLogic logic = new GoLogic();
		System.out.println("Start Game\n");
		Random randomGen = new Random();
		Class[] cArg = new Class[2];
		cArg[0] = GoBoard.class;
		cArg[1] = int.class;
		while (!logic.isTerminal(state)) {
			state.printState();
			Set<Integer> playersToAct = logic.getCurrentPlayersToAct(state);
			List<Action> actions;
			for (int playerId : playersToAct) {
				Method methodToFind = null;
				try {
					methodToFind = GoLogic.class.getMethod("generateAction", cArg);
				} catch (NoSuchMethodException | SecurityException e) {
					System.out.println("No Method Found");
				}
				if (methodToFind == null) {
					actions = logic.generateActions(state, playerId);
					if (!actions.isEmpty()) {
						logic.makeMove(state, actions.get(randomGen.nextInt(actions.size())));
					} else {
						logic.makeMove(state, new IdleAction(playerId));
					}
				} else {
					logic.makeMove(state, logic.generateAction(state, playerId));
				}

			}
		}
		state.printState();
	}

	@Test
	public void captureCorner() {
		GoBoard state = new GoBoard(3, 0, 2);
		GoLogic logic = new GoLogic();
		logic.makeMove(state, new XYAction(0, 0, 0));
		logic.makeMove(state, new XYAction(1, 0, 1));
		logic.makeMove(state, new XYAction(2, 2, 0));
		logic.makeMove(state, new XYAction(0, 1, 1));
		assertFalse(logic.validMove(state, new XYAction(0, 0, 0)));
	}

	@Test
	public void captureSide() {
		GoBoard state = new GoBoard(3, 0, 2);
		GoLogic logic = new GoLogic();
		logic.makeMove(state, new XYAction(1, 0, 0));
		logic.makeMove(state, new XYAction(0, 0, 1));
		logic.makeMove(state, new XYAction(0, 2, 0));
		logic.makeMove(state, new XYAction(1, 1, 1));
		logic.makeMove(state, new XYAction(1, 2, 0));
		logic.makeMove(state, new XYAction(2, 0, 1));
		assertFalse(logic.validMove(state, new XYAction(1, 0, 0)));
	}

	@Test
	public void captureMiddle() {
		GoBoard state = new GoBoard(3, 0, 2);
		GoLogic logic = new GoLogic();
		logic.makeMove(state, new XYAction(1, 1, 0));
		logic.makeMove(state, new XYAction(1, 0, 1));
		logic.makeMove(state, new XYAction(0, 0, 0));
		logic.makeMove(state, new XYAction(0, 1, 1));
		assertFalse(logic.validMove(state, new XYAction(0, 0, 0)));
		logic.makeMove(state, new XYAction(2, 0, 0));
		logic.makeMove(state, new XYAction(2, 1, 1));
		assertFalse(logic.validMove(state, new XYAction(2, 0, 0)));
		logic.makeMove(state, new XYAction(2, 2, 0));
		logic.makeMove(state, new XYAction(1, 2, 1));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(0, 2, 0)));
		assertFalse(logic.validMove(state, new XYAction(2, 2, 0)));
	}

	@Test
	public void captureTrueEye() {
		GoBoard state = new GoBoard(3, 0, 2);
		GoLogic logic = new GoLogic();
		logic.makeMove(state, new XYAction(1, 1, 0));
		logic.makeMove(state, new XYAction(1, 0, 1));
		logic.makeMove(state, new XYAction(0, 0, 0));
		logic.makeMove(state, new XYAction(0, 1, 1));
		assertFalse(logic.validMove(state, new XYAction(0, 0, 0)));
		logic.makeMove(state, new XYAction(2, 0, 0));
		logic.makeMove(state, new XYAction(2, 1, 1));
		assertFalse(logic.validMove(state, new XYAction(2, 0, 0)));
		logic.makeMove(state, new XYAction(2, 2, 0));
		logic.makeMove(state, new XYAction(1, 2, 1));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(0, 2, 0)));
		assertFalse(logic.validMove(state, new XYAction(2, 2, 0)));
		logic.makeMove(state, new XYAction(0, 0, 1));
		logic.makeMove(state, new XYAction(2, 0, 1));
		logic.makeMove(state, new XYAction(2, 2, 1));
		logic.makeMove(state, new XYAction(0, 2, 1));
		assertTrue(logic.validMove(state, new XYAction(1, 1, 0)));
	}

	@Test
	public void captureEye() {
		GoBoard state = new GoBoard(3, 0, 2);
		GoLogic logic = new GoLogic();
		logic.makeMove(state, new XYAction(1, 0, 0));
		logic.makeMove(state, new XYAction(1, 1, 1));
		logic.makeMove(state, new XYAction(1, 2, 0));
		logic.makeMove(state, new XYAction(0, 0, 1));
		logic.makeMove(state, new XYAction(0, 2, 0));
		logic.makeMove(state, new XYAction(2, 0, 1));
		logic.makeMove(state, new XYAction(0, 1, 0));
		logic.makeMove(state, new XYAction(2, 1, 1));
		assertTrue(logic.validMove(state, new XYAction(1, 0, 0)));
	}

	@Test
	public void koTest() {
		GoBoard state = new GoBoard(5, 0, 2);
		GoLogic logic = new GoLogic();
		logic.makeMove(state, new XYAction(0, 2, 0));
		logic.makeMove(state, new XYAction(2, 1, 1));
		logic.makeMove(state, new XYAction(1, 1, 0));
		logic.makeMove(state, new XYAction(3, 2, 1));
		logic.makeMove(state, new XYAction(1, 3, 0));
		logic.makeMove(state, new XYAction(2, 3, 1));
		logic.makeMove(state, new XYAction(2, 2, 0));
		logic.makeMove(state, new XYAction(1, 2, 1));
		assertFalse(logic.validMove(state, new XYAction(2, 2, 0)));
	}

	@Test
	public void painTest() {
		GoBoard state = new GoBoard(5, 0, 2);
		GoLogic logic = new GoLogic();
		logic.makeMove(state, new XYAction(1, 1, 0));
		logic.makeMove(state, new XYAction(1, 2, 1));
		logic.makeMove(state, new XYAction(0, 2, 0));
		logic.makeMove(state, new XYAction(3, 2, 1));
		logic.makeMove(state, new XYAction(1, 3, 0));
		logic.makeMove(state, new XYAction(0, 0, 1));
		logic.makeMove(state, new XYAction(3, 1, 0));
		logic.makeMove(state, new XYAction(4, 0, 1));
		logic.makeMove(state, new XYAction(4, 2, 0));
		logic.makeMove(state, new XYAction(0, 4, 1));
		logic.makeMove(state, new XYAction(3, 3, 0));
		logic.makeMove(state, new XYAction(4, 4, 1));
		logic.makeMove(state, new XYAction(2, 0, 0));
		logic.makeMove(state, new XYAction(2, 1, 1));
		logic.makeMove(state, new XYAction(2, 4, 0));
		logic.makeMove(state, new XYAction(2, 3, 1));
		logic.makeMove(state, new XYAction(2, 2, 0));
	}

	@Test
	public void capture4area() {
		GoBoard state = new GoBoard(9, 0, 2);
		GoLogic logic = new GoLogic();
		logic.makeMove(state, new XYAction(3, 1, 0));
		logic.makeMove(state, new XYAction(4, 1, 0));
		logic.makeMove(state, new XYAction(5, 1, 0));
		logic.makeMove(state, new XYAction(6, 1, 0));
		logic.makeMove(state, new XYAction(1, 2, 0));
		logic.makeMove(state, new XYAction(2, 2, 0));
		logic.makeMove(state, new XYAction(3, 2, 0));
		logic.makeMove(state, new XYAction(6, 2, 0));
		logic.makeMove(state, new XYAction(7, 2, 0));
		logic.makeMove(state, new XYAction(8, 2, 0));
		logic.makeMove(state, new XYAction(1, 3, 0));
		logic.makeMove(state, new XYAction(3, 3, 0));
		logic.makeMove(state, new XYAction(5, 3, 0));
		logic.makeMove(state, new XYAction(6, 3, 0));
		logic.makeMove(state, new XYAction(1, 4, 0));
		logic.makeMove(state, new XYAction(1, 5, 0));
		logic.makeMove(state, new XYAction(3, 5, 0));
		logic.makeMove(state, new XYAction(5, 5, 0));
		logic.makeMove(state, new XYAction(6, 5, 0));
		logic.makeMove(state, new XYAction(7, 5, 0));
		logic.makeMove(state, new XYAction(8, 5, 0));
		logic.makeMove(state, new XYAction(1, 6, 0));
		logic.makeMove(state, new XYAction(2, 6, 0));
		logic.makeMove(state, new XYAction(3, 6, 0));
		logic.makeMove(state, new XYAction(6, 6, 0));
		logic.makeMove(state, new XYAction(2, 7, 0));
		logic.makeMove(state, new XYAction(6, 7, 0));
		logic.makeMove(state, new XYAction(2, 8, 0));
		logic.makeMove(state, new XYAction(3, 8, 0));
		logic.makeMove(state, new XYAction(4, 8, 0));
		logic.makeMove(state, new XYAction(5, 8, 0));
		logic.makeMove(state, new XYAction(6, 8, 0));
		logic.makeMove(state, new XYAction(4, 2, 1));
		logic.makeMove(state, new XYAction(5, 2, 1));
		logic.makeMove(state, new XYAction(2, 3, 1));
		logic.makeMove(state, new XYAction(4, 3, 1));
		logic.makeMove(state, new XYAction(7, 3, 1));
		logic.makeMove(state, new XYAction(8, 3, 1));
		logic.makeMove(state, new XYAction(2, 4, 1));
		logic.makeMove(state, new XYAction(3, 4, 1));
		logic.makeMove(state, new XYAction(5, 4, 1));
		logic.makeMove(state, new XYAction(6, 4, 1));
		logic.makeMove(state, new XYAction(7, 4, 1));
		logic.makeMove(state, new XYAction(8, 4, 1));
		logic.makeMove(state, new XYAction(2, 5, 1));
		logic.makeMove(state, new XYAction(4, 5, 1));
		logic.makeMove(state, new XYAction(4, 6, 1));
		logic.makeMove(state, new XYAction(5, 6, 1));
		logic.makeMove(state, new XYAction(3, 7, 1));
		logic.makeMove(state, new XYAction(4, 7, 1));
		logic.makeMove(state, new XYAction(5, 7, 1));
		assertTrue(logic.validMove(state, new XYAction(4, 4, 0)));
		logic.makeMove(state, new XYAction(4, 4, 0));
		assertFalse(logic.validMove(state, new XYAction(4, 4, 1)));
		assertTrue(state.chainBoard[40] == 40);
		assertTrue(state.pseudoBoard[40] == 4);
	}

	@Test
	public void validMoveTest() {
		GoBoard state = new GoBoard(3, 0, 2);
		GoLogic logic = new GoLogic();
		// player 1's moves
		assertTrue(logic.validMove(state, new XYAction(0, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(1, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(1, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(0, 2, 0)));
		assertTrue(logic.validMove(state, new XYAction(1, 2, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 0)));
		// player 2's moves
		assertTrue(logic.validMove(state, new XYAction(0, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(1, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(1, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(0, 2, 1)));
		assertTrue(logic.validMove(state, new XYAction(1, 2, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 1)));
		// new move by player 1
		// at x = 1, y = 0
		logic.makeMove(state, new XYAction(1, 0, 0));
		// player 1's moves
		assertTrue(logic.validMove(state, new XYAction(0, 0, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(1, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(0, 2, 0)));
		assertTrue(logic.validMove(state, new XYAction(1, 2, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 0)));
		// player 2's moves
		assertTrue(logic.validMove(state, new XYAction(0, 0, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(1, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(0, 2, 1)));
		assertTrue(logic.validMove(state, new XYAction(1, 2, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 1)));
		// new move by player 2
		// at x = 1, y = 1
		logic.makeMove(state, new XYAction(1, 1, 1));
		// player 1's moves
		assertTrue(logic.validMove(state, new XYAction(0, 0, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(0, 2, 0)));
		assertTrue(logic.validMove(state, new XYAction(1, 2, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 0)));
		// player 2's moves
		assertTrue(logic.validMove(state, new XYAction(0, 0, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(0, 2, 1)));
		assertTrue(logic.validMove(state, new XYAction(1, 2, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 1)));
		// new move by player 1
		// at x = 1, y = 2
		logic.makeMove(state, new XYAction(1, 2, 0));
		// player 1's moves
		assertTrue(logic.validMove(state, new XYAction(0, 0, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(0, 2, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 2, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 0)));
		// player 2's moves
		assertTrue(logic.validMove(state, new XYAction(0, 0, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(0, 2, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 2, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 1)));
		// new move by player 2
		// at x = 0, y = 0
		logic.makeMove(state, new XYAction(0, 0, 1));
		// player 1's moves
		assertFalse(logic.validMove(state, new XYAction(0, 0, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(0, 2, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 2, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 0)));
		// player 2's moves
		assertFalse(logic.validMove(state, new XYAction(0, 0, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(0, 2, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 2, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 1)));
		// new move by player 1
		// at x = 0, y = 2
		logic.makeMove(state, new XYAction(0, 2, 0));
		// player 1's moves
		assertFalse(logic.validMove(state, new XYAction(0, 0, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(0, 2, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 2, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 0)));
		// player 2's moves
		assertFalse(logic.validMove(state, new XYAction(0, 0, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 1)));
		assertFalse(logic.validMove(state, new XYAction(0, 2, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 2, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 1)));
		// new move by player 2
		// at x = 2, y = 0
		logic.makeMove(state, new XYAction(2, 0, 1));
		// player 1's moves
		assertFalse(logic.validMove(state, new XYAction(0, 0, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 0, 0)));
		assertFalse(logic.validMove(state, new XYAction(2, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(0, 2, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 2, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 0)));
		// player 2's moves
		assertFalse(logic.validMove(state, new XYAction(0, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(1, 0, 1)));
		assertFalse(logic.validMove(state, new XYAction(2, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 1)));
		assertFalse(logic.validMove(state, new XYAction(0, 2, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 2, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 1)));
		// new move by player 1
		// at x = 0, y = 1
		logic.makeMove(state, new XYAction(0, 1, 0));
		// player 1's moves
		assertFalse(logic.validMove(state, new XYAction(0, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(1, 0, 0)));
		assertFalse(logic.validMove(state, new XYAction(2, 0, 0)));
		assertFalse(logic.validMove(state, new XYAction(0, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(0, 2, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 2, 0)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 0)));
		// player 2's moves
		assertFalse(logic.validMove(state, new XYAction(0, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(1, 0, 1)));
		assertFalse(logic.validMove(state, new XYAction(2, 0, 1)));
		assertFalse(logic.validMove(state, new XYAction(0, 1, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 1, 1)));
		assertFalse(logic.validMove(state, new XYAction(0, 2, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 2, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 1)));
		// new move by player 2
		// at x = 2, y = 1
		logic.makeMove(state, new XYAction(2, 1, 1));
		// player 1's moves
		assertFalse(logic.validMove(state, new XYAction(0, 0, 0)));
		assertTrue(logic.validMove(state, new XYAction(1, 0, 0)));
		assertFalse(logic.validMove(state, new XYAction(2, 0, 0)));
		assertFalse(logic.validMove(state, new XYAction(0, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(2, 1, 0)));
		assertFalse(logic.validMove(state, new XYAction(0, 2, 0)));
		assertFalse(logic.validMove(state, new XYAction(1, 2, 0)));
		assertFalse(logic.validMove(state, new XYAction(2, 2, 0)));
		// player 2's moves
		assertFalse(logic.validMove(state, new XYAction(0, 0, 1)));
		assertTrue(logic.validMove(state, new XYAction(1, 0, 1)));
		assertFalse(logic.validMove(state, new XYAction(2, 0, 1)));
		assertFalse(logic.validMove(state, new XYAction(0, 1, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 1, 1)));
		assertFalse(logic.validMove(state, new XYAction(2, 1, 1)));
		assertFalse(logic.validMove(state, new XYAction(0, 2, 1)));
		assertFalse(logic.validMove(state, new XYAction(1, 2, 1)));
		assertTrue(logic.validMove(state, new XYAction(2, 2, 1)));
	}

	@Test
	public void chainBoardTest() {
		GoBoard state = new GoBoard(3, 0, 2);
		GoLogic logic = new GoLogic();
		assertTrue(state.chainBoard[0] == -1);
		assertTrue(state.chainBoard[1] == -1);
		assertTrue(state.chainBoard[2] == -1);
		assertTrue(state.chainBoard[3] == -1);
		assertTrue(state.chainBoard[4] == -1);
		assertTrue(state.chainBoard[5] == -1);
		assertTrue(state.chainBoard[6] == -1);
		assertTrue(state.chainBoard[7] == -1);
		assertTrue(state.chainBoard[8] == -1);
		logic.makeMove(state, new XYAction(1, 0, 0));
		assertTrue(state.chainBoard[0] == -1);
		assertTrue(state.chainBoard[1] == 1);
		assertTrue(state.chainBoard[2] == -1);
		assertTrue(state.chainBoard[3] == -1);
		assertTrue(state.chainBoard[4] == -1);
		assertTrue(state.chainBoard[5] == -1);
		assertTrue(state.chainBoard[6] == -1);
		assertTrue(state.chainBoard[7] == -1);
		assertTrue(state.chainBoard[8] == -1);
		logic.makeMove(state, new XYAction(1, 1, 1));
		assertTrue(state.chainBoard[0] == -1);
		assertTrue(state.chainBoard[1] == 1);
		assertTrue(state.chainBoard[2] == -1);
		assertTrue(state.chainBoard[3] == -1);
		assertTrue(state.chainBoard[4] == 4);
		assertTrue(state.chainBoard[5] == -1);
		assertTrue(state.chainBoard[6] == -1);
		assertTrue(state.chainBoard[7] == -1);
		assertTrue(state.chainBoard[8] == -1);
		logic.makeMove(state, new XYAction(1, 2, 0));
		assertTrue(state.chainBoard[0] == -1);
		assertTrue(state.chainBoard[1] == 1);
		assertTrue(state.chainBoard[2] == -1);
		assertTrue(state.chainBoard[3] == -1);
		assertTrue(state.chainBoard[4] == 4);
		assertTrue(state.chainBoard[5] == -1);
		assertTrue(state.chainBoard[6] == -1);
		assertTrue(state.chainBoard[7] == 7);
		assertTrue(state.chainBoard[8] == -1);
		logic.makeMove(state, new XYAction(0, 0, 1));
		assertTrue(state.chainBoard[0] == 0);
		assertTrue(state.chainBoard[1] == 1);
		assertTrue(state.chainBoard[2] == -1);
		assertTrue(state.chainBoard[3] == -1);
		assertTrue(state.chainBoard[4] == 4);
		assertTrue(state.chainBoard[5] == -1);
		assertTrue(state.chainBoard[6] == -1);
		assertTrue(state.chainBoard[7] == 7);
		assertTrue(state.chainBoard[8] == -1);
		logic.makeMove(state, new XYAction(0, 2, 0));
		assertTrue(state.chainBoard[0] == 0);
		assertTrue(state.chainBoard[1] == 1);
		assertTrue(state.chainBoard[2] == -1);
		assertTrue(state.chainBoard[3] == -1);
		assertTrue(state.chainBoard[4] == 4);
		assertTrue(state.chainBoard[5] == -1);
		assertTrue(state.chainBoard[6] == 7);
		assertTrue(state.chainBoard[7] == 6);
		assertTrue(state.chainBoard[8] == -1);
		logic.makeMove(state, new XYAction(2, 0, 1));
		assertTrue(state.chainBoard[0] == 0);
		assertTrue(state.chainBoard[1] == -1);
		assertTrue(state.chainBoard[2] == 2);
		assertTrue(state.chainBoard[3] == -1);
		assertTrue(state.chainBoard[4] == 4);
		assertTrue(state.chainBoard[5] == -1);
		assertTrue(state.chainBoard[6] == 7);
		assertTrue(state.chainBoard[7] == 6);
		assertTrue(state.chainBoard[8] == -1);
		logic.makeMove(state, new XYAction(0, 1, 0));
		assertTrue(state.chainBoard[0] == 0);
		assertTrue(state.chainBoard[1] == -1);
		assertTrue(state.chainBoard[2] == 2);
		assertTrue(state.chainBoard[3] == 6);
		assertTrue(state.chainBoard[4] == 4);
		assertTrue(state.chainBoard[5] == -1);
		assertTrue(state.chainBoard[6] == 7);
		assertTrue(state.chainBoard[7] == 3);
		assertTrue(state.chainBoard[8] == -1);
		logic.makeMove(state, new XYAction(2, 1, 1));
		assertTrue(state.chainBoard[0] == 0);
		assertTrue(state.chainBoard[1] == -1);
		assertTrue(state.chainBoard[2] == 4);
		assertTrue(state.chainBoard[3] == 6);
		assertTrue(state.chainBoard[4] == 5);
		assertTrue(state.chainBoard[5] == 2);
		assertTrue(state.chainBoard[6] == 7);
		assertTrue(state.chainBoard[7] == 3);
		assertTrue(state.chainBoard[8] == -1);
	}

	@Test
	public void pseudoBoardTest() {
		GoBoard state = new GoBoard(3, 0, 2);
		GoLogic logic = new GoLogic();
		assertTrue(state.pseudoBoard[0] == 0);
		assertTrue(state.pseudoBoard[1] == 0);
		assertTrue(state.pseudoBoard[2] == 0);
		assertTrue(state.pseudoBoard[3] == 0);
		assertTrue(state.pseudoBoard[4] == 0);
		assertTrue(state.pseudoBoard[5] == 0);
		assertTrue(state.pseudoBoard[6] == 0);
		assertTrue(state.pseudoBoard[7] == 0);
		assertTrue(state.pseudoBoard[8] == 0);
		logic.makeMove(state, new XYAction(1, 0, 0));
		assertTrue(state.pseudoBoard[0] == 0);
		assertTrue(state.pseudoBoard[1] == 3);
		assertTrue(state.pseudoBoard[2] == 0);
		assertTrue(state.pseudoBoard[3] == 0);
		assertTrue(state.pseudoBoard[4] == 0);
		assertTrue(state.pseudoBoard[5] == 0);
		assertTrue(state.pseudoBoard[6] == 0);
		assertTrue(state.pseudoBoard[7] == 0);
		assertTrue(state.pseudoBoard[8] == 0);
		logic.makeMove(state, new XYAction(1, 1, 1));
		assertTrue(state.pseudoBoard[0] == 0);
		assertTrue(state.pseudoBoard[1] == 2);
		assertTrue(state.pseudoBoard[2] == 0);
		assertTrue(state.pseudoBoard[3] == 0);
		assertTrue(state.pseudoBoard[4] == 3);
		assertTrue(state.pseudoBoard[5] == 0);
		assertTrue(state.pseudoBoard[6] == 0);
		assertTrue(state.pseudoBoard[7] == 0);
		assertTrue(state.pseudoBoard[8] == 0);
		logic.makeMove(state, new XYAction(1, 2, 0));
		assertTrue(state.pseudoBoard[0] == 0);
		assertTrue(state.pseudoBoard[1] == 2);
		assertTrue(state.pseudoBoard[2] == 0);
		assertTrue(state.pseudoBoard[3] == 0);
		assertTrue(state.pseudoBoard[4] == 2);
		assertTrue(state.pseudoBoard[5] == 0);
		assertTrue(state.pseudoBoard[6] == 0);
		assertTrue(state.pseudoBoard[7] == 2);
		assertTrue(state.pseudoBoard[8] == 0);
		logic.makeMove(state, new XYAction(0, 0, 1));
		assertTrue(state.pseudoBoard[0] == 1);
		assertTrue(state.pseudoBoard[1] == 1);
		assertTrue(state.pseudoBoard[2] == 0);
		assertTrue(state.pseudoBoard[3] == 0);
		assertTrue(state.pseudoBoard[4] == 2);
		assertTrue(state.pseudoBoard[5] == 0);
		assertTrue(state.pseudoBoard[6] == 0);
		assertTrue(state.pseudoBoard[7] == 2);
		assertTrue(state.pseudoBoard[8] == 0);
		logic.makeMove(state, new XYAction(0, 2, 0));
		assertTrue(state.pseudoBoard[0] == 1);
		assertTrue(state.pseudoBoard[1] == 1);
		assertTrue(state.pseudoBoard[2] == 0);
		assertTrue(state.pseudoBoard[3] == 0);
		assertTrue(state.pseudoBoard[4] == 2);
		assertTrue(state.pseudoBoard[5] == 0);
		assertTrue(state.pseudoBoard[6] == 1);
		assertTrue(state.pseudoBoard[7] == 1);
		assertTrue(state.pseudoBoard[8] == 0);
		logic.makeMove(state, new XYAction(2, 0, 1));
		assertTrue(state.pseudoBoard[0] == 2);
		assertTrue(state.pseudoBoard[1] == 0);
		assertTrue(state.pseudoBoard[2] == 2);
		assertTrue(state.pseudoBoard[3] == 0);
		assertTrue(state.pseudoBoard[4] == 3);
		assertTrue(state.pseudoBoard[5] == 0);
		assertTrue(state.pseudoBoard[6] == 1);
		assertTrue(state.pseudoBoard[7] == 1);
		assertTrue(state.pseudoBoard[8] == 0);
		logic.makeMove(state, new XYAction(0, 1, 0));
		assertTrue(state.pseudoBoard[0] == 1);
		assertTrue(state.pseudoBoard[1] == 0);
		assertTrue(state.pseudoBoard[2] == 2);
		assertTrue(state.pseudoBoard[3] == 0);
		assertTrue(state.pseudoBoard[4] == 2);
		assertTrue(state.pseudoBoard[5] == 0);
		assertTrue(state.pseudoBoard[6] == 0);
		assertTrue(state.pseudoBoard[7] == 1);
		assertTrue(state.pseudoBoard[8] == 0);
		logic.makeMove(state, new XYAction(2, 1, 1));
		assertTrue(state.pseudoBoard[0] == 1);
		assertTrue(state.pseudoBoard[1] == 0);
		assertTrue(state.pseudoBoard[2] == 1);
		assertTrue(state.pseudoBoard[3] == 0);
		assertTrue(state.pseudoBoard[4] == 1);
		assertTrue(state.pseudoBoard[5] == 1);
		assertTrue(state.pseudoBoard[6] == 0);
		assertTrue(state.pseudoBoard[7] == 1);
		assertTrue(state.pseudoBoard[8] == 0);
	}

	@Test
	public void validMoveBug() {
		GoBoard state = new GoBoard(3, 0, 2);
		GoLogic logic = new GoLogic();
		logic.makeMove(state, new XYAction(1, 0, 0));
		logic.makeMove(state, new XYAction(1, 1, 1));
		logic.makeMove(state, new XYAction(1, 2, 0));
		logic.makeMove(state, new XYAction(0, 0, 1));
		logic.makeMove(state, new XYAction(0, 2, 0));
		assertTrue(logic.validMove(state, new XYAction(0, 1, 1)));
	}

}

package games.ingenious;

import org.junit.Test;

import za.ac.sun.cs.ingenious.games.ingenious.IngeniousBoard;

import static org.junit.Assert.assertEquals;

public class IngeniousTest {

    /**
     * Test basics of creating a board.
     */
    @Test
    public void IngeniousBoardTest() {
        IngeniousBoard ib = new IngeniousBoard(3, 2);
        assertEquals(ib.getNumColours(), 2);
        assertEquals(ib.lastMove(), null);
    }
}

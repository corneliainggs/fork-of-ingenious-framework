package games.mnk;

import org.junit.Test;

import za.ac.sun.cs.ingenious.games.mnk.MNKState;

import static org.junit.Assert.assertEquals;

public class MNKTest {

    /**
     * Test basics of creating a game state.
     */
    @Test
    public void MNKStateTest() {
        MNKState mnks = new MNKState(9, 9, 4, false, 2);
        assertEquals(mnks.getK(), 4);
        assertEquals(mnks.isPerfectInformation(), false);
        assertEquals(mnks.getWidth(), 9);
        assertEquals(mnks.getHeight(), 9);
    }

    public void MNKCopyTest() {
        MNKState mnks = new MNKState(9, 9, 4, false, 2);
        MNKState mnkc = mnks.deepCopy();
        assertEquals(mnks.equals(mnkc), true);
    }
}

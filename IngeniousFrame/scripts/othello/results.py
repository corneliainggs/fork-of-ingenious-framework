import sys
import os

def main():
    filepath = sys.argv[1]

    if not os.path.isfile(filepath):
        print("File path {} does not exist. Exiting...".format(filepath))
        sys.exit()

    first_player = sys.argv[2]
    first_player = first_player[first_player.index("Choice")+6:-5]
    second_player = sys.argv[3]

    p1_score = 0
    p2_score = 0
    game_count = 0

    f = open(filepath)
    line = f.readline()
    while line:
        split = line.split(": ")
        if (len(split) < 3):
            line = f.readline()
            continue

        nameA = split[1].strip()
        scoreA = float(split[2])

        line = f.readline()
        split = line.split(": ")
        nameB = split[1]
        scoreB = float(split[2])

        if nameA == first_player:
            if scoreA > scoreB:
                p1_score+=1
            elif scoreB > scoreA:
                p2_score+=1
            else:
                p1_score+=0.5
                p2_score+=0.5
        elif nameB == first_player:
            if scoreA > scoreB:
                p2_score+=1
            elif scoreB > scoreA:
                p1_score+=1
            else:
                p1_score+=0.5
                p2_score+=0.5

        game_count+=1
        line = f.readline()
    f.close()

    print("RESULTS:")
    print(str(first_player) + " " + str(p1_score) + "/" + str(game_count) + " " + str(p1_score*100/game_count) + "%")
    print(str(second_player) + " " + str(p2_score) + "/" + str(game_count) + " " + str(p2_score*100/game_count) + "%")


if __name__ == '__main__':
    main()
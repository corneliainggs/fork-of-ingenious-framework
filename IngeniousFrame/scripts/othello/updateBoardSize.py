import json
import sys
import os

def main():
    with open("Othello.json", "r") as f: 
        data = json.load(f)

    if (data["boardSize"] != int(sys.argv[1])):
        data["boardSize"] = int(sys.argv[1])
        with open("Othello.json", "w") as f:
            json.dump(data, f, indent=4)

if __name__ == '__main__':
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    main()
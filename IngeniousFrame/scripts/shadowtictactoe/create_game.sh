#!/bin/bash

# creates a new tictactoe game with the default configuration
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "TicTacToe.json" -game "shadowtictactoe" -lobby "mylobby" -players 2 -numMatches 1

import numpy as np
import matplotlib.pyplot as plt
import os
import json

GAMES = ["go", "othello"]

def main():

    # Check if plots directory exists
    if not os.path.exists("plots"):
        os.mkdir("plots")

    for game in GAMES:

        data = json.load(open('winrates.json'))[game]

        processedData = {}

        styleCounter = 0

        for key in data.keys():
            enhancementName = key.split(' ')[0]
            boardSize = key.split('boardSize=')[1].split(' ')[0]
            threads = int(key.split('threads=')[1].split(' ')[0])
            time = key.split('time=')[1].split(' ')[0]
            if time not in processedData:
                processedData[time] = {}
            if boardSize not in processedData[time]:
                processedData[time][boardSize] = {}
            if enhancementName not in processedData[time][boardSize]:
                processedData[time][boardSize][enhancementName] = []
            processedData[time][boardSize][enhancementName].append([threads, data[key]])

        for time in processedData.keys():
            for boardSize in processedData[time].keys():
                plt.figure(figsize=(10, 10))
                if (game == "go"):
                    plt.title(f"Winrate vs Threads for Go with boardSize={boardSize}, time={time}ms")
                elif (game == "othello"):
                    plt.title(f"Winrate vs Threads for Othello with boardSize={boardSize}, time={time}ms")
                else:
                    plt.title(f"Winrate vs Threads for boardSize={boardSize}, time={time}ms")
                plt.xlabel("Threads")
                plt.ylabel("Winrate")
                for enhancement in processedData[time][boardSize].keys():
                    if (styleCounter % 3 == 0):
                        style = '-'
                    elif (styleCounter % 3 == 1):
                        style = '--'
                    else:
                        style = '-.'
                    plt.plot(
                        [x[0] for x in processedData[time][boardSize][enhancement]],
                        [x[1] for x in processedData[time][boardSize][enhancement]],
                        linestyle=style,
                        label=enhancement
                    )
                    styleCounter += 1
                plt.legend(loc='best')
                plt.savefig(f"plots/{game}_boardSize={boardSize}_time={time}.png")
                plt.close()



if __name__ == "__main__":
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    main()

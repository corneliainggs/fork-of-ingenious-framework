import subprocess
import os
import itertools
import time
import json

game = "othello"

def main():
    boardSizes = [6, 8, 10]
    # instance, thread and match counts must be of the same length
    instanceCount = [24, 12, 6]
    threadCount = [1, 2, 4]
    matchCount = [5, 10, 20]
    turnLengths = [1000]
    enhancements = ["Vanilla", "FPU", "Rave", "MastExpansion", "Contextual", "FPUTuned", "RaveTuned", "MastExpansionTuned", "ContextualTuned"]
    for i in range(len(instanceCount)):
        instances = instanceCount[i]
        threads = threadCount[i]
        matches = matchCount[i]
        for boardSize, turnLength, enhancement in itertools.product(boardSizes, turnLengths, enhancements):
            playout(boardSize, instances, threads, matches, turnLength, enhancement)

        

def playout(boardSize, instances, threads, matches, turnLength, enhancement):

    winrates = json.load(open(f"../winRateTests/winrates.json", "r"))

    if game not in winrates.keys():
        winrates[game] = {}

    if (f"{enhancement} boardSize={boardSize} threads={threads} time={turnLength}" in winrates[game].keys()):
        print(f"Skipping {enhancement} boardSize={boardSize} threads={threads} time={turnLength}")
        return

    command = f'./test.sh {instances} {boardSize} {matches} {threads} {turnLength} ../MctsEnhancementConfigs/EnhancementChoice{enhancement}.json {enhancement}'

    result = subprocess.run(command, shell = True, capture_output = True)
    processed = result.stdout.decode('utf-8').strip().split("\n")[-3:]

    for line in processed:
            if (line.split(" ", 1)[0].replace("tChoice","").replace("ice","") == enhancement):
                wr = float(line.strip().split(' ')[-1].replace("%", ''))
                # print current time
                print(f"{time.strftime('%H:%M:%S', time.localtime())} {enhancement} boardSize={boardSize} threads={threads} time={turnLength} WR={wr}", flush=True)
                winrates[game][f"{enhancement} boardSize={boardSize} threads={threads} time={turnLength}"] = wr
                json.dump(winrates, open(f"../winRateTests/winrates.json", "w"), indent=4)

if __name__ == '__main__':
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    os.chdir(f"../{game}")
    main()
    


#!/bin/bash

if [ "$#" -eq 0 ]; then
  echo "Missing arguments. Usage: train_cfr <iterations> [-s <number of sides> | -d <number of dice> |
                          -c <number of chance samples> | -e <exploitability file name> |
                          -t <training times file name> | --load <saved strategies file> |
                          --save <new strategies file> | --print1 | --print2]"
else
  root=$(pwd)
  gradle jarFat -p "$root/../../"

  iterations=$1
  shift

  sides=6
  dice=1
  chance=-1
  exploit=false
  time=false
  loadStrategy=false
  saveStrategy=false
  print1=false
  print2=false
  exploitFile=""
  timeFile=""
  loadStrategyFile=""
  saveStrategyFile=""


  while [[ "$#" -gt 0 ]]; do
    case $1 in
      -s|--sides)
        sides=$2
        shift
        ;;
      -d|--dice)
        dice=$2
        shift
        ;;
      -c|--chance)
        chance=$2
        shift
        ;;
      -e|--exploit)
        exploit=true
        exploitFile="$2"
        shift
        ;;
      -t|--time)
        time=true
        timeFile="$2"
        shift
        ;;
      --load)
        loadStrategy=true
        loadStrategyFile="$2"
        shift
        ;;
      --save)
        saveStrategy=true
        saveStrategyFile="$2"
        shift
        ;;
      --print1)
        print1=true
        ;;
      --print2)
        print2=true
        ;;
      *) echo "Unknown parameter passed: $1"; exit 1;;
    esac
    shift
  done

  java -cp "$root/../../build/libs/IngeniousFrame-all-0.0.4.jar" za.ac.sun.cs.ingenious.search.rl.TrainingSandbox \
          DudoCFR $iterations $sides $dice $chance $exploit $time $loadStrategy $saveStrategy \
          $exploitFile $timeFile $loadStrategyFile $saveStrategyFile $print1 $print2
fi



#if [ "$#" -eq 1 ]; then
#  java -cp "$root/../../build/libs/IngeniousFrame-all-0.0.4.jar" za.ac.sun.cs.ingenious.search.rl.TrainingSandbox DudoCFR $1
#elif [ "$#" -eq 2 ]; then
#  java -cp "$root/../../build/libs/IngeniousFrame-all-0.0.4.jar" za.ac.sun.cs.ingenious.search.rl.TrainingSandbox DudoCFR $1 $2
#else
#  echo "No arguments provided"
#fi

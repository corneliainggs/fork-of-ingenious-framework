# Snake

## Game Description

The following description is taken from Appendix B of the project report for "Ingenious Reinforcement Learning".

### Rules

There are multiple variants of the game Snake. 
In the variant of Snake implemented in this project, a player controls a snake consisting of a head and body segments.
The snake can move in four directions within a move-able area.
The objective of the game is to eat food that randomly spawns within the area.
When the head reaches the food, the tail of the snake grows and the player's score is incremented.

The game starts with a snake of two segments: a head and a single body segment.
While attempting to eat spawning food, the player must prevent the snake from colliding with the borders of the playable area and also avoid colliding with the snake's body.
If this occurs, the game ends and the snake is reset to its initial position with its size and the score also being reset.
As the game progresses and the snake grows, the difficulty of the game increases due to more of the playable area being occupied by body segments that the player must avoid.

## Scripts Usage

Detailed documentation on executing these scripts can be found [in the RL readme](IngeniousFrame/src/main/java/za/ac/sun/cs/ingenious/search/rl/readme.md).

### Script summary

The scripts below require first executing `start_server.sh`.

* `./run_dqn.sh` - Runs a trained DQN agent from `python/src/trained_models`. Note that this requires a few steps to set 
up the python environment. These can be found in the RL readme.
* `./train_dqn.sh` - Trains a DQN agent that is then stored in `python/src/trained_models`. Note that this will 
circumvent the standard client-server flow in order to speed up training.  This script takes the same two parameters
as the others (number of matches, file name of level).

Note that training an agent may take up to 10 million training steps.

### Manual testing

To run the game as a human player to test manually, first start the server.
 
```
./start_server.sh
```

Build the framework.
```
gradle build shadowJar -p "./../../"
```

Start the lobby.
```
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar "./../../build/libs/IngeniousFrame-all-0.0.4.jar" create -config "./snake.json" -game "SnakeReferee" -lobby "mylobby" -numMatches 3
```

Run the game.
```
java  --add-opens java.base/java.util=ALL-UNNAMED \
        --add-opens java.desktop/java.awt=ALL-UNNAMED \
            -jar "./../../build/libs/IngeniousFrame-all-0.0.4.jar" client -username "p1" -engine "za.ac.sun.cs.ingenious.games.snake.engines.SnakeHumanPlayerEngine" -game "SnakeReferee" -config "./snake.json"
```

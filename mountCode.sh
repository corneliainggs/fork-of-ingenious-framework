RED='\033[0;31m'
GREEN='\033[1;32m'
BLUE='\033[1;34m'
NC='\033[0m'

### Development mode
# Stop the current ingenious container if it is running
# echo newline
printf "\n${GREEN}Stopping current container...${NC}\n"
docker stop ingeniousframe

# Remove the current container
printf "\n${GREEN}Removing current container...${NC}\n"
if ! docker container rm ingeniousframe
then
    sleep 5
fi

# Remove .gradle directory
printf "\n${GREEN}Removing old .gradle files...${NC}\n"
rm -rf IngeniousFrame/.gradle

os="$(uname -s)"

printf "\n${BLUE}Giving container access to local file system...${NC}\n"

sudo chown 1000:1000 ${PWD}/IngeniousFrame
sudo chmod 777 ${PWD}/IngeniousFrame

printf "\n${GREEN}Running on ${os}...${NC}\n"

case "${os}" in
    Linux*)
        printf "\n${GREEN}Attempting mount on Linux...${NC}\n"
        ## Linux
        xhost local:root
        if docker run -u root --rm -dit \
            --env DISPLAY=${DISPLAY} \
            --volume "$(pwd)/IngeniousFrame":/IngeniousFrame \
            --volume /tmp/.X11-unix:/tmp/.X11-unix \
            --network host \
            --memory=300g \
            --name ingeniousframe \
            ingenious-frame ; then
            printf "\n${BLUE}Container mounted successfully!${NC}\n"
        else
            printf "\n${RED}Container failed to mount using Linux command.${NC}\n"
        fi
        ;;
    MINGW*)
        printf "\n${GREEN}Attempting mount on Windows...${NC}\n"
        ## Windows Bash
        if docker run -u root --rm -dit --memory 300g -v "$(pwd | sed -e 's/^\///' -e 's/\//\\/g' -e 's/^./\0:/')"/IngeniousFrame:/IngeniousFrame --name ingeniousframe ingenious-frame ; then
            printf "\n${BLUE}Container mounted successfully!${NC}\n"
        else
            printf "\n${RED}Container failed to mount using Windows MINGW command.${NC}\n"
        fi
        ;;
    CYGWIN*)
        printf "\n${RED}Cygwin mount is not implemented yet.${NC}\n"
        ;;
    Darwin*)
        printf "\n${RED}Mac OS mount is not implemented yet.${NC}\n"
        ;;
    *)
        printf "\n${RED}Unkown OS. Attempting Windows PS mount, but mount may fail and only create a copy of the existing files.${NC}\n"
        ## Windows PowerShell
        if docker run -u root --rm -dit --memory 300g -v ${pwd}/IngeniousFrame:/IngeniousFrame --name ingeniousframe ingenious-frame ; then
            printf "\n${BLUE}Container mounted successfully!${NC}\n"
        else
            printf "\n${RED}Container failed to mount using Windows PS command.${NC}\n"
        fi
        ;;
esac

case "${os}" in
    Linux*)
        # Test for compilation errors
        docker exec -d -u root ingeniousframe /bin/bash -c 'cd /IngeniousFrame ; gradle shadowJar'

        # Replace CRLF line endings with LF line endings - needed for compatibility with Windows systems.
        docker exec -d -u root ingeniousframe /bin/bash -c 'find . -type f -print0 | xargs -0 -n 10 -P 4 dos2unix'
        
        docker exec -d -u root ingeniousframe bash -c '/bin/bash'
        ;;
    MINGW*)
        # Test for compilation errors
        docker exec -d -u root ingeniousframe bash -c 'cd /IngeniousFrame ; gradle shadowJar'

        # Replace CRLF line endings with LF line endings - needed for compatibility with Windows systems.
        docker exec -d -u root ingeniousframe bash -c 'find . -type f -print0 | xargs -0 -n 10 -P 4 dos2unix'

        docker exec -d -u root ingeniousframe bash -c '/bin/bash'
        ;;
    *)
        # Test for compilation errors
        docker exec -d -u root ingeniousframe bash -c 'cd /IngeniousFrame ; gradle shadowJar'

        # Replace CRLF line endings with LF line endings - needed for compatibility with Windows systems.
        docker exec -d -u root ingeniousframe bash -c 'find . -type f -print0 | xargs -0 -n 10 -P 4 dos2unix'

        docker exec -d -u root ingeniousframe bash -c '/bin/bash'
        ;;
esac

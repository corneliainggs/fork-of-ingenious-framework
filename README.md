# Ingenious Game-Playing Framework.

Still under early development - consider it pre-alpha.

All work in here is currently still under copyright and has not been released under any licence - if you want to use any portion of it, please contact the repository owner.

## Ingenious Framework

The backend of the ingenious framework is basically aiming for three things:

* Adding new games is supposed to be as easy as possible
* Adding new engies to an existing game is supposed to be as easy as possible
* The framework is supposed to be as general as possible, to allow for all different types of games to be added.
 
The framework is built in a server-client manner. This means to play a game, a **GameServer** must be running first, then a lobby with a specific game type must be opened and finally players need to join. A **GameServer** can handle an unlimited amount of different games at the same time. 

One client corresponds to one player running one engine that is specific to a certain game. It connects to the **GameServer**, requests a list of open lobbies, joins one of them and then plays the game.

## Steps to start

1. `git clone https://bitbucket.org/skroon/ingenious-framework.git`
2. `cd ingenious-framework/IngeniousFrame`
3. `gradle eclipse`
4. In Eclipse: Import->Git Project->Import exisiting git project

Now you are able to edit the source in Eclipse. The entry point to the framework is: `core.commandline.main.Driver`. This main method supports starting a game server, creating lobbies on the main server and creating clients to join the lobbies and play the games. Usage examples for this entry point can be seen in the scripts in `IngeniousFrame/scripts`. Please refer to the README in `IngeniousFrame/scripts` to start a game of Bomberman.

## Docker

Alternatively, the Ingenious Framework can be deployed in a docker container. The docker container supports GUI applications when run from a linux system with X11 installed. Procedure for setting up the container and building the project:

1. `docker pull galilai0/jdk17-mvn-gradle-py3:latest`
2. `docker build -t ingenious-frame IngeniousFrame/`

The framework can either be setup in a _development_ or _deployment_ fashion, as described in the sections below. In both cases, the framework's server is started upon creation.

### Docker development container

With the development container, the container is mounted to your local file system - changes made in the container are synced with changes made to your local file system. This is useful as the container does not have to be rebuilt to account for minor changes in code. To mount your code in the container, run `sh mountCode.sh`. Note that this script removes the old ingeniousframe container and all dangling images.

`If developing from a non-linux OS, performance may be severely degraded` when using the development container. For example with MCTS, the playout-count in a Windows-mounted container is roughly a tenth of a linux-mounted container's playout-count.

### Docker deployment container

The deployment container makes a copy of the file system, i.e. local changes and container-based changes to files are distinct. The container can be created and deployed by running `sh rebuildDocker.sh`. Note that this script removes the old ingeniousframe container and all dangling images.

### Docker commands

To attach the container's terminal to the local terminal run `docker attach ingeniousframe`. To detach the local terminal from the container's terminal use the keyboard shortcut `CTRL + P, CTRL + Q`.

To create a new interactive terminal run:

`docker exec -it -u root ingeniousframe zsh` on Linux or

`docker exec -it -u root ingeniousframe zsh` on Windows/Mac.

*Note `bash` or `/bin/bash` can be used instead of `zsh`, but `zsh` is cooler.*

The keyboard shortcut `CTRL + P, CTRL + Q` can once again be used to detach from the newly created terminal.

The docker container can be stopped or restarted with `docker stop ingeniousframe` or `docker restart ingeniousframe`.

To remove the volumes used by old containers, use the `docker volume prune` command.

## Adding and starting a new game

Check `games.tictactoe` for an example game. Also check scripts/tictactoe for example commands to start a GameServer, create a lobby and start a game.

**NOTE**: Since upgrading to Java 17, the `--add-opens` flag is required to run the framework. This flag is set in the current scripts, but if you are running the framework from a different script, you will need to add the following flags to the java command:

- `--add-opens java.base/java.util=ALL-UNNAMED` 
- `--add-opens java.desktop/java.awt=ALL-UNNAMED`

Currently, when adding a new game/engine, the following steps have to be taken:

1. Add a package and sources in za.ac.sun.cs.ingenious.games . You need at least a Referee and an Engine to start a game.
2. Compile with `gradle shadowJar`
3. Start a game server `java -jar build/libs/IngeniousFrame-all-0.0.4.jar server`
4. Create a YOURGAME.json file containing settings for your game. You will at least need to supply the number of players needed to play the game, e.g.:
`{
	"numPlayers": 2
}`
5. When MCTS is used, the selection, simulation, backpropagation, ... policies to use for MCTS can be set in a .json config file, which should be stored in the scripts/Enhancements directory. Look at the existing config files for examples. If no enhancements config file is specified in the YOURGAME.json file and it is not specified on the commandline, then the default Vanilla settings will be used. 
6. Create a lobby for your game on the game server with `java -jar build/libs/IngeniousFrame-all-0.0.4.jar create -config "YOURGAME.json" -game "YOURREFEREENAME" -lobby "mylobby"`. "YOURREFEREENAME" is the class name of the referee for your game.
7. Cretae players to join your lobby by running `java -jar build/libs/IngeniousFrame-all-0.0.4.jar client -username "PLAYERNAME" -engine "za.ac.sun.cs.ingenious.games.YOURPACKAGE.YOURENGINECLASSNAME" -game "YOURREFEREENAME" -hostname localhost -port 61234` for each player. Note that player names must be unique.
8. The game will start once enough players have joined.

## Notes on development workflow

1. No non-trivial work unless there's an issue (trivial work includes correcting spelling in documentation, adding comments, etc., but excludes any changes to actual code).
2. Assign yourself to an issue if you are working on it.
3. Fix / implement the issue/enhancement on a SEPARATE branch, typically branched from develop.  For managing branches, see [this link](http://nvie.com/posts/a-successful-git-branching-model/); you may also find these [git flow extensions](https://github.com/petervanderdoes/gitflow-avh) useful.  Name the branch accordingly, including the issue number in the name -- use underscores ex. fix_nullpointer_10.
4. Submit a pull request to merge the fix branch into develop or master, as appropriate - feel free to use pull requests on feature branches, especially when helping out on a feature. In the pull request commit message, add something like closes #100 or fixes #100, this will close the actual issue (https://confluence.atlassian.com/bitbucket/resolve-issues-automatically-when-users-push-code-221451126.html).  Note the use of the --no-ff flag for the merge.

## Framework structure

This is a quick overview of the framework. Class names are written **bold**. Please also check the java documentation of each of these classes for more detailed information.

The framework contains a network module that allows a **GameServer** to play different games with various clients. The **GameServer** creates Lobbies, the Lobbies create **Referee**s once enough players have joined, the **Referee**s play games with **Engine**s(=Players). A detailed writedown of how the network components of the framework interact with each other can be found under "Classes related to network and communication" (most of the information in that section will be irrelevant to new developers).

### Most important classes

The core classes in the framework are: **Referee** and **Engine**.

 **Referee** is abstract and is extended e.g. by **GeneralReferee** which in turn is extended by referees for specific games e.g. **TTTReferee**. Referees handle communication with the different players. They are responsible for asking players to act and to distribute the results of the players' actions to all the other players. **GeneralReferee** is a referee that provides support for virtually all types of games. It's run-method proceeds (very roughly) as follows:

1. Sends InitGameMessages to all players
2. Until the game state is terminal
    1. Asks each player allowed to act in the current state to supply an action
    2. Checks whether the action is valid, then distributes its results to the other players
3. Sends GameTerminatedMessages to all players

**Engine** is also abstract and each concrete extension of it corresponds to one strategy to play some game. For example, the tictactoe-package provides a **TTTRandomEngine** that plays random valid actions and also a **TTTMinimaxEngine** that uses Minimax-search to find the best action to play.

**Referee** and **Engine** communicate via **Message**s. These are:

* **InitGameMessage**, for when the referee starts the game.

* **GameTerminatedMessage**, for when the referee has determined that the game has ended.

* **GenActionMessage**, for when the referee asks an engine to act.

* **PlayActionMessage**, for when an engine informs the referee of which action it wants to play.

* **PlayedMoveMessage**, for when the referee informs the engines about which move they observe after some engine has made some action.

Importantly, there is a distinction between **Action**s and **Move**s. For perfect information games, these will generally be the same. For imperfect information games, there are some actions (like playing your mark in PhantomMNK) that appear as unobserved moves to other players.

Finally, the framework contains three classes to structure a game implementation with (they are supposed to be extended for specific games):

* **GameState** represents a state in some game. There are helpful utility classes like **TurnBasedGameState** for games where only one player may play at a time or **TurnBased2DBoard** for games that are turn based and played on two dimensional boards (for example chess).

* **GameLogic**, which defines common operations for all games (Which players may play when? Which actions are allowed?). It operates on instances of game states.

* **GameFinalEvaluator**, which takes a terminal game state and returns the scores for each player in that state.

The search-package makes heavy use of these three classes to offer generic implementations of search algorithms like MiniMax, MCTS or CFR.

### Classes related to network and communication

#### On the GameServer's side

The game server can be started by running `core.commandline.main.Driver` with the `server` argument.

**GameServer** constantly waits for connections from Clients on the standard TCP port and creates **ClientHandler** threads for each client that connects. **GameServer** holds a **LobbyManager** instance that is passed to each new **ClientHandler**.

**ClientHandler** first waits for the client to send a unique name (uses **LobbyManager** to check if another player of the same name has connected to the server before). Then waits for one of the possible messages:

* **NewMatchMessage** (contains a **MatchSetting** object which embodies a JSON file containing the specific game settings; on request, a **LobbyHost** object is created with the match settings and added to he **LobbyManager**)

* **LobbyOverviewRequest** (on request, sends the client a list of open lobbies with number of participating players etc.)

* **JoinMessage** (contains the name of the lobby to be joined; on request, the corresponding **LobbyHost** to that name is looked up in the **LobbyManager** and the player added to that lobby).

**LobbyManager** stores HashMaps of **LobbyHost** and **ClientHandler** objects indexed by Strings.

**LobbyHost** accepts joining players until the number of players is reached, then starts the game. Whenever a player joins, a corresponding **ServerToEngineConnection** is created and a handshake with the recently connected player is executed. When the game starts, the lobby is unregistered from the **LobbyManager** and a **Referee** for the game is created by the **RefereeFactory** and started.

**ServerToEngineConnection** implements the **PlayerRepresentation** interface and extends **SocketWrapper**. The socket is initialised with the connection to the player that joined the lobby. 

**PlayerRepresentation** is an interface modelling the view that a **Referee** has on the players of its game. Thus, its methods are: telling the player to init or terminate the game (initGame/terminateGame), telling the player that some move was played by another player (playMove), request player to supply an action it wants to play (genAction). When **PlayerRepresentation** is implemented by **ServerToEngineConnection**, these methods simply take the supplied InitGameMessage/PlayActionMessage/... objects and send them to the connected players via the socket underlying **ServerToEngineConnection**.

Classes extending **Referee** are created by the **RefereeFactory** with the **MatchSetting** of the game to be played and an array of **PlayerRepresentation** objects (these are the players participating in the game). The referee is then started in a new thread.

#### On the Client side

The server can be prompted to create a lobby for some game by running `core.commandline.main.Driver` with the `create` argument (you also have to specify game name, number of players etc... refer to the help-Message you will get when running `create` without further arguments).

Once a lobby is created, players for the game can be created by running `core.commandline.main.Driver` with the `client` argument (again, refer to the help message given and the scripts in `IngeniousFrame/scripts`).

**LobbyHandler** can ask a GameServer to create a lobby and then joins that lobby, or simply joins an existing lobby. Either way, an **EngineToServerConnection** is returned.

**Engine** gets an **EngineToServerConnection** on construction. It is also directly responsible for handling the PlayedMove and InitGameMessages sent by the **Referee** on the server side.

**EngineToServerConnection** is responsible for handling the GenActionMessages and GameTerminatedMessages sent by the **Referee**. For GenActionMessages, it implements a buffer to give the **Engine** time before it has to supply its move.

### MCTS Parameter Tuning

The directory `IngeniousFrame/ParameterTuning` contains a python script `tuner.py` which can be used to tune enhancements for MCTS. It also contains a jupyter notebook script, `test_tuner.py` that can be used to step through and visualise the output of the tuning script. Thus far tuning has only been implemented for GO on the tree-parallel MCTS algorithm. 

### Reinforcement Learning

For documentation specific to reinforcement learning, refer to the [RL readme](IngeniousFrame/src/main/java/za/ac/sun/cs/ingenious/search/rl/readme.md).
